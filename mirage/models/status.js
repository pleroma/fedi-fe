import { Model, belongsTo, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  account: belongsTo('user'),
  mediaAttachments: hasMany('attachment'),
  pleroma: belongsTo('status-extra'),
  reblog: belongsTo('status'),
  poll: belongsTo('poll'),
  mentions: hasMany('mention'),
});

import { Model, belongsTo, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  accounts: hasMany('user'),
  lastStatus: belongsTo('status'),
});

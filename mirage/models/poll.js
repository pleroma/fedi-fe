import {
  hasMany,
  Model,
} from 'ember-cli-mirage';

export default Model.extend({
  options: hasMany('poll-option'),
});

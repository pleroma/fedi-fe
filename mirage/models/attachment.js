import { Model, belongsTo } from 'ember-cli-mirage';

export default Model.extend({
  pleroma: belongsTo('attachment-extra'),
});

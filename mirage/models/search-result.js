import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  statuses: hasMany('status'),
  accounts: hasMany('user'),
  hashtags: hasMany('hashtag'),
});

import { Model, belongsTo } from 'ember-cli-mirage';

export default Model.extend({
  account: belongsTo('user'),
  pleroma: belongsTo('notification-extra'),
  status: belongsTo('status'),
});

import { Model, belongsTo, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  pleroma: belongsTo('user-extra'),
  source: belongsTo('user-source'),
  tokens: hasMany('token'),
  subscription: belongsTo('subscription'),
});

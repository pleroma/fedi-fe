import { Model, belongsTo } from 'ember-cli-mirage';

export default Model.extend({
  nodeInfo: belongsTo('node-info'),
});

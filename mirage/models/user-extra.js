import { Model, belongsTo } from 'ember-cli-mirage';

export default Model.extend({
  notificationSettings: belongsTo('notification-setting'),

  relationship: belongsTo('relationship'),
});

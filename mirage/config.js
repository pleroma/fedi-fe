import Mirage from 'ember-cli-mirage';
import config from 'pleroma-pwa/config/environment';
import camelCaseKeys from 'camelcase-keys';
import snakeCaseKeys from 'snakecase-keys';
import uuid from 'uuid/v4';
import set from 'lodash.set';
import faker from 'faker';
import { blobToDataURL } from 'blob-util';
import { debug } from '@ember/debug';

const isAuthenticated = function(schema, request) {
  let authHeader = request.requestHeaders.authorization;

  if (!authHeader) {
    return false;
  }

  let [tokenType, token] = authHeader.split(' ');

  if (!tokenType || tokenType !== 'Bearer') {
    return false;
  }

  if (!token || token === 'undefined') {
    return false;
  }

  if (!schema.tokens.findBy({ token })) {
    return false;
  }

  return true;
};

const getAuthenticatedUser = function(schema, request) {
  debug('1');
  debug(request.requestHeaders);
  debug(request.requestHeaders.authorization);
  if (!request.requestHeaders?.authorization) {
    return;
  }

  let [, token] = request.requestHeaders.authorization.split(' ');

  debug('2');
  let tokenRecord = schema.tokens.findBy({ token });

  debug('3');
  debug('tokenRecord', tokenRecord);
  debug('tokenRecord.username', tokenRecord.username);

  if (!tokenRecord?.username) {
    return;
  }

  let user = schema.users.findBy({ username: tokenRecord.username });

  if (!user) {
    return;
  }

  // debug('4');
  // debug('user', user);
  // debug('user.pleromaId', user.pleromaId);
  // let userExtra = schema.userExtras.findOrCreateBy({ id: user.pleromaId})

  // TODO: This does not make sense? Unread would have isSeen => true?
  // debug('5');
  // let unreadNotificationExtras = schema.notificationExtras.where({ isSeen: true });
  //
  // debug('6');
  // userExtra.update({'unreadNotificationsCount':  unreadNotificationExtras.length });

  // debug('7');
  return user;
};

const invalidateSession = function(schema, request) {
  let { username } = getAuthenticatedUser(schema, request);
  schema.db.tokens.remove({ username });
};

const timelineRoute = function(schema, request, modelType = 'statuses', additionalFilters = []) {
  let all = schema[modelType].all();

  additionalFilters.forEach((additionalFilter) => all = all.filter(additionalFilter));

  if (modelType === 'users' && isAuthenticated(schema, request)) {
    let sessionUser = getAuthenticatedUser(schema, request);
    all = all.filter((user) => user.id !== sessionUser.id);
  }

  if (request.queryParams.limit && request.queryParams.max_id) {
    let serialized = this.serialize(all);
    let found;

    let filtered = serialized.filter((status) => {
      if (found) { return status;}
      if (status.id === request.queryParams.max_id) {
        found = true;
      }

      return false;
    });

    let sliced = filtered.slice(0, parseInt(request.queryParams.limit));

    return sliced;
  }

  if (request.queryParams.limit && request.queryParams.min_id) {
    let sorted = all.sort((a, b) => {
      let dateA = new Date(a);
      let dateB = new Date(b);

      return dateB.getTime() > dateA.getTime();
    });

    let sinceIndex = all.models.reduce((memo, item, index) => {
      if (item.id === request.queryParams.since_id) {
        memo = index;
      }

      return memo;
    }, null);

    let newItems = sorted.models.slice(0, sinceIndex);

    return this.serialize(newItems);
  }

  if (request.queryParams.limit) {
    let serialized = this.serialize(all);

    let sliced = serialized.slice(0, parseInt(request.queryParams.limit));

    return sliced;
  }

  return this.serialize(all);
};

function parseUpdateCredentials(_input) {
  let input = camelCaseKeys(_input);
  let body = {};

  for (let key of Object.keys(input)) {
    if ([
      'hideFavorites',
      'hideFollows',
      'hideFollowsCount',
      'hideFollowers',
      'hideFollowersCount',
    ].includes(key)) {
      set(body, `pleroma.${key}`, input[key]);
    }

    if ([
      'actorType',
      'discoverable',
      'noRichText',
      'showRole',
    ].includes(key)) {
      set(body, `source.pleroma.${key}`, input[key]);
    }

    if ([
      'note',
    ].includes(key)) {
      set(body, key, input[key]);
    }
  }

  return body;
}

export default function() {
  // Uncomment this for debugging:
  // this.logging = true;

  this.urlPrefix = config.APP.testApiBaseUrl;
  // this.namespace = config.APP.mastodonApiNamespace;    // make this `/api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  this.get('/api/pleroma/captcha', async function() {
    return {
      // eslint-disable-next-line camelcase
      answer_data: faker.random.number(),
      token: faker.random.number(),
      type: 'native',
      url: faker.image.dataUri(faker.random.number(), faker.random.number()),
    };
  });

  // {
  //   "girlpower": {
  //     "tags": [
  //       "Finmoji"
  //     ],
  //     "image_url": "/finmoji/128px/girlpower-128.png"
  //   },
  //   "education": {
  //     "tags": [
  //       "Finmoji"
  //     ],
  //     "image_url": "/finmoji/128px/education-128.png"
  //   },
  //   "finnishlove": {
  //     "tags": [
  //       "Finmoji"
  //     ],
  //     "image_url": "/finmoji/128px/finnishlove-128.png"
  //   }
  // }
  // @see: https://docs-develop.pleroma.social/backend/API/pleroma_api/
  this.get('/api/pleroma/emoji', async function() {
    // TODO: format data according to API spec
    // return schema.emojis.all();
    return {};
  });

  // {
  //   "womans_clothes": "\ud83d\udc5a",
  //   "cookie": "\ud83c\udf6a",
  //   "woman_with_headscarf": "\ud83e\uddd5"
  // }
  this.get('/static/emoji.json', async function() {
    // TODO: format data according to API spec
    // return schema.emojis.all();
    return {};
  });

  this.post('/api/pleroma/change_password', function(schema, request) {
    invalidateSession(schema, request);
    return {};
  });

  this.post('/api/pleroma/delete_account', function(schema, request) {
    invalidateSession(schema, request);
    return {};
  });

  this.put('/api/pleroma/notification_settings', function(schema, request) {
    let settings = camelCaseKeys(JSON.parse(request.requestBody));
    let user = getAuthenticatedUser(schema, request);

    let { notificationSettings } = user.pleroma;
    notificationSettings.update(settings);

    let status = "success";
    return { status };
  });

  this.get('/api/v1/accounts/relationships', async function(schema, request) {
    // console.log('/api/v1/accounts/relationships');
    let currentUser = getAuthenticatedUser(schema, request);

    // console.log('currentUser', currentUser);

    if (!currentUser) {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }

    let ids = Object.values(request.queryParams).flat().filter(Boolean);
    // console.log('ids', ids);

    let found = schema.relationships.where((relationship) => {
      return ids.includes(relationship.id);
    });

    // console.log('found', found);
    // console.log('serialized?', this.serialize(found));
    // console.log('found.models', found.models);
    // console.log('found.toArray', found.models.toArray());
    // console.log([...found.toArray()]);

    return found;
    // return new Mirage.Response(200, found);
  });

  this.get('/api/v1/accounts/search', async function(schema) {
    return schema.users.all();
  });

  this.get('/api/v1/accounts/:id', async function(schema, request) {
    return schema.users.find(request.params.id) || schema.users.findBy({ username: request.params.id });
  });

  this.get('/api/v1/accounts/:id/statuses', async function(schema, request) {
    return timelineRoute.call(this, schema, request, 'statuses', [(status) => status.accountId === request.params.id]);
  });

  this.get('/api/v1/accounts/:id/following', async function(schema, request) {
    return timelineRoute.call(this, schema, request, 'users');
  });

  this.get('/api/v1/accounts/:id/followers', async function(schema, request) {
    return timelineRoute.call(this, schema, request, 'users');
  });

  this.get('/api/v1/statuses/:id', function(schema, request) {
    return schema.statuses.find(request.params.id);
  });

  this.get('/api/v1/favourites', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return timelineRoute.call(this, schema, request);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/pleroma/accounts/:account_id/favourites', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return timelineRoute.call(this, schema, request);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/mutes', function(schema, request) {
    let currentUser = getAuthenticatedUser(schema, request);

    let found = schema.users.where((user) => {
      if (currentUser.id === user.id) {
        return false;
      }

      let userExtra = schema.userExtras.find(user.pleromaId);

      if (!user.pleromaId || !userExtra?.relationship?.muting) {
        return false;
      }

      return true;
    });

    return found;
  });

  this.get('/api/v1/blocks', function(schema, request) {
    let currentUser = getAuthenticatedUser(schema, request);

    let found = schema.users.where((user) => {
      if (currentUser.id === user.id) {
        return false;
      }

      let userExtra = schema.userExtras.find(user.pleromaId);

      if (!user.pleromaId || !userExtra?.relationship?.blocking) {
        return false;
      }

      return true;
    });

    return found;
  });

  this.get('/api/v1/timelines/public', timelineRoute);

  this.get('/api/v1/timelines/home', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return timelineRoute.call(this, schema, request);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/conversations', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return timelineRoute.call(this, schema, request, 'conversations');
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/pleroma/chats/by-account-id/:user_id', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let foundChat = schema.chats.findBy({ accountId: request.params.user_id });

      if (foundChat) {
        return foundChat;
      }

      let chat = schema.chats.create({
        accountId: request.params.user_id,
        unread: 0,
        updatedAt: new Date(),
      });

      return chat;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/pleroma/chats', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return schema.chats.all();
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/pleroma/chats/:id', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return schema.chats.find(request.params.id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/pleroma/chats/:id/messages', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return schema.chatMessages.where({ chatId: request.params.id });
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/pleroma/chats/:id/messages/:message_id', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return schema.chatMessages.find(request.params.message_id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/pleroma/chats/:id/messages', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = getAuthenticatedUser(schema, request);

      let postedMessage = camelCaseKeys(JSON.parse(request.requestBody), { deep: true });

      let {
        content = null,
        mediaId: attachmentId = null,
      } = postedMessage;

      return schema.chatMessages.create({
        accountId: user.id,
        chatId: request.params.id,
        content,
        attachmentId,
      });
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.delete('/api/v1/pleroma/chats/:id/messages/:message_id', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let foundMessage = schema.chatMessages.where({
        id: request.params.message_id,
        chatId: request.params.id,
      });

      foundMessage.destroy();

      return foundMessage;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/pleroma/conversations/:id', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return schema.conversations.where({ id: request.params.id });
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/pleroma/conversations/:id/statuses', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let statuses = timelineRoute.call(this, schema, request, 'statuses', [(status) => status.visibility === 'direct']);
      return statuses.reverse(); // DM's render in reverse
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/conversations/:id/read', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      schema.db.conversations.update(request.params.id, {
        unread: false,
      });

      return schema.conversations.where({ id: request.params.id });
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/media', async function(schema, request) {
    if (isAuthenticated(schema, request)) {
      // let user = getAuthenticatedUser(schema, request);

      if (!(request.requestBody instanceof FormData)) {
        return new Mirage.Response(403, { error: 'Invalid file.' });
      }

      let body = request.requestBody;

      if (!body.get('file')) {
        return new Mirage.Response(403, { error: 'Invalid file.' });
      }

      let file = body.get('file');

      let dataUrl = await blobToDataURL(file);

      let attachmentExtra = schema.attachmentExtras.create({
        mimeType: file.type,
      });

      let type;

      let matches = file.type.match(/^([a-z]+)\//);

      if (matches && matches[1]) {
        type = matches[1];
      }

      let attachment = schema.attachments.create({
        type,
        description: file.name,
        url: dataUrl,
        previewUrl: dataUrl,
        remoteUrl: dataUrl,
        textUrl: dataUrl,
        pleroma: attachmentExtra,
      });

      return attachment;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/notifications', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return timelineRoute.call(this, schema, request, 'notifications');
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/pleroma/notifications/read', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let parsedBody = JSON.parse(request.requestBody);
      let nofificationId = parsedBody.id;
      let notificationExtra = schema.notificationExtras.create({
        isSeen: true,
      });

      schema.db.notifications.update(nofificationId, {
        pleromaId: notificationExtra.id,
      });

      return schema.notifications.find(nofificationId);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/notifications/:id', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return schema.notifications.find(request.params.id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/accounts/verify_credentials', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      return getAuthenticatedUser(schema, request);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.patch('/api/v1/accounts/update_credentials', async function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = getAuthenticatedUser(schema, request);

      let body;

      if (request.requestBody instanceof FormData) {
        body = request.requestBody;

        if (!body.get('avatar')) {
          return new Mirage.Response(403, { error: 'Invalid avatar.' });
        }

        let file = body.get('avatar');

        let dataUrl = await blobToDataURL(file);

        user.update({ avatar: dataUrl });
      } else {
        body = parseUpdateCredentials(JSON.parse(request.requestBody));

        if (body?.pleroma) {
          user.pleroma.update(body.pleroma);
        }

        if (body?.source?.pleroma) {
          user.source.pleroma.update(body.source.pleroma);
        }

        if (body.note) {
          user.update({ note: body.note });
        }
      }

      return schema.users.find(user.id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.patch('/api/v1/pleroma/accounts/update_avatar', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = getAuthenticatedUser(schema, request);
      let body = JSON.parse(request.requestBody);

      if (body.img) {
        user.update({ avatar: body.img });
      } else {
        user.update({ avatar: null });
      }

      return null;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/instance', function(schema) {
    let instance;

    if (schema.instances.first()) {
      instance = schema.instances.first().toJSON();
    } else {
      instance = schema.instances.create().toJSON();
    }

    return snakeCaseKeys(instance, { deep: true });
  });

  this.get('/instance/panel.html', function() {
    return '<h3>Pleroma Sidebar.html</h3>';
  });

  this.get('/nodeinfo/2.0.json', function(schema) {
    let instance = schema.instances.first();
    let nodeInfo;

    if (instance) {
      nodeInfo = schema.nodeInfos.find(schema.instances.first().id);
    }

    return nodeInfo || schema.nodeInfos.create();
  });

  this.post('/api/v1/accounts', function(schema, request) {
    let newUser = JSON.parse(request.requestBody);
    let requiredAttributres = ['username', 'email', 'password', 'confirm', 'agreement', 'fullname', 'nickname', 'locale'];

    if (!isAuthenticated(schema, request)) {
      return new Mirage.Response(403, { error: 'Invalid credentails.' });
    }

    let newUserKeys = Object.keys(newUser);
    let difference = requiredAttributres.filter((x) => !newUserKeys.includes(x));

    if (difference.length) {
      let error = difference.reduce((memo, key) => {
        memo[key] = 'is required';
      }, {});

      return new Mirage.Response(404, { error });
    }

    if (schema.users.findBy({ username: newUser.username })) {
      return new Mirage.Response(404, { error: { 'ap_id': 'is taken' } });
    }

    newUser.pleroma = schema.userExtras.create({});

    schema.users.create(newUser);

    let tokenRecord = schema.tokens.create({
      username: newUser.username,
      token: uuid(),
    });

    return {
      'access_token': tokenRecord.token,
      'created_at': Date.now(),
      'scope': ['read', 'write', 'follow'],
      'token_type': 'Bearer',
    };
  });

  this.post('/api/v1/accounts/:account_id/block', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = schema.users.find(request.params.account_id);

      let { relationship } = user.pleroma;

      relationship.update({
        blocking: true,
      });

      return relationship;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/accounts/:account_id/unblock', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = schema.users.find(request.params.account_id);

      let { relationship } = user.pleroma;

      relationship.update({
        blocking: false,
      });

      return relationship;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/accounts/:account_id/mute', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = schema.users.find(request.params.account_id);

      let { relationship } = user.pleroma;

      relationship.update({
        muting: true,
      });

      return relationship;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/accounts/:account_id/unmute', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = schema.users.find(request.params.account_id);

      let { relationship } = user.pleroma;

      relationship.update({
        muting: false,
      });

      return relationship;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/accounts/:account_id/follow', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = schema.users.find(request.params.account_id);

      let { relationship } = user.pleroma;

      relationship.update({
        following: true,
      });

      return relationship;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/accounts/:account_id/unfollow', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = schema.users.find(request.params.account_id);

      let { relationship } = user.pleroma;

      relationship.update({
        following: false,
      });

      return relationship;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/polls/:poll_id/votes', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let poll = schema.polls.findBy({ id: request.params.poll_id });
      let { choices } = JSON.parse(request.requestBody);

      poll.update({
        voted: true,
        ownVoted: choices,
      });

      return poll;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v2/search', function(schema, request) {
    let { queryParams: { q: query, limit } } = request;

    limit = parseInt(limit);

    query = query.replace('#', '');

    let statuses = schema.statuses
      .where(status => status.content.includes(query))
      .slice(0, limit);

    let accounts = schema.users
      .where(user => (
        user.displayName.includes(query)
        || user.username.includes(query)
        || query.includes(user.displayName)
        || query.includes(user.username)
      ))
      .slice(0, limit);

    let hashtags = schema.hashtags
      .where(hashtag => (
        hashtag.name.includes(query)
        || query.includes(hashtag.name)
      ))
      .slice(0, limit);

    let searchResult = schema.searchResults.create({
      statuses,
      accounts,
      hashtags,
    });

    return searchResult;
  });

  this.post('/api/v1/statuses/:status_id/favourite', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let statusInQuestion = schema.statuses.find(request.params.status_id);

      if (!statusInQuestion) {
        return new Mirage.Response(404, { error: 'Not Found.' });
      }

      // TODO: Eventually we might want this favorited prop to account for session user.
      schema.db.statuses.update(statusInQuestion.id, { favourited: true });

      return schema.statuses.find(request.params.status_id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.get('/api/v1/statuses/:status_id/context', function(schema, request) {
    let statusInQuestion = schema.statuses.find(request.params.status_id);

    if (!statusInQuestion) {
      return new Mirage.Response(404, { error: 'Not Found.' });
    }

    return { ancestors: [], descendants: [] };
  });

  this.post('/api/v1/statuses/:status_id/unfavourite', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let statusInQuestion = schema.statuses.find(request.params.status_id);

      if (!statusInQuestion) {
        return new Mirage.Response(404, { error: 'Not Found.' });
      }

      // TODO: Eventually we might want this favorited prop to account for session user.
      schema.db.statuses.update(statusInQuestion.id, { favourited: false });

      return schema.statuses.find(request.params.status_id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/statuses/:status_id/reblog', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let statusInQuestion = schema.statuses.find(request.params.status_id);
      let createdAt = new Date();
      let user = getAuthenticatedUser(schema, request);

      if (!statusInQuestion) {
        return new Mirage.Response(404, { error: 'Not Found.' });
      }

      // TODO: Eventually we might want this prop to account for session user.
      schema.db.statuses.update(statusInQuestion.id, { reblogged: true });

      statusInQuestion = schema.statuses.find(statusInQuestion.id);

      let createdStatus = schema.statuses.create({
        reblog: statusInQuestion,
        createdAt,
        account: user,
        content: statusInQuestion.content,
      });

      return createdStatus;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/statuses/:status_id/unreblog', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let statusInQuestion = schema.statuses.find(request.params.status_id);

      if (!statusInQuestion) {
        return new Mirage.Response(404, { error: 'Not Found.' });
      }

      if (statusInQuestion.reblog) {
        schema.db.statuses.update(statusInQuestion.id, { reblogged: false, reblog: null });

        statusInQuestion = schema.statuses.find(statusInQuestion.reblog.id);
      }

      schema.db.statuses.update(statusInQuestion.id, { reblogged: false });

      return schema.statuses.find(request.params.status_id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/statuses/:status_id/mute', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let statusInQuestion = schema.statuses.find(request.params.status_id);

      if (!statusInQuestion) {
        return new Mirage.Response(404, { error: 'Not Found.' });
      }

      let statusExtraInQuestion = schema.statusExtras.find(statusInQuestion.pleromaId);

      if (statusExtraInQuestion) {
        schema.db.statusExtras.update(statusExtraInQuestion.id, { threadMuted: true });
      } else {
        let statusExtra = schema.statusExtras.create({ threadMuted: true });

        schema.db.statuses.update(statusInQuestion.id, { pleromaId: statusExtra.id });
      }

      return schema.statuses.find(request.params.status_id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/statuses/:status_id/unmute', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let statusInQuestion = schema.statuses.find(request.params.status_id);

      if (!statusInQuestion) {
        return new Mirage.Response(404, { error: 'Not Found.' });
      }

      let statusExtraInQuestion = schema.statusExtras.find(statusInQuestion.pleromaId);

      if (statusExtraInQuestion) {
        schema.db.statusExtras.update(statusExtraInQuestion.id, { threadMuted: false });
      } else {
        let statusExtra = schema.statusExtras.create({ threadMuted: false });

        schema.db.statuses.update(statusInQuestion.id, { pleromaId: statusExtra.id });
      }

      return schema.statuses.find(request.params.status_id);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.delete('/api/v1/statuses/:status_id', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = getAuthenticatedUser(schema, request);
      let status = schema.statuses.find(request.params.status_id);

      if (user.id === status.account.id) {
        status.destroy();

        return new Mirage.Response(200, {});
      } else {
        return new Mirage.Response(403, { error: 'You can only delete your own' });
      }
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/statuses', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let postedStatus = JSON.parse(request.requestBody);
      let createdAt = new Date();

      let user = getAuthenticatedUser(schema, request);

      postedStatus.createdAt = createdAt.toISOString();
      postedStatus.accountId = user.id;

      if (postedStatus.in_reply_to_id) {
        postedStatus.inReplyToId = postedStatus.in_reply_to_id;
        delete postedStatus.in_reply_to_id;

        postedStatus.inReplyToAccountId = schema.statuses.find(postedStatus.inReplyToId).accountId;
        let inReplyToAccount = schema.users.find(postedStatus.inReplyToAccountId);

        let statusExtra = schema.statusExtras.create({
          conversationId: schema.statuses.find(postedStatus.inReplyToId).accountId,
          inReplyToAccountAcct: inReplyToAccount.username,
        });

        postedStatus.pleromaId = statusExtra.id;
      }

      if (postedStatus.media_ids && postedStatus.poll) {
        return new Mirage.Response(422, { error: 'Connot create a poll with media items' });
      }

      if (postedStatus.poll) {
        let pollOptions = postedStatus.poll.options.map((option) => schema.pollOptions.create({ title: option }));

        let now = new Date();
        now.setSeconds(now.getSeconds() + postedStatus.poll.expires_in);
        let expiresAt = now.toISOString();

        let poll = schema.polls.create({
          options: pollOptions,
          expiresAt,
        });

        delete postedStatus.poll;

        postedStatus.pollId = poll.id;
      }


      let createdStatus = schema.statuses.create(postedStatus);

      // The First DM, Create a conversation, return the status with that conversation ID.
      if (postedStatus.visibility === 'direct' && postedStatus.to) {
        let accounts = schema.users.where(user => postedStatus.to.any((to) => {
          return to.startsWith(user.username);
        }));

        let conversation = schema.conversations.create({
          accounts,
          lastStatus: createdStatus,
        });

        let statusExtra = schema.statusExtras.create({
          directConversationId: conversation.id,
        });

        createdStatus.update({
          pleroma: statusExtra,
        });
      }

      // Subsequent DM, Set this as the recent_status of the conversation, return the status with a conversationId
      if (postedStatus.visibility === 'direct' && postedStatus.inReplyToConversationId) {
        let conversation = schema.conversations.findBy({ id: postedStatus.inReplyToConversationId });

        let statusExtra = schema.statusExtras.create({
          directConversationId: conversation.id,
        });

        conversation.update({
          lastStatus: createdStatus,
        });

        createdStatus.update({
          pleroma: statusExtra,
        });
      }

      if (postedStatus.media_ids) {
        createdStatus.update({
          mediaAttachmentIds: postedStatus.media_ids,
        });
      }

      return createdStatus;
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });

  this.post('/api/v1/apps', function() {
    /* eslint-disable camelcase */
    return {
      client_id: '1234',
      client_secret: 'abcd',
      id: '26',
      name: 'pleroma-pwa_2019-08-12T18:14:23.801Z',
      redirect_uri: `${config.APP.testApiBaseUrl}/oauth-callback`,
      vapid_key: '369d61f9-f16c-4c7b-bb63-33c8aca33057', // random
      website: null,
    };
    /* eslint-enable camelcase */
  });

  this.get('/api/oauth_tokens', function(schema, request) {
    let user = getAuthenticatedUser(schema, request);
    let tokens = schema.tokens.where({ userId: user.id });
    return tokens;
  });

  this.delete('/api/oauth_tokens/:token_id', function(schema, request) {
    let user = getAuthenticatedUser(schema, request);

    let { 'token_id': tokenId } = request.params;

    let tokens = schema.tokens.where({
      id: tokenId,
      userId: user.id,
    });

    if (tokens.models.length === 0) {
      return new Mirage.Response(404);
    }

    tokens.models.map((t) => t.destroy());

    // Don't return anything for a DELETE.
    // This is consistent with the API.
  });

  this.post('/oauth/token', function(schema, req) {
    // Normalize the requestBody param
    if (req.requestHeaders['content-type'] === 'application/x-www-form-urlencoded') {
      let params = req.requestBody.split('&'); // grant_type=password&username=a%40b.com&password=123456

      /* eslint-disable no-param-reassign */
      req.requestBody = {};

      params.forEach(function(el) {
        req.requestBody[decodeURIComponent(el.split('=')[0])] = decodeURIComponent(el.split('=')[1]);
      });
      /* eslint-enable no-param-reassign */
    }

    if (req.requestBody instanceof FormData) {
      req.requestBody = Object.fromEntries(req.requestBody);
    }

    if (req.requestBody.grant_type === 'password') {
      let [user] = schema.users.where({ username: req.requestBody.username }).models;

      if (user.password === req.requestBody.password) {
        let tokenRecord = schema.tokens.findOrCreateBy({
          user,
        });

        // TODO: Dedup this logic with the token factory
        tokenRecord.update({
          // userId: user.id,
          username: user.username,
          email: user.email,
          appName: faker.lorem.sentence(),
          validUntil: faker.date.recent(),
          token: uuid(),
        });

        /* eslint-disable camelcase */
        return {
          access_token: tokenRecord.token,
          account_id: user.id,
          token_type: 'Bearer',
        };
        /* eslint-enable camelcase */
      }

      return new Mirage.Response(400, {}, {
        error: 'Invalid credentials',
      });
    }

    if (req.requestBody.grant_type === 'client_credentials') {
      let tokenRecord = schema.tokens.create();

      tokenRecord.update({
        token: uuid(),
      });

      /* eslint-disable camelcase */
      return {
        access_token: tokenRecord.token,
        token_type: 'Bearer',
      };
      /* eslint-enable camelcase */
    }

    return new Mirage.Response(400, {}, { error: JSON.stringify({ grantType: 'unsupported_grant_type' }) });
  });

  this.post('/oauth/revoke', function(schema, req) {
    // Normalize the requestBody param
    if (req.requestHeaders['content-type'] === 'application/x-www-form-urlencoded') {
      let params = req.requestBody.split('&'); // grant_type=password&username=a%40b.com&password=123456

      /* eslint-disable no-param-reassign */
      req.requestBody = {};

      params.forEach(function(el) {
        req.requestBody[decodeURIComponent(el.split('=')[0])] = decodeURIComponent(el.split('=')[1]);
      });
      /* eslint-enable no-param-reassign */
    }

    if (req.requestBody.token_type_hint === 'access_token' || req.requestBody.token_type_hint === 'refresh_token') {
      let tokenRecord = schema.tokens.findBy({ token: req.requestBody.token });
      tokenRecord.destroy();
      return new Mirage.Response(200);
    }

    return new Mirage.Response(400, {}, { error: 'unsupported_token_type' });
  });

  this.post('/auth/password', function(schema, req) {
    let { email } = JSON.parse(req.requestBody);
    let user;

    user = schema.accounts.findBy('email', email);

    if (!user) {
      user = schema.accounts.findBy('username', email);
    }

    if (user) {
      return new Mirage.Response(200);
    } else {
      return new Mirage.Response(404);
    }
  });

  this.get('/api/v1/push/subscription', function(schema, request) {
    if (isAuthenticated(schema, request)) {
      let user = getAuthenticatedUser(schema, request);

      let subscription = schema.subscriptions.findBy({
        userId: user.id,
      });

      return subscription || new Mirage.Response(404);
    } else {
      return new Mirage.Response(403, { error: 'Invalid credentials.' });
    }
  });
}

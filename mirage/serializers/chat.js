import ApplicationSerializer from './application';
import ChatMessageSerializer from './chat-message';
import sortBy from 'lodash.sortby';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'account',
      'lastMessage',
    ];
  },

  embed: true,
  root: false,

  serialize(object, request) {
    let json;

    if (object.models) {
      // many
      json = object.models.map(model => {
        return this.serialize.call(this, model, request);
      });
    } else {
      // just one
      json = ApplicationSerializer.prototype.serialize.apply(this, arguments);

      let lastMessage = null;

      if (object.messages.length) {
        let rawMessages = object.messages.models;

        let sortedMessages =
          sortBy(rawMessages, [
            ({ createdAt }) => new Date(createdAt),
          ]);

        lastMessage = sortedMessages[sortedMessages.length - 1];
      }

      if (lastMessage) {
        let serialized = ChatMessageSerializer.prototype.serialize.call(this, lastMessage, request);

        /* eslint-disable camelcase */
        json.last_message = serialized;
        /* eslint-enable camelcase */
      }
    }

    return json;
  },
});

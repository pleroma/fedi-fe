import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return ['pleroma', 'source'];
  },

  embed: true,
  root: false,
});

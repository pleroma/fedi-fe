import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'notificationSettings',
    ];
  },

  embed: true,
  root: false,
});

import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return ['pleroma'];
  },

  embed: true,
  root: false,
});

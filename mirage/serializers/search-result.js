import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'accounts',
      'hashtags',
      'statuses',
    ];
  },

  embed: true,
  root: false,
});

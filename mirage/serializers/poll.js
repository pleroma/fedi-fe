import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'options',
    ];
  },

  embed: true,
  root: false,
});

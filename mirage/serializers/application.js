import { Serializer } from 'ember-cli-mirage';
import { decamelize } from '@ember/string';

export default Serializer.extend({
  keyForAttribute(attr) {
    return decamelize(attr);
  },

  keyForEmbeddedRelationship(attributeName) {
    return decamelize(attributeName);
  },
});

import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'accounts',
      'lastStatus',
    ];
  },

  embed: true,
  root: false,
});

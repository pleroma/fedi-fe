import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  root: false,
});

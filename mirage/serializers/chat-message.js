import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'attachment',
    ];
  },

  embed: true,
  root: false,

  serialize(object, request) {
    let json;

    if (object.models) {
      // many
      json = object.models.map(model => {
        return this.serialize.call(this, model, request);
      });
    } else {
      // just one
      json = ApplicationSerializer.prototype.serialize.apply(this, arguments);

      /* eslint-disable camelcase */
      json.account_id = object.account?.id;
      json.chat_id = object.chat?.id;
      /* eslint-enable camelcase */

      if (json.account) {
        delete json.account;
      }
    }

    return json;
  },
});

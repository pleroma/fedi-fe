import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'account',
      'pleroma',
      'status',
    ];
  },

  embed: true,
  root: false,
});

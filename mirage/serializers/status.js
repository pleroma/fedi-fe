import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  include(/* request */) {
    return [
      'account',
      'mediaAttachments',
      'pleroma',
      'poll',
      'reblog',
      'mentions',
    ];
  },

  embed: true,
  root: false,
});

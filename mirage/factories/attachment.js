import { Factory, association } from 'ember-cli-mirage';
import faker from 'faker';
import { colors } from 'pleroma-pwa/mirage/images';

export default Factory.extend({
  pleroma: association(),
  type: 'image',

  url() {
    let width = Math.random() > 0.5 ? 640 : 480;
    let height = Math.random() > 0.5 ? 640 : 480;
    return faker.image.dataUri(width, height, faker.random.arrayElement(colors));
  },

  previewUrl() {
    let width = Math.random() > 0.5 ? 320 : 240;
    let height = Math.random() > 0.5 ? 320 : 240;
    return faker.image.dataUri(width, height, faker.random.arrayElement(colors));
  },

  description() { return faker.lorem.words(); },
});

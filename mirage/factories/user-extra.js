import { Factory, association } from 'ember-cli-mirage';

export default Factory.extend({
  hideFavorites: false,

  hideFollows: false,
  hideFollowsCount: false,
  hideFollowers: false,
  hideFollowersCount: false,

  notificationSettings: association(),

  relationship: association(),
});

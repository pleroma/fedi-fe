import { Factory, trait } from 'ember-cli-mirage';

export default Factory.extend({
  expiresAt(index) {
    let date = new Date();
    date.setDate(date.getDate() - index);
    return date.toISOString();
  },

  expired() {
    return false;
  },

  multiple() {
    return false;
  },

  votesCount() {
    return this.options ? Math.max(...this.options.mapBy('votesCount')) : 0;
  },

  votersCount() {
    return 0;
  },

  voted() {
    return false;
  },

  ownVotes() {
    return [];
  },

  emojis() {
    return [];
  },

  withOptions: trait({
    afterCreate(poll, server) {
      poll.update({
        options: server.createList('poll-option', 4),
      });
    },
  }),

  isExpired: trait({
    expired: true,
  }),

  isRunning: trait({
    expired: false,
  }),
});

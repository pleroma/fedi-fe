import { Factory, association } from 'ember-cli-mirage';

export default Factory.extend({
  privacy: "public",
  sensitive: false,
  pleroma: association(),
});

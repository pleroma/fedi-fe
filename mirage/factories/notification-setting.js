import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  blockFromStrangers: true,
  hideNotificationContents: true,
});

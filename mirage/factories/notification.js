import { Factory, association, trait } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  pleroma: association(),
  account: association(),
  type() { return faker.random.arrayElement(['reblog', 'favorite', 'follow', 'mention']); },
  createdAt(index) {
    let date = new Date();
    date.setDate(date.getDate() - index);
    return date.toISOString();
  },

  seen: trait({
    afterCreate(notification) {
      notification.pleroma.update({ isSeen: true });
    },
  }),
});

import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  blockedBy: false,
  blocking: false,
  domainBlocking: false,
  endorsed: false,
  followedBy: false,
  following: false,
  muting: false,
  mutingNotifications: false,
  requested: false,
  showingReblogs: false,
  subscribing: false,
});

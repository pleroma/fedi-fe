import { Factory, association, trait } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  content() { return faker.lorem.paragraph(); },

  createdAt(index) {
    let date = new Date();
    date.setTime(date.getTime() + (index * 1000));
    return date.toISOString();
  },

  unread: false,

  account: association(),

  // NOTE: Don't auto-create an attachment.
  // attachment: association(),

  chat: association(),

  isUnread: trait({
    unread: true,
  }),
});

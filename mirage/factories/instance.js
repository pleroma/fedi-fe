import { Factory, association } from 'ember-cli-mirage';
import faker from 'faker';
import { colors } from 'pleroma-pwa/mirage/images';
import config from 'pleroma-pwa/config/environment';

const { testApiBaseUrl } = config.APP;
const url = new URL(testApiBaseUrl);

export default Factory.extend({
  description: 'A Pleroma instance, an alternative fediverse server',
  email: 'example@example.com',

  thumbnail() {
    return faker.image.dataUri(
      1024,
      1024,
      faker.random.arrayElement(colors),
    );
  },

  title: 'Pleroma',

  uri() {
    return testApiBaseUrl;
  },

  version: '2.7.2 (compatible; Pleroma 1.0.0)',
  registrations: true,
  maxTootChars: 5000,

  languages() {
    return ['en'];
  },

  pollLimits() {
    return { minExpiration: 0, maxOptions: 20, maxOptionChars: 200, maxExpiration: 31536000 };
  },

  stats() {
    return {};
  },

  urls() {
    return { streamingApi: `wss://${url.host}` };
  },

  nodeInfo: association(),
});

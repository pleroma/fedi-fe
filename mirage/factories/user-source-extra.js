import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  actorType: "Person",
  discoverable: false,
  noRichText: false,
  showRole: true,
});

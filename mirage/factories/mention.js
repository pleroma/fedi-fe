import { Factory } from 'ember-cli-mirage';
import faker from 'faker';
import config from 'pleroma-pwa/config/environment';

export default Factory.extend({
  username() { return faker.internet.domainWord(); },
  url() { return `${config.APP.testApiBaseUrl}/users/${this.username}`; },
  acct() { return this.username },
});

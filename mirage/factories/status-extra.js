import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  local: true,
  conversationId: null,
  inReplyToAccountAcct: null,
  threadMuted: false,
});

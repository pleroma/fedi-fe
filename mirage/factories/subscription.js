import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  endpoint: "https://yourdomain.example/listener",
  'server_key': "BCk-QqERU0q-CfYZjcuB6lnyyOYfJ2AifKqfeGIm7Z-HiTU5T9eTG5GxVA0_OH5mMlI4UkkDTpaZwozy0TzdZ2M=",
  alerts() {
    return {
      follow: true,
      favourite: true,
      reblog: true,
      mention: true,
      poll: true,
    };
  },
});

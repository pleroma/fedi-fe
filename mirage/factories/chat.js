import { Factory, association } from 'ember-cli-mirage';

export default Factory.extend({
  unread: 0,

  updatedAt() {
    return new Date();
  },

  // NOTE: lastMessage is handled automatically in the Mirage serializer.

  account: association(),
});

import { Factory, association } from 'ember-cli-mirage';

export default Factory.extend({
  accounts: association(),
  statuses: association(),
  hashtag: association(),
});

import { Factory, association, trait } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  account: association(),

  content() { return faker.lorem.paragraph(); },

  createdAt(index) {
    let date = new Date();
    date.setDate(date.getDate() - index);
    return date.toISOString();
  },

  favourited: false,

  pleroma: association(),

  reblog: null,

  withExpiredPoll: trait({
    afterCreate(status, server) {
      status.update({
        poll: server.create('poll', 'isExpired', 'withOptions'),
      });
    },
  }),

  withRunningPoll: trait({
    afterCreate(status, server) {
      status.update({
        poll: server.create('poll', 'isRunning', 'withOptions'),
      });
    },
  }),
});

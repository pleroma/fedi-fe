import { Factory } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  votesCount: 0,
  title() {
    return faker.lorem.sentence();
  },
});

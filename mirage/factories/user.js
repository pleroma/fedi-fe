import { Factory, association, trait } from 'ember-cli-mirage';
import { authenticateSession } from 'ember-simple-auth/test-support';
import { settled } from '@ember/test-helpers';
import { avatars } from 'pleroma-pwa/mirage/images';
import faker from 'faker';
import config from 'pleroma-pwa/config/environment';

export default Factory.extend({
  acct() { return this.username },
  avatar() { return config.APP.DEFAULT_AVATAR; },
  displayName() { return faker.name.firstName(); },
  email() { return faker.internet.email(); },
  password() { return faker.internet.password(); },
  username() { return faker.internet.domainWord(); },
  url() { return `${config.APP.testApiBaseUrl}/users/${this.username}`; },
  pleroma: association(),
  source: association(),

  withAvatar: trait({
    avatar() { return faker.random.arrayElement(avatars); },
  }),

  withToken: trait({
    afterCreate(user, server) {
      server.create('token', { user });
    },
  }),

  withSession: trait({
    afterCreate(user) {
      let { tokens } = user;

      if (tokens.models.length) {
        let tokenRecord = tokens.models[0];

        (async function() {
          await authenticateSession({
            'token_type': 'Bearer',
            'access_token': tokenRecord.token,
          });

          await settled();
        })();
      }
    },
  }),

  blockedBy: trait({
    afterCreate(user) {
      user.pleroma.relationship.update({ blockedBy: true });
    },
  }),

  blocked: trait({
    afterCreate(user) {
      user.pleroma.relationship.update({ blocking: true });
    },
  }),

  followed: trait({
    afterCreate(user) {
      user.pleroma.relationship.update({ following: true });
    },
  }),

  muted: trait({
    afterCreate(user) {
      user.pleroma.relationship.update({ muting: true });
    },
  }),

  federated: trait({
    acct() { return `${this.username}@${faker.internet.domainName()}` },
  }),

  withSubscription: trait({
    afterCreate(user, server) {
      server.create('subscription', { user });
    },
  }),
});

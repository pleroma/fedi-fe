import { Factory } from 'ember-cli-mirage';
import uuid from 'uuid/v4';
import faker from 'faker';

export default Factory.extend({
  token() { return uuid(); },
  username() { return this.user.username; },
  email() { return this.user.email; },
  appName() { return faker.lorem.sentence(); },
  validUntil() { return faker.date.recent(); },
});

export let colors = [
  'orange',
  'red',
  'green',
  'cyan',
  'yellow',
  'brown',
  'grey',
  'magenta',
  'blue',
];

export let avatars = [
  `/assets/images/avatars/01.jpg`,
  `/assets/images/avatars/02.jpg`,
  `/assets/images/avatars/03.jpg`,
  `/assets/images/avatars/04.jpg`,
  `/assets/images/avatars/05.jpg`,
  `/assets/images/avatars/06.jpg`,
  `/assets/images/avatars/07.jpg`,
];

export let photos = [
  `/assets/images/photos/01.jpg`,
  `/assets/images/photos/02.jpg`,
  `/assets/images/photos/03.jpg`,
  `/assets/images/photos/04.jpg`,
  `/assets/images/photos/05.jpg`,
  `/assets/images/photos/06.jpg`,
  `/assets/images/photos/07.jpg`,
  `/assets/images/photos/08.jpg`,
];

const path = require('path');
const sassdoc = require('sassdoc');
const watchman = require('fb-watchman');
const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const envTarget = process.env.DEPLOY_TARGET || EmberApp.env();
const isInDevelopment = ['development'].indexOf(envTarget) > -1;

module.exports = {
  name: require('./package').name,

  isDevelopingAddon() {
    return true;
  },

  included(app) {
    this._super.included && this._super.included.apply(this, arguments);

    this.app = app;
    this.app.options = this.app.options || {};
    this.app.options[this.name] = Object.assign({
      enabled: isInDevelopment,
      configPath: '.sassdocrc.js',
    }, this.app.options[this.name]);

    if (this.app.options[this.name].enabled) {
      this.ui.writeInfoLine('Ember-cli-sassdoc is enabled.');

      this.buildSassDoc()
        .then(() => this.initiateFileWatch());
    } else {
      this.ui.writeInfoLine('Ember-cli-sassdoc is disabled.');
    }
  },

  buildSassDoc() {
    this.ui.startProgress('Ember-cli-sassdoc building sassdoc...');

    let sassdocrc = require(path.join(this.project.root, this.app.options[this.name].configPath));

    sassdocrc.dest = path.join(__dirname, 'dist');

    return sassdoc(path.join(this.project.root, 'app/styles'), sassdocrc)
      .then(() =>{
        this.ui.stopProgress('Ember-cli-sassdoc built sassdoc.');
        this.ui.writeInfoLine('------------------ SASSDOC ------------------');
        this.ui.writeInfoLine(`Sassdoc available at: file://${path.join(__dirname, 'dist', 'index.html')}`);
        this.ui.writeInfoLine('------------------ SASSDOC ------------------');
      });
  },

  initiateFileWatch() {
    let client = new watchman.Client();
    let watch = path.join(this.project.root, 'app/styles');

    return new Promise((resolve, reject) => {
      client.capabilityCheck({ optional: [], required: ['relative_root'] },
        (error) => {
          if (error) {
            this.ui.writeErrorLine(`Ember-cli-sassdoc uses watchman to observe file changes, ensure you have watchman installed correctly: ${error}`);
            reject(error);
          }

          client.command(['subscribe', watch, 'mysubscription', {
            expression: ['match', '*.scss'],
          }], (error) => {
            if (error) {
              // Probably an error in the subscription criteria
              this.ui.writeErrorLine(`Ember-cli-sassdoc failed to subscribe to your stylesheets: ${error}`);
              return reject();
            }

            this.ui.writeInfoLine('Ember-cli-sassdoc is watching your stylesheets.');
            resolve();
          });

          client.on('subscription', (/* resp */) => {
            this.ui.writeInfoLine('Ember-cli-sassdoc stylesheets changed.');
            this.buildSassDoc();
          });
        });
    });
  },
};

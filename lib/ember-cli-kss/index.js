const fse = require('fs-extra');
const path = require('path');
const kss = require('kss');
const watchman = require('fb-watchman');
const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const envTarget = process.env.DEPLOY_TARGET || EmberApp.env();
const isInDevelopment = ['development'].indexOf(envTarget) > -1;

module.exports = {
  name: require('./package').name,

  isDevelopingAddon() {
    return true;
  },

  included(app) {
    this._super.included && this._super.included.apply(this, arguments);

    this.app = app;
    this.app.options = this.app.options || {};
    this.app.options[this.name] = Object.assign({
      enabled: isInDevelopment,
      configPath: './kss-config.json',
    }, this.app.options[this.name]);

    if (this.app.options[this.name].enabled) {
      this.ui.startProgress('Ember-cli-kss is enabled.');

      this.buildKss()
        .then(() => this.initiateFileWatch());
    } else {
      this.ui.writeInfoLine('Ember-cli-kss is disabled.');
    }
  },

  buildKss() {
    let options = fse.readFileSync(path.join(this.project.root, this.app.options[this.name].configPath));

    options = JSON.parse(options);

    options.destination = path.join(__dirname, 'dist');

    this.ui.startProgress('Ember-cli-kss building styleguide.');

    fse.emptyDirSync(path.join(__dirname, 'dist'));

    return kss(options)
      .then(() => {
        this.ui.stopProgress('Ember-cli-kss built kss.');
        this.ui.writeInfoLine('--------------- KSS ---------------');
        this.ui.writeInfoLine(`KSS available at: file://${path.join(__dirname, 'dist', 'index.html')}`);
        this.ui.writeInfoLine('--------------- KSS ---------------');
      })
      .catch((error) => {
        this.ui.writeErrorLine(`Ember-cli-kss failed to build KSS styleguide: ${error}`);
      });
  },

  initiateFileWatch() {
    let client = new watchman.Client();
    let watch = path.join(this.project.root, 'app/styles');

    return new Promise((resolve, reject) => {
      client.capabilityCheck({ optional: [], required: ['relative_root'] },
        (error) => {
          if (error) {
            this.ui.writeErrorLine(`Ember-cli-kss uses watchman, ensure you have watchman installed correctly: ${error}`);
            reject(error);
          }

          client.command(['subscribe', watch, 'mysubscription', {
            expression: ['match', '*.scss'],
          }], (error) => {
            if (error) {
              // Probably an error in the subscription criteria
              this.ui.writeErrorLine(`Ember-cli-kss failed to subscribe to your stylesheets: ${error}`);
              return reject();
            }

            this.ui.writeInfoLine('Ember-cli-kss is watching your stylesheets.');
            resolve();
          });

          client.on('subscription', (/* resp */) => {
            this.ui.writeInfoLine('Ember-cli-kss stylesheets changed.');
            this.buildKss();
          });
        });
    });
  },
};

const narwin = require('stylelint-config-narwin');

module.exports = {
  extends: ['stylelint-config-standard'],
  plugins: [...narwin.plugins, 'stylelint-scss'],
  rules: {
    ...narwin.rules,
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,
  },
};

# Debugging

The [Sentry](https://sentry.io) application monitoring platform is available for advanced debugging of FediFE.
The monitoring and analytics framework is not included in FediFE builds unless a valid `SENTRY_DSN` environment variable is set during the build process.

More details of how to activate Sentry and provide the required data to our team will be added soon.

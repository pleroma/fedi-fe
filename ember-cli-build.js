'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const Funnel = require('broccoli-funnel');
const broccoliAssetRevDefaults = require('broccoli-asset-rev/lib/default-options');
const envTarget = EmberApp.env();
const config = require('./config/environment')(envTarget);
const isProductionLikeBuild = ['production', 'staging'].indexOf(envTarget) > -1;
const isDevelopmentBuild = ['development'].indexOf(envTarget) > -1;
const path = require('path');
const flag = require('node-env-flag');
const projectVersionStrategy = flag(process.env.PROJECT_VERSION_STRATEGY, false);

module.exports = function(defaults) {
  let trees = {};

  trees.app = new Funnel('app', {
    exclude: ['styles/narwin-stylekit'],
  });

  let app = new EmberApp(defaults, {
    trees,

    sourcemaps: {
      enabled: true,
    },

    babel: {
      plugins: [
        '@babel/plugin-proposal-optional-chaining',
        require.resolve('ember-auto-import/babel-plugin'),
      ],
    },

    'ember-cli-image-transformer': {
      images: [
        {
          inputFilename: 'public/assets/icons/globe.svg',
          outputFileName: 'appicon-',
          convertTo: 'png',
          destination: 'assets/icons/',
          sizes: [32, 192, 280, 512],
        },
      ],
    },

    'ember-cli-favicon': {
      enabled: true,
      iconPath: 'assets/icons/globe.svg',
      faviconsConfig: {
        appleStatusBarStyle: 'default',
        path: isProductionLikeBuild ? config.rootURL : '/',
        icons: {
          favicons: true,
        },
      },
    },

    'ember-cli-kss': {
      enabled: isDevelopmentBuild,
      configPath: './kss-config.json',
    },

    'ember-cli-sassdoc': {
      enabled: isDevelopmentBuild,
      configPath: './.sassdocrc.json',
    },

    'ember-composable-helpers': {
      only: [
        'contains',
        'drop',
        'filter-by',
        'find-by',
        'join',
        'map-by',
        'map',
        'noop',
        'optional',
        'pipe',
        'queue',
        'reject-by',
        'repeat',
        'sort-by',
        'take',
        'toggle',
      ],
    },

    'ember-math-helpers': {
      only: [
        'add',
        'div',
        'max',
        'mult',
        'round',
        'sub',
        'trunc',
      ],
    },

    'esw-cache-first': {
      patterns: ['/assets/fonts/.*'],
    },

    'esw-index': {
      excludeScope: [
        /\/pleroma\/admin(\/.*)?$/,
        /\/mailer\/unsubscribe(\/.*)?$/,
        /\/oauth(\/.*)?$/,
      ],

      includeScope: [
        /\/about/,
        /\/sign-in/,
        /\/sign-out/,
        /\/sign-up/,
        /\/feeds(\/.*)?$/,
        /\/notifications(\/.*)?$/,
        /\/account(\/.*)?$/,
        /\/users(\/.*)?$/,
        /\/notice(\/.*)?$/,
        /\/settings(\/.*)?$/,
        /\/search(\/.*)?$/,
        /\/tag(\/.*)?$/,
        /\/messages(\/.*)?$/,
        /\/message(\/.*)?$/,
      ],
    },

    'ember-fetch': {
      preferNative: true,
    },

    'ember-service-worker': {
      // Only turn on in deployed environments.
      enabled: isProductionLikeBuild,

      // Do not use inline strategy in dev.
      // @see https://github.com/DockYard/ember-service-worker/issues/88
      // @see https://github.com/DockYard/ember-service-worker/issues/84
      registrationStrategy: isProductionLikeBuild ? 'after-ember' : 'async',
      registrationDistPath: '/assets/',

      rootUrl: '/assets/',
      scope: '/',

      // Only version by `project-version` in actual Prod environment, so
      // staging and preview environments can be more easily-updateable.
      versionStrategy:
        projectVersionStrategy && config.deployTarget === 'production'
          ? 'project-version'
          : 'every-build',
    },

    fingerprint: {
      extensions: broccoliAssetRevDefaults.extensions.concat(['webmanifest']),
      enabled: isProductionLikeBuild, // Only turn on in deployed environments.
      generateAssetMap: true,
    },

    postcssOptions: {
      compile: {
        extension: 'scss',
        parser: require('postcss-scss'),
        plugins: [
          {
            module: require('@csstools/postcss-sass'),
            options: {
              includePaths: [
                path.join(__dirname, 'app/styles/narwin-stylekit'),
              ],
            },
          },
        ],
      },

      filter: {
        enabled: isProductionLikeBuild,
        plugins: [
          {
            module: require('autoprefixer'),
          },
        ],
      },
    },

    svgJar: {
      strategy: 'inline',
    },
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.
  app.import('node_modules/cropperjs/dist/cropper.css');

  return app.toTree();
};

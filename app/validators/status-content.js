import { validate } from 'ember-validators';
import { isEmpty } from '@ember/utils';

export default function validateStatusContent(options = {}) {
  options.presence = true;

  return (key, value, _oldValue, changes) => {
    if (!isEmpty(changes.mediaIds)) {
      return true;
    }

    let result = validate('presence', value, options, null, key);

    return result?.context?.message || result;
  };
}

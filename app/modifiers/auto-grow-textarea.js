import Modifier from 'ember-modifier';
import { inject as service } from '@ember/service';
import { debug } from '@ember/debug';
import autosize from 'autosize';

export default class AutoGrowTextareaModifier extends Modifier {
  @service features;

  initialized = false;

  didReceiveArguments() {
    if (this.features.isEnabled('useUpdatedTextareaAutoGrow')) {
      if (this.initialized) {
        debug('autosize update');
        autosize.update(this.element);
      } else {
        debug('autosize initialize');
        autosize(this.element);
        this.initialized = true;
      }
    } else {
      debug('height update');
      /* eslint no-param-reassign: ["error", { "props": false }]*/
      this.element.style.height = 'auto';
      this.element.style.height = `${this.element.scrollHeight}px`;
    }
  }

  willRemove() {
    if (this.features.isEnabled('useUpdatedTextareaAutoGrow')) {
      debug('autosize destroy');
      autosize.destroy(this.element);
    }
  }
}

import { setModifierManager } from '@ember/modifier';
import {
  bindKeyboardShortcuts,
  unbindKeyboardShortcuts,
} from 'ember-keyboard-shortcuts';

export default setModifierManager(
  () => ({
    createModifier() {
      return {
        element: null,
        keyboardShortcuts: {},
        focusElement(event) {
          event.stopPropagation();
          event.preventDefault();

          this.element.focus();
        },
      };
    },

    installModifier(state, element, { named }) {
      state.element = element;

      if (named.key) {
        this.setupShortcuts(state, named.key);
      }
    },

    updateModifier(state, { named }) {
      if (state.key !== named.key) {
        this.setupShortcuts(state, named.key);
      }
    },

    setupShortcuts(state, key) {
      unbindKeyboardShortcuts(state);

      state.key = key;

      state.keyboardShortcuts[key] = {
        action: state.focusElement,
        global: false, // whether to trigger inside input
      };

      bindKeyboardShortcuts(state);
    },

    destroyModifier(state) {
      unbindKeyboardShortcuts(state);
    },
  }),
  class CaptureFocusOn {},
);

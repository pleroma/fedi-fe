import { setModifierManager } from '@ember/modifier';
import { scheduleOnce, cancel } from '@ember/runloop';

export default setModifierManager(
  () => ({
    createModifier() {
      return { schedule: null };
    },

    installModifier(state, element) {
      state.schedule = scheduleOnce('afterRender', element, this.doFocus);
    },

    doFocus() {
      this.focus();
    },

    updateModifier(/* state, args */) {},

    destroyModifier(state) {
      cancel(state.schedule);
    },
  }),
  class FocusAfterRender {},
);

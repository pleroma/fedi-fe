import { setModifierManager } from '@ember/modifier';
import { htmlSafe } from '@ember/string';
import { isEmpty } from '@ember/utils';

export default setModifierManager(
  () => ({
    createModifier() {
      return {
        element: null,
        updateBackground(image) {
          this.element.style.backgroundImage = htmlSafe(`url('${image}')`);
        },
      };
    },

    installModifier(modifier, element, { named }) {
      modifier.element = element;
      modifier.updateBackground = modifier.updateBackground.bind(modifier);

      if (named.url && (isEmpty(named.enabled) || named.enabled)) {
        modifier.updateBackground(named.url);
      }
    },

    updateModifier(modifier, { named }) {
      if (named.url && (isEmpty(named.enabled) || named.enabled)) {
        modifier.updateBackground(named.url);
      }
    },

    destroyModifier(/* state */) {

    },
  }),
  class BackgroundImageModifier {},
);

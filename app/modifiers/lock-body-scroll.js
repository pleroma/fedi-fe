import { setModifierManager } from '@ember/modifier';
import {
  disableBodyScroll,
  enableBodyScroll,
} from 'body-scroll-lock';

export default setModifierManager(
  () => ({
    createModifier() {
      return {
        element: null,
        options: {},
      };
    },

    installModifier(state, element, args) {
      let { enabled } = args.named;

      state.element = element;

      if (enabled) {
        disableBodyScroll(state.element, state.options);
      }
    },

    updateModifier(state, args) {
      let { enabled } = args.named;

      if (enabled) {
        disableBodyScroll(state.element, state.options);
      } else {
        enableBodyScroll(state.element, state.options);
      }
    },

    destroyModifier(state) {
      enableBodyScroll(state.element, state.options);
    },
  }),
  class FocusAfterRender {},
);

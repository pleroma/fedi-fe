import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';
import fetch from 'fetch';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  pathForType() {
    return this._super('media');
  },

  createRecord(store, type, snapshot) {
    let { file } = snapshot.attributes();

    let formData = new FormData();
    formData.append('file', file);

    let url = this.buildURL(type.modelName, null, snapshot, 'createRecord');

    let headers = { ...this.headers };
    delete headers['content-type'];

    let options = {
      method: 'POST',
      headers,
      body: formData,
    };

    return fetch(url, options).then((resp) => resp.json());
  },
});

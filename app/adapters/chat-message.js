import ApplicationAdapter from './application';
import config from 'pleroma-pwa/config/environment';

export default ApplicationAdapter.extend({
  namespace: config.APP.pleromaApiNamespace,
});

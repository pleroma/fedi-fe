import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';
import config from 'pleroma-pwa/config/environment';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  urlForQueryRecord() {
    return this.pleromaApi.endpoints.clientCredentials;
  },

  ajaxOptions(url, type, options) {
    let hash = this._super(url, type, options);

    hash.method = 'POST';
    hash.type = 'POST';

    hash.body = JSON.stringify({
      /* eslint-disable camelcase */
      client_name: `${config.APP.name}_${(new Date()).toISOString()}`,
      redirect_uris: `${window.location.origin}/oauth-callback`,
      /* eslint-enable camelcase */
      scopes: config.APP.OAUTH_SCOPES.join(' '),
    });

    hash.headers = {
      'Content-Type': 'application/json',
    };

    delete hash.data;

    hash.url = url;

    return hash;
  },
});

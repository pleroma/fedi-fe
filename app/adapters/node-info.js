import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  urlForQueryRecord() {
    return this.pleromaApi.endpoints.nodeInfo;
  },
});

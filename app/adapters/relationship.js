import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';
import { resolve } from 'rsvp';

export default ApplicationAdapter.extend({
  coalesceFindRequests: true,

  pleromaApi: service(),
  session: service(),

  handleResponse(code, headers, payload, requestData) {
    if (!this.session.isAuthenticated) {
      code = 200;
      payload = [];
    }

    return this._super(code, headers, payload, requestData);
  },

  findRecord(store, type, id) {
    return this.queryForIds([id]);
  },

  findMany(store, type, ids) {
    return this.queryForIds(ids);
  },

  query(store, type, query) {
    let ids = Object.values(query).flat().filter(Boolean);
    return this.queryForIds(ids);
  },

  /**
   * @private
   */
  queryForIds(ids) {
    if (!this.session.isAuthenticated) {
      return resolve(this.defaultResponse(ids));
    }

    return this.ajax(
      this.pleromaApi.endpoints.relationships,
      'GET',
      { data: { id: ids } },
    ).catch(() => this.defaultResponse(ids));
  },

  /**
   * @private
   */
  defaultResponse(ids = []) {
    return ids.map(id => ({ id }));
  },
});

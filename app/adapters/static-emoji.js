import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  namespace: 'static',

  pathForType() {
    return 'emoji.json';
  },
});

import ApplicationAdapter from 'pleroma-pwa/adapters/application';
import { inject as service } from '@ember/service';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  ajaxOptions(url, type, options) {
    if (['POST'].includes(type) && options.data) {
      // TODO: When adding in file upload, be sure to add the Idempotency-id to the request headers. This will ensure we can re-start / recover.

      // De-nest the posted obj. Thanks ember-data.
      options.data = options.data.status;
    }

    return this._super(url, type, options);
  },
});

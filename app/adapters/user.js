import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  pathForType() {
    return this._super('account');
  },

  urlForQuery() {
    return this.pleromaApi.endpoints.accountSearch;
  },

  urlForQueryRecord(query, type) {
    if (query.me) {
      delete query.me;
      return this.pleromaApi.endpoints.currentUser;
    }

    return this._super(query, type);
  },
});

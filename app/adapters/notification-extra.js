import {inject as service} from '@ember/service';
import ApplicationAdapter from 'pleroma-pwa/adapters/application';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  buildURL(modelName, record, snapshot, urlType) {
    if (urlType === 'markAsRead') {
      return this.pleromaApi.endpoints.notificationsMarkAsRead;
    }

    return this._super(modelName, record, snapshot, urlType);
  },

  updateRecord(store, type, snapshot) {
    if (snapshot.adapterOptions.dontPersist) {
      return new Promise(function(resolve) {
        return resolve(snapshot.record);
      });
    } else {
      this.super(...arguments);
    }
  },
});

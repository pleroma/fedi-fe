import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  namespace: 'api/pleroma',

  pathForType() {
    return 'emoji';
  },
});

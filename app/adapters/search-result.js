import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  pathForType() {
    return this._super('search_result');
  },

  urlForQueryRecord() {
    return this.pleromaApi.endpoints.search;
  },
});

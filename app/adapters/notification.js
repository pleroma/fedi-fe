import ApplicationAdapter from 'pleroma-pwa/adapters/application';
import { inject as service } from '@ember/service';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  buildURL(modelName, record, snapshot, urlType) {
    if (urlType === 'markAsRead') {
      return this.pleromaApi.endpoints.notificationsMarkAsRead;
    }

    return this._super(modelName, record, snapshot, urlType);
  },
});

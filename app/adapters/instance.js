import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  urlForQueryRecord() {
    return this.pleromaApi.endpoints.instance;
  },

  ajaxOptions(url, type, options) {
    let hash = this._super(url, type, options);
    hash.url = url;
    return hash;
  },
});

import RESTAdapter from '@ember-data/adapter/rest';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import config from 'pleroma-pwa/config/environment';

export default RESTAdapter.extend({
  error: service(),
  instances: service(),
  intl: service(),
  pleromaApi: service(),
  sentry: service(),
  session: service(),

  host: readOnly('pleromaApi.apiBaseUrl'),
  namespace: config.APP.mastodonApiNamespace,

  headers: readOnly('session.headers'),

  handleResponse(code, headers, payload, requestData) {
    code = this.error.detectErrantSuccessCode(code, payload);

    if (code >= 400) {
      payload = this.error.formatPayloadErrors(payload);
    }

    return this._super(code, headers, payload, requestData);
  },
});

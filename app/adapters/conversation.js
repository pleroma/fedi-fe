import ApplicationAdapter from 'pleroma-pwa/adapters/application';
import { inject as service } from '@ember/service';

export default ApplicationAdapter.extend({
  pleromaApi: service(),

  urlForFindRecord(id) {
    return this.pleromaApi.endpoints.conversation(id);
  },
});

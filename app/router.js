import config from './config/environment';
import EmberRouterScroll from 'ember-router-scroll';
import { setupRouter, reset } from 'ember-app-scheduler';

export default class Router extends EmberRouterScroll {
  location = config.locationType;

  rootURL = config.rootURL;

  constructor(...args) {
    super(...args);
    setupRouter(this);
  }

  willDestroy(...args) {
    reset();
    super.destroy(...args);
  }
}

Router.map(function() {
  this.route('about');
  this.route('sign-in');
  this.route('sign-out');
  this.route('sign-up');

  this.route('feed', { path: '/feeds/:id' });

  this.route('notifications', function() {
    this.route('new-follows');
    this.route('mentions');
    this.route('reposts');
    this.route('favorites');
  });

  this.route('account', { path: 'account/:id' }, function() {
    this.route('followers');
    this.route('following');
    this.route('statuses');
    this.route('media');
    this.route('favorites');
  });

  this.route('user', { path: 'users/:id' });

  this.route('status', { path: '/notice' }, function() {
    this.route('details', { path: '/:id' });
  });

  this.route('settings', function() {
    this.route('profile');
    this.route('security');
    this.route('notifications');
    this.route('blocks');
    this.route('mutes');
    this.route('advanced');
  });

  this.route('search', function() {
    this.route('results', { path: ':query' }, function() {
      this.route('statuses');
      this.route('people');
      this.route('hashtags');
    });
  });

  this.route('hashtag', { path: '/tag/:hashtag' });

  this.route('messages');

  this.route('message', function() {
    this.route('chat', { path: '/chat/:id' });
    this.route('compose');
    this.route('details', { path: '/:id' });
  });

  this.route('not-found', { path: '*path' });
});

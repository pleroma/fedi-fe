import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default Route.extend(AuthenticatedRouteMixin, {
  intl: service(),
  session: service(),

  model() {
    let user = this.session.currentUser;

    return {
      refresh: () => this.refresh(),
      user,
    };
  },

  titleToken() {
    return this.intl.t('userSettings');
  },
});

import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default Route.extend(AuthenticatedRouteMixin, {
  feeds: service(),

  model() {
    let feed = this.modelFor('notifications');

    this.feeds.subscribe(feed.id);

    return {
      feed,
    };
  },

  afterModel(model) {
    model.feed.loadNew.perform();
  },

  titleToken() {
    return this.intl.t('allNotifications');
  },
});

import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default Route.extend(AuthenticatedRouteMixin, {
  feeds: service(),

  feedId: 'notifications-reposts',

  model() {
    let factory = getOwner(this).factoryFor(`feed:${this.feedId}`);
    let tmpFeed = factory.create({});
    let feed = this.feeds.registerFeed(tmpFeed);

    try {
      this.feeds.subscribe(this.feedId);
    } catch (_) {
      feed = {};
    }

    return {
      feed,
    };
  },

  afterModel(model) {
    model.feed.loadNew.perform();
  },

  deactivate() {
    this.feeds.unsubscribe(this.feedId);
  },

  titleToken() {
    return this.intl.t('reposts');
  },
});

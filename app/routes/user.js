import Route from '@ember/routing/route';

export default class extends Route {
  model({ id }) {
    return id;
  }

  redirect(id) {
    this.replaceWith('account', id);
  }
}

import Route from '@ember/routing/route';

export default class SettingsIndexRoute extends Route {
  redirect() {
    this.replaceWith('settings.profile');
  }
}

import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default Route.extend(AuthenticatedRouteMixin, {
  intl: service(),

  afterModel({ user }) {
    return user.load('blocks');
  },

  titleToken() {
    return this.intl.t('blocks');
  },
});

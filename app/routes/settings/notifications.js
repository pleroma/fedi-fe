import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';

export default class SettingsNotificationsRoute extends Route.extend(AuthenticatedRouteMixin) {
  @service() intl;

  @readOnly('session.currentUser.pleroma.notificationSettings')
  notificationSettings;

  model() {
    return this.notificationSettings.data || this.notificationSettings;
  }

  titleToken() {
    return this.intl.t('notifications');
  }
}

import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class SettingsMutesRoute extends Route {
  @service() intl;

  afterModel({ user }) {
    return user.load('mutes');
  }

  titleToken() {
    return this.intl.t('mutes');
  }
}

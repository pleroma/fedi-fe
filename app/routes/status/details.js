import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { cancel, later } from '@ember/runloop';
import { whenRouteIdle }  from 'ember-app-scheduler';

export default Route.extend({
  features: service(),
  intl: service(),
  store: service(),

  async model(params) {
    let status;

    // Experiment to see if avoiding cache fixes rendering errors.
    if (this.features.isEnabled('statusDetailsFindRecord')) {
      status = await this.store.findRecord('status', params.id);
    } else {
      status = await this.store.loadRecord('status', params.id);
    }

    await status.loadConversation();

    return {
      status,
    };
  },

  afterModel() {
    this._super(...arguments);

    whenRouteIdle().then(() => {
      this.scrollSchedule = later(this, 'scrollToActiveItem', 200);
    });
  },

  deactivate(...args) {
    cancel(this.scrollSchedule);
    return this._super(...args);
  },

  // Only run when first entering route, never again during internal route transitions.
  scrollToActiveItem() {
    let activeItem = document.querySelector('[data-is-active-item]');

    if (!activeItem) {
      return;
    }

    activeItem.scrollIntoView({
      block: 'center',
    });
  },

  titleToken() {
    return this.intl.t('conversation');
  },
});

import Route from '@ember/routing/route';
import { action } from '@ember/object';
import { registerDisposable, runDisposables } from 'ember-lifeline';
import { inject as service } from '@ember/service';

export default Route.extend({
  session: service(),

  store: service(),

  async model({ id }) {
    let user = await this.store.loadRecord('user', id);

    let { id: userId } = user;
    await this.store.loadRecord('relationship', userId);

    return {
      refresh: () => this.refresh(),
      user,
    };
  },

  activate() {
    // Adding logic to refresh this route because of the precarious async data
    // relationship between user and relationship. In short, there could be
    // times when they don't contain the right data. This ensures when
    // authentication happens and this set of routes is active, this data is
    // refreshed and made current.
    this.session.on('authenticationSucceeded', this.doRefresh);

    registerDisposable(this, () => {
      this.session.off('authenticationSucceeded', this.doRefresh);
    });
  },

  deactivate() {
    runDisposables(this);
  },

  @action
  doRefresh() {
    this.refresh();
  },

  titleToken(model) {
    return model.user.username;
  },
});

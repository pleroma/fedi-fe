import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default class SearchResultsRoute extends Route {
  @service search;

  model({ query }) {
    this.search.setQuery(query);

    let searchResult = this.search.results();

    return hash({ query, searchResult });
  }
}

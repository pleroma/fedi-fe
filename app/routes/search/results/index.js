import Route from '@ember/routing/route';

export default class extends Route {
  redirect({ searchResult }) {
    if (searchResult.statuses.length) {
      this.replaceWith('search.results.statuses');
    } else if (searchResult.accounts.length) {
      this.replaceWith('search.results.people');
    } else if (searchResult.hashtags.length) {
      this.replaceWith('search.results.hashtags');
    } else {
      this.replaceWith('search.results.statuses');
    }
  }
}

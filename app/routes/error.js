import { setProperties } from '@ember/object';
import Route from '@ember/routing/route';
import { ForbiddenError, NotFoundError, UnauthorizedError } from '@ember-data/adapter/error';

export default Route.extend({
  setupController(controller, model) {
    this._super(controller, model);

    setProperties(controller, {
      is404: model instanceof NotFoundError,
      isAuthenticationError: [ForbiddenError, UnauthorizedError].any((errorType) => model instanceof errorType),
    });
  },

  actions: {
    reload() { window.location.reload(true); },
  },

  resetConroller(controller, isExiting) {
    if (isExiting) {
      setProperties(controller, {
        is404: false,
        isAuthenticationError: false,
      });
    }
  },
});

import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { registerDisposable, runDisposables } from 'ember-lifeline';
import { getOwner } from '@ember/application';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default class MessageDetailsRoute extends Route.extend(AuthenticatedRouteMixin) {
  @service() feeds;

  async model(params, transition) {
    let conversation = await this.store.loadRecord('conversation', params.id);

    let previousModel = this.modelFor(this.routeName);

    let factory = getOwner(this).factoryFor('feed:directMessage');
    let tmpFeed = factory.create({ conversationId: conversation.id });
    let { id: feedId } = tmpFeed;

    if (transition.from && previousModel && (previousModel.feed.id !== feedId)) {
      // Unsubscribe from previousFeed when navigating between direct messages.
      this.feeds.unsubscribe(previousModel.feed.id);
    }

    let feed = this.feeds.registerFeed(tmpFeed);

    try {
      await this.feeds.subscribe(feedId);

      registerDisposable(this, () => {
        this.feeds.unsubscribe(feedId);
      });
    } catch (_) {
      feed = {};
    }

    return {
      feed,
      conversation,
      initialLinks: params.links,
    }
  }

  deactivate() {
    runDisposables(this);
  }
}

import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default class MessageDetailsRoute extends Route.extend(AuthenticatedRouteMixin) {
  @service chatActions;

  @service error;

  @service router;

  @service toast;

  async afterModel(models) {
    let { chat } = models;
    let lastReadId = chat.lastMessage?.id;
    let chatId = chat.id;

    if (lastReadId && chatId) {
      this.chatActions.markChatAsRead.perform(chatId, lastReadId, { delayed: false });
    }
  }

  async model(params) {
    try {
      let chat = await this.chatActions.assertChat.perform(params.id);

      // TODO: Handle participants like message.compose route did before. Do this when multiple participants are possible.

      await chat.messages;

      return {
        chat,
        initialLinks: params.links,
      }
    } catch (err) {
      let formatted = this.error.formatPayloadErrors(err);

      for (let { detail: message } of formatted.errors) {
        this.toast.notify({ message, type: 'error' });
      }

      this.transitionTo('messages');
    }

  }
}

import Route from '@ember/routing/route';
import { all } from 'rsvp';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';
import { isEmpty } from '@ember/utils';
import { scheduleOnce } from '@ember/runloop';
import { inject as service } from '@ember/service';

export default class MessageComposeRoute extends Route.extend(AuthenticatedRouteMixin) {
  @service intl;

  @service router;

  reasonForAuthenticationKey = 'youMustSignInToMessageSomeone';

  async model(params) {
    if (isEmpty(params.participants)) {
      scheduleOnce('afterRender', this, this.showRecipientsForm);
    } else {
      let participants = await all(params.participants.map((participantId) => this.store.loadRecord('user', participantId)));

      return {
        participants,
        initialLinks: params.links,
      };
    }
  }

  showRecipientsForm() {
    this.send('createStatus', 'direct');
  }

  titleToken() {
    return this.intl.t('compose');
  }
}

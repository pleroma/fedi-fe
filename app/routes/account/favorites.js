import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { registerDisposable, runDisposables } from 'ember-lifeline';
import { getOwner } from '@ember/application';

export default class AccountFavorites extends Route {
  @service intl;

  @service feeds;

  async model() {
    let accountModel = this.modelFor('account');
    let { user: { id: userId } } = accountModel;

    let factory = getOwner(this).factoryFor('feed:favorites');
    let tmpFeed = factory.create({ userId });
    let { id: feedId } = tmpFeed;

    let feed = this.feeds.registerFeed(tmpFeed);

    try {
      await this.feeds.subscribe(feedId);

      registerDisposable(this, () => {
        this.feeds.unsubscribe(feedId);
      });
    } catch (_) {
      feed = {};
    }

    return {
      feed,
      ...accountModel,
    }
  }

  deactivate() {
    runDisposables(this);
  }

  titleToken() {
    return this.intl.t('favorites');
  }
}

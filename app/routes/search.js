import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class SearchRoute extends Route {
  @service intl;

  @service search;

  deactivate() {
    this.search.clearQuery();
  }

  titleToken() {
    return this.intl.t('search');
  }
}

import Route from '@ember/routing/route';
import { reject } from 'rsvp';
import { NotFoundError } from '@ember-data/adapter/error';

export default Route.extend({
  beforeModel() {
    return reject(new NotFoundError());
  },
});

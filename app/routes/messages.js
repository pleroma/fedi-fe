import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default class MessagesRoute extends Route.extend(AuthenticatedRouteMixin) {
  @service() feeds;

  @service() intl;

  reasonForAuthenticationKey = 'youMustSignInToSeeYourDirectMessages';

  async model() {
    let feed = this.feeds.getFeed('chats');

    if (feed.subscribedTo) {
      feed.loadNew.perform();
    } else {
      await this.feeds.subscribe(feed.id);
    }

    return {
      feed,
    };
  }

  titleToken() {
    return this.intl.t('directMessages');
  }
}

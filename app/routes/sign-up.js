import Route from '@ember/routing/route';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import { inject as service } from '@ember/service';
import { addListener, removeListener } from '@ember/object/events';
import config from 'pleroma-pwa/config/environment';
import { setProperties } from '@ember/object';

export default class SignUpRoute extends Route.extend(UnauthenticatedRouteMixin) {
  @service authModal;

  constructor(...args) {
    super(...args);

    setProperties(this, config.APP.emberSimpleAuthConfig);
  }

  activate() {
    addListener(this.authModal, 'auth-modal-hidden', this, this.onAuthModalClosed);
  }

  deactivate() {
    removeListener(this.authModal, 'auth-modal-hidden', this, this.onAuthModalClosed);
  }

  onAuthModalClosed() {
    this.replaceWith('/');
  }
}

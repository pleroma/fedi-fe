import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { debug } from '@ember/debug';
import AuthenticatedRouteMixin from 'pleroma-pwa/mixins/authenticated-route';

export default class MessagesRoute extends Route.extend(AuthenticatedRouteMixin) {
  @service error;

  @service feeds;

  @service pleromaApi;

  reasonForAuthenticationKey = 'youMustSignInToSeeYourNotifications';

  async activate() {
    this.markAllNotificationsAsRead()
  }

  async model() {
    let feed = this.feeds.getFeed('notifications');

    if (feed.subscribedTo) {
      feed.loadNew.perform();
    } else {
      await this.feeds.subscribe(feed.id);
    }

    return feed;
  }

  async markAllNotificationsAsRead() {
    let maxId = this.feeds.notifications.getMaxId();

    try {
      let url = this.pleromaApi.endpoints.notificationsMarkAsRead;
      let { headers } = this.session;
      let response = await fetch(url, {
        method: 'POST',
        headers,
        body: JSON.stringify({ 'max_id': maxId }),
      });

      let json = await response.json();
      let status = this.error.detectErrantSuccessCode(response.status, json);

      if (status === 200) {
        this.feeds.notifications.markAllAsRead();
      }
    } catch(e) {
      debug(e);
    }
  }
}

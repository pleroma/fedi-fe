import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { registerDisposable, runDisposables } from 'ember-lifeline';
import { getOwner } from '@ember/application';

export default class HashtagRoute extends Route {
  @service() intl;

  @service() feeds;

  async model({ hashtag }, transition) {
    let previousModel = this.modelFor(this.routeName);

    let factory = getOwner(this).factoryFor('feed:hashtag');
    let tmpFeed = factory.create({ hashtag });
    let { id: feedId } = tmpFeed;

    if (transition.from && previousModel && (previousModel.feed.id !== feedId)) {
      // Unsubscribe from previousFeed when navigating between direct messages.
      this.feeds.unsubscribe(previousModel.feed.id);
    }

    let feed = this.feeds.registerFeed(tmpFeed);

    try {
      await this.feeds.subscribe(feedId);

      registerDisposable(this, () => {
        this.feeds.unsubscribe(feedId);
      });
    } catch (_) {
      feed = {};
    }

    return {
      feed,
      hashtag,
    }
  }

  deactivate() {
    runDisposables(this);
  }

  titleToken(model) {
    return this.intl.t('tagWithHash', { name: model.hashtag });
  }
}

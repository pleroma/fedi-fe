import { helper } from '@ember/component/helper';
import scrollToTop from 'pleroma-pwa/utils/scroll-to-top';

export default helper(function scrollToTopHelper() {
  return function() { scrollToTop(); };
});

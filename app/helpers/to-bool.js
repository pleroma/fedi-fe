import { helper } from '@ember/component/helper';

export default helper(function toBool([value]) {
  return !!value;
});

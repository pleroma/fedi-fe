import Helper from '@ember/component/helper';
import { getOwner } from '@ember/application';

export default Helper.extend({
  compute([serviceName]) {
    return getOwner(this).lookup(`service:${serviceName}`);
  },
});

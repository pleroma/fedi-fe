import { helper } from '@ember/component/helper';
import { assert } from '@ember/debug';
import config from 'pleroma-pwa/config/environment';

export default helper(function stopPropagation([handler]) {
  return function(event) {
    assert(
      `Expected '${event}' to be an Event and have a 'stopPropagation' method.`,
      event && typeof event.stopPropagation === 'function',
    );

    event.stopPropagation();

    if (config.environment === 'test') { event.propagationStopped = true; }

    if (handler) {
      return handler(event);
    }

    return event;
  };
});

import Helper from '@ember/component/helper';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';

export default Helper.extend({
  router: service(),

  compute() {
    let { router } = this;

    assert('Unable to lookup router', router);

    return function() {
      router.transitionTo(...arguments);
    };
  },
});

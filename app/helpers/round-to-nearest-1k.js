import { helper } from '@ember/component/helper';

export default helper(function roundToNearest1k(number) {
  if (number < 1000) {
    return number;
  } else {
    return `${Math.floor(number / 100) / 10}k`;
  }
});

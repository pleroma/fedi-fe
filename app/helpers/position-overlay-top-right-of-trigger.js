import { helper } from '@ember/component/helper';
import overTheTopRightOfTrigger from 'pleroma-pwa/overlay-positions/over-the-top-right-of-trigger';

export default helper(function positionOverlayTopRightOfTrigger([triggerSelector]) {
  if (triggerSelector) {
    return (trigger, content) => overTheTopRightOfTrigger(trigger.querySelector(triggerSelector), content);
  }

  return overTheTopRightOfTrigger;
});

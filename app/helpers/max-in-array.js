import { helper } from '@ember/component/helper';

export default helper(function maxInArray([arrayOfNumbers]) {
  return Math.max(...arrayOfNumbers);
});

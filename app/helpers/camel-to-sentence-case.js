import { helper } from '@ember/component/helper';

/**
 * Will accept a camelCase string and return a Sentence Case string.
 */
export default helper(function camelToSentenceCase([text = '']) {
  let withSpaces = text.replace( /([A-Z])/g, ' $1');
  let sentence = withSpaces.charAt(0).toUpperCase() + withSpaces.slice(1);
  return sentence;
});

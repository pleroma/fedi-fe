import { helper } from '@ember/component/helper';

export default helper(function windowOpenHelper(params/* , hash */) {
  return function() {
    window.open(...params);
  };
});

import { helper } from '@ember/component/helper';

// ----
// Supports both {{truncate <string> <chars>}} and {{map (truncate <chars>) (map-by <name> <array>)}}
// ----

function truncateString(string, chars) {
  if (string.length > chars) {
    return `${string.substring(0, chars)}...`;
  }

  return string;
}

export default helper(function truncate([string, chars]) {
  // If only one argument passed, curry from the outside in
  if (!chars && string) {
    return function(innerString) {
      return truncateString(innerString, string);
    }
  }

  return truncateString(string, chars);
});

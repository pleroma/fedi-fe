import { helper } from '@ember/component/helper';
import { default as unesc } from 'lodash.unescape';

export function unescape([html]) {
  return unesc(html);
}

export default helper(unescape);

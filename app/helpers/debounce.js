import { helper } from '@ember/component/helper';
import { assert } from '@ember/debug';
import { debounce } from '@ember/runloop';

export default helper(function debounceHelper([callback]) {
  assert(
    'Expected a callback to be passed to the debounce helper',
    callback,
  );

  return function(...args) {
    debounce(null, callback, ...args, 300);
  };
});

import { helper } from '@ember/component/helper';

export default helper(function preventDefault([eventHandler]) {
  return function(event) {
    event.preventDefault();
    // TODO: Remove this stop-propagation, and use the helper instead
    event.stopPropagation();

    if (eventHandler) {
      return eventHandler(event);
    }

    return event;
  };
});

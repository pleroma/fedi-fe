import Helper from '@ember/component/helper';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';
import { resolve } from 'rsvp';

export default Helper.extend({
  intl: service(),
  toast: service(),

  compute(messageKey) {
    let { intl, toast } = this;

    assert('Unable to lookup intl service', intl);
    assert('Unable to lookup toast service', toast);

    return function() {
      toast.notify({ message: intl.t(messageKey) });
      return resolve(...arguments);
    };
  },
});

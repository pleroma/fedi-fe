import { helper } from '@ember/component/helper';
import { A as emberArray } from '@ember/array';

export default helper(function emberObjectHelper() {
  return emberArray(...arguments);
});

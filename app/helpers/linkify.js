import { inject as service } from '@ember/service';
import Helper from '@ember/component/helper';
import { matchesMention } from 'pleroma-pwa/utils/mention-matcher';

const parser = new DOMParser();

export default Helper.extend({
  pleromaApi: service(),

  compute([html, mentions = []]) {
    let { apiBaseUrl } = this.pleromaApi;
    let { location: { origin } } = window;

    let doc = parser.parseFromString(html, 'text/html');
    let links = doc.getElementsByTagName('a');

    if (links.length === 0) {
      return html;
    }

    for (let link of links) {
      if (link.href.startsWith(`${apiBaseUrl}/users/`)) {
        // handle mentions
        link.classList.add('linkified');
        link.href = link.href.replace(`${apiBaseUrl}/users/`, '/account/');
      } else if (link.href === `${origin}/main/all`) {
        // handle old link to all feed
        link.classList.add('linkified');
        link.href = '/feeds/all';
      } else {
        // handle remote mentions
        let mention = mentions.find(mention => matchesMention(mention, link.href));

        if (mention) {
          link.classList.add('linkified');
          link.href = `/account/${mention.id}`;
        }
      }
    }

    let body = doc.getElementsByTagName('body').item(0);
    return body.innerHTML;
  },
});

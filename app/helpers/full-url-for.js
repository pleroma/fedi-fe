import Helper from '@ember/component/helper';
import { inject as service } from '@ember/service';

export default Helper.extend({
  pleromaApi: service(),

  router: service(),

  compute([routeName, model]) {
    return new URL(this.router.urlFor(routeName, model), this.pleromaApi.apiBaseUrl);
  },
});

import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

const matchOperatorsRegex = /[|\\{}()[\]^$+*?.-]/g;

export function addEmojis(string, emojis = []) {
  return htmlSafe(emojis.reduce((acc, emoji) => {
    let regexSafeShortCode = emoji.shortcode.replace(matchOperatorsRegex, '\\$&')
    return acc.replace(
      new RegExp(`:${regexSafeShortCode}:`, 'g'),
      `<img src='${emoji.url}' alt=':${emoji.shortcode}:' title=':${emoji.shortcode}:' class='emoji' />`,
    )
  }, string));
}

export function removeEmojis(string, emojis = []) {
  return emojis.reduce((acc, emoji) => {
    let regexSafeShortCode = emoji.shortcode.replace(matchOperatorsRegex, '\\$&')
    return acc.replace(
      new RegExp(`:${regexSafeShortCode}:`, 'g'),
      '',
    )
  }, string).trim();
}

export function emojify([string, emojis = []]) {
  return addEmojis(string, emojis || []);
}

export default helper(emojify);

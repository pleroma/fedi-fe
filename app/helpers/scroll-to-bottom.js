import { helper } from '@ember/component/helper';
import { next } from '@ember/runloop';
import scrollToBottom from 'pleroma-pwa/utils/scroll-to-bottom';

export default helper(function scrollToBottomHelper([targetSelector]) {
  return function() {
    let targetElement;

    if (typeof targetSelector === 'string') {
      targetElement = targetSelector ? document.querySelector(targetSelector) : window;
    } else {
      targetElement = targetSelector;
    }

    next(() => scrollToBottom(targetElement));
  };
});

import { helper } from '@ember/component/helper';

export default helper(function focus() {
  return function(element) {
    return element?.focus();
  }
});

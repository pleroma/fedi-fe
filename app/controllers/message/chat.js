import Controller from '@ember/controller';

export default class MessageChatController extends Controller {
  queryParams = [
    {
      links: {
        type: 'array',
      },
    },
  ];

  links = null;
}

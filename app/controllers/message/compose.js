import Controller from '@ember/controller';

export default class MessageComposeController extends Controller {
  queryParams = [
    {
      participants: {
        type: 'array',
      },
    },
    {
      links: {
        type: 'array',
      },
    },
  ];

  participants = [];

  links = null;
}

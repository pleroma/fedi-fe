import Controller from '@ember/controller';

export default class MessageDetailsController extends Controller {
  queryParams = [
    {
      links: {
        type: 'array',
      },
    },
  ];

  links = null;
}

import Controller from '@ember/controller';

export default class ApplicationController extends Controller {
  queryParams = [
    'auth',
    { gallerySubject: 'gallery-subject' },
    { galleryAttachment: 'gallery-attachment' },
    { gallerySubjectType: 'gallery-subject-type' },
    { inReplyToStatusId: 'in-reply-to' },
    { compose: 'compose' },
    { directMode: 'directMode' },
    {
      initialMentions: {
        as: 'initial-mentions',
        type: 'array',
      },
    },
    {
      initialLinks: {
        as: 'initial-links',
        type: 'array',
      },
    },
    {
      initialParticipants: {
        as: 'initial-recipients',
        type: 'array',
      },
    },
  ];

  auth = null;

  gallerySubject = null;

  galleryAttachment = null;

  gallerySubjectType = null;

  inReplyToStatusId = null;

  initialMentions = null;

  initialParticipants = null;

  initialLinks = null;

  compose = null;

  directMode = null;
}

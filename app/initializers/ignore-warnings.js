import { registerWarnHandler } from '@ember/debug';

const IGNORED_WARNINGS = [
  // Ignore the sync-relationship known-to-be-empty.
  // This is thrown when a payload includes a ember-data-"link" but the relationship is marked as sync.
  // https://github.com/embermap/ember-data-storefront/issues/24
  'ds.store.push-link-for-sync-relationship',
];

export default {
  name: 'ignore-warnings',
  initialize() {
    registerWarnHandler(function(message, options, next) {
      if (!ignoreWarning(options)) {
        next(...arguments);
      }
    });
  },
};

function ignoreWarning(options) {
  return options && options.id && IGNORED_WARNINGS.includes(options.id);
}

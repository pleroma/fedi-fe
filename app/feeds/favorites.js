import BaseFeed from 'pleroma-pwa/feeds/base';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';
import uniqueArrayBy from 'pleroma-pwa/utils/unique-array-by';

export default class FavoritesFeed extends BaseFeed {
  @service pleromaApi;

  @service session;

  userId = null;

  constructor(...args) {
    super(...args);
  }

  get id() {
    assert('You must pass a userId to this feeds constructor', this.userId);
    return `favorites-${this.userId}`;
  }

  get url() {
    assert('You must pass a userId to this feeds constructor', this.userId);

    if (this.session?.currentUser?.id === this.userId) {
      return this.pleromaApi.endpoints.currentUsersFavorites;
    }

    return this.pleromaApi.endpoints.accountFavorites(this.userId);
  }

  modelName = 'status';

  authenticated = false;

  hidesMutedStatuses = true;

  addNewItems(items) {
    if (this.enqueNewItems) {
      // TODO: Delete this when Pleroma starts returning actual new items
      let actualNewItems = uniqueArrayBy(this.content, items, 'id');

      this.newItems.pushObjectsUnique(actualNewItems);
    } else {
      this.content.pushObjectsUnique(items);
    }
  }
}

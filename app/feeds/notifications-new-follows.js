import NotificationsFeed from 'pleroma-pwa/feeds/notifications';

export default class NotificationsNewFollowsFeed extends NotificationsFeed {
  id = 'notifications-new-follows';

  get name() {
    return this.intl.t('newFollows');
  }

  get params()  {
    let params = {
      includeTypes: [
        'follow',
      ],
    };

    return params;
  }

  shouldShow = [
    'follow',
  ];

  collectUnreadCount () {
  }
}

import NotificationsFeed from 'pleroma-pwa/feeds/notifications';

export default class NotificationsMentionsFeed extends NotificationsFeed {
  id = 'notifications-mentions';

  get name() {
    return this.intl.t('mentions');
  }

  get params()  {
    let params = {
      includeTypes: [
        'mention',
      ],
    };

    return params;
  }

  shouldShow = [
    'mention',
  ];

  collectUnreadCount () {
  }
}

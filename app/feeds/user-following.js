import BaseFeed from 'pleroma-pwa/feeds/base';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';

export default class UserFollowingFeed extends BaseFeed {
  @service() pleromaApi;

  constructor(...args) {
    super(...args);
  }

  modelName = 'user';

  userId = null;

  static feedId(userId) {
    return `userFollowingFeed${userId}`
  }

  get id() {
    assert('You must pass a userId to this feeds constructor', this.userId);
    return this.constructor.feedId(this.userId);
  }

  get url() {
    assert('You must pass a userId to this feeds constructor', this.userId);
    return this.pleromaApi.endpoints.accountFollowing(this.userId);
  }
}

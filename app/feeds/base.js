import EmberObject, { set } from '@ember/object';
import { or } from '@ember/object/computed';
import Poller from 'pleroma-pwa/utils/poller';
import ArrayProxy from '@ember/array/proxy';
import Evented from '@ember/object/evented';
import { A } from '@ember/array';
import { inject as service } from '@ember/service';
import { restartableTask, dropTask, enqueueTask } from 'ember-concurrency-decorators';
import config from 'pleroma-pwa/config/environment';
import uniqueArrayBy from 'pleroma-pwa/utils/unique-array-by';
import { assert } from '@ember/debug';
import fetch from 'fetch';
import { decamelize } from '@ember/string';
import { typeOf } from '@ember/utils';

function pushObjectsUnique(items) {
  this.pushObjects(uniqueArrayBy(this.content, items, 'id'));
}

function unshiftObjectsUnique(items) {
  this.unshiftObjects(uniqueArrayBy(this.content, items, 'id'));
}

export default class BaseFeed extends EmberObject.extend(Evented) {
  @service feeds;

  @service session;

  @service store;

  @service error;

  pageSize = config.APP.FEED_PAGE_SIZE;

  content = ArrayProxy.create({ content: A(), pushObjectsUnique, unshiftObjectsUnique });

  newItems = ArrayProxy.create({ content: A(), pushObjectsUnique, unshiftObjectsUnique });

  hidesMutedStatuses = false;

  subscribedTo = false;

  canLoadMore = true;

  enqueNewItems = true;

  addItems(items) {
    this.content.unshiftObjectsUnique(items);
  }

  removeItem(item) {
    this.content.removeObject(item);
  }

  syncNewItems() {
    this.addItems(this.newItems.content);
    this.newItems.clear();
  }

  @restartableTask*
  collectFeedMeta() {}

  longPoller = new Poller(() => { this.loadNew.perform(); });

  @or('loadNew.isRunning', 'loadMore.isRunning') isLoading; // TODO: use concurrency groups to express inter-task concurrency

  @dropTask*
  subscribe() {
    // Kick um outa here!
    if (this.authenticated === true && !this.session.isAuthenticated) {
      return this;
    }

    // Only block the first time
    if (!this.content.length && this.canLoadMore) {
      yield this.loadMoreAndSubscribe();
    } else {
      if (!this.canLoadMore) {
        // Give the feed a chance to redeem itself if it failed originally.
        set(this, 'canLoadMore', true);
      }

      this.loadMoreAndSubscribe();
    }

    return yield this;
  }

  loadMoreAndSubscribe() {
    // Ensure we don't fetch older posts if the feed is already loaded.
    if (this.content.length > 0) {
      this.startPollerAndSubscribe();
    } else {
      return this.loadMore.perform().then(() => {
        // Only register long poller if no error.
        this.startPollerAndSubscribe();
      }).catch(() => {
        // Mark feed as not able to load more if there was a load error.
        set(this, 'canLoadMore', false);
      });
    }
  }

  startPollerAndSubscribe() {
    this.longPoller.start();

    // Only mark as subscribedTo if all setup is done.
    set(this, 'subscribedTo', true);
  }

  @dropTask*
  unsubscribe() {
    this.longPoller.stop();

    set(this, 'subscribedTo', false);

    return yield this;
  }

  async reset() {
    this.content.clear();
    this.newItems.clear();

    set(this, 'subscribedTo', false);
    set(this, 'canLoadMore', true);
    set(this, 'enqueNewItems', true);
  }

  getMostRecentItem() {
    return this.newItems.firstObject || this.content.firstObject;
  }

  addNewItems(items) {
    if (this.enqueNewItems) {
      this.newItems.unshiftObjectsUnique(items);
    } else {
      this.content.unshiftObjectsUnique(items);
    }
  }

  get url() { return assert('You must define a url method when creating a feed', false); }

  @restartableTask*
  loadNew() {
    if (this.authenticated === true && !this.session.isAuthenticated) {
      return [];
    }

    if (this.loadMore.isRunning) { // TODO, use task groups to ensure inter-task concurrency
      return;
    }

    let { headers } = this.session;

    let url = this.loadNewUrl;

    let response = yield fetch(url, { headers });

    let results = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, results);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(results);
    }

    let records = this.store.normalizeAndStore(this.modelName, results);

    if (records.length) {
      this.addNewItems(records);
    }

    this.trigger('feed:loaded-new', records);

    return this;
  }

  // Returns URL object
  get loadNewUrl() {
    let mostRecentItem = this.getMostRecentItem();

    let url = new URL(this.url);

    let query = {
      limit: this.pageSize,
      // eslint-disable-next-line camelcase
      ...mostRecentItem && { min_id: mostRecentItem.id },
      ...this.params,
    };

    Object.keys(query).forEach(key => {
      // Pleroma expects arrays in urls to be formatted PHP style, ?prop[]=one&prop[]=two
      if (typeOf(query[key]) === 'array') {
        let arrayKey = `${decamelize(key)}[]`;

        query[key].forEach(param => url.searchParams.append(arrayKey, param));
      } else {
        url.searchParams.append(decamelize(key), query[key]);
      }
    });

    return url;
  }

  getOldestItem() {
    return this.content.lastObject;
  }

  addMoreItems(items) {
    this.content.pushObjectsUnique(items);
  }

  @enqueueTask*
  loadMore() {
    if (this.authenticated === true && !this.session.isAuthenticated) {
      return;
    }

    if (!this.canLoadMore || this.loadNew.isRunning) { // TODO: use task groups to ensure inter-task concurrency
      return;
    }

    let { headers } = this.session;

    let url = this.loadMoreUrl;

    let response = yield fetch(url, { headers });

    let results = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, results);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(results);
    }

    let records = this.store.normalizeAndStore(this.modelName, results);

    if (records.length) {
      this.addMoreItems(records);
    }

    set(this, 'canLoadMore', records.length !== 0);

    this.trigger('feed:loaded-more', records);
  }

  // Returns URL object
  get loadMoreUrl() {
    let url = new URL(this.url);

    let oldestItem = this.getOldestItem();

    let query = {
      limit: this.pageSize,
      // eslint-disable-next-line camelcase
      ...oldestItem && { max_id: oldestItem.id },
      ...this.params,
    };

    Object.keys(query).forEach(key => {
      // Pleroma expects arrays in urls to be formatted PHP style, ?prop[]=one&prop[]=two
      if (typeOf(query[key]) === 'array') {
        let arrayKey = `${decamelize(key)}[]`;

        query[key].forEach(param => url.searchParams.append(arrayKey, param));
      } else {
        url.searchParams.append(decamelize(key), query[key]);
      }
    });

    return url;
  }
}

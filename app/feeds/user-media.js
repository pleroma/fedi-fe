import BaseFeed from 'pleroma-pwa/feeds/base';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';

export default class AccountMediaFeed extends BaseFeed {
  @service() pleromaApi;

  constructor(...args) {
    super(...args);
  }

  modelName = 'status';

  userId = null;

  get id() {
    assert('You must pass a userId to this feeds constructor', this.userId);
    return `userMediaFeed${this.userId}`;
  }

  get url() {
    assert('You must pass a userId to this feeds constructor', this.userId);
    return this.pleromaApi.endpoints.accountMedia(this.userId);
  }
}

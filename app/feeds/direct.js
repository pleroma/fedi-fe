import BaseFeed from 'pleroma-pwa/feeds/base';
import { inject as service } from '@ember/service';

export default class DirectFeed extends BaseFeed {
  @service features;

  @service intl;

  @service pleromaApi;

  id = 'direct';

  constructor(...args) {
    super(...args);
  }

  get name() {
    return this.intl.t('directMessages');
  }

  modelName = 'status';

  params = {
    withMuted: true,
  };

  authenticated = true;

  url = this.pleromaApi.endpoints.timelineDirect;
}

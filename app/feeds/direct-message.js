import BaseFeed from 'pleroma-pwa/feeds/base';
import uniqueArrayBy from 'pleroma-pwa/utils/unique-array-by';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';

export default class DirectMessageFeed extends BaseFeed {
  @service() pleromaApi;

  modelName = 'status';

  authenticated = true;

  params = {};

  conversationId = null;

  getMostRecentItem() {
    return this.newItems.lastObject || this.content.lastObject;
  }

  getOldestItem() {
    return this.content.firstObject;
  }

  addItems(items) {
    this.content.pushObjectsUnique(items);
  }

  addNewItems(items) {
    if (this.enqueNewItems) {
      // TODO: Delete this when Pleroma starts returning actual new items
      let actualNewItems = uniqueArrayBy(this.content, items, 'id');

      this.newItems.pushObjectsUnique(actualNewItems);
    } else {
      this.content.pushObjectsUnique(items);
    }
  }

  addMoreItems(items) {
    this.content.unshiftObjectsUnique(items);
  }

  get id() {
    assert('You must pass a conversationId to this feeds constructor', this.conversationId);
    return `conversationFeed${this.conversationId}`;
  }

  get url() {
    assert('You must pass a conversationID to this feeds constructor', this.conversationId);
    return this.pleromaApi.endpoints.directMessageFeed(this.conversationId);
  }
}

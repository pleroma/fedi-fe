import NotificationsFeed from 'pleroma-pwa/feeds/notifications';

export default class NotificationsFavoriteFeed extends NotificationsFeed {
  id = 'notifications-favorites';

  get name() {
    return this.intl.t('favorites');
  }

  get params()  {
    let params = {
      includeTypes: [
        'favourite',
      ],
    };

    return params;
  }

  shouldShow = [
    'favourite',
  ];

  collectUnreadCount () {
  }
}

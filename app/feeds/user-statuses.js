import BaseFeed from 'pleroma-pwa/feeds/base';
import { inject as service } from '@ember/service';
import { assert } from '@ember/debug';

export default class UserStatusesFeed extends BaseFeed {
  @service() pleromaApi;

  constructor(...args) {
    super(...args);
  }

  userId = null;

  modelName = 'status';

  get id() {
    assert('You must pass a userId to this feeds constructor', this.userId);
    return `userStatusesFeed${this.userId}`;
  }

  get url() {
    assert('You must pass a userId to this feeds constructor', this.userId);
    return this.pleromaApi.endpoints.accountStatuses(this.userId);
  }
}

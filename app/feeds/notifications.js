import BaseFeed from 'pleroma-pwa/feeds/base';
import { inject as service } from '@ember/service';
import { set } from '@ember/object';
import { run } from '@ember/runloop';
import { isEmpty } from '@ember/utils';
import { all, resolve } from 'rsvp';

export default class NotificationsFeed extends BaseFeed {
  @service features;

  @service intl;

  @service pleromaApi;

  @service unreadCounts;

  id = 'notifications';

  get name() { return this.intl.t('notifications'); }

  get params()  {
    let params = {
      includeTypes: [
        'follow',
        'mention',
        'reblog',
        'favourite',
        'poll',
        ...(this.features.isEnabled('directMessages') ? ['pleroma:chat_mention'] : []),
      ],
    };

    return params;
  }

  shouldShow = [
    'follow',
    'mention',
    'reblog',
    'favourite',
    'poll',
  ];

  shouldSilenceDirectNotifications = true;

  modelName = 'notification';

  authenticated = true;

  url = this.pleromaApi.endpoints.notifications;

  constructor() {
    super(...arguments);

    this.on('feed:loaded-more', () => {
      run(() => {
        return this.silenceAllConfigured().then(() => {
          return this.collectUnreadCount();
        });
      });
    });

    this.on('feed:loaded-new', () => {
      run(() => {
        return this.silenceAllConfigured().then(() => {
          return this.collectUnreadCount();
        });
      });
    });
  }

  markAllAsRead() {
    let allItems = [...this.content.content, ...this.newItems.content];

    allItems.forEach((item) => {
      set(item.pleroma, 'isSeen', true);
      item.pleroma.save({adapterOptions: {dontPersist: true}});
    });

    this.unreadCounts.setUnreadNotificationsCount(0);
  }

  getMaxId() {
    let allItems = [...this.content.content, ...this.newItems.content];
    let maxId = 0;

    allItems.forEach((item) => {
      maxId = Math.max(maxId, item.id);
    });

    return maxId;
  }

  collectUnreadCount() {
    let allItems = [...this.content.content, ...this.newItems.content];

    let unreadNotificationsCount = allItems.reduce((memo, notification) => {
      if (notification?.pleroma?.isSeen) {
        return memo;
      }

      return memo + 1;
    }, 0);

    this.unreadCounts.setUnreadNotificationsCount(unreadNotificationsCount);

    return this.unreadCounts.unreadNotificationsCount;
  }

  silenceAllConfigured() {
    return all([
      this.silenceDirectNotifications(),
    ]);
  }

  silenceDirectNotifications() {
    if (!this.shouldSilenceDirectNotifications) {
      return resolve();
    }

    let allItems = [...this.content.content, ...this.newItems.content];

    let allMatched = allItems.filter(notification => {
      return notification.type === 'mention' &&
        notification.status?.visibility === 'direct' &&
        !notification.pleroma.isSeen;
    });

    if (isEmpty(allMatched)) {
      return resolve();
    }

    return all(allMatched.map(notification => {
      return notification.markAsRead();
    }));
  }
}

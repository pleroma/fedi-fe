import BaseFeed from 'pleroma-pwa/feeds/base';
import { inject as service } from '@ember/service';

export default class HomeFeed extends BaseFeed {
  @service intl;

  @service pleromaApi;

  id = 'home';

  constructor(...args) {
    super(...args);
  }

  get name() { return this.intl.t('myFeed'); }

  params = {
    withMuted: false,
    excludeVisibilities: ['direct'],
  };

  modelName = 'status';

  authenticated = true;

  hidesMutedStatuses = true;

  url = this.pleromaApi.endpoints.timelineHome;
}

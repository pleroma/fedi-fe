import BaseFeed from 'pleroma-pwa/feeds/base';
import { set } from '@ember/object';
import { inject as service } from '@ember/service';
import { run } from '@ember/runloop';

export default class ChatsFeed extends BaseFeed {
  @service features;

  @service intl;

  @service pleromaApi;

  @service unreadCounts;

  id = 'chats';

  get name() {
    if (this.features.isEnabled('showDirectMessagesClassic')) {
      return this.intl.t('chats');
    }

    return this.intl.t('directMessages');
  }

  params = {};

  modelName = 'chat';

  authenticated = true;

  enqueNewItems = false;

  url = this.pleromaApi.endpoints.chats;

  constructor() {
    super(...arguments);

    this.on('feed:loaded-more', () => {
      run(() => {
        set(this, 'canLoadMore', false);
        return this.collectUnreadCount();
      });
    });

    this.on('feed:loaded-new', () => {
      run(() => {
        return this.collectUnreadCount();
      });
    });
  }

  collectUnreadCount() {
    let unreadChats = this.content.reduce((memo, chat) => {
      return memo + (chat?.unread || 0);
    }, 0);

    this.unreadCounts.setUnreadChatCount(unreadChats);

    return this.unreadCounts.unreadChatCount;
  }

  async reset() {
    await super.reset(...arguments);
    set(this, 'enqueNewItems', false);
  }

  get loadMoreUrl() {
    return this.url;
  }

  get loadNewUrl() {
    return this.url;
  }
}

import BaseFeed from 'pleroma-pwa/feeds/base';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';

export default class HashtagFeed extends BaseFeed {
  @service() pleromaApi;

  modelName = 'status';

  authenticated = false;

  params = {};

  hashtag = null;

  get id() {
    assert('You must pass a hashtag to this feeds constructor', this.hashtag);
    return `hashtagFeed${this.hashtag}`;
  }

  get url() {
    assert('You must pass a hashtag to this feeds constructor', this.hashtag);
    return this.pleromaApi.endpoints.hashtagFeed(this.hashtag);
  }
}

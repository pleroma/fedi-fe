import BaseFeed from 'pleroma-pwa/feeds/base';
import { inject as service } from '@ember/service';

export default class PublicFeed extends BaseFeed {
  @service pleromaApi;

  @service instances;

  id = 'public';

  constructor(...args) {
    super(...args);
  }

  get name() {
    return this.instances.current.title;
  }

  params = {
    withMuted: true,
    local: true,
  };

  modelName = 'status';

  authenticated = false;

  url = this.pleromaApi.endpoints.timelineAll;
}

import BaseFeed from 'pleroma-pwa/feeds/base';
import { inject as service } from '@ember/service';

export default class AllFeed extends BaseFeed {
  @service pleromaApi;

  @service intl;

  id = 'all';

  constructor(...args) {
    super(...args);
  }

  get name() { return this.intl.t('wholeKnownNetwork'); }

  params = {
    withMuted: true,
  };

  modelName = 'status';

  authenticated = false;

  url = this.pleromaApi.endpoints.timelineAll;
}

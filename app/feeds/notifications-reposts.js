import NotificationsFeed from 'pleroma-pwa/feeds/notifications';

export default class NotificationsRepostsFeed extends NotificationsFeed {
  id = 'notifications-reposts';

  get name() {
    return this.intl.t('reposts');
  }

  get params()  {
    let params = {
      includeTypes: [
        'reblog',
      ],
    };

    return params;
  }

  shouldShow = [
    'reblog',
  ];

  collectUnreadCount () {
  }
}

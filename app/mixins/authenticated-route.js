import Mixin from '@ember/object/mixin';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import config from 'pleroma-pwa/config/environment';

// eslint-disable-next-line ember/no-new-mixins
export default Mixin.create(AuthenticatedRouteMixin, {
  authModal: service(),
  session: service(),
  intl: service(),

  ...config.APP.emberSimpleAuthConfig,

  triggerAuthentication() {
    this.session.attemptedTransition.abort();

    let reasonKey = this.reasonForAuthenticationKey || 'youMustSignInToSeeThatPage';

    // is very first visited page, nowhere to render the signin modal, take um to the sign-in route.
    if (this.session.attemptedTransition.sequence === 0) {
      this.session.setReasonForAuthentication(reasonKey);
      this.replaceWith('sign-in');
    } else {
      this.session.setReasonForAuthentication(reasonKey);
      this.authModal.showSignInModal();
    }
  },
});

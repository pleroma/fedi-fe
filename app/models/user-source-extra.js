import Model, { attr } from '@ember-data/model';

// https://docs.joinmastodon.org/entities/source/
export default Model.extend({
  actorType: attr('string'),
  discoverable: attr('boolean'),
  noRichText: attr('boolean'),
  showRole: attr('boolean'),
});

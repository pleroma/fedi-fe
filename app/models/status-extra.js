import Model, { attr, hasMany } from '@ember-data/model';

// https://docs-develop.pleroma.social/backend/API/differences_in_mastoapi_responses/#statuses
export default class StatusExtraModel extends Model {
  @attr('boolean') local;

  @attr('string') conversationId;

  @attr('string') inReplyToAccountAcct;

  @attr('boolean') threadMuted;

  @attr('string') directConversationId;

  @attr('object') content;

  @hasMany('emoji-reaction', {
    async: false,
    inverse: null,
  }) emojiReactions;
}

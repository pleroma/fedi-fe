import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
  accessToken: attr('string'),
  createdAt: attr('number'),
  expiresIn: attr('number'),
  refreshToken: attr('string'),
  scope: attr('string'),
  tokenType: attr('string'),
});

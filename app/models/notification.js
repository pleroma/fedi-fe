import Model, { belongsTo, attr } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';
import { computed } from '@ember/object';
import { equal } from '@ember/object/computed';
import { getOwner } from '@ember/application';
import { inject as concern } from 'ember-concerns';
import { inject as service } from '@ember/service';

export default Model.extend(LoadableModel, {
  type: attr('string'),
  createdAt: attr('date'),

  status: belongsTo('status', {
    async: false,
    inverse: null,
  }),

  account: belongsTo('user', {
    async: false,
    inverse: null,
  }),

  chatMessage: belongsTo('chat-message', {
    async: false,
    inverse: null,
  }),

  pleroma: belongsTo('notification-extra', {
    async: false,
    inverse: null,
  }),

  isRepost: equal('type', 'reblog'),
  isFavorite: equal('type', 'favourite'),
  isFollow: equal('type', 'follow'),
  isMention: equal('type', 'mention'),
  isChatMention: equal('type', 'pleroma:chat_mention'),

  itemable: concern(),

  shouldShow: computed('type', function() {
    let feed = this.feeds.getFeed('notifications');
    return feed.shouldShow.includes(this.type) &&
      !(this.type === 'mention' && this.status?.visibility === 'direct');
  }),

  markAsRead(response) {
    let store = getOwner(this).lookup('service:store');
    let feedsService = getOwner(this).lookup('service:feeds');
    let unreadCounts = getOwner(this).lookup('service:unread-counts');

    let normalized = store.serializerFor('notification').normalizeSingleResponse(
      store,
      'notification',
      response,
      `${response?.id || '(unknown)'}`,
      'findRecord',
    );

    feedsService.refreshFeedMeta('notifications');

    unreadCounts.decrementNotificationsCount();

    return store.push(normalized);
  },

  deleteNotification() {
    let feedsService = getOwner(this).lookup('service:feeds');
    let unreadCounts = getOwner(this).lookup('service:unread-counts');

    feedsService.removeItemFromAllFeeds(this);
    feedsService.refreshFeedMeta('notifications');
    unreadCounts.decrementNotificationsCount();
  },

  feeds: service(),
});

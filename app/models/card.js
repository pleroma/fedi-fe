import Model, { attr } from '@ember-data/model';

export default class CardModel extends Model {
  @attr('string') image;

  @attr('string') url;

  @attr('string') title;

  @attr('string') description;

  @attr('string') type;

  @attr('string') providerName;

  @attr('string') providerUrl;
}

import Model, { attr } from '@ember-data/model';

export default Model.extend({
  isSeen: attr('boolean'),
});

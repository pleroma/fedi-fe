import Model, { attr, belongsTo } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';
import { inject as concern } from 'ember-concerns';
import { computed } from '@ember/object';

export default Model.extend(LoadableModel, {
  type: attr('string'),
  url: attr('string'),
  previewUrl: attr('string'),
  remoteUrl: attr('string'),
  textUrl: attr('string'),
  description: attr('string'),
  blurhash: attr('string'),
  meta: attr('object'),

  // Uploads won't work without this being specified.
  // eslint-disable-next-line ember/no-empty-attrs
  file: attr(),

  pleroma: belongsTo('attachment-extra', {
    async: false,
    inverse: null,
  }),

  uploader: concern('attachment/uploader'),

  hasOriginalUrl: computed('previewUrl', 'url', function() {
    return this.previewUrl !== this.url;
  }),

  dimensions: computed('meta', function() {
    let { meta } = this;
    if (meta.small) return meta.small
    if (meta.original) return meta.original
    return {}
  }),
});
// type	unknown, image, gifv, video, Meta
// remote_url	String (URL)		0.6.0
// preview_url	String (URL)		0.6.0
// text_url	String (URL)		0.6.0
// meta	Hash		1.5.0
// description

// May contain subtrees small and original.
// Images may contain width, height, size, aspect, while videos (including GIFV) may contain width, height, frame_rate, duration and bitrate.
// There may be another top-level object, focus with the coordinates x and y. These coordinates can be used for smart thumbnail cropping, see this for reference.

import Model, { attr, belongsTo } from '@ember-data/model';

export default Model.extend({
  appName: attr('string'),
  validUntil: attr('date'),

  // START for testing only --
  username: attr('string'),
  token: attr('string'),
  email: attr('string'),
  // END for testing only ----

  user: belongsTo('user', { async: false }),
});

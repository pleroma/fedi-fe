import { computed } from '@ember/object';
import Model, { attr } from '@ember-data/model';

export default Model.extend({
  mimeType: attr('string'),

  playable: computed('mime-type', function() {
    let isImage = this.mimeType.includes('image')
    let isVideo = this.mimeType.includes('video')
    return isImage || isVideo
  }),
});

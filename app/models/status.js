import uuid from 'uuid/v4';
import config from 'pleroma-pwa/config/environment';
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';
import { computed } from '@ember/object';
import { memberAction } from 'ember-api-actions';
import { inject as concern } from 'ember-concerns';
import { inject as service } from '@ember/service';
const { APP: { CONVERSATION_MAX_DEPTH } } = config;

const afterMemberActionSerialize = function(response) {
  let serializer = this.store.serializerFor('status');
  let normalized = serializer.normalizeResponse(this.store, this.store.modelFor('status'), response, response.id, 'findRecord');

  return this.store.push(normalized);
};

export default Model.extend(LoadableModel, {
  spoilerText: attr('string'),
  content: attr('string'),
  createdAt: attr('date'),
  favourited: attr('boolean', { defaultValue() { return false; } }),
  favouritesCount: attr('number', { defaultValue() { return 0; } }),
  inReplyToAccountId: attr('string'),
  inReplyToId: attr('string'),
  reblogged: attr('boolean', { defaultValue() { return false; } }),
  reblogsCount: attr('number', { defaultValue() { return 0; } }),
  repliesCount: attr('number', { defaultValue() { return 0; } }),
  uri: attr('string'),
  visibility: attr('string', { defaultValue() { return 'public'; } }),
  to: attr('array'),
  inReplyToConversationId: attr('string'), // Will reply to a given conversation, addressing only the people who are part of the recipient set of that conversation. Sets the visibility to direct.
  sensitive: attr('boolean', { defaultValue() { return false; } }),

  // NOTE: This is needed for creating new status with media attachments.
  mediaIds: attr('array'),

  account: belongsTo('user', {
    async: false,
    inverse: null,
  }),

  card: belongsTo('card', {
    async: false,
    inverse: null,
  }),

  reblog: belongsTo('status', {
    async: false,
    inverse: null,
  }),

  emojis: hasMany('emoji', {
    async: false,
    inverse: null,
  }),

  mediaAttachments: hasMany('attachment', {
    async: false,
    inverse: null,
  }),

  mentions: hasMany('mention', {
    async: false,
    inverse: null,
  }),

  ancestors: hasMany('status', { async: false, inverse: null }),
  descendants: hasMany('status', { async: false, inverse: null }),

  pleroma: belongsTo('status-extra', {
    async: false,
    inverse: null,
  }),

  poll: belongsTo('poll', {
    async: false,
    inverse: null,
  }),

  favoritesCount: computed.reads('favouritesCount'),
  favorited: computed.reads('favourited'),
  repostsCount: computed.reads('reblogsCount'),
  reposted: computed.reads('reblogged'),
  repostOf: computed.reads('reblog'),

  isReply: computed.and('inReplyToId', 'inReplyToAccountId'),

  isDirectMessage: computed.equal('visibility', 'direct'),
  isPrivate: computed.equal('visibility', 'private'),
  isPublic: computed.equal('visibility', 'public'),
  isUnlisted: computed.equal('visibility', 'unlisted'),

  visuallyMuted: computed.or('muted', 'pleroma.threadMuted', 'account.pleroma.relationship.muting'),

  emojiState: concern('status/emoji-state'),

  itemable: concern(),

  visibilityState: concern('status/visibility'),

  conversation: computed('descendants.[]', 'ancestors.[]', function() {
    // Turn singly linked list into tree.
    let everything = [...this.ancestors.toArray(), this, ...this.descendants.toArray()];
    let obj = {};
    let rootId = null;

    everything.forEach((status) => obj[status.id] = { status, children: [], containsCurrent: false });

    everything.forEach((status) => {
      if (status.inReplyToId) {
        obj[status.inReplyToId].children.push(obj[status.id]);
      } else {
        if (rootId) {
          // Create pseudo tombstone objects and attach to them the replies to non-existing statuses.
          let tombstoneId = `${status.id}_tombstone_${uuid()}`;

          obj[tombstoneId] = {
            status,
            tombstone: true,
            children: [obj[status.id]],
            containsCurrent: false,
          }

          obj[rootId].children.push(obj[tombstoneId]);
        } else {
          rootId = status.id;
        }
      }
    });

    // grab the status-in-question and recurse up its inReplyTo's, marking them as containing the current statusDetails
    if (this.inReplyToId) {
      let isReplyOf = this.inReplyToId;
      while (isReplyOf) {
        obj[isReplyOf].containsCurrent = true;
        isReplyOf = obj[isReplyOf].status.inReplyToId;
      }
    }

    let maxLevels = CONVERSATION_MAX_DEPTH;
    let shouldTrim = everything.length > maxLevels;

    // find new rootId
    if (shouldTrim) {
      let numAdditionalLevels = maxLevels - 1;
      let currentHasChild = everything.find(status => status.inReplyToId === this.id);
      if (currentHasChild) {
        numAdditionalLevels = numAdditionalLevels - 1;
      }

      let currentStatusId = this.id;

      for (let i = 0; i < numAdditionalLevels; i++) {
        let s = obj[currentStatusId].status;
        if (s.inReplyToId) {
          currentStatusId = s.inReplyToId;
        } else {
          break;
        }
      }

      rootId = currentStatusId;
    }

    return obj[rootId];

    // [
    //   { parent: null, id: A, },
    //   { parent: A, id: B, },
    //   { parent: A, id: C, },
    //   { parent: B, id: D, },
    //   { parent: B, id: E, },
    //   { parent: D, id: F, }
    // ]
    //
    // ==>
    //
    // {
    //   id: A,
    //   children: [
    //     {
    //       id: B,
    //       children: [
    //         {
    //           id: D,
    //           children: [
    //             {
    //               id: F,
    //               children: []
    //             }
    //           ],
    //         },
    //         {
    //           id: E,
    //           children: [],
    //         },
    //       ],
    //     },
    //     {
    //       id: C,
    //       children: [],
    //     }
    //   ]
    // }
  }),

  favorite: memberAction({
    path: 'favourite',
    type: 'post',
    after: afterMemberActionSerialize,
  }),

  unfavorite: memberAction({
    path: 'unfavourite',
    type: 'post',
    after: afterMemberActionSerialize,
  }),

  repost: memberAction({
    path: 'reblog',
    type: 'post',
    after: afterMemberActionSerialize,
  }),

  unrepost: memberAction({
    path: 'unreblog',
    type: 'post',
    after: afterMemberActionSerialize,
  }),

  mute: memberAction({
    path: 'mute',
    type: 'post',
    after: afterMemberActionSerialize,
  }),

  unmute: memberAction({
    path: 'unmute',
    type: 'post',
    after: afterMemberActionSerialize,
  }),

  loadConversation: memberAction({
    path: 'context',
    type: 'get',
    after(response) {
      this.ancestors.clear();
      this.descendants.clear();

      this.ancestors.pushObjects(response.ancestors.map((record) => afterMemberActionSerialize.call(this, record)));
      this.descendants.pushObjects(response.descendants.map((record) => afterMemberActionSerialize.call(this, record)));
    },
  }),

  store: service(),
});

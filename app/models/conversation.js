import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';
import { memberAction } from 'ember-api-actions';

const afterMemberActionSerialize = function(response) {
  let serializer = this.store.serializerFor('conversation');
  let normalized = serializer.normalizeResponse(this.store, this.store.modelFor('conversation'), response, response.id, 'findRecord');

  return this.store.push(normalized);
};

export default Model.extend(LoadableModel, {
  unread: attr('boolean'),

  accounts: hasMany('user', {
    async: false,
    inverse: null,
  }),

  lastStatus: belongsTo('status', {
    async: false,
    inverse: null,
  }),

  markAsRead: memberAction({
    path: 'read',
    type: 'post',
    after: afterMemberActionSerialize,
  }),
});

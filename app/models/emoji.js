import Model, { attr } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';

export default class EmojiModel extends Model.extend(LoadableModel) {
  @attr('string') shortcode;

  @attr('string') staticUrl;

  @attr('string') url;

  @attr('boolean', {
    defaultValue: () => false,
  }) visibleInPicker;

  @attr('array', {
    defaultValue: () => [],
  }) tags;

  // TODO: Create a common interface to show emoji in picker.
}

import Model, { attr } from '@ember-data/model';

export default class HashtagModel extends Model {
  @attr('string') name;

  @attr('string') url;

  get tag() {
    return `#${this.name}`;
  }
}

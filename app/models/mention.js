import Model, { attr } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';

export default Model.extend(LoadableModel, {
  username: attr('string'),
  url: attr('string'),
  acct: attr('string'),
});

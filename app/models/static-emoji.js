import Model, { attr } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';

export default class StaticEmojiModel extends Model.extend(LoadableModel) {
  @attr('string') shortcode;

  @attr('string') value;

  @attr('boolean', {
    defaultValue: () => true,
  }) visibleInPicker;

  // TODO: Create a common interface to show emoji in picker.
}

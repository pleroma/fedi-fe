import Model, { attr, belongsTo } from '@ember-data/model';

// https://docs.joinmastodon.org/entities/relationship/
export default Model.extend({
  blockedBy: attr('boolean', { defaultValue: false }),
  blocking: attr('boolean', { defaultValue: false }),
  domainBlocking: attr('boolean', { defaultValue: false }),
  endorsed: attr('boolean', { defaultValue: false }),
  followedBy: attr('boolean', { defaultValue: false }),
  following: attr('boolean', { defaultValue: false }),
  muting: attr('boolean', { defaultValue: false }),
  mutingNotifications: attr('boolean', { defaultValue: false }),
  requested: attr('boolean', { defaultValue: false }),
  showingReblogs: attr('boolean', { defaultValue: false }),
  subscribing: attr('boolean', { defaultValue: false }),

  pleroma: belongsTo('user-extra', {
    async: false,
    inverse: 'relationship',
  }),
});

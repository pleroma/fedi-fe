import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';
import { inject as concern } from 'ember-concerns';

export default Model.extend(LoadableModel, {
  unread: attr('number', { defaultValue() { return 0; } }),

  updatedAt: attr('date'),

  lastMessage: belongsTo('chat-message', {
    async: false,
    inverse: null,
  }),

  account: belongsTo('user', {
    async: false,
    inverse: null,
  }),

  /* eslint-disable ember-data-sync-relationships/no-async-relationships */
  messages: hasMany('chat-message', {
    async: true,
    inverse: 'chat',
  }),
  /* eslint-enable ember-data-sync-relationships/no-async-relationships */

  reloadable: concern('chat/reloadable'),
});

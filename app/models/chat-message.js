import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';

export default class ChatMessageModel extends Model.extend(LoadableModel) {
  @attr('string') content;

  @attr('date') createdAt;

  @attr('boolean', {
    defaultValue() { return false; },
  }) unread;

  /* eslint-disable ember-data-sync-relationships/no-async-relationships */
  @belongsTo('user', {
    async: true,
    inverse: null,
  }) account;
  /* eslint-enable ember-data-sync-relationships/no-async-relationships */

  @belongsTo('attachment', {
    async: false,
    inverse: null,
  }) attachment;

  get mediaAttachments() { return this.attachment ? [this.attachment] : null; };

  @hasMany('emoji', {
    async: false,
    inverse: null,
  }) emojis;

  /* eslint-disable ember-data-sync-relationships/no-async-relationships */
  @belongsTo('chat', {
    async: true,
    inverse: 'messages',
  }) chat;
  /* eslint-enable ember-data-sync-relationships/no-async-relationships */
}

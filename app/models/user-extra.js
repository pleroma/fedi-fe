import Model, { attr, belongsTo } from '@ember-data/model';

// https://docs-develop.pleroma.social/backend/API/differences_in_mastoapi_responses/#accounts
export default Model.extend({
  backgroundImage: attr('string'),

  hideFavorites: attr('boolean'),

  hideFollows: attr('boolean'),
  hideFollowsCount: attr('boolean'),
  hideFollowers: attr('boolean'),
  hideFollowersCount: attr('boolean'),

  user: belongsTo('user', {
    async: false,
  }),

  notificationSettings: belongsTo('notification-setting', {
    async: false,
    inverse: null,
    defaultValue() {
      return {};
    },
  }),

  /* eslint-disable ember-data-sync-relationships/no-async-relationships */
  relationship: belongsTo('relationship', {
    async: true,
    inverse: 'pleroma',
    defaultValue() {
      return {};
    },
  }),
  /* eslint-enable ember-data-sync-relationships/no-async-relationships */
});

import Model, { attr, hasMany } from '@ember-data/model';

// https://docs-develop.pleroma.social/backend/API/pleroma_api/#emoji-reactions
export default class EmojiReactionModel extends Model {
  @attr('number') count;

  @attr('boolean') me;

  @attr('string') name;

  @hasMany('user', {
    async: false,
    inverse: null,
  }) accounts;
}

import Model, { attr, hasMany } from '@ember-data/model';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';
import { computed } from '@ember/object';

// https://docs.joinmastodon.org/entities/poll/
export default class PollModel extends Model.extend(LoadableModel) {
  @attr('date') expiresAt;

  @attr('number') expiresIn; // Only used for poll creation, In seconds

  @attr('boolean') expired;

  @attr('boolean') multiple;

  @attr('number', { defaultValue() { return 0; }}) votesCount;

  @attr('number', { defaultValue() { return 0; }}) votersCount;

  @attr('boolean') voted;

  @attr('array') ownVotes;

  @hasMany('poll-option', { async: false, inverse: null }) options;

  @attr('array') emojis;

  @computed('options.@each.votesCount')
  get winningVoteCount() {
    return Math.max(...this.options.mapBy('votesCount'));
  }
}

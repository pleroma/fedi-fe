import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import { memberAction } from 'ember-api-actions';
import { reads } from '@ember/object/computed';
import { computed } from '@ember/object';
import { inject as concern } from 'ember-concerns';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';

export default Model.extend(LoadableModel, {
  acct: attr('string'),
  avatar: attr('string'),
  displayName: attr('string'),
  email: attr('string'),
  username: attr('string'), // @greg
  locale: attr('string'),
  followersCount: attr('number'),
  followingCount: attr('number'),
  statusesCount: attr('number'),
  locked: attr('boolean'),
  note: attr('string'),
  url: attr('string'),

  /* eslint-disable ember-data-sync-relationships/no-async-relationships */
  mutes: hasMany('user', { inverse: null }),

  blocks: hasMany('user', { inverse: null }),

  tokens: hasMany('token', { inverse: 'user' }),
  /* eslint-enable ember-data-sync-relationships/no-async-relationships */

  emojis: hasMany('emoji', {
    async: false,
    inverse: null,
  }),

  pleroma: belongsTo('user-extra', {
    async: false,
    inverse: 'user',
  }),

  // Represents display or publishing preferences of user's own account. Returned as an additional entity when verifying and updated credentials, as an attribute of Account.
  source: belongsTo('user-source', {
    async: false,
    inverse: null,
  }),

  bio: reads('note'),

  defaultScope: reads('source.privacy'),

  names: concern('user/names'),

  remoteFollowUrl: computed('url', function() {
    let serverUrl = new URL(this.url);
    return `${serverUrl.protocol}//${serverUrl.host}/main/ostatus`;
  }),

  isLocal: computed('acct', function() {
    return !this.acct.includes('@');
  }),

  block: memberAction({
    path: 'block',
    type: 'post',
  }),

  unblock: memberAction({
    path: 'unblock',
    type: 'post',
  }),

  mute: memberAction({
    path: 'mute',
    type: 'post',
  }),

  unmute: memberAction({
    path: 'unmute',
    type: 'post',
  }),

  follow: memberAction({
    path: 'follow',
    type: 'post',
  }),

  unfollow: memberAction({
    path: 'unfollow',
    type: 'post',
  }),
});
// return {
//   acct: 'Greg',
//   avatar: 'https://stupid-brown-camel.gigalixirapp.com/images/avi.png',
//   avatar_static: 'https://stupid-brown-camel.gigalixirapp.com/images/avi.png',
//   bot: false,
//   created_at: '2019-08-12T16:47:02.000Z',
//   display_name: 'Greg',
//   emojis: [],
//   fields: [],
//   followers_count: 0,
//   following_count: 0,
//   header: 'https://stupid-brown-camel.gigalixirapp.com/images/banner.png',
//   header_static: 'https://stupid-brown-camel.gigalixirapp.com/images/banner.png',
//   id: '9loeB9ihTWXWHOl7xo',
//   locked: false,
//   note: 'GregGreg',
//   statuses_count: 0,
//   url: 'https://stupid-brown-camel.gigalixirapp.com/users/Greg',
//   username: 'Greg',
//   pleroma: {
//     background_image: null,
//     chat_token: 'SFMyNTY.g3QAAAACZAAEZGF0YW0AAAASOWxvZUI5aWhUV1hXSE9sN3hvZAAGc2lnbmVkbgYAKXbeh2wB.CC-0T7atRe2iSjVNbNXgvlq9RGEPjvoCuBpxoihn9zk',
//     confirmation_pending: true,
//     hide_favorites: true,
//     hide_followers: false,
//     hide_follows: false,
//     is_admin: false,
//     is_moderator: false,
//     settings_store: {},
//     skip_thread_containment: false,
//     tags: [],
//
//     notification_settings: {
//       block_from_strangers: true,
//       hide_notification_contents: true,
//     },
//
//     relationship: {
//       blocked_by: false,
//       blocking: false,
//       domain_blocking: false,
//       endorsed: false,
//       followed_by: true,
//       following: true,
//       id: '9loeB9ihTWXWHOl7xo',
//       muting: false,
//       muting_notifications: false,
//       requested: false,
//       showing_reblogs: true,
//       subscribing: false,
//     },
//   },
//
//   source: {
//     note: 'GregGreg',
//     no_rich_text: false,
//     show_role: true,
//     privacy: 'public',
//     sensitive: false,
//
//     pleroma: {
//       show_role: true,
//       no_rich_text: false,
//     },
//   },
// };

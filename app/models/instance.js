import Model, { attr, belongsTo } from '@ember-data/model';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import LoadableModel from 'ember-data-storefront/mixins/loadable-model';
import config from 'pleroma-pwa/config/environment';

// https://docs-develop.pleroma.social/backend/configuration/cheatsheet/
export default Model.extend(LoadableModel, {
  description: attr('string'), // "A Pleroma instance, an alternative fediverse server"
  email: attr('string'), // "example@example.com"
  registrations: attr('boolean'), // true
  thumbnail: attr('string'), // "https://stupid-brown-camel.gigalixirapp.com/instance/thumbnail.jpeg"
  title: attr('string'), // "Pleroma"
  uri: attr('string'), // "https://stupid-brown-camel.gigalixirapp.com"
  version: attr('string'), // "2.7.2 (compatible; Pleroma 1.0.0)"
  urls: attr('object'), // { streamingApi: "wss://stupid-brown-camel.gigalixirapp.com"}
  stats: attr('object'),

  avatarUploadLimit: attr('number', { // File size limit of user’s profile avatars.
    defaultValue() {
      return config.APP.MAX_AVATAR_SIZE;
    },
  }),

  bannerUploadLimit: attr('number', { // File size limit of user’s profile banners.
    defaultValue() {
      return config.APP.MAX_BANNER_SIZE;
    },
  }),

  uploadLimit: attr('number', { // File size limit of uploads (except for avatar, background, banner).
    defaultValue() {
      return config.APP.MAX_UPLOAD_SIZE;
    },
  }),

  backgroundUploadLimit: attr('number', { // File size limit of user’s profile backgrounds.
    defaultValue() {
      return config.APP.MAX_BANNER_SIZE;
    },
  }),

  maxTootChars: attr('number', {
    defaultValue() {
      return config.APP.MAX_STATUS_CHARS;
    },
  }),

  userBioLength: attr('number', { // A user bio maximum length (default: 5000).
    defaultValue() {
      return config.APP.MAX_BIO_LENGTH;
    },
  }),

  userNameLength: attr('number', { // A user name maximum length (default: 100).
    defaultValue() {
      return config.APP.USER_NAME_LENGTH;
    },
  }),

  languages: attr('array', {
    defaultValue() {
      return ['en'];
    },
  }),

  pollLimits: attr('object', {
    defaultValue() {
      return {
        maxExpiration: 31536000,
        maxOptionChars: 200,
        maxOptions: 20,
        minExpiration: 0,
      };
    },
  }),

  credentials: belongsTo('client-credential', {
    async: false,
    inverse: null,
  }),

  token: belongsTo('client-token', {
    async: false,
    inverse: null,
  }),

  nodeInfo: belongsTo('node-info', { // Fetched and set at runtime
    async: false,
    inverse: null,
  }),

  backgroundImageUrl: computed('backgroundImage', 'uri', function() {
    return this.backgroundImage && `${this.uri}${this.backgroundImage}`;
  }),

  chosenLanguage: computed('languages', 'intl.locales', function() {
    // TODO: Someday this should also account for the language set in the web-app manifest. If it gives us trouble, take another stab.
    // TODO: This should also account for the locale on the sessions user (if set)
    if (this.languages) {
      if (this.intl.locales.includes(this.languages.firstObject && this.languages.firstObject.toLowerCase())) {
        return this.languages.firstObject;
      }

      // Pleroma uses: https://en.wikipedia.org/wiki/ISO_639... ember-intl uses: https://github.com/andyearnshaw/Intl.js/tree/master/locale-data/json
      if (this.languages.includes('en')) {
        return 'en';
      }
    }

    return 'en';
  }),

  thumbnailUrl: computed('thumbnail', 'uri', function() {
    if (!this.thumbnail || !this.thumbnail.startsWith('/') || this.thumbnail.startsWith('data:')) {
      return this.thumbnail;
    }

    return `${this.uri}${this.thumbnail}`;
  }),

  intl: service(),
});

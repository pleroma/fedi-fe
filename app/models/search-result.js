import Model, { hasMany } from '@ember-data/model';

export default Model.extend({
  hashtags: hasMany('hashtag', {
    async: false,
  }),

  accounts: hasMany('user', {
    async: false,
  }),

  statuses: hasMany('status', {
    async: false,
  }),
});

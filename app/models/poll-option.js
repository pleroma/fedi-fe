import Model, { attr } from '@ember-data/model';

export default class PollOptionModel extends Model {
  @attr('string') title;

  @attr('number', { defaultValue() { return 0; }}) votesCount;
}

import Model, { attr, belongsTo } from '@ember-data/model';

// https://docs.joinmastodon.org/entities/source/
export default Model.extend({
  note: attr('string'),

  privacy: attr('string', {
    defaultValue: () => 'public',
  }),

  pleroma: belongsTo('user-source-extra', {
    async: false,
    inverse: null,
  }),
});

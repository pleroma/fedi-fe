import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
  clientId: attr('string'),
  clientSecret: attr('string'),
  name: attr('string'),
  redirectUri: attr('string'),
  vapidKey: attr('string'),
  website: attr('string'),
});

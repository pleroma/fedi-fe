import Model, { attr } from '@ember-data/model';

// https://docs-develop.pleroma.social/backend/API/differences_in_mastoapi_responses/#accounts
export default Model.extend({
  blockFromStrangers: attr('boolean'),
  hideNotificationContents: attr('boolean'),
});

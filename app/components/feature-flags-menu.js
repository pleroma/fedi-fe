import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { action, get, set } from '@ember/object';
import {
  bindKeyboardShortcuts,
  unbindKeyboardShortcuts,
} from 'ember-keyboard-shortcuts';

export default class FeatureFlagsMenuComponent extends Component {
  @service features;

  showing = false;

  keyboardShortcuts = {
    'command+e': () => this.toggleShow(),
    'command+y': () => this.toggleShow(),
  };

  constructor(owner, args) {
    super(owner, args);
  }

  toggleShow() {
    set(this, 'showing', !this.showing);
  }

  @action
  toggleFlag(flag) {
    if (get(this.features, flag)) {
      this.features.disable(flag);
    } else {
      this.features.enable(flag);
    }
  }

  @action
  onInsert() {
    bindKeyboardShortcuts(this);
  }

  @action
  onDestroy() {
    unbindKeyboardShortcuts(this);
  }
}

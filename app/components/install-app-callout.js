import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { set } from '@ember/object';
import { task } from 'ember-concurrency';

export default Component.extend({
  intl: service(),

  deferredPrompt: null,
  viewingInstalledVersion: false,

  init(...args) {
    this._super(...args);
    this.onBeforeInstallPrompt = this.onBeforeInstallPrompt.bind(this);
  },

  didInsertElement(...args) {
    this._super(...args);
    window.addEventListener('beforeinstallprompt', this.onBeforeInstallPrompt);

    if (window.matchMedia('(display-mode: standalone)').matches || window.navigator.standalone === true) {
      set(this, 'viewingInstalledVersion', true);
    }
  },

  willDestroyElement(...args) {
    this._super(...args);
    window.removeEventListener('beforeinstallprompt', this.onBeforeInstallPrompt);
  },

  onBeforeInstallPrompt(event) {
    set(this, 'deferredPrompt', event);
  },

  installPwa: task(function* () {
    if (!this.deferredPrompt) {
      return;
    }

    this.deferredPrompt.prompt();

    let choiceResult = yield this.deferredPrompt.userChoice;

    set(this, 'deferredPrompt', null);

    return choiceResult.outcome === 'accepted' ? this.intl.t('pwaInstallSuccess') : this.intl.t('pwaInstallFail');
  }).drop(),
});

import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { keepLatestTask } from 'ember-concurrency-decorators';
import { action, get, getWithDefault, notifyPropertyChange, set } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';

export default class BlockSettingsComponent extends Component {
  @service error;

  @service intl;

  @tracked errors = [];

  @tracked checked = {};

  block() {}

  unblock() {}

  get properties() {
    let properties = this.blocks.toArray().map(({ id }) => id);
    return properties;
  }

  get propertyData() {
    return Object.fromEntries(
      this.properties.map(id => [id, this.property(id)]),
    );
  }

  get isEveryoneChecked() {
    if (this.blocks.length === 0) {
      return false;
    }

    return this.blocks.length === this.checkedUsers.length;
  }

  get checkedUsers() {
    let { propertyData } = this;
    return this.blocks.filter(({ id }) => propertyData[id]);
  }

  property(id) {
    return getWithDefault(this.checked, id, false);
  }

  get showActionsForAll() {
    if (this.properties.length === 0) {
      return false;
    }

    let some = this.properties.some(id => this.property(id));

    return some;
  }

  @keepLatestTask*
  mapSelected(actionName = 'block', event) {
    event.stopPropagation();
    event.preventDefault();

    // either this.block or this.unblock
    let action = this[actionName];

    try {
      let { checkedUsers } = this;

      for (let user of checkedUsers) {
        yield tryLinkedTask(action, user);
      }

      return yield checkedUsers;
    } catch (err) {
      let errors = this.error.formErrors(err, 'errorSavingBlockSettings');

      errors.forEach(({ detail }) => {
        this.errors.push({ validation: detail });
      });
    }
  }

  @action
  reset() {
    this.errors = [];
    this.checked = {};
  }

  @action
  toggleEveryone() {
    let newValue = !this.isEveryoneChecked;

    for (let property of this.properties) {
      set(this.checked, property, newValue);
    }

    notifyPropertyChange(this, 'checked');
  }

  @action
  toggle(id) {
    set(this.checked, id, !get(this.checked, id));

    notifyPropertyChange(this, 'checked');
  }
}

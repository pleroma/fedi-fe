import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class BodyClassComponent extends Component {
  @action
  handleDidInsert() {
    document.body.classList.add(this.args.class);
  }

  @action
  handleWillDestroy() {
    document.body.classList.remove(this.args.class);
  }
}

import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { tryInvoke } from '@ember/utils';

export default Component.extend({
  router: service(),

  tagName: '',
  handler: null,

  didInsertElement() {
    this._super(...arguments);
    this.router.on('routeWillChange', this, 'invokeHandler');
  },

  willDestroyElement() {
    this.router.off('routeWillChange', this, 'invokeHandler');
    this._super(...arguments);
  },

  invokeHandler() {
    tryInvoke(this, 'handler');
  },
});

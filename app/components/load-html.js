import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import fetch from 'fetch';
import { restartableTask } from 'ember-concurrency-decorators';
import { htmlSafe } from '@ember/string';
import { whenRouteIdle } from 'ember-app-scheduler';

export default class LoadHtml extends Component {
  @service pleromaApi;

  @service preload;

  'data-test-selector' = 'load-html';

  @computed('attrs.path')
  get url() {
    return `${this.pleromaApi.apiBaseUrl}/${this.path}`;
  }

  @computed('fetchHtmlFile.lastSuccessful.value')
  get sanitizedHtml() {
    let html = this.fetchHtmlFile.lastSuccessful.value;
    html = this.stripScriptTags(html);
    html = this.stripStyleTags(html);
    html = this.stripInlineStyles(html);
    html = this.stripClasses(html);
    html = this.stripImages(html);
    html = htmlSafe(html);
    return html;
  }

  constructor(...args) {
    super(...args);

    this.fetchHtmlFile.perform();
  }

  @restartableTask*
  fetchHtmlFile() {
    try {
      yield whenRouteIdle();

      // try preload first
      let html = this.preload.data[`/${this.path}`];
      if (html) {
        return html;
      }

      // fall back to loading url
      let results = yield fetch(this.url);
      if (results.ok) {
        let html = yield results.text();
        return html;
      } else {
        return '';
      }
    } catch(e) {
      return '';
    }
  }

  stripScriptTags(htmlString) {
    return htmlString.replace(/<script[\s\S]+?<\/script>/g, '');
  }

  stripStyleTags(htmlString) {
    return htmlString.replace(/<style[\s\S]+?<\/style>/g, '');
  }

  stripInlineStyles(htmlString) {
    return htmlString.replace(/style=['"].+?['"]/g, '');
  }

  stripClasses(htmlString) {
    return htmlString.replace(/class=['"].+?['"]/g, '');
  }

  stripImages(htmlString) {
    return htmlString.replace(/<img[\s\S]+?>/g, '');
  }
}

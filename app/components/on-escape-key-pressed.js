import Component from '@ember/component';
import {
  bindKeyboardShortcuts,
  unbindKeyboardShortcuts,
} from 'ember-keyboard-shortcuts';
import { tryInvoke } from '@ember/utils';

export default Component.extend({
  tagName: '',
  handler: null,

  init(...args) {
    this.keyboardShortcuts = {
      esc: () => tryInvoke(this, 'handler', []),
    };
    this._super(...args);
  },

  didInsertElement() {
    this._super(...arguments);
    bindKeyboardShortcuts(this);
  },

  willDestroyElement() {
    unbindKeyboardShortcuts(this);
    this._super(...arguments);
  },
});

import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { restartableTask } from 'ember-concurrency-decorators';
import { inject as service } from '@ember/service';
import { timeout } from 'ember-concurrency';

export default class SearchSuggestionsOverlayComponent extends Component {
  @service router;

  @service search;

  @tracked
  dropdown = null;

  constructor(...args) {
    super(...args);

    this.router.on('routeWillChange', this.closeDropdown);
  }

  willDestroy() {
    super.willDestroy(...arguments);

    this.router.off('routeWillChange', this.closeDropdown);
  }

  @action
  closeDropdown() {
    this.dropdown.actions.close();
  }

  @action
  onQueryUpdated() {
    let { query } = this.search;

    if (query?.length >= 3) {
      this.getSuggestionsTask.perform();
      this.dropdown.actions.open();
    } else {
      this.dropdown.actions.close();
      this.getSuggestionsTask.cancelAll();
    }
  }

  @action
  registerDropdown(dropdown) {
    this.dropdown = dropdown;
  }

  @restartableTask*
  getSuggestionsTask() {
    try {
      yield timeout(300);

      let results = yield this.search.getResults.linked().perform();

      return results;
    } catch (e) {
      this.dropdown.actions.close();
      console.log(e);
    }
  }
}

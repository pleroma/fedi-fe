import Component from '@ember/component';

export default class SplashScreenRemover extends Component {
  constructor(...args) {
    super(...args);

    let splashScreen = document.getElementById('splash-screen');

    if (splashScreen) {
      splashScreen.parentNode.removeChild(splashScreen);
    }
  }
}

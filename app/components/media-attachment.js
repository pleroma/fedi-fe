import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class MediaAttachment extends Component {
  @tracked loadingImage = true;

  get showPreview() {
    let { previewUrl, url } = this.args.attachment;
    let { isPreview } = this.args;
    return previewUrl !== url && isPreview;
  }

  get previewStyle() {
    let height = `${this.args.attachment?.meta?.small?.height || 272}px`;
    let width = this.args.attachment?.meta?.small?.width;

    return `height: ${height}; width: ${width ? `${width}px` : '100%'}`;
  }

  // On a slow connection, iOS may display only a part of the image if the url is used before the image is preloaded
  // and it won't display the fully loaded image until the user zooms in.
  // Preloading the image in advance before using its URL fixes this issue.
  get imageSource() {
    if (this.args.isPreview) {
      return this.args.attachment.previewUrl;
    } else {
      return this.args.attachment.url;
    }
  }

  get paddingBottomStyle() {
    if (!this.mediaDimensions) return ``
    let { width, height } = this.mediaDimensions;
    return `padding-bottom: ${parseFloat(height / width * 100).toFixed(2)}%`
  }

  get originalAspectRatio() {
    return !!(this.args.showOriginalAspectRatio && this.mediaDimensions)
  }

  get mediaDimensions() {
    let meta = this.args.attachment.meta || {}
    if (meta?.small?.width && meta?.small?.height) return meta.small
    if (meta?.original?.width && meta?.original?.height) return meta.original
    return undefined
  }

  @action
  didInsertImage() {
    if (!this.args.preloadImage) return

    let img = new Image();
    img.onload = () => {
      this.loadingImage = false;
    }

    img.src = this.args.attachment.url;
  }
}

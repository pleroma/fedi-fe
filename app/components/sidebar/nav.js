import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default class extends Component {
  @service router;

  @service store;

  @computed('router.currentRouteName')
  get classicDirectMessageRelated() {
    let { currentRoute, currentRouteName } = this.router;

    if (['message.compose', 'message.details'].includes(currentRouteName)) {
      return true;
    }

    // try to guess if current model is a direct status
    if (currentRouteName === 'status.details' && currentRoute.params?.id) {
      let maybeCurrentStatus = this.store.peekRecord('status', currentRoute.params.id);

      if (maybeCurrentStatus?.visibility === 'direct') {
        return true;
      }
    }


    return false;
  }
}

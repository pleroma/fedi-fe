import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action, computed } from '@ember/object';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { dropTask } from 'ember-concurrency-decorators';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import { timeout } from 'ember-concurrency';
import {
  validateLength,
  validatePresence,
} from 'ember-changeset-validations/validators';
import config from 'pleroma-pwa/config/environment';

export default class OnboardingBioFormComponent extends Component {
  @service authModal;

  @service error;

  @service instances;

  @service intl;

  validations = {
    bio: [
      validatePresence({
        presence: true,
        message: this.intl.t('youMustEnterABio'),
      }),
      validateLength({
        max: this.bioLengthLimit,
        allowBlank: false,
        message: this.intl.t('yourBioIsTooLong', { max: this.bioLengthLimit }),
      }),
    ],
  };

  @action
  reset() {
    this.changeset = new Changeset(
      { bio: null },
      lookupValidator(this.validations),
      this.validations,
    );
  }

  constructor() {
    super(...arguments);
    this.reset();
  }

  @computed('instances.current')
  get bioLengthLimit() {
    return this.instances.current && this.instances.current.userBioLength;
  }

  @dropTask*
  submit(event) {
    event.stopPropagation();
    event.preventDefault();

    yield this.changeset.validate();

    if (this.changeset.isValid) {
      try {
        yield tryLinkedTask(this.args.updateUserBio, { note: this.changeset.bio });

        // Slow things down a little bit then close the modal.
        if (config.environment !== 'test') {
          yield timeout(2000);
        }

        yield tryLinkedTask(this.args.onClose);
      } catch (err) {
        let errors = this.error.formErrors(err, 'errorTryAgain');

        errors.forEach(({ source, detail }) => {
          this.changeset.pushErrors(source, detail);
        });
      }
    }
  }
}

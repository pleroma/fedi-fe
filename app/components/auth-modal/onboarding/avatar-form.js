import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { restartableTask, dropTask } from 'ember-concurrency-decorators';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import config from 'pleroma-pwa/config/environment';

export default class OnboardingAvatarFormComponent extends Component {
  @service authModal;

  @service instances;

  @service intl;

  @service session;

  @service userActions;

  defaultAvatar = config.APP.DEFAULT_AVATAR;

  @restartableTask*
  onFilesSelected(event) {
    let { files } = event.target;
    let [avatarFile] = files;

    yield tryLinkedTask(this.args.updateAvatar, avatarFile);

    event.target.value = null;
  }

  @dropTask*
  clearUserAvatar() {
    yield tryLinkedTask(this.args.clearAvatar);
  }
}

import Component from '@ember/component';
import { dropTask } from 'ember-concurrency-decorators';
import { inject as service } from '@ember/service';
import { validatePresence } from 'ember-changeset-validations/validators';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { get } from '@ember/object';

export default class ForgotPasswordForm extends Component {
  @service() intl;

  @service() pleromaApi;

  @service() authModal;

  validations = {
    email: [
      validatePresence({
        presence: true,
        message: this.intl.t('youMustEnterUsernameOrEmail'),
      }),
    ],
  };

  constructor(...args) {
    super(...args);

    this.changeset = new Changeset({}, lookupValidator(this.validations), this.validations);
  }

  @dropTask()*
  requestPasswordResetEmail(event) {
    event.stopPropagation();
    event.preventDefault();

    yield this.changeset.validate();

    if (this.changeset.isValid) {
      let url = this.pleromaApi.endpoints.requestForgotPasswordEmail;

      try {
        yield fetch(url, {
          method: 'POST',

          body: JSON.stringify({
            email: get(this.changeset, 'email'),
          }),
        });
      } catch(e) {
        // Do nothing with server errors. Always show success.
      } finally {
        // No matter what, we show success.
        this.authModal.showForgotPasswordSuccess();
      }
    }
  }
}

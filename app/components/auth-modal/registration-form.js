import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import {
  validateConfirmation,
  validateFormat,
  validateLength,
  validatePresence,
} from 'ember-changeset-validations/validators';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { dropTask } from 'ember-concurrency-decorators';
import { action, computed } from '@ember/object';
import { tryInvoke } from '@ember/utils';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import config from 'pleroma-pwa/config/environment';

export default class RegistrationFormComponent extends Component {
  @service authModal;

  @service captcha;

  @service error;

  @service instances;

  @service intl;

  @service pleromaApi;

  @service router;

  @service session;

  @service store;

  @service userActions;

  @service window;

  @tracked changeset;

  @tracked captchaUrl;

  get captchaEnabled() {
    return this.captcha.enabled;
  }

  captchaToken;

  captchaAnswerData;

  validations = {
    username: [
      validatePresence({
        ignoreBlank: true,
        message: this.intl.t('youMustEnterUsername'),
        presence: true,
      }),
      validateLength({
        max: this.usernameLengthLimit,
        allowBlank: true,
        message: this.intl.t('yourUsernameIsTooLong', { max: this.usernameLengthLimit }),
      }),
      validateFormat({
        regex: config.APP.USERNAME_REGEX,
        message: this.intl.t('yourUsernameContainsInvalidCharacters'),
      }),
    ],

    displayName: [
      validatePresence({
        ignoreBlank: true,
        presence: true,
        message: this.intl.t('youMustEnterDisplayName'),
      }),
    ],

    password: [
      validatePresence({
        presence: true,
        message: this.intl.t('youMustEnterPassword'),
      }),
    ],

    email: [
      validatePresence({
        presence: true,
        message: this.intl.t('youMustEnterEmail'),
      }),
      validateFormat({
        allowBlank: true,
        type: 'email',
        message: this.intl.t('invalidEmail'),
      }),
    ],

    passwordConfirmation: [
      validatePresence({
        presence: true,
        message: this.intl.t('youMustConfirmYourPassword'),
      }),
      validateConfirmation({
        on: 'password',
        message: this.intl.t('yourConfirmPasswordDoesNotMatch'),
      }),
    ],

    captcha: [
      validatePresence({
        presence: true,
        message: this.intl.t('youMustEnterCaptcha'),
      }),
    ],
  };

  constructor() {
    super(...arguments);

    this.changeset = new Changeset({}, lookupValidator(this.validations), this.validations, { skipValidate: true });
  }

  @computed('instances.current')
  get usernameLengthLimit() {
    return this.instances.current && this.instances.current.userNameLength;
  }

  @dropTask()*
  submit(event) {
    event.stopPropagation();
    event.preventDefault();

    yield this.changeset.validate();

    if (this.changeset.isValid) {
      try {
        yield this.changeset.execute();

        let implicitAuthGrant = yield tryLinkedTask(this.args.registerUser, {
          agreement: true,

          // TODO: Lets fix the API so it can accept a user model for registration and not this other thing.
          nickname: this.changeset.displayName,
          fullname: this.changeset.displayName,

          username: this.changeset.username,
          email: this.changeset.email,

          password: this.changeset.password,
          confirm: this.changeset.passwordConfirmation,

          /* eslint-disable camelcase */
          captcha_solution: this.changeset.captcha,
          captcha_token: this.captchaToken,
          captcha_answer_data: this.captchaAnswerData,
          /* eslint-enable camelcase */

          locale: this.instances.current.chosenLanguage,

          locked: false,
        });

        tryInvoke(this.args, 'allowExit');

        tryInvoke(this.args, 'onRegistered');

        // authenticates then redirects the user -> check the application route sessionAuthenticated method
        yield this.session.authenticateImplicitGrant(implicitAuthGrant);

      } catch(err) {
        let errors = this.error.formErrors(err, 'errorCreatingAccount');

        errors.forEach(({ source, detail }) => {
          // TODO: when we normalize registration, lets return proper field names.
          if (source === 'nickname') {
            source = 'displayName';
            detail = detail.replace('Nickname', this.intl.t('displayName'));
          }

          if (source === 'fullname') {
            source = 'displayName';
            detail = detail.replace('Fullname', this.intl.t('displayName'));
          }

          if (source === 'name') {
            source = 'username';
            detail = detail.replace('Name', this.intl.t('registerUsernameLabel'));
          }

          this.changeset.pushErrors(source, detail);
        });

        // Upon any errors, set a new captcha and clear the captch input
        this.setCaptcha();
        this.changeset.captcha = null;
      }
    }
  }

  @action
  async setCaptcha() {
    let data = await this.captcha.new();

    let { answerData, token, url } = data;

    this.captchaUrl = url;
    this.captchaToken = token;
    this.captchaAnswerData = answerData;
  }
}

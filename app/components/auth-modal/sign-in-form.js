import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';
import { get, set } from '@ember/object';
import { validatePresence } from 'ember-changeset-validations/validators';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';

export default Component.extend({
  instances: service(),
  intl: service(),
  session: service(),

  serverError: null,

  tagName: '',

  init(...args) {
    this._super(...args);

    let validations = {
      username: [
        validatePresence({
          presence: true,
          message: this.intl.t('youMustEnterUsernameOrEmail'),
        }),
      ],

      password: [
        validatePresence({
          presence: true,
          message: this.intl.t('youMustEnterPassword'),
        }),
      ],
    };

    this.changeset = new Changeset({}, lookupValidator(validations), validations);

    this.changeset.on('beforeValidation', () => {
      set(this, 'serverError', null);
    });
  },

  authenticateTask: task(function* (event) {
    event.stopPropagation();
    event.preventDefault();

    set(this, 'serverError', null);
    yield this.changeset.validate();

    if (this.changeset.isValid) {
      try {
        yield this.session.authenticateBasic(get(this.changeset, 'username'), get(this.changeset, 'password'));
      } catch(e) {
        set(this, 'serverError', e.error);
      }
    }
  }).drop(),
});

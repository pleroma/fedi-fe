import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default class InViewportNotifier extends Component {
  @service('in-viewport') inViewport;

  tagName = '';

  didInsertNode(element, [instance]) {
    let viewportTolerance = { bottom: 0 };
    let { onEnter, onExit } = instance.inViewport.watchElement(element, { viewportTolerance });

    onEnter(instance.onViewportEntered);
    onExit(instance.onViewportExited);
  }

  willDestroyNode(element, [instance]) {
    instance.inViewport.stopWatching(element);
  }
}

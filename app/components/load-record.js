import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { restartableTask } from 'ember-concurrency-decorators';
import { whenRouteIdle } from 'ember-app-scheduler';
import { tryInvoke } from '@ember/utils';

export default class LoadRecord extends Component {
  @service store;

  constructor(owner, args) {
    super(owner, args);

    this.query.perform();
  }

  get cache() {
    if (this.args.cache) {
      return !!this.args.cache;
    }

    return false;
  }

  @restartableTask*
  query() {
    yield whenRouteIdle();

    if (!this.args.modelId) {
      return;
    }

    try {
      let value;

      if (this.cache) {
        value = this.store.peekRecord(this.args.modelName, this.args.modelId);
      }

      if (!value) {
        value = yield this.store.loadRecord(this.args.modelName, this.args.modelId);
      }

      return value;
    } catch(e) {
      tryInvoke(this.args, 'onNotFound');
    }
  }
}

import Component from '@ember/component';
import { computed, set } from '@ember/object';
import { task } from 'ember-concurrency';
import { validatePresence, validateLength, validateNumber } from 'ember-changeset-validations/validators';
import validateStatusContent from 'pleroma-pwa/validators/status-content';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { inject as concern } from 'ember-concerns';
import { inject as service } from '@ember/service';
import config from 'pleroma-pwa/config/environment';
import { tryInvoke, isEmpty } from '@ember/utils';
import { scheduleOnce } from '@ember/runloop';
import {
  bindKeyboardShortcuts,
  unbindKeyboardShortcuts,
} from 'ember-keyboard-shortcuts';
import {
  durationFromSeconds,
} from 'pleroma-pwa/utils/duration';

export default Component.extend({
  emojiPicker: service(),
  error: service(),
  instances: service(),
  intl: service(),
  session: service(),
  statusActions: service(),
  store: service(),

  tagName: '',

  scope: null,

  fileInputElement: null,

  onStatusCreated: null,

  directMode: 'classic',

  media: concern('status/media'),

  statusCharacterLimit: computed('instances.current', function() {
    return this.instances.current?.maxTootChars || config.APP.MAX_STATUS_CHARS;
  }),

  init(...args) {
    this._super(...args);

    let visibility = this.session.currentUser.defaultScope;
    let isReplyToDirectMessage = this.inReplyToStatus && this.inReplyToStatus.pleroma.directConversationId;

    if (this.inReplyToStatus) {
      if (isReplyToDirectMessage) {
        visibility = 'direct';
      } else {
        ({ visibility } = this.inReplyToStatus);
      }
    } else if (this.compose === 'direct') {
      visibility = 'direct';
    }

    if (!this.directMode) {
      this.directMode = 'classic';
    }

    this.changeset = new Changeset(this.store.createRecord('status', {
      mediaIds: [],
      visibility,
      sensitive: false,
      ...this.inReplyToStatus && { inReplyToId: this.inReplyToStatus.id },
      ...isReplyToDirectMessage && { inReplyToConversationId: this.inReplyToStatus.pleroma.directConversationId },
    }), lookupValidator(this.statusValidations), this.statusValidations);

    if (this.compose === 'upload') {
      scheduleOnce('afterRender', this, this.doOpenFileSelect);
    }

    if (this.compose === 'poll') {
      set(this.changeset, 'poll', this.createPoll());
    }

    if ((this.compose === 'direct' || this.compose === 'mention' || this.compose === 'reply') && this.initialMentions) {
      let content = '';

      this.initialMentions.forEach((mention) => {
        content += `${this.intl.t('atHandle', { handle: mention })} `;
      });

      set(this.changeset, 'content', content);
    } else if (this.compose === 'direct' && this.initialLinks) {
      let content = '@ ';

      this.initialLinks.forEach((link) => {
        content += `${link} `;
      });

      set(this.changeset, 'content', content);
    }

    this.keyboardShortcuts = {
      'command+enter': () => this.createStatusTask.perform(),
      'ctrl+enter': () => this.createStatusTask.perform(),
    };

    bindKeyboardShortcuts(this);
  },

  willDestroyElement(...args) {
    unbindKeyboardShortcuts(this);
    this._super(...args);
  },

  actions: {
    openFileSelect() {
      this.fileInputElement.click();
    },

    startPoll() {
      set(this.changeset, 'poll', this.createPoll());
    },

    addPollOption() {
      this.changeset.poll.options.pushObject(this.createPollOption());
    },

    removePollOption(option) {
      this.changeset.poll.options.removeObject(option);
    },

    removePoll() {
      set(this.changeset, 'poll', null);
    },

    setVisibility(value) {
      if (['directMessageChat', 'directMessageClassic'].includes(value)) {
        set(this.changeset, 'visibility', 'direct');

        if (value === 'directMessageChat') {
          set(this, 'directMode', 'chat');
        } else if (value === 'directMessageClassic') {
          set(this, 'directMode', 'classic');
        }

        return;
      }

      set(this.changeset, 'visibility', value);
    },

    updatePollExpiration(expiresIn) {
      set(this.changeset.poll, 'expiresIn', expiresIn);
    },

    addStaticEmoji(staticEmoji) {
      this.emojiPicker.withNewContent(staticEmoji, (newContent) => {
        set(this.changeset, 'content', newContent);
      });
    },
  },

  setCaretPosition(textarea) {
    textarea.focus();

    if (textarea.value.startsWith('@ ')) {
      textarea.setSelectionRange(1, 1);
    } else {
      let textLength = textarea.value.length;
      textarea.setSelectionRange(textLength, textLength);
    }
  },

  get statusValidations() {
    return {
      sensitive: validatePresence(true),

      content: [
        validateStatusContent({
          message: this.intl.t('yourPostMustHaveContent'),
        }),
        validateLength({
          max: this.statusCharacterLimit,
          allowBlank: true,
          message: this.intl.t('yourPostIsTooLong', { max: this.statusCharacterLimit }),
        }),
      ],

      'poll.expiresIn': [
        validateNumber({
          allowBlank: true,
          gte: this.instances.current.pollLimits.minExpiration,
          message: this.intl.t('pollDurationTooShort', { min: this.intl.t('hoursMinutesSeconds', durationFromSeconds(this.instances.current.pollLimits.minExpiration)) }),
        }),
        validateNumber({
          allowBlank: true,
          lte: this.instances.current.pollLimits.maxExpiration,
          message: this.intl.t('pollDurationTooLong', { max: this.intl.t('hoursMinutesSeconds', durationFromSeconds(this.instances.current.pollLimits.maxExpiration)) }),
        }),
      ],

      'poll.options': [
        validateLength({
          allowBlank: true,
          min: 2,
          message: this.intl.t('pollNeedsAtLeastTwoOptions'),
        }),
        validateLength({
          allowBlank: true,
          max: this.instances.current.pollLimits.maxOptions,
          message: this.intl.t('pollHasTooManyOptions', { max: this.instances.current.pollLimits.maxOptions }),
        }),
        (key, value) => { // Validate Presence
          if (isEmpty(value)) { // Allow Blank
            return true;
          }

          let result = value?.every((pollOption) => {
            return !isEmpty(pollOption.title);
          });

          return result || this.intl.t('pollOptionsEmpty');
        },
        (key, value) => { // Validate Option Length
          if (isEmpty(value)) { // Allow Blank
            return true;
          }

          let result = value?.every((pollOption) => {
            if (pollOption.title) {
              return this.instances.current.pollLimits.maxOptionChars >= pollOption.title.length;
            }

            return true;
          });

          return result || this.intl.t('pollOptionTooLong', { max: this.instances.current.pollLimits.maxOptionChars });
        },
        (key, value) => { // Validate Option Uniqueness
          if (isEmpty(value)) { // Allow Blank
            return true;
          }

          let titles = value?.mapBy('title')
            .filter(Boolean);

          let uniqueOptions = titles
            .map(title => title.toLowerCase())
            .uniq();

          return (uniqueOptions.length === titles.length) || this.intl.t('pollOptionsUnique');
        },
      ],
    };
  },

  doOpenFileSelect() {
    this.send('openFileSelect');
  },

  createPoll() {
    return this.store.createRecord('poll', {
      options: [this.createPollOption(), this.createPollOption()], // Array of poll answer strings	Required
      expiresIn: config.APP.DEFAULT_POLL_EXPIRATION, // Duration the poll should be open for in seconds	Required
      // multiple:	// Whether multiple choices should be allowed	Optional
      // hide_totals: //	Whether to hide totals until the poll ends	Optional
    });
  },

  createPollOption() {
    return this.store.createRecord('poll-option');
  },

  createStatusTask: task(function* (event) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }

    set(this, 'serverError', null);
    yield this.changeset.validate();

    if (this.changeset.isValid) {
      try {
        let savedRecord = yield this.changeset.save();

        this.media.reset();

        yield this.statusActions.onStatusCreated.perform(savedRecord);

        tryInvoke(this, 'onStatusCreated', [savedRecord]);

        return savedRecord;
      } catch(e) {
        let errors = this.error.formErrors(e, 'somethingWentWrongTryAgain');
        set(this, 'serverError', errors?.firstObject?.detail);
      }
    }
  }).drop(),
});

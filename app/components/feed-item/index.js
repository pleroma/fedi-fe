import Component from '@glimmer/component';
import { action } from '@ember/object';
import { schedule } from '@ember/runloop';
import { inject as service } from '@ember/service';

export default class extends Component {
  @service features;

  @service inViewport;

  get shouldShowVideoControls() {
    return this.args.feedItem.mediaAttachments &&
      this.args.feedItem.mediaAttachments.length === 1 &&
      this.args.feedItem.mediaAttachments.firstObject.type === 'video';
  }

  get showOriginalAspectRatio() {
    return this.args.feedItem.mediaAttachments &&
      this.args.feedItem.mediaAttachments.length === 1 &&
      this.args.feedItem.mediaAttachments.firstObject.pleroma.playable;
  }

  get showLinkPreviewCardImage() {
    let { feedItem } = this.args;

    return feedItem?.card?.image && !feedItem.sensitive;
  }

  @action
  setUpViewportNotifier(element) {
    if (!this.features.isEnabled('hideUnseenFeedItems')) {
      return;
    }

    let { feedItem } = this.args;

    let viewportTolerance = {
      top: 3000,
      bottom: 3000,
    };

    let { onEnter, onExit } =
      this.inViewport.watchElement(element, {
        viewportSpy: true,
        viewportTolerance,
      });

    onEnter(() => {
      if (feedItem.visibilityState) {
        schedule('render', () => {
          feedItem.visibilityState.show();
        });
      }
    });

    onExit(() => {
      if (feedItem.visibilityState) {
        schedule('render', () => {
          feedItem.visibilityState.hide();

          schedule('afterRender', () => {
            let { height } = document.defaultView.getComputedStyle(element, null);
            feedItem.visibilityState.setHeight(height);
          });
        });
      }
    });
  }

  @action
  tearDownViewportNotifier(element) {
    if (!this.features.isEnabled('hideUnseenFeedItems')) {
      return;
    }

    this.inViewport.stopWatching(element);
  }
}

import Component from '@glimmer/component';
import { dropTask } from 'ember-concurrency-decorators';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import Poller from 'pleroma-pwa/utils/poller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class FeedItemPollComponent extends Component {
  @service store;

  longPoller = null;

  @dropTask*
  vote(optionIndex) {
    yield tryLinkedTask(this.args.vote, this.args.feedItem.poll, optionIndex);
  }

  @action
  startLongPoll() {
    if (!this.longPoller) {
      this.longPoller = new Poller(() => {
        let pollRecord = this.store.peekRecord('poll', this.args.feedItem.poll.id);
        pollRecord && pollRecord.reload();
      });
    }

    this.longPoller.start();
  }

  @action
  stopLongPoll() {
    this.longPoller && this.longPoller.stop();
  }

  willDestroy() {
    this.stopLongPoll();
  }
}

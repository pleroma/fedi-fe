import Component from '@ember/component';
import { getOwner } from '@ember/application';
import { computed, set } from '@ember/object';
import { tryInvoke } from '@ember/utils';
import { inject as service } from '@ember/service';
import {
  A11y,
  Keyboard,
  Lazy,
  Navigation,
  Pagination,
  Swiper,
} from 'swiper/js/swiper.esm.js';

export default class AttachmentGalleryModalGallery extends Component {
  @service intl;

  tagName = '';

  subject = null;

  attachments = null;

  centerAttachmentId = null;

  onCenterChanged = null;

  swiperComponentInstance = null;

  constructor() {
    super(...arguments);
    let appRoute = getOwner(this).lookup('route:application');
    set(appRoute, 'preventCreateStatusClose', true);
  }

  willDestroy() {
    let appRoute = getOwner(this).lookup('route:application');
    set(appRoute, 'preventCreateStatusClose', false);
  }

  @computed('attachments', 'centerAttachmentId')
  get initialSlideIndex() {
    return this.attachments && this.attachments.reduce((memo, attachment, index) => {
      if (attachment.id === this.centerAttachmentId) {
        memo = index;
      }

      return memo;
    }, null);
  }

  handleOnChange() {
    let newAttachment = this.attachments.objectAt(this.swiperComponentInstance.realIndex);
    tryInvoke(this, 'onCenterChanged', [this.subject.id, newAttachment.id, this.subject.constructor.modelName]);
  }

  didInsertSwiper(swiperContainerElement) {
    Swiper.use([
      A11y,
      Keyboard,
      Lazy,
      Navigation,
      Pagination,
    ]);

    set(this, 'swiperComponentInstance',  new Swiper(swiperContainerElement, {
      spaceBetween: 30,
      loop: this.attachments.length > 1,
      initialSlide: this.initialSlideIndex,

      grabCursor: true,
      autoHeight: true,

      containerModifierClass: 'status-gallery-',
      slideClass: 'status-gallery__slide',
      slideActiveClass:	'status-gallery__slide--active',
      slideDuplicateActiveClass: 'status-gallery__slide--duplicate--active',
      slideVisibleClass: 'status-gallery__slide--visible',
      slideDuplicateClass: 'status-gallery__slide--duplicate',
      slideNextClass: 'status-gallery__slide--next',
      slideDuplicateNextClass: 'status-gallery__slide--duplicate--next',
      slidePrevClass: 'status-gallery__slide--previous',
      slideDuplicatePrevClass: 'status-gallery__slide--duplicate--previous',
      wrapperClass: 'status-gallery__wrapper',

      navigation: {
        nextEl: '.status-gallery__button--next',
        prevEl: '.status-gallery__button--previous',
        disabledClass: '.status-gallery__button--disabled',
        hiddenClass: '.status-gallery__button--hidden',
      },

      pagination: {
        el: '.status-gallery__pagination',
        type: 'fraction',
        hiddenClass: '.status-gallery__pagination--hidden',
        lockClass: '.status-gallery__pagination--locked',
      },

      keyboard: {
        enabled: true,
      },

      lazy: {
        loadPrevNext: true,
        elementClass: 'status-gallery__slide__image',
        loadingClass:	'status-gallery__slide__image--loading',
        loadedClass: 'status-gallery__slide__image--loaded',
        preloaderClass: 'status-gallery__slide__preloader',
      },

      a11y: {
        prevSlideMessage: this.intl.t('previousAttachment'),
        nextSlideMessage: this.intl.t('nextAttachment'),
        firstSlideMessage: this.intl.t('firstAttachment'),
        lastSlideMessage: this.intl.t('lastAttachment'),
        notificationClass: 'status-gallery__notification',
      },
    }));

    this.swiperComponentInstance.on('slideChangeTransitionEnd', () => { this.handleOnChange(); });

    this.swiperComponentInstance.on('touchStart', (event) => tryInvoke(this, 'onTouchStart', [event]));
    this.swiperComponentInstance.on('touchEnd', (event) => tryInvoke(this, 'onTouchEnd', [event]));
    this.swiperComponentInstance.on('click', (event) => {
      if (event.target.getAttribute('data-close-on-click')) {
        event.preventDefault();
        event.stopPropagation();

        // Artificial delay to prevent long tap from triggering another click
        // under the overlay (e.g., opening the same image again)
        setTimeout(() => {
          tryInvoke(this, 'onClose', [event]);
        }, 50);
      }
    });

    this.swiperComponentInstance.on('lazyImageReady', () => {
      this.swiperComponentInstance.updateAutoHeight();
    });
  }

  willDestroySwiper() {
    this.swiperComponentInstance.destroy(true);
    set(this, 'swiperComponentInstance', null);
  }
}

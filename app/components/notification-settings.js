import Component from '@ember/component';
import { assert } from '@ember/debug';
import { inject as service } from '@ember/service';
import { validatePresence } from 'ember-changeset-validations/validators';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { dropTask } from 'ember-concurrency-decorators';
import { action, get, set, getProperties } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { isPresent, isBlank, typeOf } from '@ember/utils';
import isEqual from 'lodash.isequal';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';

export default class NotificationSettingsComponent extends Component {
  @service error;

  @service intl;

  @tracked changeset;

  @tracked settings = {};

  @tracked fromEveryone;

  validations = {
    blockFromStrangers: validatePresence(true),
    hideNotificationContents: validatePresence(true),
  };

  save() {}

  @action
  initChangeset(settings) {
    if (isPresent(settings) && ['instance', 'object'].includes(typeOf(settings))) {
      this.settings = settings;
    }

    // Assert correct settings data is present.
    let errorMessage = 'Must pass valid notification settings data';

    assert(errorMessage, this.properties.length !== 0);

    for (let property of this.properties) {
      assert(`${errorMessage}: ${property}`, isPresent(this.settings[property]));
    }

    // Initialize changeset.
    this.changeset = new Changeset(
      this.settings,
      lookupValidator(this.validations),
      this.validations,
      { skipValidate: true },
    );

    this.fromEveryone = this.notificationsAreOnFromEveryone;
  }

  get properties() {
    return Object.keys(this.settings);
  }

  get noChanges() {
    if (!this.changeset) {
      return true;
    }

    let changesetData = getProperties(this.changeset, this.properties);
    let settingsData = getProperties(this.settings, this.properties);

    return isEqual(changesetData, settingsData);
  }

  @dropTask*
  saveSettings(event) {
    event.stopPropagation();
    event.preventDefault();

    yield this.changeset.validate();

    if (this.changeset.isValid) {
      try {
        yield this.changeset.execute();

        let data = getProperties(this.changeset, this.properties);

        yield tryLinkedTask(this.save, data);

        return data;
      } catch (err) {
        let errors = this.error.formErrors(err, 'errorSavingNotificationSettings');

        errors.forEach(({ source, detail }) => {
          this.changeset.pushErrors(source, detail);
        });
      }
    }
  }

  @action
  toggle(flag) {
    let currentValue = get(this.changeset, flag);

    if (isBlank(currentValue)) {
      return;
    }

    set(this.changeset, flag, !get(this.changeset, flag));
  }
}

import Component from '@glimmer/component';

export default class extends Component {
  get shouldShow() {
    let { status } = this.args;

    if (!status.visibilityState) {
      // if not a status, return true
      return true;
    }

    return status.visibilityState.shouldShow;
  }
}

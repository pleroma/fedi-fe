import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { keepLatestTask } from 'ember-concurrency-decorators';
import { validatePresence } from 'ember-changeset-validations/validators';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { action, getProperties } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import isEqual from 'lodash.isequal';

export default class extends Component {
  @service error;

  @service intl;

  @service window;

  @tracked changeset;

  @tracked data = {};

  validations = {
    password: [
      validatePresence({
        presence: true,
        message: this.intl.t('youMustEnterCurrentPassword'),
      }),
    ],
  };

  dataAttributes() {
    return [
      'password',
    ];
  }

  defaultData() {
    return {
      password: '',
    };
  }

  saveData() {
    let saveData = getProperties(
      this.data,
      this.dataAttributes(),
    );

    return saveData;
  }

  @action
  reset() {
    this.data = this.defaultData();
  }

  constructor() {
    super(...arguments);
    this.reset();
  }

  get noChanges() {
    return isEqual(this.defaultData(), this.saveData());
  }

  @keepLatestTask*
  submit(event) {
    event.stopPropagation();
    event.preventDefault();

    let data = this.saveData();

    this.changeset = new Changeset(
      data,
      lookupValidator(this.validations),
      this.validations,
      { skipValidate: true },
    );

    yield this.changeset.validate();

    if (!this.changeset.isValid) {
      return;
    }

    if (!this.window.confirm(this.intl.t('confirmDeleteAccount'))) {
      return;
    }

    try {
      yield tryLinkedTask(this.args.remove, data);

      this.reset();

      return yield data;
    } catch (err) {
      let errors = this.error.formErrors(err, 'errorDeletingAccount');

      errors.forEach(({ source, detail }) => {
        this.changeset.pushErrors(source, detail);
      });
    }
  }
}

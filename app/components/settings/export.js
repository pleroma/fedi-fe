import config from 'pleroma-pwa/config/environment';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action, set } from '@ember/object';
import { next } from '@ember/runloop';
import { keepLatestTask } from 'ember-concurrency-decorators';
import { inject as service } from '@ember/service';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import { Promise } from 'rsvp';

export default class extends Component {
  @service error;

  @service session;

  errors;

  form;

  filename = 'export.csv';

  @tracked processing = false;

  constructor() {
    super(...arguments);
    this.reset();

    if (this.args.filename) {
      this.filename = this.args.filename;
    }
  }

  @action
  handleDidInsert(form) {
    this.form = form;
  }

  @action
  reset() {
    set(this, 'errors', []);
  }

  @keepLatestTask*
  submit(event) {
    event.stopPropagation();
    event.preventDefault();

    try {
      let data = yield tryLinkedTask(this.args.export, this.args.params);

      return data;
    } catch (err) {
      let errors = this.error.formErrors(err, 'errorTryAgain');
      set(this, 'errors', errors);
    }
  }

  @action
  download(users) {
    let fullScreenNames = users.map(u => u.names.fullScreenName);
    let content = fullScreenNames.join('\n');

    let link = document.createElement('a');
    link.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(content)}`);
    link.setAttribute('download', this.filename);
    link.style.display = 'none';

    if (config.environment === 'test') {
      link.setAttribute('data-test-export-count', users.length);
    }

    this.form.appendChild(link);

    return new Promise((resolve) => {
      next(() => {
        if (config.environment !== 'test') {
          link.click();
        }

        resolve(users);
      });

      this.processing = true;

      // Not using `later` here, so the test can go ahead and assert attribute
      // and not wait for run loop to be empty.
      setTimeout(() => {
        this.form.removeChild(link);
        this.processing = false;
      }, 2000);
    });
  }
}

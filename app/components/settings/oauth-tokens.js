import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action, set } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class extends Component {
  @service error;

  @service intl;

  @tracked changeset;

  tokens = [];

  @action
  async reset() {
    let { user } = this.args;

    if (!user.id) {
      throw new Error(this.intl.t('pleaseProvideValidUser'));
    }

    let tokens = await user.load('tokens');

    set(this, 'tokens', tokens);

    return tokens;
  }

  @action
  showDate() {
    return true;
  }

  constructor() {
    super(...arguments);
    this.reset();
  }
}

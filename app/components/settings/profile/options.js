import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { keepLatestTask } from 'ember-concurrency-decorators';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { action, getProperties } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import isEqual from 'lodash.isequal';

export default class extends Component {
  @service error;

  @service intl;

  @tracked changeset;

  @tracked data = {};

  validations = {};

  dataAttributes() {
    return [
      'hideFavorites',
      'locked',
      'hideFollows',
      'hideFollowsCount',
      'hideFollowers',
      'hideFollowersCount',
      'defaultScope',
    ];
  }

  defaultData() {
    let defaultData = getProperties(
      this.args,
      this.dataAttributes(),
    );

    return defaultData;
  }

  saveData() {
    let saveData = getProperties(
      this.changeset,
      this.dataAttributes(),
    );

    return saveData;
  }

  @action
  afterToggle() {
    if (this.changeset.hideFollows === false) {
      this.changeset.hideFollowsCount = false;
    }

    if (this.changeset.hideFollowers === false) {
      this.changeset.hideFollowersCount = false;
    }
  }

  @action
  reset() {
    this.data = this.defaultData();

    this.changeset = new Changeset(
      this.data,
      lookupValidator(this.validations),
      this.validations,
      { skipValidate: true },
    );
  }

  constructor() {
    super(...arguments);
    this.reset();
  }

  get noChanges() {
    return isEqual(this.defaultData(), this.saveData());
  }

  @keepLatestTask*
  submit(event) {
    event.stopPropagation();
    event.preventDefault();

    yield this.changeset.validate();

    if (!this.changeset.isValid) {
      return;
    }

    try {
      let data = this.saveData();

      yield tryLinkedTask(this.args.save, data);

      this.reset();

      return yield data;
    } catch (err) {
      let errors = this.error.formErrors(err, 'errorTryAgain');

      errors.forEach(({ source, detail }) => {
        this.changeset.pushErrors(source, detail);
      });
    }
  }
}

import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { keepLatestTask } from 'ember-concurrency-decorators';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { action } from '@ember/object';
import { run, next, later } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import { Promise, reject } from 'rsvp';
import { arg } from 'ember-arg-types';
import { bool } from 'prop-types';
import Cropper from 'cropperjs';
import wait from 'wait-promise';

export default class extends Component {
  @service error;

  @service intl;

  @tracked changeset;

  @tracked cropper = null;

  @tracked currentState = 'inactive';

  @tracked dataUrl = null;

  @arg(bool)
  crop = false;

  validations = {};

  saveData() {
    let { files } = this.fileInput;

    if (files.length === 0) {
      throw new Error(this.intl.t('pleaseUploadValidFile'));
    }

    // Only one file upload supported.
    let avatar = files.item(0);

    return { avatar };
  }

  @action
  reset() {
    if (this.cropper) {
      this.cropper.destroy();
    }

    this.cropper = null;
    this.currentState = 'inactive';
    this.dataUrl = null;

    if (this.fileInput) {
      this.fileInput.value = '';
    }

    this.changeset = new Changeset(
      {},
      lookupValidator(this.validations),
      this.validations,
      { skipValidate: true },
    );
  }

  constructor() {
    super(...arguments);
    this.reset();
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.reset();
  }

  @action
  select() {
    let { avatar } = this.saveData();

    this.currentState = 'selected';

    return { avatar };
  }

  @action
  async openCropModal({ avatar }) {
    this.setUpCropButton.click();

    await run(async () => {
      await wait.before(6000).till(() => {
        return this.cropImage?.nodeName === 'IMG';
      });
    });

    return { avatar };
  }

  @action
  async setUpCrop({ avatar }) {
    await new Promise((resolve) => {
      let reader = new FileReader();

      reader.onload = (ev) => {
        this.dataUrl = ev.target.result;
        next(() => resolve());
      };

      reader.readAsDataURL(avatar);
    });

    return new Promise((resolve) => {
      next(() => {
        this.cropper = new Cropper(this.cropImage, {
          aspectRatio: 1,
          autoCropArea: 1,
          viewMode: 1,
          movable: false,
          zoomable: false,
          guides: false,
        });

        this.currentState = 'cropping';

        resolve();
      });
    });
  }

  @action
  getCroppedImage() {
    let { avatar } = this.saveData();

    let { cropper } = this;

    if (!cropper) {
      return reject(new Error(this.intl.t('pleaseUploadValidFile')));
    }

    return new Promise((resolve) => {
      cropper
        .getCroppedCanvas()
        .toBlob(
          (blob) => resolve(blob),
          avatar.type,
        );
    });
  }

  @action
  async saveCrop() {
    let avatar = await this.getCroppedImage();

    return { avatar };
  }

  @keepLatestTask*
  submit({ avatar }) {
    try {
      yield tryLinkedTask(this.args.save, avatar);

      return yield avatar;
    } catch (err) {
      let errors = this.error.formErrors(err, 'errorTryAgain');

      errors.forEach(({ source, detail }) => {
        this.changeset.pushErrors(source, detail);
      });

      this.currentState = 'error';

      later(() => this.reset(), 6000);
    }
  }
}

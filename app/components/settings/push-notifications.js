import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class SettingsPushNotificationsComponent extends Component {
  @service() pushNotifications;

  @tracked initialSettings;

  @tracked follow;

  @tracked favourite;

  @tracked reblog;

  @tracked mention;

  @tracked poll;

  constructor(...args) {
    super(...args);

    this.reset();
  }

  @action
  reset() {
    this.follow = this.args.subscription?.alerts?.follow;
    this.favourite = this.args.subscription?.alerts?.favourite;
    this.reblog = this.args.subscription?.alerts?.reblog;
    this.mention = this.args.subscription?.alerts?.mention;
    this.poll = this.args.subscription?.alerts?.poll;

    this.initialSettings = {
      follow: this.args.subscription?.alerts?.follow,
      favourite: this.args.subscription?.alerts?.favourite,
      reblog: this.args.subscription?.alerts?.reblog,
      mention: this.args.subscription?.alerts?.mention,
      poll: this.args.subscription?.alerts?.poll,
    };
  }

  get isPristine() {
    return Object.keys(this.initialSettings)
      .every((initialSetting) => this.initialSettings[initialSetting] === this[initialSetting]);
  }
}

import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action, set } from '@ember/object';
import { keepLatestTask } from 'ember-concurrency-decorators';
import { inject as service } from '@ember/service';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import uuid from 'uuid/v4';

export default class extends Component {
  @service error;

  @service intl;

  @service session;

  errors;

  fileInputElement;

  @tracked fileName = null;

  @tracked fileChosen = false;

  @tracked id = null;

  inputInterval;

  @tracked processing = false;

  constructor() {
    super(...arguments);
    this.reset();
  }

  get importId() {
    return `settings-import-${this.id}`;
  }

  @action
  onFileInputInsert(el) {
    this.fileInputElement = el;

    this.inputInterval = setInterval(() => {
      let length = this.fileInputElement?.files?.length || 0;
      this.fileChosen = length > 0;

      if (length > 0) {
        let file = this.fileInputElement.files[0];
        this.fileName = file.name;
      }
    }, 600);
  }

  @action
  onFileInputDestroy() {
    clearInterval(this.inputInterval);
  }

  @action
  reset() {
    this.id = uuid();

    set(this, 'errors', []);

    if (this.fileInputElement) {
      this.fileInputElement.value = '';
    }

    this.fileChosen = false;
    this.fileName = null;
  }

  @keepLatestTask*
  submit(event) {
    event.stopPropagation();
    event.preventDefault();

    try {
      let { files } = this.fileInputElement;

      if (files.length === 0) {
        throw new Error(this.intl.t('pleaseUploadValidFile'));
      }

      // Only one file upload supported.
      let file = files.item(0);

      yield tryLinkedTask(this.args.import, file);

      return file;
    } catch (err) {
      let errors = this.error.formErrors(err, 'errorTryAgain');
      set(this, 'errors', errors);
    }
  }
}

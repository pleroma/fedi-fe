import Component from '@glimmer/component';

export default class PreviewMediaAttachments extends Component {
  toggleVideo(videoElement) {
    if(videoElement) {
      if(videoElement.paused) {
        videoElement.play();
      } else {
        videoElement.pause();
      }
    }
  }
}

import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { htmlSafe } from '@ember/string';

export default class BodyBackgroundImage extends Component {
  @service('-document') documentService;

  constructor(...args) {
    super(...args);

    if (this.args.url) {
      this.documentService.body.style.backgroundImage = htmlSafe(`url('${this.args.url}')`);
    }
  }

  willDestroy() {
    this.documentService.body.style.backgroundImage = htmlSafe('none');
  }
}

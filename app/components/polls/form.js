import Component from '@glimmer/component';
import { action } from '@ember/object';
import config from 'pleroma-pwa/config/environment';
import { tryInvoke } from '@ember/utils';
import { durationFromSeconds } from 'pleroma-pwa/utils/duration';

export default class PollsFormComponent extends Component {
  constructor(...args) {
    super(...args);
    this.duration = this.defaultDuration();
  }

  defaultDuration() {
    let minExpiration = (this.args.minExpiration > config.APP.DEFAULT_POLL_EXPIRATION) ? this.args.minExpiration : config.APP.DEFAULT_POLL_EXPIRATION;
    let duration = durationFromSeconds(minExpiration);

    // Were dealing with strings here, since these are used in a <select
    return {
      days: duration.days.toString(),
      hours: duration.hours.toString(),
      minutes: duration.minutes.toString(),
    }
  }

  @action
  updatePollExpiration() {
    let minute = 60;
    let hour = 60 * minute;
    let day = 24 * hour;

    let expiresIn = Number(this.duration.days) * day
      + Number(this.duration.hours) * hour
      + Number(this.duration.minutes) * minute;

    tryInvoke(this.args, 'updatePollExpiration', [expiresIn]);
  }
}

import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class extends Component {
  @service router;

  @action
  intercept(e) {
    let { target } = e;
    let parent = target.parentNode;

    let link;

    if (target.nodeName === 'A') {
      link = target;
    }

    if (!link && parent?.nodeName === 'A') {
      link = parent;
    }

    if (link && link.classList.contains('linkified')) {
      e.stopPropagation();
      e.preventDefault();

      let { location: { origin } } = window;
      let { href } = link;

      if (href.startsWith(origin)) {
        href = href.slice(origin.length);
      }

      this.router.transitionTo(href);
    }
  }

  @action
  handleDidInsert() {
    document.addEventListener('click', this.intercept, false);
  }

  @action
  handleWillDestroy() {
    document.removeEventListener('click', this.intercept);
  }
}

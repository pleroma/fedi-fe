import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { next } from '@ember/runloop';
import { isEmpty } from '@ember/utils';
import {
  bindKeyboardShortcuts,
  unbindKeyboardShortcuts,
} from 'ember-keyboard-shortcuts';

export default class extends Component {
  nativeScrollable;

  @service emojiPicker;

  constructor() {
    super(...arguments);

    this.emojiPicker.reset();
  }

  get filteredEmojis() {
    let { filteredEmojis } = this.emojiPicker;

    // `exclude` expected to be an array of emoji character strings.
    if (!isEmpty(this.args.exclude)) {
      filteredEmojis = filteredEmojis.filter(emoji => {
        return !this.args.exclude.includes(emoji.value);
      });
    }

    return filteredEmojis;
  }

  get offsetTop() {
    if (this.args.offsetTop) {
      return parseInt(this.args.offsetTop);
    }

    return 0;
  }

  get shouldFocusAfterRender() {
    if (this.args.shouldFocusAfterRender) {
      return !!this.args.shouldFocusAfterRender;
    }

    return false;
  }

  @action
  maybeFocusAfterRender(input) {
    if (this.shouldFocusAfterRender) {
      next(() => input.focus());
    }
  }

  @action
  bindKeyboardShortcuts(input) {
    input.keyboardShortcuts = {
      'enter': {
        action: () => this.emojiPicker.chooseIfOnlyOrExactMatch.perform(),
        targetElement: input,
      },
    };

    bindKeyboardShortcuts(input);
  }

  @action
  unbindKeyboardShortcuts(input) {
    unbindKeyboardShortcuts(input);
  }

  @action
  findNativeScrollable(el) {
    let emberViewEls = el.querySelectorAll('.ember-view');

    for (let viewEl of emberViewEls) {
      if (viewEl.style.position === 'absolute') {
        this.nativeScrollable = viewEl;
        this.nativeScrollableChange(this.nativeScrollable);
      }
    }
  }

  nativeScrollableChange(el) {
    el.style.top = `${this.offsetTop}px`;
  }
}

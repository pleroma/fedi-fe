import Component from '@glimmer/component';
import { action } from '@ember/object';
import { scheduler, Token } from 'ember-raf-scheduler';
import scrollToBottom from 'pleroma-pwa/utils/scroll-to-bottom';
import { tracked } from '@glimmer/tracking';

export default class DirectMessagesFeedComponent extends Component {
  polyfillSentinalIsInViewport = null;

  @tracked didInitialScroll = null;

  constructor(...args) {
    super(...args);

    this.token = new Token();
  }

  // https://blog.eqrion.net/pin-to-bottom/
  // https://caniuse.com/#search=overflow-anchor
  get browserNeedsOverflowAnchorPolyfill() {
    return !(CSS.supports('( overflow-anchor: none; )') || 'overflowAnchor' in document.body.style);
  }

  @action
  didInsertFeed(selector) {
    scheduler.schedule('measure', () => {
      let element = document.querySelector(selector);
      scrollToBottom(element);

      this.didInitialScroll = true;
    }, this.token);
  }

  @action
  willDestroyFeed() {
    scheduler.forget(this.token);
  }

  @action
  maybeScrollToBottom(selector) {
    if (this.browserNeedsOverflowAnchorPolyfill && this.polyfillSentinalIsInViewport) {
      scheduler.schedule('measure', () => {
        let element = document.querySelector(selector);
        scrollToBottom(element);
      }, this.token);
    }
  }
}

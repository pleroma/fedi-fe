import Component from '@glimmer/component';
import { action } from '@ember/object';
import { restartableTask } from 'ember-concurrency-decorators';
import { inject as service } from '@ember/service';
import { isBlank } from '@ember/utils';
import { timeout } from 'ember-concurrency';
import { A } from '@ember/array';
import { runTask, runDisposables } from 'ember-lifeline';
import { all } from 'rsvp';

export default class DirectMessageChooseRecipientsForm extends Component {
  @service() store;

  @service() router;

  @service() session;

  @service() toast;

  @service() intl;

  participants = A();

  get directMode() {
    if (this.args.directMode) {
      return this.args.directMode;
    }

    return 'classic';
  }

  get initialLinks() {
    if (this.args.initialLinks) {
      return this.args.initialLinks;
    }

    return null;
  }

  @action
  async startDirectMessage() {
    if (this.directMode === 'classic') {
      return this.router.transitionTo('message.compose', {
        queryParams: {
          participants: this.participants.mapBy('id'),
          links: this.initialLinks,
        },
      });
    } else if (this.directMode === 'chat') {
      // TODO: Only one user allowed for now.
      let [participant] = this.participants;

      return this.router.transitionTo('message.chat', participant.id, {
        queryParams: {
          participants: this.participants.mapBy('id'),
          links: this.initialLinks,
        },
      });
    }
  }

  @action
  addParticipant(user) {
    if (user.id === this.session.currentUser.id) { return; }
    this.participants.pushObject(user);
  }

  @action
  removeParticipant(user) {
    this.participants.removeObject(user);
  }

  @action
  toggleParticipant(user) {
    if (this.participants.includes(user)) {
      this.removeParticipant(user);
    } else {
      this.addParticipant(user);
    }
  }

  @action
  handleDidInsert() {
    if (this.args.initialParticipants) {
      runTask(this, () => this.hydrateInitialParticipants.perform());
    }
  }

  @action
  fetchConversations() {
    runTask(this, () => this.recentConversations.perform(this.directMode));
  }

  @action
  handleWillDestroy() {
    runDisposables(this);
  }

  @action
  resumeConversation(conversation) {
    if (this.directMode === 'classic') {
      return this.router.transitionTo('message.details', conversation.id, {
        queryParams: {
          links: this.initialLinks,
        },
      });
    } else if (this.directMode === 'chat') {
      // TODO: Fix participants when more than 1:1 chats are allowed.
      return this.router.transitionTo('message.chat', conversation.account.id, {
        queryParams: {
          // participants: this.participants.mapBy('id'),
          links: this.initialLinks,
        },
      });
    }
  }

  @restartableTask()*
  search(query) {
    if (isBlank(query)) { return { results: [], query }; }

    yield timeout(600);

    try {
      let searchResult = yield this.store.queryRecord('search-result', {
        q: query,
        resolve: true,
        limit: 10,
        type: 'accounts',
      });

      let results = searchResult.accounts.rejectBy('id', this.session.currentUser.id);

      return {
        results,
        query,
      };
    } catch (e) {
      this.toast.notify({ message: e.error || this.intl.t('somethingWentWrongTryAgain'), type: 'error' });
    }
  }

  @restartableTask()*
  recentConversations(directMode) {
    if (directMode === 'classic') {
      return yield this.store.loadRecords('conversation', {
        limit: 10,
      });
    } else if (directMode === 'chat') {
      return yield this.store.loadRecords('chat', {
        limit: 10,
      }).then(chats => {
        return chats.filterBy('lastMessage');
      });
    }
  }

  @restartableTask()*
  hydrateInitialParticipants() {
    let participants = yield all(this.args.initialParticipants.map((participantId) => this.store.loadRecord('user', participantId)));

    participants.forEach(this.toggleParticipant);
  }
}

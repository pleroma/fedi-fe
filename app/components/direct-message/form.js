import Component from '@glimmer/component';
import { set, action, computed } from '@ember/object';
import { dropTask } from 'ember-concurrency-decorators';
import { validateLength } from 'ember-changeset-validations/validators';
import validateStatusContent from 'pleroma-pwa/validators/status-content';
import lookupValidator from 'ember-changeset-validations';
import Changeset from 'ember-changeset';
import { inject as concern } from 'ember-concerns';
import { inject as service } from '@ember/service';
import { bindKeyboardShortcuts, unbindKeyboardShortcuts } from 'ember-keyboard-shortcuts';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';
import scrollToBottom from 'pleroma-pwa/utils/scroll-to-bottom';
import { next } from '@ember/runloop';
import autosize from 'autosize';

// How do direct messages work:
// https://docs-develop.pleroma.social/backend/API/differences_in_mastoapi_responses/#post-apiv1statuses
// The 1st status posted uses the `to` field, a way to specify recipients WITHOUT shoving them into the message body as mentions.
// A list of nicknames (like lain@soykaf.club or lain on the local server) that will be used to determine who is going to be addressed by this post. Using this will disable the implicit addressing by mentioned names in the status body, only the people in the to list will be addressed. The normal rules for for post visibility are not affected by this and will still apply.
// The 2nd status posted uses the `in_reply_to_conversation_id`, a way to reply to a conversation and not add new recipients

export default class DirectMessageFormComponent extends Component {
  @service() emojiPicker;

  @service() instances;

  @service() intl;

  @service() store;

  @service() router;

  @service() toast;

  @service() feeds;

  @service() statusActions;

  @concern('status/media') media;

  validations = {
    content: [
      validateStatusContent({
        message: this.intl.t('yourPostMustHaveContent'),
      }),
      validateLength({
        max: this.statusCharacterLimit,
        allowBlank: true,
        message: this.intl.t('yourPostIsTooLong', { max: this.statusCharacterLimit }),
      }),
    ],
  };

  keyboardShortcuts = {
    'command+enter': () => this.createDirectMessageTask.perform(),
    'ctrl+enter': () => this.createDirectMessageTask.perform(),
  };

  get directMode() {
    if (this.args.directMode) {
      return this.args.directMode;
    }

    return 'chat';
  }

  @computed('instances.current.maxTootChars')
  get statusCharacterLimit() {
    return this.instances.current?.maxTootChars;
  }

  reset() {
    let content = null;

    // Todo: This will use short-codes eventually, for now, just slap them on the front of the message.
    if (this.args.initialLinks) {
      content = '';
      this.args.initialLinks.forEach((link) => {
        content += `${link} `;
      });

      next(() => {
        if (this.messageBodyInput) {
          autosize.update(this.messageBodyInput);
        }
      });
    }

    set(this, 'changeset', new Changeset(this.store.createRecord('status', {
      mediaIds: [],
      visibility: 'direct',
      ...this.args.conversation && { inReplyToConversationId: this.args.conversation.id },
      content,
      ...(!this.args.conversation && this.args.participants) && { to: this.args.participants.mapBy('acct') },
    }), lookupValidator(this.validations), this.validations, { skipValidate: true }));
  }

  constructor() {
    super(...arguments);
    this.reset();
  }

  get chatId() {
    return this.args.chat?.id;
  }

  @dropTask()*
  createDirectMessageTask(event) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }

    this.changeset.rollbackInvalid('mediaIds');

    yield this.changeset.validate();

    if (this.changeset.isValid) {
      if (this.directMode === 'chat') {
        let message = yield tryLinkedTask(this.args.createDirectMessage, this.chatId, this.changeset);

        this.reset();

        // TODO: This is too imperative and might not be good enough to handle images loading async.
        // This was added in order to attempt to keep that feed scrolled down while adding new things.
        // Lets remove this when we fix: ch2826 (re-structure the DOM so we dont need this)
        let feedContainer = document.querySelector('[data-direct-message-feed]');
        feedContainer && next(() => scrollToBottom(feedContainer));

        // actions to take after successful
        this.media.reset();
        this.handleFocus();

        return message;
      } else if (this.directMode === 'classic') {
				let savedStatus = yield tryLinkedTask(this.args.createDirectMessage, this.changeset);

				this.reset();

				// TODO: This is too imperative and might not be good enough to handle images loading async.
				// This was added in order to attempt to keep that feed scrolled down while adding new things.
				// Lets remove this when we fix: ch2826 (re-structure the DOM so we dont need this)
				let feedContainer = document.querySelector('[data-direct-message-feed]');
				feedContainer && next(() => scrollToBottom(feedContainer));

				if (this.args.feed) {
					this.feeds.addItem(this.args.feed.id, savedStatus);
				}

				if (this.args.conversation) {
					set(this.args.conversation, 'lastStatus', savedStatus);
				} else if(savedStatus?.pleroma?.directConversationId) {
					let conversation = yield this.store.loadRecord('conversation', savedStatus.pleroma.directConversationId);

					this.feeds.addItem('direct', conversation);

					this.router.transitionTo('message.details', savedStatus.pleroma.directConversationId);
				}

        // actions to take after successful
        this.media.reset();
        this.handleFocus();

				return savedStatus;
      }
    }
  }

  @action
  addStaticEmoji(staticEmoji) {
    this.emojiPicker.withNewContent(staticEmoji, (newContent) => {
      set(this.changeset, 'content', newContent);
    });
  }

  @action
  handleDidInsert() {
    bindKeyboardShortcuts(this);
  }

  @action
  handleWillDestroy() {
    unbindKeyboardShortcuts(this);
  }

  @action
  addMedia() {
    if (this.fileInputElement) {
      this.fileInputElement.click();
    }
  }

  @action
  handleFocus() {
    if (this.messageBodyInput) {
      next(() => this.messageBodyInput.focus());
    }
  }
}

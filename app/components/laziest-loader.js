import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { dropTask } from 'ember-concurrency-decorators';
import { timeout } from 'ember-concurrency';
import { action } from '@ember/object';
import config from 'pleroma-pwa/config/environment';
import { next } from '@ember/runloop';
import { tryLinkedTask } from 'pleroma-pwa/utils/tasks';

export default class LaziestLoader extends Component {
  @service('in-viewport') inViewport;

  get timeout() {
    let timeout = config.APP.MARK_AS_READ_TIMEOUT;

    if (this.args.timeout) {
      timeout = parseInt(this.args.timeout);
    }

    return timeout;
  }

  @action
  onEnter() {
    this.beLazy.perform();
  }

  @action
  onExit() {
    this.beLazy.cancelAll();
  }

  @dropTask*
  beLazy() {
    if (config.environment === 'test') { return; } // escape hatch for tests

    yield timeout(this.timeout);

    yield tryLinkedTask(this.args.onLoad);

    if (this.args.continuous) {
      next(this, () => this.beLazy.perform());
    }
  }
}

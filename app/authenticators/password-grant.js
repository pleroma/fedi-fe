import OAuth2PasswordGrant from 'ember-simple-auth/authenticators/oauth2-password-grant';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default OAuth2PasswordGrant.extend({
  instances: service(),
  pleromaApi: service(),
  session: service(),

  refreshAccessTokens: false,

  sendClientIdAsQueryParam: true,

  serverTokenEndpoint: computed(function() {
    return this.pleromaApi.endpoints.token;
  }),

  serverTokenRevocationEndpoint: computed(function() {
    return this.pleromaApi.endpoints.revokeToken;
  }),

  async makeRequest(url, data, headers) {
    let originalMakeRequest = this._super;

    await this.instances.ensureCurrent();

    /* eslint-disable camelcase */
    data.client_id = this.instances.current.credentials.clientId;
    data.client_secret = this.instances.current.credentials.clientSecret;
    /* eslint-enable camelcase */

    return originalMakeRequest.call(this, url, data, headers).then(response => {
      // TODO: Properly allow for refreshing access token.
      if (response.expires_in) {
        delete response.expires_in;
      }

      return response;
    });
  },
});

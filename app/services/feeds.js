import Service, { inject as service } from '@ember/service';
import { set, action } from '@ember/object';
import { enqueueTask } from 'ember-concurrency-decorators';
import { all } from 'rsvp';
import { A } from '@ember/array';
import { assert, debug } from '@ember/debug';
import { getOwner } from '@ember/application';

// Feeds service.
// I wanted a special place in the application to manipulate/subscribe to these top level feeds of statuses
// The Service exposes helper methods to manipulate feeds
// If using from a model hook, you will want to `subscribe` to a feed. This will ensure the feed is watched for updates (long-poll, websockets).
// Make sure you unsubscribe!

export default class FeedsService extends Service {
  @service bus;

  @service session;

  @service store;

  feedList = A();

  constructor(...args) {
    super(...args);

    let AllFeed = getOwner(this).lookup('feed:all');
    let ChatsFeed = getOwner(this).lookup('feed:chats');
    let DirectFeed = getOwner(this).lookup('feed:direct');
    let HomeFeed = getOwner(this).lookup('feed:home');
    let NotificationsFeed = getOwner(this).lookup('feed:notifications');
    let NotificationsNewFollowsFeed = getOwner(this).lookup('feed:notifications-new-follows');
    let NotificationsMentionsFeed = getOwner(this).lookup('feed:notifications-mentions');
    let NotificationsRepostsFeed = getOwner(this).lookup('feed:notifications-reposts');
    let NotificationsFavoritesFeed = getOwner(this).lookup('feed:notifications-favorites');
    let PublicFeed = getOwner(this).lookup('feed:public');

    this.registerFeed(AllFeed);
    this.registerFeed(ChatsFeed);
    this.registerFeed(DirectFeed);
    this.registerFeed(HomeFeed);
    this.registerFeed(NotificationsFeed);
    this.registerFeed(NotificationsNewFollowsFeed);
    this.registerFeed(NotificationsMentionsFeed);
    this.registerFeed(NotificationsRepostsFeed);
    this.registerFeed(NotificationsFavoritesFeed);
    this.registerFeed(PublicFeed);

    this.bus.on('muted', (user) => this.onUserMuted(user));

    this.bus.on('unmuted', (user) => this.onUserUnmuted(user));

    this.bus.on('blocked', (user) => this.hideStatusesFromUser(user));

    this.bus.on('unblocked', () => this.refreshAll());

    this.bus.on('followed', ({ id: userId }) => {
      let { id: currentUserId } = this.session.currentUser;
      this.loadNewFromFeed('user-following', [currentUserId]);
      this.loadNewFromFeed('user-followers', [userId]);
    });

    this.bus.on('unfollowed', ({ id: userId }) => {
      let { id: currentUserId } = this.session.currentUser;
      this.refreshFromFeed('user-following', [currentUserId]);
      this.refreshFromFeed('user-followers', [userId]);
    });
  }

  touch() {}

  async loadNewFromFeed(feed, userIds = []) {
    let filteredUserIds = userIds.filter(Boolean);

    let factory = getOwner(this).factoryFor(`feed:${feed}`);

    if (!factory || !factory.class || !factory.class.feedId) {
      return;
    }

    let feedIds = filteredUserIds
      .map(userId => factory.class.feedId(userId));

    for (let feedId of feedIds) {
      try {
        let feed = this.getFeed(feedId);
        debug(`loadNew from ${feedId}`);
        feed.loadNew.perform();
      } catch (_) {
        // ignore
        debug(`could not loadNew for ${feedId}`);
      }
    }
  }

  async refreshFromFeed(feed, userIds = []) {
    let filteredUserIds = userIds.filter(Boolean);

    let factory = getOwner(this).factoryFor(`feed:${feed}`);

    if (!factory || !factory.class || !factory.class.feedId) {
      return;
    }

    let feedIds = filteredUserIds
      .map(userId => factory.class.feedId(userId));

    for (let feedId of feedIds) {
      try {
        debug(`refreshFeed from ${feedId}`);
        this.refreshFeed(feedId);
      } catch (_) {
        // ignore
        debug(`could not refreshFeed for ${feedId}`);
      }
    }
  }

  @action
  addFeed(newFeed) {
    this.feedList.addObject(newFeed);

    // Expose feeds on service so we can dot notation into them from templates.
    set(this, newFeed.id, newFeed);
  }

  @action
  registerFeed(feed) {
    let { id } = feed;

    if (this[id]) {
      // If feed already exists with matching id,
      // Return originally-registered feed.
      return this[id];
    }

    // Otherwise, add feed and return it.
    this.addFeed(feed);

    return feed;
  }

  @action
  async subscribe(feedId) {
    let feed = this.getFeed(feedId);

    await feed.subscribe.perform();

    return feed;
  }

  @action
  unsubscribe(feedId) {
    let feed = this.getFeed(feedId);

    feed.unsubscribe.perform();
  }

  @action
  getFeed(feedId) {
    let feed = this.feedList.findBy('id', feedId);

    assert(`You must pass a feed.id to the feeds service that we know about, you passed ${feedId}`, feed);

    return feed;
  }

  @action
  async loadMore(feedId) {
    let feed = this.getFeed(feedId);

    await feed.loadMore.perform();

    return feed;
  }

  @action
  addItem(feedId, item) {
    this.addItems(feedId, [item]);
  }

  @action
  addItems(feedId, items) {
    let feed = this.getFeed(feedId);

    feed.addItems(items);
  }

  @action
  addNewItem(feedId, item) {
    let feed = this.getFeed(feedId);

    feed.addNewItems([item]);
  }

  removeItem(feedId, item) {
    let feed = this.getFeed(feedId);

    feed.removeItem(item);
  }

  @action
  removeItemFromAllFeeds(item) {
    this.feedList.forEach((feed) => this.removeItem(feed.id, item));
  }

  @action
  hideStatusesFromUser(user) {
    this.feedList
      .forEach((feed) => {
        let items = feed.content.filter((item) => item.account === user);

        items.forEach((item) => this.removeItem(feed.id, item));
      });
  }

  onUserMuted(user) {
    this.feedList
      .forEach((feed) => {
        if (feed.hidesMutedStatuses) {
          let items = feed.content.filter((item) => {
            if (item.repostOf) {
              return item.repostOf.account === user;
            } else {
              return item.account === user
            }
          });
          items.forEach((item) => this.removeItem(feed.id, item));
        }
      });
  }

  onUserUnmuted(/* user */) {
    this.feedList
      .forEach((feed) => {
        if (feed.hidesMutedStatuses) {
          this.refreshFeed(feed.id);
        }
      });
  }

  onStatusMuted(status) {
    let effectedStatuses = this.getAllStatusesInConversation(status.pleroma.conversationId);

    this.feedList
      .forEach((feed) => {
        if (feed.hidesMutedStatuses) {
          this.removeItem(feed.id, status);

          effectedStatuses.forEach((effectedStatus) => {
            this.removeItem(feed.id, effectedStatus);
          });
        }
      });
  }

  getAllStatusesInConversation(conversationId) {
    let allStatuses = this.store.peekAll('status');
    return allStatuses.filter((status) => status.pleroma.conversationId === conversationId);
  }

  onStatusUnmuted() {
    this.feedList
      .forEach((feed) => {
        if (feed.hidesMutedStatuses) {
          this.refreshFeed(feed.id);
        }
      });
  }

  @action
  refreshAll() {
    return this.doRefreshAll.perform();
  }

  @enqueueTask*
  doRefreshAll() {
    return yield all(this.feedList.map((feed) => this.refreshFeed(feed.id)));
  }

  @action
  syncNewItems(feedId) {
    let feed = this.getFeed(feedId);

    feed.syncNewItems();
  }

  @action
  async refreshFeedMeta(feedId) {
    let feed = this.getFeed(feedId);

    feed.collectFeedMeta.perform();
  }

  async refreshFeed(feedId) {
    try {
      let feed = this.getFeed(feedId);

      let wasSubscribedTo = feed.subscribedTo;

      if (feed.authenticated === true && !this.session.isAuthenticated) {
        return [];
      }

      await this.unsubscribe(feedId);

      feed.reset();

      this.registerFeed(feed);

      if (wasSubscribedTo) {
        await this.subscribe(feedId);
      }
    } catch(err) {
      debug(err);
    }
  }
}

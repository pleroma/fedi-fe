import Service, { inject as service } from '@ember/service';
import { restartableTask, dropTask, enqueueTask } from 'ember-concurrency-decorators';
import fetch from 'fetch';
import { action } from '@ember/object';
import toUint8Array from 'urlb64touint8array';

export default class PushNotificationsService extends Service {
  @service features;

  @service instances;

  @service intl;

  @service pleromaApi;

  @service session;

  @service toast;

  get browserSupported() { return 'serviceWorker' in navigator && ('PushManager' in window || 'pushManager' in ServiceWorkerRegistration.prototype) }

  get browserPermissionDenied() { return window.Notification.permission === 'denied'; }

  get browserPermissionDefault() { return window.Notification.permission === 'default'; }

  get browserPermissionGranted() { return window.Notification.permission === 'granted'; }

  get vapidPublicKey() {
    return this.instances.current.credentials.vapidKey;
  }

  get isFetchingInitial() {
    return !this.vapidPublicKey || !this.existingSubscriptionTask.last;
  }

  get isSupported() {
    return this.vapidPublicKey && this.browserSupported;
  }

  get isUpdatingSubscription() {
    return this.subscribe.isRunning || this.unsubscribe.isRunning || this.updatePushSubscription.isRunning;
  }

  defaultNotificationSettings = {
    follow: true,
    favourite: true,
    reblog: true,
    mention: true,
    poll: true,
  };

  @dropTask*
  subscribe() {
    if (!this.features.isEnabled('pushNotifications')) { return; }
    if (!this.browserSupported) { return; }
    if (!this.session.isAuthenticated) { return; }

    // Wait for service worker registration
    yield navigator.serviceWorker.ready;

    // Attempt to request permission
    yield window.Notification.requestPermission();

    if (this.browserPermissionGranted) {
      if (!this.existingSubscriptionTask.last) {
        yield this.existingSubscriptionTask.linked().perform();
      }

      if (this.existingSubscriptionTask.last.value) {
        return; // Bail out if a subscription exists
      }

      yield this.updatePushSubscription.linked().perform(this.defaultNotificationSettings, true);

      let notification = new Notification(this.intl.t('approved'), {
        body: this.intl.t('pushNotificationsApproved'),
        icon: this.instances.current.thumbnail,
      });

      setTimeout(() => notification.close(), 1000 * 8);
    }
  }

  @enqueueTask*
  updatePushSubscription(notificationSettings, createSubscription) {
    if (!this.browserSupported) { return; }
    if (!this.session.isAuthenticated) { return; }

    if (!this.existingSubscriptionTask.last) {
      yield this.existingSubscriptionTask.linked().perform();
    }

    if (this.browserPermissionGranted && (createSubscription || this.existingSubscriptionTask.last.value)) {
      let registration = yield navigator.serviceWorker.ready;

      let subscribeOptions = {
        userVisibleOnly: true,
        applicationServerKey: toUint8Array(this.vapidPublicKey),
      };

      let subscription = yield registration.pushManager.subscribe(subscribeOptions);

      yield this.sendSubscriptionToApi(subscription, notificationSettings);

      yield this.existingSubscriptionTask.linked().perform();

      if (!createSubscription) {
        this.toast.notify({ message: this.intl.t('notificationSettingsHaveBeenSaved') });
      }
    }
  }

  @dropTask*
  unsubscribe() {
    if (!this.existingSubscriptionTask.last) {
      yield this.existingSubscriptionTask.perform();
    }

    let subscription = this.existingSubscriptionTask.last.value;

    if (subscription) {
      yield subscription.serviceWorkerSubscription.unsubscribe();

      yield fetch(this.pleromaApi.endpoints.pushSubscription, {
        method: 'DELETE',
        headers: this.session.headers,
      });

      yield this.existingSubscriptionTask.linked().perform();
    }
  }

  @restartableTask*
  existingSubscriptionTask() {
    try {
      let { headers } = this.session;
      let response = yield fetch(this.pleromaApi.endpoints.pushSubscription, { headers });

      let foundSubscription = response.status === 200;
      let apiSubscription = yield response.json();

      let registration = yield navigator.serviceWorker.ready;
      let serviceWorkerSubscription = yield registration.pushManager.getSubscription();

      // only return the browser subscription if the API and the browser agree that there is a subscription
      return foundSubscription && serviceWorkerSubscription && {
        apiSubscription,
        serviceWorkerSubscription,
      };
    } catch (e) {
      console.debug(e);
      return false;
    }
  }

  @action
  toggle() {
    if (this.existingSubscriptionTask.last?.value) {
      return this.unsubscribe.perform();
    } else {
      return this.subscribe.perform();
    }
  }

  async sendSubscriptionToApi(subscription, notificationSettings) {
    let response = await fetch(this.pleromaApi.endpoints.pushSubscription, {
      method: 'POST',
      headers: this.session.headers,
      body: JSON.stringify({
        subscription,

        data: {
          alerts: {
            ...this.defaultNotificationSettings,
            ...notificationSettings,
          },
        },
      }),
    });

    await response.json();
  }
}

import Service, { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { capitalize } from '@ember/string';
import { isPresent, typeOf } from '@ember/utils';
import AdapterError from '@ember-data/adapter/error';
import { resolve, reject } from 'rsvp';

function hasUppercase(string) {
  for (let i = 0; i < string.length; i++) {
    let ch = string.charCodeAt(i);
    if (ch >= 65 && ch <= 90) {
      return true;
    }
  }

  return false;
}

export default class ErrorService extends Service {
  @service intl;

  genericFallbackErrorKey = 'somethingWentWrongTryAgain';

  genericSources = ['generic', 'error'];

  /**
   * Returns an array of error objects with format { source, detail },
   * ready for use in displaying in forms.
   *
   * @public
   */
  formErrors(_input = {}, fallbackErrorKey = this.genericFallbackErrorKey) {
    let errors = [];

    let input = this.formatPayloadErrors(_input, fallbackErrorKey);

    if (input.errors) {
      errors = input.errors.map((error) => {
        let source =
          error.source && !this.genericSources.includes(error.source)
          ? error.source
          : 'generic';

        let detail =
          source === 'generic' || (error.detail && hasUppercase(error.detail[0]))
          ? error.detail
          : `${capitalize(source)} ${error.detail}`;

        return {
          ...error,
          source,
          detail,
        };
      });
    } else if (input.message) {
      errors.push({
        source: 'generic',
        detail: input.message,
      });
    } else if (typeOf(input) === 'string') {
      errors.push({
        source: 'generic',
        detail: input,
      });
    } else {
      errors.push({
        source: 'generic',
        detail: this.intl.t(fallbackErrorKey),
      });
    }

    return errors;
  }

  /**
   * Takes payloads in many forms and attempts to return payload in the form of
   * { errors: [{ source, detail }] }
   *
   * @public
   */
  formatPayloadErrors(payload = {}, fallbackErrorKey = this.genericFallbackErrorKey) {
    let payloadCopy;

    if (typeOf(payload) === 'string' && payload[0] === '{') {
      payloadCopy = JSON.parse(payload);
    }

    if (typeOf(payloadCopy?.error) === 'string') {
      payloadCopy.errors = this.constructErrors(payloadCopy.error);
    }

    if (payload instanceof AdapterError || payload.isAdapterError) {
      let errors;

      if (payload?.errors[0]?.detail[0] === '{') {
        errors = this.constructErrors(payload.errors[0].detail, fallbackErrorKey);
      } else {
        errors = this.constructErrors(payload?.errors, fallbackErrorKey);
      }

      payloadCopy = { errors };
    } else if (payload instanceof Error) {
      let errors = this.constructErrors(payload?.message, fallbackErrorKey);
      payloadCopy = { errors };
    } else {
      payloadCopy = payload;
    }

    if (payloadCopy.error) {
      payloadCopy.errors = this.constructErrors(payloadCopy.error, fallbackErrorKey);
      delete payloadCopy.error;
    }

    // TODO: Lets get the API to return correct errors and attr names
    if (payloadCopy.errors && payloadCopy.errors.ap_id) {
      payloadCopy.errors.username = payloadCopy.errors.ap_id;
      delete payloadCopy.errors.ap_id;
    }

    if (typeOf(payloadCopy.errors) === 'object') {
      payloadCopy.errors = Object.keys(payloadCopy.errors).map((key) => {
        return {
          source: key,
          detail: Array(payloadCopy.errors[key]).flat().join(','),
        };
      });
    }

    if (typeOf(payloadCopy.errors) === 'array') {
      let { errors } = payloadCopy;

      let anyHasMessage = payloadCopy.errors.any(e => isPresent(e.message));
      let anyIsString = payloadCopy.errors.any(e => typeOf(e) === 'string');
      if (anyHasMessage) {
        errors = payloadCopy.errors.map((error) => {
          let source = 'generic';
          let detail = error.message;

          return {
            ...error,
            source,
            detail,
          };
        });
      } else if (anyIsString) {
        errors = payloadCopy.errors.map((error) => {
          let source = 'generic';
          let detail = error;

          return {
            source,
            detail,
          };
        });
      }

      payloadCopy.errors = errors;
    }

    return payloadCopy;
  }

  /**
   * Detect if a response payload has been given an improper success status
   * code. If detected as improper, it is given a generic 400 Bad Request
   * status code.
   *
   * @public
   * @return number
   */
  detectErrantSuccessCode(code, payload) {
    if (payload && (payload.error || payload.errors) && code < 400) {
      // Pretend it's a generic bad request error.
      code = 400;
    }

    return parseInt(code);
  }

  constructErrors(input, fallbackErrorKey = this.genericFallbackErrorKey) {
    let output;

    if (typeOf(input) === 'string') {
      if (input[0] === '{') {
        try {
          output = JSON.parse(input);
        } catch(err) {
          output = [
            {
              source: 'generic',
              detail: this.intl.t(fallbackErrorKey),
            },
          ];
        }
      } else {
        output = [
          {
            source: 'generic',
            detail: input,
          },
        ];
      }
    } else {
      output = input;
    }

    // Special case for that extra special captcha error, comes through like so: "{"captcha":["Invalid CAPTCHA"]}"
    if (output.captcha) {
      output = [
        {
          source: 'captcha',
          detail: output.captcha[0],
        },
      ]
    }

    if (output.errors) {
      return output.errors;
    }

    return output;
  }

  @action
  pipe(value) {
    /* eslint-disable no-console */
    console.debug('error.pipe', 'value', value);
    /* eslint-enable no-console */

    if (value) {
      return resolve(value);
    }

    return reject(value);
  }
}

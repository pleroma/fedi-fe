import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action, set } from '@ember/object';
import { restartableTask } from 'ember-concurrency-decorators';

export default class SearchService extends Service {
  @service router;

  @service store;

  @tracked query;

  @action
  clearQuery() {
    if (!this.isDestroying && !this.isDestroyed) {
      set(this, 'query', '');
    }
  }

  @action
  setQuery(value) {
    set(this, 'query', value);
  }

  @action
  onSubmit(e) {
    e.preventDefault();

    if (this.query === '') {
      return;
    }

    this.router.transitionTo('search.results', this.query);
  }

  searchParameters() {
    return {
      q: this.query,
      resolve: true,
      limit: 40,
    };
  }

  results() {
    return this.store.queryRecord(
      'search-result',
      this.searchParameters(),
    );
  }

  @restartableTask*
  getResults() {
    if (this.query) {
      return yield this.results();
    }
  }
}

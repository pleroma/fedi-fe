import Service, { inject as service } from '@ember/service';
import Evented from '@ember/object/evented';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class VisibilityService extends Service.extend(Evented) {
  @service('-document') document;

  @tracked hidden = false;

  @tracked visibilityState = 'visible';

  touch() {}

  constructor() {
    super(...arguments);

    this.document.addEventListener('visibilitychange', this.onVisibilityChange);
  }

  willDestroy() {
    this.document.removeEventListener('visibilitychange', this.onVisibilityChange);
  }

  @action
  onVisibilityChange() {
    let { hidden, visibilityState } = this.document;
    this.hidden = hidden;
    this.visibilityState = visibilityState;
    this.trigger('visibilitychange', { hidden, visibilityState });
  }
}

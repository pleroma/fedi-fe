import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import Service, { inject as service } from '@ember/service';
import { isEmpty, typeOf } from '@ember/utils';
import fetch from 'fetch';
import isEqual from 'lodash.isequal';
import { Base64 } from 'js-base64';

const parser = new DOMParser();

export default class PreloadService extends Service {
  @service('-document') document;

  @service feeds;

  @service features;

  @service pleromaApi;

  @service store;

  /**
   * The raw JSON preloaded data. Data will typically be extracted and
   * put either into Ember Data store or its Feed equivalent. Should
   * not need to access directly, but it's possible.
   *
   * @public
   */
  @tracked data = {};

  /**
   * @public
   */
  @tracked nodeInfoId = null;

  /**
   * @public
   */
  @tracked instanceId = null;

  /**
   * Indicates if preloaded data has been loaded or not.
   *
   * @public
   */
  get loaded() {
    return !isEqual(this.data, {});
  }

  /**
   * @public
   */
  async load() {
    if (!this.features.isEnabled('usePreload')) {
      return;
    }

    let doc;

    if (this.document.getElementById('initial-results')) {
      doc = this.document;
    } else {
      let resp = await fetch(`${this.pleromaApi.apiBaseUrl}/main/all`);
      let html = await resp.text();
      doc = parser.parseFromString(html, 'text/html');
    }

    let escapedJson = doc.getElementById('initial-results')?.innerHTML;

    if (isEmpty(escapedJson)) {
      return;
    }

    try {
      let preloadedData = JSON.parse(escapedJson);

      let parsedPreloadedData = Object.entries(preloadedData).map(([key, value]) => {
        return [key, JSON.parse(Base64.decode(value))];
      });

      preloadedData = Object.fromEntries(parsedPreloadedData);

      this.data = preloadedData;

      this.instance(this.data['/api/v1/instance']);
      this.nodeInfo(this.data['/nodeinfo/2.0.json']);
      // this.publicFeed(Base64.decode(this.data['/api/v1/timelines/public']));
    } catch (err) {
      debug(err);
    }
  }

  /**
   * @private
   */
  instance(data = {}) {
    if (typeOf(data) !== 'object' || isEqual(data, {})) {
      return;
    }

    let [instance] = this.store.normalizeAndStore('instance', [data]);
    this.instanceId = instance.id;
  }

  /**
   * @private
   */
  nodeInfo(data = {}) {
    if (typeOf(data) !== 'object' || isEqual(data, {})) {
      return;
    }

    let [nodeInfo] = this.store.normalizeAndStore('node-info', [data]);
    this.nodeInfoId = nodeInfo.id;
  }

  /**
   * @private
   */
  publicFeed(data = []) {
    if (typeOf(data) !== 'array' || isEmpty(data)) {
      return;
    }

    let feed = this.feeds.getFeed('public');
    let records = this.store.normalizeAndStore(feed.modelName, data);

    if (records.length) {
      feed.addItems(records);
    }
  }
}

import Features from 'ember-feature-flags/services/features';
import { inject as service } from '@ember/service';
import { computed, get, getWithDefault } from '@ember/object';
import flag from 'node-env-flag';
import { assign } from '@ember/polyfills';
const { keys } = Object;

/**
 * Extends the Features service from `ember-feature-flags`
 * and adds custom loading logic.
 *
 * This service is designed to only load and report on
 * enabled/disabled flags.
 *
 * Please use the `feature-decisions` service for adding
 * custom logic, such as dependencies between flags,
 * depending on current user state, etc.
 */
export default Features.extend({
  config: service(),
  cookies: service(),

  deployTarget: computed('config.deployTarget', function() {
    return getWithDefault(this, 'config.deployTarget', '');
  }),

  /**
   * Runs when service is initialized.
   * Responsible for setting up initial state.
   * @method init
   * @public
   */
  init() {
    this._super(...arguments);
    this.setup(this.configuredFlags());
  },

  /**
   * Provides flags from configuration.
   * @method configuredFlags
   * @returns {Object}
   * @public
   */
  configuredFlags() {
    let configuredFlags = {};

    if (get(this, 'config.featureFlags')) {
      let configFlags = get(this, 'config.featureFlags');

      let normalizedConfigFlags = keys(configFlags).reduce((obj, key) => {
        let value = flag(configFlags[key], false);
        // eslint-disable-next-line no-underscore-dangle
        let convertedKey = this._normalizeFlag(key);
        obj[convertedKey] = value;
        return obj;
      }, {});

      assign(configuredFlags, normalizedConfigFlags);
    }

    let { deployTarget } = this;

    // Import flags from cookies only if in less-privileged environment.
    if (
      get(this, 'config.environment') === 'production' &&
      deployTarget === 'production'
    ) {
      // no-op
    } else {
      let cookies = this.cookies.read();

      let cookieFlags = keys(cookies).reduce((obj, key) => {
        if (key.toLowerCase().startsWith('ff') && key.length > 3) {
          // eslint-disable-next-line no-underscore-dangle
          let convertedKey = this._normalizeFlag(key.slice(3));
          let value = flag(cookies[key], false);
          obj[convertedKey] = value;
        }

        return obj;
      }, {});

      assign(configuredFlags, cookieFlags);
    }

    return configuredFlags;
  },
});

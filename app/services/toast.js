import Service, { inject as service } from '@ember/service';
import { A } from '@ember/array';
import { guidFor } from '@ember/object/internals';
import { set, computed, action } from '@ember/object';
import { timeout } from 'ember-concurrency';
import config from 'pleroma-pwa/config/environment';
import { filterBy } from '@ember/object/computed';
import { task } from 'ember-concurrency-decorators';

export default class ToastService extends Service {
  @service bus;

  toastTimeout = config.APP.TOAST_TIMEOUT;

  activeToasts = A();

  nonSuccessToasts = ['error', 'dismissibleMessage'];

  @filterBy('activeToasts', 'type', 'error') errorToasts;

  @filterBy('activeToasts', 'type', 'dismissibleMessage') dismissibleMessages;

  @computed('activeToasts.[]')
  get successToasts() {
    // Ony show the last one of the `success` toasts
    return [this.activeToasts.filter((toast) => !this.nonSuccessToasts.includes(toast.type)).lastObject];
  }

  constructor(...args) {
    super(...args);

    this.bus.on('muted', (user) => this.notify({ user, type: 'userMuted' }));

    this.bus.on('unmuted', (user) => this.notify({ user, type: 'userUnmuted' }));

    this.bus.on('blocked', (user) => this.notify({ user, type: 'userBlocked' }));

    this.bus.on('unblocked', (user) => this.notify({ user, type: 'userUnblocked' }));

    this.bus.on('followed', (user) => this.notify({ user, type: 'userFollowed' }));

    this.bus.on('unfollowed', (user) => this.notify({ user, type: 'userUnfollowed' }));
  }

  touch() {}

  @action
  notify(payload) {
    let toast = {
      id: guidFor(payload),
      type: payload.type || 'success',
      payload,
    };

    this.activeToasts.pushObject(toast);

    if (!this.nonSuccessToasts.includes(toast.type) && !(config.environment === 'test')) {
      this.dismissLater.perform(toast.id);
    }

    return toast;
  }

  @action
  dismiss(id) {
    set(this, 'activeToasts', this.activeToasts.rejectBy('id', id));
  }

  @task*
  dismissLater(toastId) {
    yield timeout(this.toastTimeout);

    this.dismiss(toastId);
  }
}

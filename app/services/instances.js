import Service, { inject as service } from '@ember/service';
import { assert } from '@ember/debug';
import { computed, set } from '@ember/object';
import { dropTask } from 'ember-concurrency-decorators';
import RSVP from 'rsvp';

export default class InstancesService extends Service {
  @service features;

  @service session;

  @service preload;

  @service store;

  @service pleromaApi;

  currentId = null;

  @dropTask*
  refreshTask() {
    // Find or fetch data, depending on whether preload is enabled/loaded or not.
    let instance, nodeInfo, clientCredential;

    // TODO: Right now, there is an open PR on pleroma-api to preload data into the index.html.
    // https://git.pleroma.social/pleroma/pleroma/-/merge_requests/2381
    // Once that is ready, let jump on board and move as much stuff as possible into the index.html.
    if (this.features.isEnabled('usePreload') && this.preload.loaded) {
      // Use as much as possible from preloaded data.
      instance = this.store.peekRecord('instance', this.preload.instanceId);
      set(this, 'currentId', instance.id);

      nodeInfo = this.store.peekRecord('node-info', this.preload.nodeInfoId);

      // TODO: Get these to be in preloaded data.
      [clientCredential] = yield RSVP.all([
        this.store.queryRecord('client-credential', { current: true }),
      ]);
    } else {
      instance = yield this.store.queryRecord('instance', { current: true });
      set(this, 'currentId', instance.id);

      // Load separately if not using preloading.
      [nodeInfo, clientCredential] = yield RSVP.all([
        this.store.queryRecord('node-info', { current: true }),
        this.store.queryRecord('client-credential', { current: true }),
      ]);
    }

    set(instance, 'nodeInfo', nodeInfo);
    set(instance, 'credentials', clientCredential);

    assert('current instance should be available', instance.id === this.current.id);

    let clientToken = yield this.store.queryRecord('client-token', {
      current: true,
      clientId: clientCredential.clientId,
      clientSecret: clientCredential.clientSecret,
      redirectUri: clientCredential.redirectUri,
    });

    set(instance, 'token', clientToken);
  }

  @computed('currentId')
  get current() {
    return this.currentId &&
      this.store.peekRecord('instance', this.currentId);
  }

  ensureCurrent() {
    if (this.refreshTask.last) {
      return this.refreshTask.last;
    } else {
      return this.refreshTask.perform();
    }
  }
}

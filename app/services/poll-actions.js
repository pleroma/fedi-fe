import Service, { inject as service } from '@ember/service';
import { dropTask } from 'ember-concurrency-decorators';
import { assert } from '@ember/debug';
import { later } from '@ember/runloop';

export default class PollActionsService extends Service {
  @service authModal;

  @service error;

  @service intl;

  @service pleromaApi;

  @service session;

  @service store;

  @service toast;

  reloadCache = {};

  @dropTask*
  vote(poll, option) {
    assert('You must pass a poll to the poll service vote action', poll);
    assert('You must pass an option to the poll service vote action', option !== undefined);

    if (this.session.isAuthenticated) {
      let url = this.pleromaApi.endpoints.vote(poll.id);

      let response = yield fetch(url, {
        method: 'POST',
        headers: this.session.headers,
        body: JSON.stringify({
          choices: [option],
        }),
      });

      let json = yield response.json();

      let status = this.error.detectErrantSuccessCode(response.status, json);

      if (status >= 400) {
        if (status === 422) {
          this.toast.notify({ message: json.error, type: 'error' });
        } else {
          this.toast.notify({ message: this.intl.t('errorVotingTryAgain'), type: 'error' });
        }
      }

      this.toast.notify({ message: this.intl.t('voteSaved'), type: 'success' });

      this.store.normalizeAndStore('poll', [json]);
    } else {
      this.session.setReasonForAuthentication('youMustSignInToVote');
      this.authModal.showSignInModal();
    }
  }

  reloadPollIn(pollId, milliseconds) {
    if (!this.reloadCache[pollId]) {
      this.reloadCache[pollId] = later(this, () => {
        let poll = this.store.peekRecord('poll', pollId);

        if (poll) {
          poll.reload();
        }
      }, milliseconds);
    }
  }
}

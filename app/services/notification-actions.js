import Service, { inject as service } from '@ember/service';
import { debug } from '@ember/debug';
import { enqueueTask } from 'ember-concurrency-decorators';

export default class NotificationActionsService extends Service {
  @service error;

  @service feeds;

  @service pleromaApi;

  @service session;

  @enqueueTask*
  markAsRead(notification)  {
    if (this.session.isAuthenticated) {
      try {
        let url = this.pleromaApi.endpoints.notificationsMarkAsRead;
        let { headers } = this.session;
        let response = yield fetch(url, {
          method: 'POST',
          headers,
          body: JSON.stringify({ id: notification.id }),
        });

        let json = yield response.json();
        let status = this.error.detectErrantSuccessCode(response.status, json);

        if (status >= 400 && status < 500) {
          notification.deleteNotification();
          return yield notification;
        }

        notification.markAsRead(json);

        return yield notification;
      } catch(e) {
        debug(e);
      }
    }
  }
}

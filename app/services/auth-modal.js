import Service, { inject as service } from '@ember/service';
import { set, action } from '@ember/object';
import { sendEvent } from '@ember/object/events';
import { run } from '@ember/runloop';

export default class AuthModalService extends Service {
  @service intl;

  @service routerScroll;

  @service router;

  @service session;

  @service window;

  authModal = null;

  shouldBlockClosingAuthModal = false;

  @action
  showSignInModal() {
    this.show('sign-in');
  }

  @action
  showRegistrationModal() {
    this.show('register');
  }

  @action
  showForgotPasswordModal() {
    this.show('forgot-password');
  }

  @action
  showForgotPasswordSuccess() {
    this.show('forgot-password-success');
  }

  @action
  showOnboardingBioForm() {
    this.show('onboarding-bio-form');
  }

  @action
  showOnboardingAvatarForm() {
    this.show('onboarding-avatar-form');
  }

  show(tabName) {
    if (this.shouldBlockClosingAuthModal) { // If navigating between auth forms
      if (!this.window.confirm(this.intl.t('ifYouLeaveProgressWillBeLost'))) {
        return;
      } else {
        this.allowExit();
      }
    }

    this.setTriggerElementSelector();

    this.router.transitionTo({
      queryParams: {
        auth: tabName,
      },
    });

    set(this.routerScroll, 'preserveScrollPosition', true);
  }

  setTriggerElementSelector() {
    run(() => {
      let { activeElement } = document;
      if (activeElement && !this.triggerElementSelector) {
        let now = Date.now();
        activeElement.setAttribute('data-triggered-sign-in', now);
        set(this, 'triggerElementSelector', `[data-triggered-sign-in="${now}"]`);
      }
    });
  }

  returnFocusToTrigger() {
    run(() => {
      if (this.triggerElementSelector) {
        let triggerElement = document.querySelector(this.triggerElementSelector);

        if (triggerElement && triggerElement.tabIndex > -1) {
          triggerElement.focus();
        }
      }

      this.clearTriggerSelector();
    });
  }

  clearTriggerSelector() {
    set(this, 'triggerElementSelector', null);
  }

  @action
  onAuthenticated() {
    // Typically we would want to close on authentication.
    // But if we are in the registration flow, we want the modal to stay open and to show the onboarding forms.
    if (this.router.currentRoute) { // This is false in integration tests that load the sessions user
      let { queryParams: { auth } } = this.router.currentRoute.find((route) => route.name === 'application');

      if (auth === 'onboarding-avatar-form' || auth === 'onboarding-bio-form') {
        return;
      }

      this.close();
    }
  }

  @action
  blockExit() {
    set(this, 'shouldBlockClosingAuthModal', true);
  }

  @action
  allowExit() {
    set(this, 'shouldBlockClosingAuthModal', false);
  }

  @action
  close(dd, event) {
    if (this.shouldBlockClosingAuthModal) {
      if (this.window.confirm(this.intl.t('ifYouLeaveProgressWillBeLost'))) {
        this.allowExit();
        this.actuallyClose();
      }
    } else {
      this.actuallyClose();
    }

    // Always return false, the closing is done as part of the transition
    event && event.stopPropagation();
    event && event.preventDefault();
    return false;
  }

  actuallyClose() {
    if (this.router.currentRoute) {
      this.router.transitionTo({
        queryParams: {
          auth: null,
        },
      });
    }

    this.returnFocusToTrigger();
    this.session.clearSessionRedirectRoute();
    this.session.clearReasonForAuthentication();

    sendEvent(this, 'auth-modal-hidden');

    set(this.routerScroll, 'preserveScrollPosition', false);
  }
}

import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import fetch from 'fetch';
import camelCaseKeys from 'camelcase-keys';

export default class CaptchaService extends Service {
  @service error;

  @service pleromaApi;

  @service session;

  @tracked enabled = false;

  async new() {
    let url = this.pleromaApi.endpoints.captcha;
    let { headers } = this.session;

    let response = await fetch(url, {
      method: 'GET',
      headers,
    });

    let data = camelCaseKeys(await response.json());

    let status = this.error.detectErrantSuccessCode(response.status, data);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(data);
    }

    let { type } = data;
    this.enabled = type !== 'none';

    return data;
  }
}

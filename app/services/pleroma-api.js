import Service, { inject as service } from '@ember/service';
import { inject as concern } from 'ember-concerns';
import { assert } from '@ember/debug';
import { computed } from '@ember/object';
import { isPresent } from '@ember/utils';
import config from 'pleroma-pwa/config/environment';

export default class PleromaApiService extends Service {
  @concern('pleroma-api/endpoints') endpoints;

  @service instances;

  @computed('instances.current')
  get apiBaseUrl() {
    if (config.environment === 'test') {
      return config.APP.testApiBaseUrl;
    }

    if (isPresent(this.instances?.current?.uri)) {
      return this.instances.current.uri;
    }

    if (isPresent(config?.APP?.apiBaseUrl)) {
      return config.APP.apiBaseUrl;
    }

    if (config.environment !== 'test' && isPresent(window?.location?.origin)) {
      return window.location.origin;
    }

    assert('should have found apiBaseUrl', true);

    return '';
  }
}

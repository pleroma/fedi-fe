import Service from '@ember/service';

export default class WindowService extends Service {
  confirm(message) {
    return window.confirm(message);
  }
}

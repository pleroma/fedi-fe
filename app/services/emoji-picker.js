import Service, { inject as service } from '@ember/service';
import { action, computed } from '@ember/object';
import { dropTask, restartableTask } from 'ember-concurrency-decorators';
import { debounce, next } from '@ember/runloop';
import { isEmpty, tryInvoke } from '@ember/utils';
import { tracked } from '@glimmer/tracking';

export default class EmojiPickerService extends Service {
  @tracked rawQuery = null;

  @service store;

  textarea;

  @tracked content;

  common = ['👍', '😠', '👀', '😂', '🔥'];

  constructor() {
    super(...arguments);

    this.getEmojis.perform();
    this.reset();
  }

  @action
  reset() {
    this.rawQuery = null;
  }

  touch() {}

  get emojis() {
    return this.getEmojis.last?.value;
  }

  get cursorPosition() {
    let { textarea } = this;

    if (!textarea) {
      return null;
    }

    // Get position of cursor.
    let { selectionStart, selectionEnd } = textarea;

    // only proceed if cursor is in a single spot, not selecting something.
    if (selectionStart !== selectionEnd) {
      return null;
    }

    return selectionStart;
  }

  @computed('cursorPosition', 'content')
  get emojiInProgress() {
    let { cursorPosition } = this;

    if (!cursorPosition) {
      return null;
    }

    let { content } = this;

    let exp = /:([a-z0-9_-]{1,})/g;
    let inProgress = null;
    let match;

    while ((match = exp.exec(content)) !== null) {
      let [matchText] = match;
      if (cursorPosition === match.index + matchText.length) {
        inProgress = match;
        break;
      }
    }

    return inProgress;
  }

  withNewContent(staticEmoji, fn) {
    let startIndex = this.emojiInProgress.index;
    let endIndex = this.emojiInProgress.index + this.emojiInProgress[0].length;

    let newContent =
      this.content.slice(0, startIndex) +
      staticEmoji.value +
      this.content.slice(endIndex);

    this.content = newContent;

    if (fn && typeof fn === 'function') {
      fn(newContent);
    }

    if (this.textarea?.setSelectionRange) {
      next(() => {
        this.reset();

        this.textarea.focus();

        this.textarea.setSelectionRange(
          startIndex + 1,
          startIndex + 1,
        );
      });
    }
  }

  get filteredEmojis() {
    if (this.emojis?.length) {
      let { emojis } = this;

      if (!isEmpty(this.queryNormalized)) {
        emojis = emojis.filter(emoji => {
          return emoji.shortcode.includes(this.queryNormalized);
        });
      }

      return emojis;
    }

    return [];
  }

  get commonEmojis() {
    if (this.getEmojis.isRunning || !this.getEmojis.last.value) {
      return [];
    }

    let found = this.getEmojis.last.value.filter(emoji => {
      return this.common.includes(emoji.value);
    });

    return this.common.map(emojiCharacter => {
      return found.find(e => e.value === emojiCharacter);
    });
  }

  get query() {
    if (this.rawQuery !== null) {
      return this.rawQuery;
    }

    return '';
  }

  get queryNormalized() {
    return this.query.toLowerCase();
  }

  @action
  interceptEnter(ev) {
    // Ensure it's possible to add new lines when the picker is not shown.
    if (ev.keyCode === 13 && this.emojiInProgress) {
      ev.stopPropagation();
      ev.preventDefault();

      this.chooseIfOnlyOrExactMatch.perform();
    }
  }

  @action
  doSetEmojiQuery() {
    if (!this.emojiInProgress) {
      return;
    }

    this.setQuery(this.emojiInProgress[1]);
  }

  @action
  setContentWithEvent(ev) {
    let { target: input } = ev;
    this.content = input.value;
  }

  @action
  setQueryWithEvent(ev) {
    let { target: input } = ev;
    debounce(this, 'rawSetQuery', input.value, 50);
  }

  @action
  setQuery(value) {
    debounce(this, 'rawSetQuery', value, 50);
  }

  rawSetQuery(value) {
    this.rawQuery = value;
  }

  @dropTask*
  getEmojis() {
    try {
      // Load them to have them around.
      if (isEmpty(this.store.peekAll('emoji'))) {
        this.store.loadRecords('emoji');
      }

      let staticEmoji = this.store.peekAll('static-emoji');

      if (isEmpty(staticEmoji)) {
        staticEmoji = this.store.loadRecords('static-emoji');
      }

      return yield staticEmoji;
    } catch(e) {
      tryInvoke(this.args, 'onNotFound');
    }
  }

  @action
  choose(shortcode) {
    let el = document
      .querySelector(`button[data-shortcode="${shortcode}"]`);

    if (el) {
      el.click();
    }
  }

  @restartableTask*
  chooseIfOnlyOrExactMatch() {
    let shortcode;

    if (this.filteredEmojis.length === 1) {
      ({ shortcode } = this.filteredEmojis[0]);
    } else {
      // See if there is an exact match.
      let exact = this.filteredEmojis.find(emoji => emoji.shortcode === this.queryNormalized);

      // If there is, assume user wants to pick it.
      if (exact) {
        ({ shortcode } = exact);
      }
    }

    if (shortcode) {
      yield this.choose(shortcode);
    }
  }
}

import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { resolve, reject } from 'rsvp';

export default class OfflineService extends Service {
  @service intl;

  @service toast;

  @tracked isOffline = false;

  turnOffline = () => (this.isOffline = true);

  turnOnline = () => (this.isOffline = false);

  constructor() {
    super(...arguments);
    this.isOffline = !window.navigator.onLine;
    this.setUpOfflineListeners();
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.tearDownOfflineListeners();
  }

  setUpOfflineListeners() {
    window.addEventListener('offline', this.turnOffline);
    window.addEventListener('online', this.turnOnline);
  }

  tearDownOfflineListeners() {
    window.removeEventListener('offline', this.turnOffline);
    window.removeEventListener('online', this.turnOnline);
  }

  @action
  errorIfOffline() {
    if (this.isOffline) {
      this.toast.notify({ message: this.intl.t('offlineNotice'), type: 'error' });
      return reject(new Error(this.intl.t('offlineNotice')));
    }

    return resolve();
  }
}

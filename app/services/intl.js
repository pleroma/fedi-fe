import EmberIntlService from 'ember-intl/services/intl';
import { computed } from '@ember/object';
import { A } from '@ember/array';

export default class PleromaIntlService extends EmberIntlService {
  rightToLeftLocales = A();

  @computed('primaryLocale', 'rightToLeftLocales')
  get isRTL() {
    return this.rightToLeftLocales.includes(this.primaryLocale);
  }
}

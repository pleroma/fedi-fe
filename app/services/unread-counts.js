import Service, { inject as service } from '@ember/service';
import Poller from 'pleroma-pwa/utils/poller';
import { restartableTask } from 'ember-concurrency-decorators';
import { set } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class UnreadCountsService extends Service {
  @service pleromaApi;

  @service session;

  longPoller = new Poller(() => this.collectUnreadCounts.perform());

  @tracked unreadNotificationsCount = null;

  @tracked unreadChatCount = null;

  @tracked unreadConversationCount = null;

  startWatching() {
    this.collectUnreadCounts.perform();

    this.longPoller.start();
  }

  stopWatching() {
    this.longPoller.stop();
  }

  @restartableTask*
  collectUnreadCounts() {
    let response = yield fetch(this.pleromaApi.endpoints.currentUser, { headers: this.session.headers });
    let currentUser = yield response.json();

    /* eslint-disable-next-line camelcase */
    this.setUnreadConversationCount(currentUser?.pleroma?.unread_conversation_count);
  }

  setUnreadChatCount(value) {
    set(this, 'unreadChatCount', value || null);
  }

  setUnreadConversationCount(value) {
    set(this, 'unreadConversationCount', value || null);
  }

  setUnreadNotificationsCount(value) {
    set(this, 'unreadNotificationsCount', value || null);
  }

  decrementUnreadChatCount() {
    set(this, 'unreadChatCount', Math.max(this.unreadChatCount - 1, 0) || null);
  }

  decrementUnreadConversationCount() {
    set(this, 'unreadConversationCount', Math.max(this.unreadConversationCount - 1, 0) || null);
  }

  decrementNotificationsCount() {
    set(this, 'unreadNotificationsCount', Math.max(this.unreadNotificationsCount - 1, 0) || null);
  }
}

import { inject as service } from '@ember/service';
import { assert } from '@ember/debug';
import { set, computed } from '@ember/object';
import RSVP from 'rsvp';
import ESASessionService from 'ember-simple-auth/services/session';
import config from 'pleroma-pwa/config/environment';

export default class PleromaSessionService extends ESASessionService {
  @service authModal;

  @service instances;

  @service store;

  @service sentry;

  currentUser = null;

  reasonForAuthenticationKey = null;

  async authenticate(...args) {
    try {
      await super.authenticate(...args);
      await this.loadCurrentUser();
      await this.authModal.onAuthenticated();
      await this.clearReasonForAuthentication();
    } catch(error) {
      this.sentry.throwException('Session was invalidated failing to load the current user');
      this.invalidate();
      return RSVP.reject(error.responseJSON);
    }
  }

  async loadCurrentUser() {
    let currentUser = await this.store.queryRecord('user', {
      me: true,
    });

    set(this, 'currentUser', currentUser);

    this.trigger('currentUserLoaded');

    return currentUser;
  }

  @computed('isAuthenticated', 'instances.current.token')
  get headers() {
    if (this.isAuthenticated) {
      return {
        'authorization': `${this.data.authenticated.token_type} ${this.data.authenticated.access_token}`,
        'content-type': 'application/json',
      };
    } else if (this.instances.current?.token) { // Some calls require the clients token, (like registration, and viewing users followers while unauthenticated)
      return {
        'authorization': `${this.instances.current.token.tokenType} ${this.instances.current.token.accessToken}`,
        'content-type': 'application/json',
      };
    } else {
      return {};
    }
  }

  async invalidate(...args) {
    await super.invalidate(...args);

    this.reset();
  }

  reset() {
    set(this, 'currentUser', null);
    set(this, 'reasonForAuthenticationKey', null);
  }

  authenticateBasic(username, password) {
    assert('You must pass both an username and password to the session services authenticateBasic method', (username || password));

    return this.authenticate('authenticator:password-grant', username, password, config.APP.OAUTH_SCOPES);
  }

  authenticateImplicitGrant(options) {
    assert('You must pass both a tokenType and accessToken to the session services authenticateWithAccessToken method', (options.token_type || options.access_token));

    return this.authenticate('authenticator:implicit-grant', options);
  }

  clearReasonForAuthentication() {
    set(this, 'reasonForAuthenticationKey', null);
  }

  setReasonForAuthentication(reasonKey) {
    set(this, 'reasonForAuthenticationKey', reasonKey);
  }

  clearSessionRedirectRoute() {
    set(this, 'attemptedTransition', null);
  }
}

import Service, { inject as service } from '@ember/service';
import { debug, assert } from '@ember/debug';
import { A as emberArray } from '@ember/array';
import { dropTask, enqueueTask, keepLatestTask } from 'ember-concurrency-decorators';
import { set, setProperties } from '@ember/object';
import snakeCaseKeys from 'snakecase-keys';
import camelCaseKeys from 'camelcase-keys';
import fetch from 'fetch';
import humanizeFileSize from 'pleroma-pwa/utils/humanize-file-size';

export default class UserActionsService extends Service {
  @service authModal;

  @service bus;

  @service error;

  @service features;

  @service feeds;

  @service instances;

  @service intl;

  @service pleromaApi;

  @service router;

  @service session;

  @service store;

  @service toast;

  @enqueueTask*
  muteUser(user) {
    if (this.session.isAuthenticated) {
      if (this.session.currentUser.id === user.id) {
        this.toast.notify({ message: this.intl.t('youCannotMuteYourself'), type: 'error' });
        return;
      }

      try {
        let relationship = yield user.mute();

        yield this.updateRelationship.linked().perform(user, relationship);
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorMutingUserTryAgain', { username: user.username }), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToMuteUser');
      this.authModal.showSignInModal();
    }
  }

  @enqueueTask*
  unmuteUser(user) {
    if (this.session.isAuthenticated) {
      try {
        let relationship = yield user.unmute();

        yield this.updateRelationship.linked().perform(user, relationship);
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorUnmutingUserTryAgain', { username: user.username }), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToUnmuteUser');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  blockUser(user) {
    assert('Must pass a user to the user-actions blockUser task', user);

    if (this.session.isAuthenticated) {
      if (this.session.currentUser.id === user.id) {
        this.toast.notify({ message: this.intl.t('youCannotBlockYourself'), type: 'error' });
        return;
      }

      try {
        let relationship = yield user.block();

        yield this.updateRelationship.linked().perform(user, relationship);
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorBlockingUserTryAgain'), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToBlockUser');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  unblockUser(user) {
    assert('Must pass a user to the user-actions unblockUser task', user);

    if (this.session.isAuthenticated) {
      try {
        let relationship = yield user.unblock();

        yield this.updateRelationship.linked().perform(user, relationship);
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorUnblockingUserTryAgain'), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToUnblockUser');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  followUser(user) {
    assert('Must pass a user to the user-actions followUser task', user);

    if (this.session.isAuthenticated) {
      if (this.session.currentUser.id === user.id) {
        this.toast.notify({ message: this.intl.t('youCannotFollowYourself'), type: 'error' });
        return;
      }

      try {
        let relationship = yield user.follow();

        yield this.updateRelationship.linked().perform(user, relationship);
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorFollowingUserTryAgain', { username: user.username }), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToFollowUser');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  unfollowUser(user) {
    assert('Must pass a user to the user-actions unfollowUser task', user);

    if (this.session.isAuthenticated) {
      try {
        let relationship = yield user.unfollow();

        yield this.updateRelationship.linked().perform(user, relationship);
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorUnfollowingUserTryAgain', { username: user.username }), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToUnfollowUser');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  reportUser(user) {
    assert('Must pass a user to the user-actions report user task', user);

    if (!this.features.isEnabled('reportUsers')) {
      yield this.toast.notify({ message: this.intl.t('featureNotEnabled', { feature: "Report Users" }), type: 'error' });
    }
  }

  @dropTask*
  saveNotificationSettings(settings) {
    assert('Must pass settings to the user-actions save notification settings task', settings);

    let modifiedSettings = snakeCaseKeys(settings);

    let url = this.pleromaApi.endpoints.notificationSettings;
    let { headers } = this.session;

    let response = yield fetch(url, {
      method: 'PUT',
      headers,
      body: JSON.stringify(modifiedSettings),
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    for (let setting of Object.keys(settings)) {
      set(this.session.currentUser.pleroma.notificationSettings, setting, settings[setting]);
    }

    this.toast.notify({ message: this.intl.t('notificationSettingsHaveBeenSaved') });
  }

  @dropTask*
  changePassword({ password, newPassword, newPasswordConfirmation }) {
    let url = this.pleromaApi.endpoints.changePassword;
    let { headers } = this.session;

    let response = yield fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify(snakeCaseKeys({
        password,
        newPassword,
        newPasswordConfirmation,
      })),
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    this.toast.notify({ message: this.intl.t('passwordHasBeenChanged') });
  }

  @dropTask*
  deleteAccount({ password }) {
    let url = this.pleromaApi.endpoints.deleteAccount;
    let { headers } = this.session;

    let response = yield fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify({ password }),
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    this.toast.notify({ message: this.intl.t('accountHasBeenDeleted') });
  }

  /**
   * Get all users user with given ID is following.
   *
   * @return [User]
   * @public
   */
  @keepLatestTask*
  exportBlocking() {
    let response = yield this.fetchBlocking();
    let blocking = yield response.json();
    let records = this.store.normalizeAndStore('user', blocking);
    return yield records;
  }

  /**
   * Get all users user with given ID is following.
   *
   * @return [User]
   * @public
   */
  @keepLatestTask*
  exportFollowing({ id }) {
    if (!id) {
      throw new Error(this.intl.t('pleaseProvideValidUser'));
    }

    let following = emberArray();
    let more = true;

    while (more) {
      let maxId = following.length > 0 ? following.lastObject.id : undefined;
      let response = yield this.fetchFollowing({ id, maxId });
      let users = yield response.json();

      following.pushObjects(users);

      if (users.length === 0) {
        more = false;
      }
    }

    let records = this.store.normalizeAndStore('user', following);

    return yield records;
  }

  fetchBlocking() {
    let url = this.pleromaApi.endpoints.accountBlocks;
    let { headers } = this.session;
    return fetch(url, { headers });
  }

  /**
   * @private
   */
  fetchFollowing({ id, maxId, sinceId, limit = 20 }) {
    let url = this.pleromaApi.endpoints.accountFollowing(id);
    let { headers } = this.session;

    let args = [
      maxId && `max_id=${maxId}`,
      sinceId && `since_id=${sinceId}`,
      limit && `limit=${parseInt(limit)}`,
    ].filter(Boolean).join('&');

    url = url + (args ? `?${args}` : '');

    return fetch(url, { headers });
  }

  /**
   * Set the avatar of the current session User
   *
   * @param {File} avatar
   * @return
   * @public
   */
  @dropTask*
  setAvatar(avatar) {
    let url = this.pleromaApi.endpoints.updateUser;
    let headers = Object.assign({}, this.session.headers);

    if (!(avatar instanceof File || avatar instanceof Blob) || !avatar.size || !avatar.type) {
      throw new Error(this.intl.t('pleaseUploadValidFile'));
    }

    if (avatar.size > this.instances.current.avatarUploadLimit) {
      throw new Error(this.intl.t('fileTooLarge', { max: humanizeFileSize(this.instances.current.avatarUploadLimit) }));
    }

    let body = new FormData();
    body.append('avatar', avatar);
    delete headers['content-type'];

    let response = yield fetch(url, {
      method: 'PATCH',
      headers,
      body,
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    this.store.normalizeAndStore('user', [json], this.store);
  }

  /**
   * Clear the avatar of the session user
   *
   * @public
   */
  @dropTask*
  clearAvatar() {
    let url = this.pleromaApi.endpoints.setAvatar;
    let { headers } = this.session;
    headers['content-type'] = 'application/json';

    let response = yield fetch(url, {
      method: 'PATCH',
      headers,
      body: '{"img": ""}',
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    yield this.session.loadCurrentUser();
  }

  @dropTask*
  setBackgroundImage(backgroundImage) {
    let url = this.pleromaApi.endpoints.updateUser;
    let { headers } = this.session;

    if (!backgroundImage.size || !backgroundImage.type) {
      throw new Error(this.intl.t('pleaseUploadValidFile'));
    }

    if (backgroundImage.size > this.instances.current.backgroundUploadLimit) {
      throw new Error(this.intl.t('fileTooLarge', { max: humanizeFileSize(this.instances.current.backgroundUploadLimit) }));
    }

    let body = new FormData();
    body.append('pleroma_background_image', backgroundImage);
    delete headers['content-type'];

    let response = yield fetch(url, {
      method: 'PATCH',
      headers,
      body,
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    this.store.normalizeAndStore('user', [json], this.store);
  }

  @dropTask*
  clearBackgroundImage() {
    let url = this.pleromaApi.endpoints.setBackground;
    let { headers } = this.session;
    headers['content-type'] = 'application/json';

    let response = yield fetch(url, {
      method: 'PATCH',
      headers,
      body: '{"img": ""}',
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    yield this.session.loadCurrentUser();
  }

  @dropTask*
  updateUser(data) {
    let url = this.pleromaApi.endpoints.updateUser;
    let { headers } = this.session;

    let body = JSON.stringify(snakeCaseKeys(data, { deep: true }));

    let response = yield fetch(url, {
      method: 'PATCH',
      headers,
      body,
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    this.store.normalizeAndStore('user', [json], this.store);

    this.toast.notify({ message: this.intl.t('accountHasBeenUpdated') });
  }

  /**
   * @param {File} file
   * @public
   */
  @dropTask*
  importFollowing(file) {
    if (!(file instanceof File)) {
      throw new Error(this.intl.t('pleaseUploadValidFile'));
    }

    let formData = new FormData();
    formData.append('list', file);

    let url = this.pleromaApi.endpoints.followImport;

    let { headers } = this.session;
    delete headers['content-type'];

    let response = yield fetch(url, {
      method: 'POST',
      headers,
      body: formData,
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    return yield file;
  }

  /**
   * @param {File} file
   * @public
   */
  @dropTask*
  importBlocking(file) {
    if (!(file instanceof File)) {
      throw new Error(this.intl.t('pleaseUploadValidFile'));
    }

    let formData = new FormData();
    formData.append('list', file);

    let url = this.pleromaApi.endpoints.blocksImport;

    let { headers } = this.session;
    delete headers['content-type'];

    let response = yield fetch(url, {
      method: 'POST',
      headers,
      body: formData,
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    return yield file;
  }

  /**
   * @param {File} file
   * @public
   */
  @dropTask*
  revokeToken(id) {
    if (!id) {
      throw new Error(this.intl.t('pleaseProvideValidTokenId'));
    }

    let url = this.pleromaApi.endpoints.oauthToken(id);

    let { headers } = this.session;

    let response = yield fetch(url, {
      method: 'DELETE',
      headers,
    });

    let json;

    try {
      // DELETE responses should be empty, but you never know.
      json = yield response.json();
    } catch (_) {
      json = {};
    }

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    return yield id;
  }

  /**
   * @param {Object} user
   * @public
   */
  @dropTask*
  registerUser(user) {
    let url = this.pleromaApi.endpoints.registration;

    let response = yield fetch(url, {
      method: 'POST',
      headers: this.session.headers,
      body: JSON.stringify(user),
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    return json;
  }

  /**
   * @param {Object} user
   * @public
   */
  @enqueueTask*
  updateRelationship(user, relationship) {
    let currentRelationship = yield this.store.loadRecord('relationship', user.id);

    let camelCasedRelationship = camelCaseKeys(relationship);

    // detect what's changed
    // trigger events for what's happened

    let events = [];

    Object.entries(camelCasedRelationship).forEach(([key, value]) => {
      let origValue = currentRelationship[key];

      if (origValue !== value) {
        if (key === 'blocking') {
          if (value === true) {
            events.push({ event: 'blocked', args: [user] });
          } else {
            events.push({ event: 'unblocked', args: [user] });
          }
        } else if (key === 'following') {
          if (value === true) {
            events.push({ event: 'followed', args: [user] });
          } else {
            events.push({ event: 'unfollowed', args: [user] });
          }
        } else if (key === 'muting') {
          if (value === true) {
            events.push({ event: 'muted', args: [user] });
          } else {
            events.push({ event: 'unmuted', args: [user] });
          }
        }
      }
    });

    setProperties(currentRelationship, camelCasedRelationship);

    events.forEach(({ event, args }) => {
      debug(`event: ${event}`);
      debug(`args: ${args}`);
      this.bus.trigger(event, ...args);
    });
  }
}

import Service from '@ember/service';
import { set, action } from '@ember/object';
import { not } from '@ember/object/computed';
import Evented from '@ember/object/evented';
import {
  bindKeyboardShortcuts,
  unbindKeyboardShortcuts,
} from 'ember-keyboard-shortcuts';
import elementIsVisibile from 'pleroma-pwa/utils/element-is-visible';
import { A } from '@ember/array';
import { next } from '@ember/runloop';

export default class OffCanvasNavService extends Service.extend(Evented) {
  isOpen = false;

  @not('isOpen') isClosed;

  previouslyActiveElement = null;

  keyboardShortcuts = null;

  keyboardShortcuts = {
    esc: this.close,
    home: this.focusFirstSection,
    end: this.focusLastSection,
  };

  @action
  open() {
    set(this, 'isOpen', true);
    this.trigger('opened');

    if (!this.previouslyActiveElement) {
      set(this, 'previouslyActiveElement', document.activeElement);
    }

    bindKeyboardShortcuts(this);

    next(() => this.focusFirstSection());
  }

  @action
  close() {
    if (!this.isDestroyed) {
      set(this, 'isOpen', false);
      this.trigger('closed');

      if (this.previouslyActiveElement) {
        this.previouslyActiveElement.focus();
        set(this, 'previouslyActiveElement', null);
      }
    }

    unbindKeyboardShortcuts(this);
  }

  getAllFocusableSections() {
    // Could have been written more declarative... this is simple and probably most performant
    // If need to refactor, look at ember-ref-modifier
    let focusables = A([...document.querySelectorAll('#menu button, #menu a')]); // NodeList to Ember Array
    return focusables.filter((focusable) => elementIsVisibile(focusable));
  }

  focusFirstSection() {
    this.getAllFocusableSections().firstObject && this.getAllFocusableSections().firstObject.focus();
    return false;
  }

  focusLastSection() {
    this.getAllFocusableSections().lastObject && this.getAllFocusableSections().lastObject.focus();
    return false;
  }
}

import config from 'pleroma-pwa/config/environment';
import Service, { inject as service } from '@ember/service';
import { debug } from '@ember/debug';
import { dropTask } from 'ember-concurrency-decorators';
import { get, set } from '@ember/object';
const { APP: { CONVERSATION_MAX_DEPTH } } = config;

export default class StatusActionsService extends Service {
  CONVERSATION_MAX_DEPTH = CONVERSATION_MAX_DEPTH || 6;

  @service authModal;

  @service clipboard;

  @service error;

  @service feeds;

  @service instances;

  @service intl;

  @service toast;

  @service pleromaApi;

  @service router;

  @service session;

  @service store;

  @service window;

  @service userActions;

  @dropTask*
  toggleFavorite(status)  {
    if (this.session.isAuthenticated) {
      if (!status.favorited) {
        try {
          yield status.favorite();
        } catch(e) {
          debug(e);

          this.toast.notify({ message: this.intl.t('errorFavoritingTryAgain'), type: 'error' });
        }
      } else {
        try {
          yield status.unfavorite();
        } catch(e) {
          debug(e);

          this.toast.notify({ message: this.intl.t('errorUnfavoritingTryAgain'), type: 'error' });
        }
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToFavoriteStatus');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  toggleRepost(status) {
    if (this.session.isAuthenticated) {
      if (!status.reposted) {
        try {
          let newStatus = yield status.repost();

          this.feeds.addNewItem('home', newStatus);

          this.toast.notify({ type: 'statusReposted', newStatus });
        } catch(e) {
          debug(e);

          this.toast.notify({ message: this.intl.t('errorRepostingTryAgain'), type: 'error' });
        }
      } else {
        try {
          let originalStatus = status;

          yield originalStatus.unrepost();

          let statuses = this.store.peekAll('status');
          let reposts = statuses.filter((status) => status.repostOf && status.repostOf.id === originalStatus.id);
          let [repostInQuestion] = reposts.filter((status) => status.account.id === this.session.currentUser.id);

          this.feeds.removeItem('home', repostInQuestion);

          this.toast.notify({ message: this.intl.t('statusUnreposted'), type: 'success' });
        } catch(e) {
          debug(e);

          this.toast.notify({ message: this.intl.t('errorUnrepostingTryAgain'), type: 'error' });
        }
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToRepostStatus');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  shareStatusToClipboard(status) {
    let fullStatusUrl = new URL(this.router.urlFor('status.details', status.id), window.location.origin);

    try {
      yield this.clipboard.copy(fullStatusUrl.href);

      this.toast.notify({ message: this.intl.t('statusUrlCopiedToClipboard'), type: 'success' });
    } catch(e) {
      debug(e);

      this.toast.notify({ message: this.intl.t('errorCopyingStatusUrlToClipboard'), type: 'error' });
    }
  }

  @dropTask*
  unmuteConversationOrUser(status) {
    if (status.pleroma.threadMuted || status.muted) {
      return yield this.unmuteConversation.perform(status);
    } else if (get(status.account.pleroma.relationship, 'muting')) {
      return yield this.userActions.unmuteUser.perform(status.account);
    }
  }

  @dropTask*
  muteConversation(status) {
    if (this.session.isAuthenticated) {
      try {
        yield status.mute();

        if (status.pleroma.conversationId) {
          let effectedStatuses = this.getAllStatusesInConversation(status.pleroma.conversationId);

          effectedStatuses.forEach((status) => {
            set(status.pleroma, 'threadMuted', true);
          });
        }

        this.feeds.onStatusMuted(status);

        this.toast.notify({
          status,
          type: 'conversationMuted',
        });
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorMutingConversationTryAgain'), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToMuteConversations');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  unmuteConversation(status) {
    if (this.session.isAuthenticated) {
      try {
        yield status.unmute();

        if (status.pleroma.conversationId) {
          let effectedStatuses = this.getAllStatusesInConversation(status.pleroma.conversationId);

          effectedStatuses.forEach((status) => {
            set(status.pleroma, 'threadMuted', false);
          });
        }

        this.feeds.onStatusUnmuted();

        this.toast.notify({
          status,
          type: 'conversationUnmuted',
        });
      } catch(e) {
        debug(e);

        this.toast.notify({ message: this.intl.t('errorUnmutingConversationTryAgain'), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToUnmuteConversations');
      this.authModal.showSignInModal();
    }
  }

  getAllStatusesInConversation(conversationId) {
    let allStatuses = this.store.peekAll('status');
    return allStatuses.filter((status) => status.pleroma.conversationId === conversationId);
  }

  @dropTask*
  deleteStatus(status) {
    if (this.session.isAuthenticated) {
      if (status.account.id === this.session.currentUser.id) {
        try {
          let confirmed = this.window.confirm(this.intl.t('confirmDeleteStatus'));

          if (confirmed) {
            yield status.destroyRecord();

            this.feeds.removeItemFromAllFeeds(status);

            this.toast.notify({ message: this.intl.t('statusDeleted'), type: 'success' });
          }
        } catch(e) {
          debug(e);

          this.toast.notify({ message: this.intl.t('errorDeletingStatusTryAgain'), type: 'error' });
        }
      } else {
        this.toast.notify({ message: this.intl.t('youCanOnlyDeleteYourOwnStatuses'), type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToDeleteStatus');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  onStatusCreated(newStatus) {
    if (newStatus.isReply) {
      this.toast.notify({ type: 'newReply', newStatus });

      let originalStatus = yield this.store.loadRecord('status', newStatus.inReplyToId);

      set(originalStatus, 'repliesCount', originalStatus.repliesCount + 1);

      originalStatus.descendants.pushObject(newStatus);

      this.feeds.addItem('home', newStatus);
    }

    if (!newStatus.isReply) {
      this.toast.notify({ type: 'newStatus', newStatus });
    }

    if (newStatus.isPublic) {
      this.feeds.addItem('public', newStatus);
      this.feeds.addItem('all', newStatus);
    }

    if (newStatus.isPrivate) {
      this.feeds.addItem('home', newStatus);
    }
  }

  @dropTask*
  createDirectMessage(changeset) {
    try {
      return yield changeset.save();
    } catch(e) {
      let errors = this.error.formErrors(e, 'somethingWentWrongTryAgain');
      this.toast.notify({ message: errors?.firstObject?.detail, type: 'error' });
    }
  }

  @dropTask*
  fetchEmojiReactions(status) {
    let { id } = status;

    // doesn't matter if you're logged in or not.
    try {
      if (!id) {
        throw new Error('Please provide a valid status.');
      }

      if (status.emojiState.isFetched) {
        return;
      }

      let url = this.pleromaApi.endpoints.statusEmojiReactions(id);
      let { headers } = this.session;

      let response = yield fetch(url, { headers });

      let json = yield response.json();

      let statusCode = this.error.detectErrantSuccessCode(response.status, json);

      if (statusCode >= 400) {
        throw this.error.formatPayloadErrors(json);
      }

      json = json.map(reaction => {
        /* eslint-disable camelcase */
        return {
          ...reaction,
          status_id: id,
        };
        /* eslint-enable camelcase */
      });

      this.store.normalizeAndStore('emoji-reaction', json);

      status.emojiState.setFetched();
    } catch (err) {
      let errors = this.error.formErrors(err, 'somethingWentWrongTryAgain');
      this.toast.notify({ message: errors?.firstObject?.detail, type: 'error' });
    }
  }

  @dropTask*
  emojiReact(status, emoji) {
    let { id } = status;

    if (this.session.isAuthenticated) {
      try {
        if (!id) {
          throw new Error('Please provide a valid status.');
        }

        let url = this.pleromaApi.endpoints.statusEmojiReact(id, emoji);
        let { headers } = this.session;

        let response = yield fetch(url, {
          method: 'PUT',
          headers,
        });

        let json = yield response.json();

        let statusCode = this.error.detectErrantSuccessCode(response.status, json);

        if (statusCode >= 400) {
          throw this.error.formatPayloadErrors(json);
        }

        status.emojiState.unsetFetched();

        this.store.normalizeAndStore('status', [json]);
      } catch (err) {
        let errors = this.error.formErrors(err, 'somethingWentWrongTryAgain');
        this.toast.notify({ message: errors?.firstObject?.detail, type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToReactWithEmoji');
      this.authModal.showSignInModal();
    }
  }

  @dropTask*
  emojiReactUndo(status, emoji) {
    let { id } = status;

    if (this.session.isAuthenticated) {
      try {
        if (!id) {
          throw new Error('Please provide a valid status.');
        }

        let url = this.pleromaApi.endpoints.statusEmojiReact(id, emoji);
        let { headers } = this.session;

        let response = yield fetch(url, {
          method: 'DELETE',
          headers,
        });

        let json = yield response.json();

        let statusCode = this.error.detectErrantSuccessCode(response.status, json);

        if (statusCode >= 400) {
          throw this.error.formatPayloadErrors(json);
        }

        status.emojiState.unsetFetched();

        this.store.normalizeAndStore('status', [json]);
      } catch (err) {
        let errors = this.error.formErrors(err, 'somethingWentWrongTryAgain');
        this.toast.notify({ message: errors?.firstObject?.detail, type: 'error' });
      }
    } else {
      this.session.setReasonForAuthentication('youMustSignInToReactWithEmoji');
      this.authModal.showSignInModal();
    }
  }
}

/* global Sentry */

import Service, { inject as service } from '@ember/service';
import { action } from '@ember/object';
import config from 'pleroma-pwa/config/environment';
import humanizeFileSize from 'pleroma-pwa/utils/humanize-file-size';

export default class SentryService extends Service {
  @service feeds;

  @service intl;

  @service instances;

  @service router;

  @service session;

  @service store;

  @service toast;

  @service userPerformanceMonitoring;

  enabled = !!config.APP.SENTRY_DSN && config.environment !== 'test';

  sentryDSN = config.APP.SENTRY_DSN;

  async setup() {
    if (this.enabled) {
      let Sentry = await import('@sentry/browser');

      window.Sentry = Sentry; // Expose yourself.

      let { Ember: EmberIntegration } = await import('@sentry/integrations');

      Sentry.init({
        dsn: this.sentryDSN,
        environment: config.deployTarget ? config.deployTarget : 'other',
        ignoreErrors: this.ignoreErrors,
        blacklistUrls: this.blacklistUrls,
        integrations: [
          new EmberIntegration(),
        ],

        beforeSend: this.beforeSend,
      });

      this.userPerformanceMonitoring
        .on('timingEvent', this.handleTimingEvent)
        .listen();

      this.session.on('currentUserLoaded', this.handleAuthentication);

      this.session.on('invalidationSucceeded', this.handleInvalidation);

      Sentry.setContext('instance', this.instances.current.toJSON());

      this.toast.notify({
        message: this.intl.t('trackingEnabled'),
        type: 'dismissibleMessage',
        link: this.router.urlFor('about'),
        learnMore: this.intl.t('learnMore'),
      });
    }
  }

  willDestroy(...args) {
    super.willDestroy(...args);

    if (this.enabled) {
      this.userPerformanceMonitoring.off('timingEvent', this.handleTimingEvent);

      this.session.off('currentUserLoaded', this.handleAuthentication);

      this.session.off('invalidationSucceeded', this.handleInvalidation)
    }
  }

  @action
  throwException(error) {
    if (this.enabled) {
      Sentry.captureException(new Error(error));
    }
  }

  @action
  beforeSend(event /* hint */) {
    event.extra = {
      ...event.extra,
      memory: this.generateMemoryMetrics(),
      timing: this.generateTimingMetrics(),
      store: this.generateStoreMetrics(),
      feeds: this.generateFeedMetrics(),
    };

    // Check if it is an exception, and if so, show the report dialog
    // if (event.exception) {
    //   Sentry.showReportDialog({ eventId: event.event_id });
    // }

    return event;
  }

  @action
  handleAuthentication() {
    Sentry.setUser({
      id: this.session.currentUser.id,
      username: this.session.currentUser.username,
    });
  }

  @action
  handleInvalidation() {
    Sentry.setUser({
      id: null,
      username: null,
    });
  }

  @action
  handleTimingEvent(eventName, eventDetails, additionalDetails) {
    Sentry.setContext(eventName, {
      ...eventDetails,
      ...additionalDetails,
    });
  }

  generateMemoryMetrics() {
    let { memory } = window.performance;

    if (!memory) {
      return {};
    }

    return {
      jsHeapSizeLimit: humanizeFileSize(memory.jsHeapSizeLimit),
      totalJSHeapSize: humanizeFileSize(memory.totalJSHeapSize),
      usedJSHeapSize: humanizeFileSize(memory.usedJSHeapSize),
    };
  }

  generateTimingMetrics() {
    let events = [];

    for (let event in window.performance.timing) {
      events.push([event, window.performance.timing[event]]);
    }

    return events
      .filter((event) => event[1] > 0)
      .sort((a, b) => a[1] - b[1])
      .map((event, i, eventList) => {
        let prevEvent = eventList[i - 1];

        if (prevEvent) {
          return [`${prevEvent[0]} -> ${event[0]}`, event[1] - prevEvent[1]];
        }
      })
      .filter(Boolean)
      .map((event) => `${event[0]} : ${event[1]}`);
  }

  generateStoreMetrics() {
    return {
      statuses: this.store.peekAll('status').length,
      accounts: this.store.peekAll('user').length,
    };
  }

  generateFeedMetrics() {
    let metrics = {};

    this.feeds.feedList.forEach((feed) => {
      metrics[feed.id] = {
        content: feed.content.length,
        newItems: feed.newItems.length,
      };

      metrics.totalLength += (feed.newItems.length + feed.content.length);
    });

    return metrics;
  }

  ignoreErrors = [ // https://docs.sentry.io/platforms/javascript/#decluttering-sentry
    // Random plugins/extensions
    'top.GLOBALS',
    // See: http://blog.errorception.com/2012/03/tale-of-unfindable-js-error.html
    'originalCreateNotification',
    'canvas.contentDocument',
    'MyApp_RemoveAllHighlights',
    'http://tt.epicplay.com',
    'Can\'t find variable: ZiteReader',
    'jigsaw is not defined',
    'ComboSearch is not defined',
    'http://loading.retry.widdit.com/',
    'atomicFindClose',
    // Facebook borked
    'fb_xd_fragment',
    // ISP "optimizing" proxy - `Cache-Control: no-transform` seems to
    // reduce this. (thanks @acdha)
    // See http://stackoverflow.com/questions/4113268
    'bmi_SafeAddOnload',
    'EBCallBackMessageReceived',
    // See http://toolbar.conduit.com/Developer/HtmlAndGadget/Methods/JSInjection.aspx
    'conduitPage',
  ];

  blacklistUrls = [ // https://docs.sentry.io/platforms/javascript/#decluttering-sentry
    // Facebook flakiness
    /graph\.facebook\.com/i,
    // Facebook blocked
    /connect\.facebook\.net\/en_US\/all\.js/i,
    // Woopra flakiness
    /eatdifferent\.com\.woopra-ns\.com/i,
    /static\.woopra\.com\/js\/woopra\.js/i,
    // Chrome extensions
    /extensions\//i,
    /^chrome:\/\//i,
    // Other plugins
    /127\.0\.0\.1:4001\/isrunning/i,  // Cacaoweb
    /webappstoolbarba\.texthelp\.com\//i,
    /metrics\.itunes\.apple\.com\.edgesuite\.net\//i,
  ];
}

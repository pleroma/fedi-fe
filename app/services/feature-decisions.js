import Service, { inject as service } from '@ember/service';
import { computed, get } from '@ember/object';

export default class FeatureDecisionsService extends Service {
  @service features;

  @service instances;

  @computed('features.emojiReactions', 'instances.current.nodeInfo.metadata.features.[]')
  get emojiReactionsSupported() {
    let backendFeaturesSupported = get(this.instances, 'current.nodeInfo.metadata.features');
    return this.features.isEnabled('emojiReactions') &&
      backendFeaturesSupported.includes('pleroma_emoji_reactions');
  }
}

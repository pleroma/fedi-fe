import Service from '@ember/service';
import Evented from '@ember/object/evented';
import { assert } from '@ember/debug';

export default class BusService extends Service.extend(Evented) {
  events = {
    'blocked': true,
    'unblocked': true,
    'followed': true,
    'unfollowed': true,
    'muted': true,
    'unmuted': true,
  };

  touch() {}

  on(event) {
    assert(`You must pass a valid event that we know about, you passed: ${event}`, this.events[event]);
    super.on(...arguments);
  }

  trigger(event) {
    assert(`You must pass a valid event that we know about, you passed: ${event}`, this.events[event]);
    super.trigger(...arguments);
  }
}

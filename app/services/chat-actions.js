import Service, { inject as service } from '@ember/service';
import { timeout } from 'ember-concurrency';
import { task, dropTask, enqueueTask, restartableTask } from 'ember-concurrency-decorators';
import snakeCaseKeys from 'snakecase-keys';

export default class ChatActionsService extends Service {
  @service error;

  @service intl;

  @service pleromaApi;

  @service session;

  @service store;

  @service toast;

  @service unreadCounts;

  // Input is typically a user ID but could be a group ID later on
  @dropTask*
  assertChat(accountId) {
    if (!this.session.isAuthenticated) {
      this.session.setReasonForAuthentication('pleaseSignIn');
      this.authModal.showSignInModal();
      return;
    }

    try {
      if (!accountId) {
        throw new Error(this.intl.t('pleaseProvideValidUser'));
      }

      let chat;

      // try to see if user exists first.
      try {
        yield this.store.loadRecord('user', accountId);
        yield this.store.loadRecord('relationship', accountId);
      } catch (err) {
        /* eslint-disable no-console */
        console.debug('err', err);
        /* eslint-enable no-console */
        throw new Error(this.intl.t('pleaseProvideValidUser'));
      }

      let url = this.pleromaApi.endpoints.assertChat(accountId);

      let { headers } = this.session;

      let response = yield fetch(url, {
        method: 'POST',
        headers,
      });

      let json = yield response.json();

      let status = this.error.detectErrantSuccessCode(response.status, json);

      if (status >= 400) {
        throw this.error.formatPayloadErrors(json);
      }

      [chat] = this.store.normalizeAndStore('chat', [json]);

      return yield chat;
    } catch (err) {
      let formatted = this.error.formatPayloadErrors(err);

      for (let { detail: message } of formatted.errors) {
        this.toast.notify({ message, type: 'error' });
      }
    }
  }

  @enqueueTask*
  sendMessage(chatId, data) {
    if (!this.session.isAuthenticated) {
      this.session.setReasonForAuthentication('pleaseSignIn');
      this.authModal.showSignInModal();
      return;
    }

    try {
      let url = this.pleromaApi.endpoints.chatMessages(chatId);

      let { headers } = this.session;

      let {
        content = '',
        mediaIds = [],
      } = data;

      if (!content) {
        content = '';
      }

      // TODO: Remove restriction for one media attachment when chat messages support multiple.
      let [mediaId = null] = mediaIds;

      let sendData = { content, mediaId };

      if (!mediaId) {
        delete sendData.mediaId;
      }

      let response = yield fetch(url, {
        method: 'POST',
        headers,
        body: JSON.stringify(snakeCaseKeys(sendData)),
      });

      let json = yield response.json();

      let status = this.error.detectErrantSuccessCode(response.status, json);

      if (status >= 400) {
        throw this.error.formatPayloadErrors(json);
      }

      let [message] = this.store.normalizeAndStore('chat-message', [json]);

      return yield message;
    } catch (err) {
      let formatted = this.error.formatPayloadErrors(err);

      for (let { detail: message } of formatted.errors) {
        this.toast.notify({ message, type: 'error' });
      }
    }
  }

  @task*
  deleteMessage(chatId, messageId) {
    if (!this.session.isAuthenticated) {
      this.session.setReasonForAuthentication('pleaseSignIn');
      this.authModal.showSignInModal();
      return;
    }

    try {
      let url = this.pleromaApi.endpoints.chatMessage(chatId, messageId);

      let { headers } = this.session;

      let response = yield fetch(url, {
        method: 'DELETE',
        headers,
      });

      let json = yield response.json();

      let status = this.error.detectErrantSuccessCode(response.status, json);

      if (status >= 400) {
        throw this.error.formatPayloadErrors(json);
      }

      let [message] = this.store.normalizeAndStore('chat-message', [json]);

      this.store.unloadRecord(message);

      return yield message;
    } catch (err) {
      let formatted = this.error.formatPayloadErrors(err);

      for (let { detail: message } of formatted.errors) {
        this.toast.notify({ message, type: 'error' });
      }
    }
  }

  // @task*
  // markAsRead(messageId) {
  //   if (!this.session.isAuthenticated) {
  //     this.session.setReasonForAuthentication('pleaseSignIn');
  //     this.authModal.showSignInModal();
  //     return;
  //   }
  //
  //   yield timeout(6000);
  //
  //   try {
  //     // be optimistic
  //     this.unreadCounts.decrementUnreadChatCount();
  //
  //     let messageOrig = yield this.store.peekRecord('chat-message', messageId);
  //
  //     if (!messageOrig) {
  //       throw new Error(this.intl.t('messageNotFound'));
  //     }
  //
  //     // console.debug('messageOrig', messageOrig);
  //     // console.debug('unread?', messageOrig.unread);
  //
  //     // let messageData = yield messageOrig.read();
  //     // console.debug('messageData', messageData);
  //
  //     // don't mark as read if it's already read :shrug:
  //     // don't mark as read if it's my msg :shrug:
  //
  //     // console.debug('messageOrig.account.id', messageOrig.account.id);
  //     // console.debug('this.session.currentUser.id', this.session.currentUser.id);
  //
  //     if (!messageOrig.unread || messageOrig.account.id === this.session.currentUser.id) {
  //       return messageOrig;
  //     }
  //
  //     let url = this.pleromaApi.endpoints.chatMessageMarkAsRead(messageOrig.chat.id, messageId);
  //     // console.debug('url', url);
  //
  //     // // return yield url;
  //     // // throw new Error(url);
  //
  //     let { headers } = this.session;
  //
  //     let response = yield fetch(url, {
  //       method: 'POST',
  //       headers,
  //     });
  //
  //     let json = yield response.json();
  //
  //     let status = this.error.detectErrantSuccessCode(response.status, json);
  //
  //     if (status >= 400) {
  //       throw this.error.formatPayloadErrors(json);
  //     }
  //
  //     // console.debug('json', json);
  //
  //     let [message] = this.store.normalizeAndStore('chat-message', [json]);
  //
  //     // console.debug('message', message);
  //     // console.debug('unread? should be false', message.unread);
  //
  //     // this.store.unloadRecord(message);
  //
  //     return yield message;
  //   } catch (err) {
  //     let formatted = this.error.formatPayloadErrors(err);
  //
  //     for (let { detail: message } of formatted.errors) {
  //       this.toast.notify({ message, type: 'error' });
  //     }
  //   }
  // }

  @task*
  getChatMessage(chatId, messageId) {
    let url = this.pleromaApi.endpoints.chatMessage(chatId, messageId);

    let { headers } = this.session;

    let response = yield fetch(url, {
      method: 'GET',
      headers,
    });

    let json = yield response.json();

    let status = this.error.detectErrantSuccessCode(response.status, json);

    if (status >= 400) {
      throw this.error.formatPayloadErrors(json);
    }

    let [message] = this.store.normalizeAndStore('chat-message', [json]);

    return message;
  }

  @restartableTask*
  markChatAsRead(chatId, lastReadId, options = {}) {
    let delayed = !!options.delayed;

    if (!this.session.isAuthenticated) {
      this.session.setReasonForAuthentication('pleaseSignIn');
      this.authModal.showSignInModal();
      return;
    }

    try {
      let chatOrig = yield this.store.peekRecord('chat', chatId);

      if (!chatOrig) {
        throw new Error(this.intl.t('directMessageNotFound'));
      }

      if (chatOrig.unread === 0) {
        return;
      }

      let message = yield this.store.peekRecord('chat-message', lastReadId);

      if (!message.unread) {
        return;
      }

      // pause here so task can be cancelled if it restarts.
      // this happens when multiple messages are viewed at once. the latest one wins.
      if (delayed) {
        yield timeout(1500);
      }

      // be optimistic in decrementing unread chat count
      for (let i = 0; i < chatOrig.unread; i++) {
        this.unreadCounts.decrementUnreadChatCount();
      }

      let url = this.pleromaApi.endpoints.chatMarkAsRead(chatId);

      let sendData = { lastReadId };

      let { headers } = this.session;

      let response = yield fetch(url, {
        method: 'POST',
        headers,
        body: JSON.stringify(snakeCaseKeys(sendData)),
      });

      let json = yield response.json();

      let status = this.error.detectErrantSuccessCode(response.status, json);

      if (status >= 400) {
        throw this.error.formatPayloadErrors(json);
      }

      let [chat] = this.store.normalizeAndStore('chat', [json]);

      return yield chat;
    } catch (err) {
      let formatted = this.error.formatPayloadErrors(err);

      for (let { detail: message } of formatted.errors) {
        this.toast.notify({ message, type: 'error' });
      }
    }
  }
}

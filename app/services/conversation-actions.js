import Service, { inject as service } from '@ember/service';
import { debug, assert } from '@ember/debug';
import { dropTask } from 'ember-concurrency-decorators';

export default class ConversationActionsService extends Service {
  @service() unreadCounts;

  @dropTask*
  markAsRead(conversation) {
    assert('Must pass a conversation to the conversation-actions markAsRead task', conversation);

    try {
      yield conversation.markAsRead();

      this.unreadCounts.collectUnreadCounts.unlinked().perform();
    } catch (e) {
      debug(e);
    }
  }
}

export function durationFromSeconds(seconds) {
  let minute = 60;
  let hour = 60 * minute;
  let day = 24 * hour;

  // Were dealing with strings here, since these are used in a <select
  return {
    days: Math.floor(seconds / day),
    hours: Math.floor(seconds % day / hour),
    minutes: Math.floor(seconds % hour / minute),
  };
}

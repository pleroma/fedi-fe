import { set } from '@ember/object';
import { assert } from '@ember/debug';
import config from 'pleroma-pwa/config/environment';
import { isPresent, typeOf } from '@ember/utils';
import { run } from '@ember/runloop';

export default class Poller {
  intervalId = null;

  func = null;

  intervalTimeout = config.APP.POLLING_TIMEOUT;

  constructor(func, intervalTimeout = config.APP.POLLING_TIMEOUT) {
    assert('you must pass a func to the poller util, were going to call this func when we poll', typeOf(func) === 'function');
    this.func = func;
    this.intervalTimeout = intervalTimeout;
  }

  get started() {
    return isPresent(this.intervalId);
  }

  start() {
    // escape hatch for tests
    // http://ember-concurrency.com/docs/testing-debugging
    if (config.environment !== 'test') {
      window.clearInterval(this.intervalId);
      set(this, 'intervalId', window.setInterval(() => run(this.func), this.intervalTimeout));
    }
  }

  stop() {
    window.clearInterval(this.intervalId);
  }
}

import { typeOf } from '@ember/utils';

export function tryLinkedTask(task, ...args) {
  if (task?.linked) {
    return task.linked().perform(...args);
  } else if (typeOf(task) === 'function') {
    return task(...args);
  }
}

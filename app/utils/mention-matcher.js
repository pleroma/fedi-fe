export const matchesMention = (mention, url) => {
  if (url === mention.url) return true;

  let [username, instance] = mention.acct.split('@');
  let matchString = new RegExp(`://${instance}/.*${username}$`, 'g');

  return !!url.match(matchString);
}

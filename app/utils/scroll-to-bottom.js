export default function scrollToBottom(element) {
  if (!element?.nodeName) {
    return;
  }

  let newScrollTop = element.scrollHeight - element.clientHeight;
  let behavior = 'auto';

  if (newScrollTop === element.scrollTop) {
    return;
  }

  if (Math.abs(newScrollTop - element.scrollTop) < 300) {
    behavior = 'smooth';
  }

  element.scrollTo({ top: newScrollTop, behavior });
}

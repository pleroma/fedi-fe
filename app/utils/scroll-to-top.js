export default function scrollToTop(element=window) {
  let behavior = 'auto';

  if (0 === element.scrollTop) {
    return;
  }

  if (Math.abs(element.scrollTop) < 300) {
    behavior = 'smooth';
  }

  element.scrollTo({ top: 0, behavior });
}

import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import { inject as service } from '@ember/service';

export default ApplicationSerializer.extend(EmbeddedRecordsMixin, {
  pleromaApi: service(),

  attrs: {
    account: { embedded: 'always' },
    lastMessage: { embedded: 'always' },
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'chat': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'chat': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  // This overrides `normalize` from `EmbeddedRecordsMixin`.
  normalize(typeClass, hash, prop) {
    hash.links = {
      messages: this.pleromaApi.endpoints.chatMessages(hash.id),
    };

    return this._super(typeClass, hash, prop);
  },
});

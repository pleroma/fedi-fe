import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';

export default ApplicationSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    pleroma: { embedded: 'always' },
  },

  modelNameFromPayloadKey() {
    return 'attachment';
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    if (!id && payload.id) {
      // eslint-disable-next-line prefer-destructuring
      id = payload.id;
    }

    let wrappedPayload = {
      attachment: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  // This overrides `normalize` from `EmbeddedRecordsMixin`.
  normalize(typeClass, hash, prop) {
    // attachment-extra
    if (hash.id && hash.pleroma) {
      hash.pleroma = {
        ...hash.pleroma,
        id: hash.id,
      };
    }

    return this._super(typeClass, hash, prop);
  },
});

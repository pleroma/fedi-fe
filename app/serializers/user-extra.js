import JSONSerializer from 'ember-data/serializers/json';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import { underscore } from '@ember/string';
import isEqual from 'lodash.isequal';

export default JSONSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    notificationSettings: { embedded: 'always' },
  },

  keyForAttribute(attr /* , method */) {
    return underscore(attr);
  },

  normalize(model, hash, prop) {
    /* eslint-disable camelcase */
    // notification-setting
    if (hash.id && hash.notification_settings) {
      hash.notification_settings = {
        ...hash.notification_settings,
        id: hash.id,
      };
    }
    /* eslint-enable camelcase */

    let relationship = {
      type: 'relationship',
      id: hash.id,
    };

    if (!hash.relationship || isEqual(hash.relationship, {})) {
      hash.relationship = relationship;
    } else {
      if (hash.relationship?.id) {
        delete hash.relationship.id;
      }

      hash.relationship = {
        ...relationship,
        attributes: hash.relationship,
      };
    }

    return this._super(model, hash, prop);
  },
});

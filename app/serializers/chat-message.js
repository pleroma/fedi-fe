import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';

export default ApplicationSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    attachment: { embedded: 'always' },
    emojis: { deserialize: 'records' },
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'chat-message': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'chat-message': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalize(typeClass, hash, prop) {
    if (!hash.account && hash.account_id) {
      hash.account = {
        type: 'user',
        id: hash.account_id,
      };

      delete hash.account_id;
    }

    if (!hash.chat && hash.chat_id) {
      hash.chat = {
        type: 'chat',
        id: hash.chat_id,
      };

      delete hash.chat_id;
    }

    return this._super(typeClass, hash, prop);
  },
});

import ApplicationSerializer from './application';
import { dasherize } from '@ember/string';

export default class PollOptionSerializer extends ApplicationSerializer {
  extractId(modelClass, resourceHash) {
    return `${resourceHash.poll_id}-${dasherize(resourceHash.title)}`;
  }
}

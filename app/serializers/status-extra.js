import JSONSerializer from 'ember-data/serializers/json';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import { underscore } from '@ember/string';

export default JSONSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    emojiReactions: { embedded: 'always' },
  },

  keyForAttribute(attr /* , method */) {
    return underscore(attr);
  },

  normalize(model, hash, prop) {
    /* eslint-disable camelcase */
    // emoji-reaction
    if (hash.id && hash.emoji_reactions) {
      hash.emoji_reactions = hash.emoji_reactions.map(reaction => {
        return {
          ...reaction,
          status_id: hash.id,
        };
      });
    }
    /* eslint-enable camelcase */

    return this._super(model, hash, prop);
  },
});

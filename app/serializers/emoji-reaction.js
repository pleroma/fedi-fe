import JSONSerializer from 'ember-data/serializers/json';
import { underscore } from '@ember/string';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import { Base64 } from 'js-base64';

export default JSONSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    accounts: { embedded: 'always' },
  },

  keyForAttribute(attr /* , method */) {
    return underscore(attr);
  },

  /* eslint-disable camelcase */
  extractId(_, { status_id, name }) {
    return Base64.encode(`:${status_id}:${name}`);
  },
  /* eslint-enable camelcase */
});

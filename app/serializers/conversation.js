import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';

export default ApplicationSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    accounts: {
      deserialize: 'records',
    },

    lastStatus: {
      deserialize: 'records',
    },
  },

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      conversations: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      conversation: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },
});

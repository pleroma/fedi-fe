import ApplicationSerializer from './application';
import { guidFor } from '@ember/object/internals';

export default ApplicationSerializer.extend({
  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'client-token': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  extractId(modelClass, resourceHash) {
    return guidFor(resourceHash);
  },
});

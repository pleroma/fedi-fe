import ApplicationSerializer from './application';
import camelCaseKeys from 'camelcase-keys';
import uuid from 'uuid/v4';

export default ApplicationSerializer.extend({
  keyForAttribute(attr) {
    return attr; // dont call super, we dont want to decamelize these
  },

  normalize(model, hash) {
    return this._super(model, camelCaseKeys(hash, { deep: true }));
  },

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      instance: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      instance: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  extractId() {
    let value = this._super(...arguments);

    if (!value) {
      return uuid();
    }

    return value;
  },
});

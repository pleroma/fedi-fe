import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import { inject as service } from '@ember/service';
import { setProperties } from '@ember/object';

export default ApplicationSerializer.extend(EmbeddedRecordsMixin, {
  pleromaApi: service(),

  attrs: {
    emojis: { deserialize: 'records' },
    pleroma: { embedded: 'always' },
    source: { embedded: 'always' },

    avatar: {
      serialize: false,
    },

    displayName: {
      serialize: false,
    },
  },

  normalize(model, hash, prop) {
    hash.links = {
      mutes: this.pleromaApi.endpoints.accountMutes,
      blocks: this.pleromaApi.endpoints.accountBlocks,
      tokens: this.pleromaApi.endpoints.oauthTokens,
    };

    // user-source
    if (hash.id && hash.source) {
      hash.source = {
        ...hash.source,
        id: hash.id,
      };
    }

    // user-extra
    if (hash.id && hash.pleroma) {
      hash.pleroma = {
        ...hash.pleroma,
        id: hash.id,
      };
    }

    return this._super(model, hash, prop);
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      user: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      users: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  serializeIntoHash(hash, type, record, options) {
    setProperties(hash, this.serialize(record, options));
  },
});

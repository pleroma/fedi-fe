import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';

export default ApplicationSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    account: { embedded: 'always' },
    chatMessage: { embedded: 'always' },
    pleroma: { embedded: 'always' },
    status: { embedded: 'always' },
  },

  // This overrides `normalize` from `EmbeddedRecordsMixin`.
  normalize(typeClass, hash, prop) {
    // notification-extra
    if (hash.id && hash.pleroma) {
      hash.pleroma = {
        ...hash.pleroma,
        id: hash.id,
      };
    }

    return this._super(typeClass, hash, prop);
  },

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      notifications: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      notification: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },
});

import JSONSerializer from 'ember-data/serializers/json';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import { underscore } from '@ember/string';

export default JSONSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    pleroma: { embedded: 'always' },
  },

  keyForAttribute(attr /* , method */) {
    return underscore(attr);
  },

  normalize(model, hash, prop) {
    // user-source-extra
    if (hash.id && hash.pleroma) {
      hash.pleroma = {
        ...hash.pleroma,
        id: hash.id,
      };
    }

    return this._super(model, hash, prop);
  },
});

import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import uuid from 'uuid/v4';

export default ApplicationSerializer.extend(EmbeddedRecordsMixin, {
  attrs: {
    accounts: {
      deserialize: 'records',
    },

    statuses: {
      deserialize: 'records',
    },

    hashtags: {
      deserialize: 'records',
    },
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    // Make up a UUID on the fly.
    id = uuid();

    let wrappedPayload = {
      'search_result': { ...payload, id },
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },
});

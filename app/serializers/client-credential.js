import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'client-credential': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },
});

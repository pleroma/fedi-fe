import ApplicationSerializer from './application';
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import { inject as service } from '@ember/service';
import config from 'pleroma-pwa/config/environment';

export default class PollSerializer extends ApplicationSerializer.extend(EmbeddedRecordsMixin) {
  @service pollActions;

  attrs = {
    options: {
      embedded: 'always',
    },
  };

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      polls: payload,
    };

    return super.normalizeSingleResponse(store, primaryModelClass, wrappedPayload, id, requestType);
  };

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      poll: payload,
    };

    return super.normalizeSingleResponse(store, primaryModelClass, wrappedPayload, id, requestType);
  };

  normalize(modelClass, resourceHash, prop) {
    // Expose poll_id on the options so we can use the poll_id when generating the guid
    resourceHash.options.forEach((option) => {
      // eslint-disable-next-line camelcase
      option.poll_id = resourceHash.id;
    });

    let normalized = super.normalize(modelClass, resourceHash, prop);

    // If the poll is not expired, register it for expiration
    if (!normalized.data.attributes.expired && config.environment !== 'test') {
      let diff = Math.abs(new Date() - normalized.data.attributes.expiresAt);

      this.pollActions.reloadPollIn(normalized.data.id, diff);
    }

    return normalized;
  };
}

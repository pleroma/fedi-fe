import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  // {
  //   "womans_clothes": "\ud83d\udc5a",
  //   "cookie": "\ud83c\udf6a",
  //   "woman_with_headscarf": "\ud83e\uddd5"
  // }
  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let normalized = Object.entries(payload).map(([shortcode, value]) => {
      return {
        shortcode,
        value,
        visibleInPicker: true,
      };
    });

    let wrappedPayload = {
      'static_emoji': normalized,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  extractId(_, { shortcode }) {
    return shortcode;
  },
});

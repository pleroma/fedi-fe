import ApplicationSerializer from './application';
// import uuid from 'uuid/v4';

export default ApplicationSerializer.extend({
  modelNameFromPayloadKey() {
    return 'media';
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      media: payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },
});

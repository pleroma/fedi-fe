import ApplicationSerializer from './application';
import camelCaseKeys from 'camelcase-keys';
import uuid from 'uuid/v4';

export default ApplicationSerializer.extend({
  keyForAttribute(attr) {
    return attr; // dont call super, we dont want to decamelize these
  },

  // This endpoint currently already returns camelCase
  // keys. It's one of the few that does. Putting this here
  // in case that ever changes.
  normalize(model, hash) {
    return this._super(model, camelCaseKeys(hash, { deep: true }));
  },

  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'node-info': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    let wrappedPayload = {
      'node-info': payload,
    };

    return this._super(store, primaryModelClass, wrappedPayload, id, requestType);
  },

  extractId() {
    return this._super(...arguments) || uuid();
  },
});

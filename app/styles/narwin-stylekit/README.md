# Narwin Stylekit

A bunch of helper mixins and functions to help you write Sass/PostCSS more
quickly.

## Goals

- Makes repetitive code quicker to write through mixins and functions
- `narwin-stylekit` should never on its own create any output when compiled
- Totally design-agnostic; doesn't impose colors, breakpoints, dimensions on
you, other than the occasional "sane default" with overrides

## Usage

`@import "narwin-stylekit/narwin-stylekit";`

## Documentation

First install [SassDoc](http://sassdoc.com) dependencies:

`$ npm i`

Install the `narwin-sassdoc-theme` submodule:

`$ git submodule update --init`

To compile SassDoc docs:

`$ npx sassdoc .`

View the docs in your browser at `[project-path]/narwin-stylekit/docs/index.html`

[SassDoc annotation documentation](http://sassdoc.com/annotations/)

import Concern from 'ember-concerns';
import { inject as service } from '@ember/service';
import { dropTask } from 'ember-concurrency-decorators';
import { tracked } from '@glimmer/tracking';
import humanizeFileSize from 'pleroma-pwa/utils/humanize-file-size';
import { isPresent } from '@ember/utils';
import { debug } from '@ember/debug';
import { guidFor } from '@ember/object/internals';

export default class AttachmentUploaderConcern extends Concern {
  // Services

  @service features;

  @service instances;

  @service intl;

  // Variables

  @tracked attempts = 0;

  maxAttempts = 3;

  @tracked shouldRetry = true;

  @tracked uploadError = null;

  constructor() {
    super(...arguments);
    this.reset();
  }

  reset() {
    this.attempts = 0;
    this.uploadError = null;
  }

  get id() {
    return guidFor(this.model);
  }

  get uploaded() {
    return isPresent(this.model.id);
  }

  get canRetry() {
    return this.shouldRetry && this.attempts < this.maxAttempts;
  }

  @dropTask*
  upload() {
    if (this.uploaded || !this.canRetry) {
      return this.model;
    }

    let { file } = this.model;

    try {
      if (!(file instanceof File || file instanceof Blob) || !file.size || !file.type) {
        this.shouldRetry = false;
        throw new Error(this.intl.t('pleaseUploadValidFile'));
      }

      if (file.size > this.instances.current.uploadLimit) {
        this.shouldRetry = false;
        throw new Error(this.intl.t('fileTooLarge', { max: humanizeFileSize(this.instances.current.uploadLimit) }));
      }

      debug(`attempts: ${this.attempts}`);

      this.uploadError = null;

      // TODO: remove this conditional. this simulates an error.
      if (
        this.features.isEnabled('simulateFirstUploadFail') &&
        this.attempts === 0
      ) {
        throw new Error(this.intl.t('uploadFailed'));
      }

      yield this.model.save();

      this.reset();

      return this.model;
    } catch(err) {
      this.uploadError = err;

      debug('trying again');

      this.attempts = this.attempts + 1;

      return yield this.model;
    }
  }

  /**
   * Receive a number that indicates which retry is occurring.
   * Return number of milliseconds to wait before that retry occurs.
   * Some randomization is added.
   *
   * @private
   */
  exponentialWaitTime(whichRetry = 1) {
    return (
      Math.pow(2, parseInt(whichRetry)) * 1000 +
      Math.floor(Math.random() * 1000)
    );
  }
}

import Concern from 'ember-concerns';

let fetched = {};

export default class StatusEmojiStateConcern extends Concern {
  get isFetched() {
    let { id } = this.model;
    return !!fetched[id];
  }

  setFetched() {
    let { id } = this.model;
    fetched[id] = true;
  }

  unsetFetched() {
    let { id } = this.model;
    fetched[id] = false;
  }
}

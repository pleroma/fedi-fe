import Concern from 'ember-concerns';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { allSettled } from 'ember-concurrency';
import { task, dropTask } from 'ember-concurrency-decorators';
import { action, notifyPropertyChange, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { guidFor } from '@ember/object/internals';
import { debug } from '@ember/debug';
import { filesFromClipboard } from 'pleroma-pwa/utils/clipboard';

export default class StatusMediaConcern extends Concern {
  @service error;

  @service intl;

  @service statusActions;

  @service store;

  @tracked attachments = [];

  @tracked uploaderTasks = {};

  @tracked files = [];

  constructor() {
    super(...arguments);
    this.reset();
  }

  @action
  reset() {
    this.attachments = [];
    this.files = [];
    this.uploaderTasks = {};
    return this.attachments;
  }

  get uploadCount() {
    return this.attachments?.length || 0;
  }

  get allSuccessful() {
    return this.attachments.every(a => a?.uploader?.uploaded);
  }

  get ids() {
    return this.attachments.map(upload => upload?.id);
  }

  @dropTask*
  addAttachments() {
    debug('addAttachments');
    let { files } = this;

    if (isEmpty(files)) {
      throw new Error(this.intl.t('pleaseUploadValidFile'));
    }

    let newAttachments = Array.from(files)
      .map(file =>
        this.store.createRecord('attachment', { file }),
      );

    this.attachments = this.attachments.concat(newAttachments);

    return yield newAttachments;
  }

  @task*
  tryUpload(attachments = []) {
    yield allSettled(attachments.map(attachment => {
      return this.tryAttachment(attachment);
    }));

    this.setMediaIds();

    this.throwIfError(attachments);

    this.checkIfAllSuccessful();
  }

  @action
  checkIfAllSuccessful() {
    debug('checkIfAllSuccessful');

    if (this.allSuccessful) {
      debug('all successful');
    }
  }

  tryAttachment(attachment) {
    this.uploaderTasks[guidFor(attachment)] =
      attachment.uploader.upload.perform();

    return this.uploaderTasks[guidFor(attachment)];
  }

  get cancelReason() {
    return this.intl.t('uploadCancelled');
  }

  async handleSelectedFiles(files) {
    this.files = files;

    try {
      let newAttachments = await this.addAttachments.perform();
      await this.tryUpload.perform(newAttachments);

      return this.attachments;
    } catch (rawErr) {
      let err = rawErr;

      if (err?.message?.includes(this.cancelReason)) {
        // do nothing. user knows they cancelled.
      } else {
        let errors = this.error.formErrors(err, 'uploadFailed');

        errors.forEach(({ detail }) => {
          let foundIdentical = this.model.changeset.errors.any(e => {
            return e.key === 'mediaIds' && e.validation.includes(detail);
          });

          if (!foundIdentical) {
            this.model.changeset.pushErrors('mediaIds', detail);
          }
        });
      }

      notifyPropertyChange(this, 'attachments');

      return this.attachments;
    }
  }

  @action
  cancel(attachment) {
    debug('cancel');

    let uploader = this.uploaderTasks[guidFor(attachment)];

    if (uploader) {
      uploader.cancel(this.cancelReason);
    }

    this.removeAttachment(attachment);
  }

  @action
  throwIfError(attachments = []) {
    let erroredFn = u => u.uploader.uploadError;
    let anyErrored = attachments.any(erroredFn);

    if (anyErrored) {
      let erroredAttachments = attachments.filter(erroredFn);

      let errors = erroredAttachments.map(erroredFn).filter(Boolean);

      throw { errors };
    }
  }

  @action
  setMediaIds() {
    set(this.model.changeset, 'mediaIds', this.ids.filter(Boolean));
  }

  @action
  async onFilesSelected(event) {
    let files = Array.from(event.target.files);
    event.target.value = null;

    this.handleSelectedFiles(files);
  }

  @action
  async onFilesPasted(event) {
    let files = filesFromClipboard(event);

    if (files.length > 0) {
      event.stopPropagation();
      event.preventDefault();
      this.handleSelectedFiles(files);
    }
  }

  @action
  removeAttachment(attachment) {
    debug('removeAttachment');

    this.attachments = this.attachments.filter(a =>
      guidFor(a) !== guidFor(attachment),
    );

    this.setMediaIds();

    this.checkIfAllSuccessful();

    return this.model.changeset.mediaIds;
  }
}

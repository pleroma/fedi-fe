import Concern from 'ember-concerns';
import { tracked } from '@glimmer/tracking';

export default class StatusVisibilityStateConcern extends Concern {
  // cache height per status, should be string in CSS format `<num>px`
  @tracked height;

  @tracked shouldShow = true;

  show() {
    this.shouldShow = true;
  }

  hide() {
    this.shouldShow = false;
  }

  setHeight(height) {
    this.height = height;
  }
}

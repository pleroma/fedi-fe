import Concern from 'ember-concerns';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { addEmojis, removeEmojis } from 'pleroma-pwa/helpers/emojify';

export default class UserNamesConcern extends Concern {
  @service pleromaApi;

  @computed('model.acct', 'pleromaApi.apiBaseUrl')
  get fullScreenName() {
    let { acct } = this.model;

    if (acct.includes('@')) {
      return acct;
    }

    let { apiBaseUrl } = this.pleromaApi;
    let url = new URL(apiBaseUrl);

    return `${acct}@${url.hostname}`;
  }

  @computed('model.{displayName,emojis}')
  get displayNameWithEmojis() {
    let { displayName, emojis } = this.model;

    return addEmojis(displayName, emojis || []);
  }

  @computed('model.{displayName,emojis}')
  get displayNameWithoutEmojis() {
    let { displayName, emojis } = this.model;

    return removeEmojis(displayName, emojis || []);
  }
}

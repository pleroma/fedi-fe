import Concern from 'ember-concerns';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ChatReloadableConcern extends Concern {
  @tracked reloadable = true;

  @action
  reloadAll(override = false) {
    if (!this.reloadable && !override) {
      return;
    }

    this.reload();
    this.reloadMessages();
  }

  @action
  reload(override = false) {
    if (!this.reloadable && !override) {
      return;
    }

    return this.model.reload();
  }

  @action
  reloadMessages(override = false) {
    if (!this.reloadable && !override) {
      return;
    }

    return this.model.hasMany('messages').reload();
  }

  @action
  enable() {
    this.reloadable = true;
  }

  @action
  disable() {
    this.reloadable = false;
  }
}

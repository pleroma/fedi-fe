import Concern from 'ember-concerns';
import { inject as service } from '@ember/service';

/**
 * Being an item means it's an object that ends up linking to a status at some
 * point.
 *
 * The object could be a status itself.
 *
 * It could also be a notification about a status, like a repost/boost or
 * favourite status notification.
 *
 * It could also be a poll.
 *
 * @public
 */
export default class ItemableConcern extends Concern {
  @service router;

  /**
   * The logic for this was spread in multiple places in a template, so it was
   * brought here to centralize it.
   *
   * @public
   */
  get isItem() {
    let item = this.model;

    return !!(item.content || item.poll || item.createdAt) && !item.isFollow;
  }

  get isCurrentItem() {
    let { currentRoute, currentRouteName } = this.router;

    return currentRouteName === 'status.details' &&
      currentRoute.params?.id === this.model.id;
  }
}

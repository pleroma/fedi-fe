import {
  click,
  currentRouteName,
  fillIn,
  find,
  settled,
  triggerEvent,
  visit,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import QUnit from 'qunit';
const { assert } = QUnit;

/**
 * NOTE: Do only test this when specifically testing authentication, otherwise use the withToken/withSession user traits
 * Signs in the user passed. WIll use the auth-modal if it is on the screen. Async Test Helper.
 *
 * @param {Object} user - test selector name. defaults to `selector`
 * @public
 */

export default async function signIn(user) {
  if (!find(generateTestSelector('auth-modal'))) {
    if (find(generateTestSelector('account-cta-sign-in-button'))) {
      await click(generateTestSelector('account-cta-sign-in-button'));
    } else {
      await visit('/sign-in');

      assert.equal(currentRouteName(), 'sign-in', 'Navigated to the sign-in route');
    }
  }

  assert.dom(generateTestSelector('sign-in-form')).exists();
  assert.dom(generateTestSelector('sign-in-button')).exists();
  assert.dom(generateTestSelector('username-input')).exists();
  assert.dom(generateTestSelector('password-input')).exists();

  user.username && fillIn(generateTestSelector('username-input'), user.username);
  user.password && fillIn(generateTestSelector('password-input'), user.password);

  await settled();

  await triggerEvent(generateTestSelector('sign-in-form'), 'submit');

  await settled();
}

import { triggerEvent } from '@ember/test-helpers';

export default function selectChoose(selector, value) {
  let dropdown;
  if (selector instanceof HTMLElement) {
    dropdown = selector;
  } else {
    dropdown = find(selector);
  }

  dropdown.value = value;
  triggerEvent(dropdown, 'change');
}

import { settled, visit as _visit } from '@ember/test-helpers';

export default async function dangerouslyVisit(url) {
  try {
    await _visit(url);
  } catch(e) {
    if (e.message !== 'TransitionAborted') {
      throw e;
    }
  }

  await settled();
}

import {
  click,
  currentRouteName,
  fillIn,
  find,
  settled,
  triggerEvent,
  visit,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import QUnit from 'qunit';
const { assert } = QUnit;
import faker from 'faker';

/**
 * Registers the user passed. WIll use the auth-modal if it is on the screen. Async Test Helper.
 *
 * @param {Object} user - test selector name. defaults to `selector`
 * @public
 */
async function register(user) {
  if (!find(generateTestSelector('registration-form'))) {
    if (find(generateTestSelector('account-cta-create-account-button'))) {
      await click(generateTestSelector('account-cta-create-account-button'));
    } else {
      await visit('/sign-up');

      assert.equal(currentRouteName(), 'sign-up');
    }
  }

  assert.dom(generateTestSelector('registration-form')).exists();

  user = user || {
    password: faker.internet.password(),
    username: faker.internet.domainWord(),
    displayName: faker.name.firstName(),
    email: faker.internet.email(),
    captcha: faker.internet.password(),
  };

  user.username && fillIn(generateTestSelector('username-input'), user.username);
  user.email && fillIn(generateTestSelector('email-input'), user.email);
  user.password && fillIn(generateTestSelector('password-input'), user.password);
  user.password && fillIn(generateTestSelector('password-confirmation-input'), user.password);
  user.displayName && fillIn(generateTestSelector('display-name-input'), user.displayName);
  user.captcha && fillIn(generateTestSelector('registration-captcha-input'), user.captcha);

  await settled();

  await triggerEvent(generateTestSelector('registration-form'), 'submit');
  await settled();

  return user;
}

export default register;

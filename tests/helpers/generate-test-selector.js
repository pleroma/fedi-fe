/**
 * Generates the selector for the passed in argument, corresponding to the
 * `data-test-selector` attribute. If selector and value is passed in, it will
 * generate for `[data-test-${selector}=${value}]`
 *
 * @param {String} selector - test selector name. defaults to `selector`
 * @param {String} value - value of the test selector
 * @returns {String} selector string for a test selector
 * @public
 */
function generateTestSelector(selector, value) {
  return `[data-test-${value ? selector : 'selector'}='${value || selector}']`;
}

export default generateTestSelector;

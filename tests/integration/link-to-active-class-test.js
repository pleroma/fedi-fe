import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Integration | Ensure we correctly modify the link-to active class', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('it properly modifies the default link-to active class to be is-active', async function(assert) {
    // eslint-disable-next-line no-underscore-dangle
    this._active = function() {
      return true;
    };

    await render(hbs`<LinkTo @route="feed" @model="public" @_active={{this._active}} data-test-selector="link-component"/>`);

    assert.dom(generateTestSelector('link-component')).exists();

    assert.dom(generateTestSelector('link-component')).hasClass('is-active');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';

module('Integration | Modifiers | class-list', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('gives a default class to the element', async function(assert) {
    await render(hbs`<div {{class-list "default-class"}} data-test-selector="testable-one"></div>`);

    assert.dom(generateTestSelector('testable-one')).hasClass('default-class');
  });

  test('it toggles classes on elements', async function(assert) {
    this.set('classOne', false);
    this.set('classTwo', 0);
    this.set('classThree', null);

    await render(hbs`<div
      {{class-list "default"
        class-one=this.classOne
        class-two=this.classTwo
        class-three=this.classThree
      }} data-test-selector="testable-one"></div>`);

    assert.dom(generateTestSelector('testable-one')).hasClass('default');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-one');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-two');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-three');

    this.set('classOne', true);

    await settled();

    assert.dom(generateTestSelector('testable-one')).hasClass('default');
    assert.dom(generateTestSelector('testable-one')).hasClass('class-one');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-two');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-three');

    this.set('classTwo', 1);

    await settled();

    assert.dom(generateTestSelector('testable-one')).hasClass('default');
    assert.dom(generateTestSelector('testable-one')).hasClass('class-one');
    assert.dom(generateTestSelector('testable-one')).hasClass('class-two');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-three');

    this.set('classThree', {});

    await settled();

    assert.dom(generateTestSelector('testable-one')).hasClass('default');
    assert.dom(generateTestSelector('testable-one')).hasClass('class-one');
    assert.dom(generateTestSelector('testable-one')).hasClass('class-two');
    assert.dom(generateTestSelector('testable-one')).hasClass('class-three');

    this.set('classOne', false);
    this.set('classTwo', 0);
    this.set('classThree', null);

    await settled();

    assert.dom(generateTestSelector('testable-one')).hasClass('default');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-one');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-two');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-three');
  });

  test('it respects splattributes', async function(assert) {
    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));

    this.set('classOne', false);

    let status = this.server.create('status');
    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    // The feed item has splattributes AND uses this class-list-modifier.
    await render(hbs`<FeedItem
      @feedItem={{this.status}}
      class="default-class {{if this.classOne "class-one"}}"
      data-test-selector="testable-one"
    />`);

    assert.dom(generateTestSelector('testable-one')).hasClass('default-class');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-one');

    this.set('classOne', true);

    await settled();

    assert.dom(generateTestSelector('testable-one')).hasClass('default-class');
    assert.dom(generateTestSelector('testable-one')).hasClass('class-one');

    this.set('classOne', false);

    await settled();

    assert.dom(generateTestSelector('testable-one')).hasClass('default-class');
    assert.dom(generateTestSelector('testable-one')).doesNotHaveClass('class-one');
  });
});

import { module, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  find,
  render,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Modifiers | capture-focus-on', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  skip('gives focus to an element when a passed key is pressed', async function(assert) {
    this.set('key', '/');

    await render(hbs`<input data-test-selector="testable-input" {{capture-focus-on key=this.key }}>`);
    await settled();

    document.body.blur();

    assert.dom(generateTestSelector('testable-input'))
      .isNotFocused();

    await triggerKeyEvent(document, 'keydown', this.key);
    await settled();

    assert.dom(generateTestSelector('testable-input'))
      .isFocused();
  });

  skip('does not put that pressed key into the input (calls preventDefault)', async function(assert) {
    this.set('key', '/');

    await render(hbs`<input {{capture-focus-on key=this.key }} data-test-selector="testable-one">`);

    await triggerKeyEvent(document, 'keydown', this.key);
    await settled();

    assert.dom(generateTestSelector('testable-one'))
      .hasNoValue();
  });

  skip('the key can be updated', async function(assert) {
    this.set('key', '/');

    await render(hbs`<input {{capture-focus-on key=this.key }} data-test-selector="testable-one">`);

    await triggerKeyEvent(document, 'keydown', this.key);
    await settled();

    assert.dom(generateTestSelector('testable-one'))
      .isFocused();

    find(generateTestSelector('testable-one')).blur();

    assert.dom(generateTestSelector('testable-one'))
      .isNotFocused();

    this.set('key', 'p');

    await triggerKeyEvent(document, 'keydown', this.key);
    await settled();

    assert.dom(generateTestSelector('testable-one'))
      .isFocused();
  });

  skip('does not capture focus when focus is inside another input', async function(assert) {
    this.set('key', '/');

    await render(hbs`
      <input data-test-selector="other-input">
      <input {{capture-focus-on key=this.key }} data-test-selector="testable-one">
    `);

    find(generateTestSelector('other-input')).focus();

    assert.dom(generateTestSelector('testable-one'))
      .isNotFocused();

    await triggerKeyEvent(document, 'keydown', this.key);
    await settled();

    assert.dom(generateTestSelector('other-input'))
      .isFocused();

    assert.dom(generateTestSelector('testable-one'))
      .isNotFocused();
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Integration | Modifiers | focus after render', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('it gives focus to elements it is attached to when they are rendered', async function(assert) {
    await render(hbs`<input {{focus-after-render}} data-test-selector="testable-one" />`);

    assert.dom(generateTestSelector('testable-one')).isFocused();
  });
});

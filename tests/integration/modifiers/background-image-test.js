import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  find,
  render,
  settled,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import { photos } from 'pleroma-pwa/mirage/images';
import faker from 'faker';

module('Integration | Modifiers | background-image', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('gives sets the background image property on an element', async function(assert) {
    this.set('image', faker.random.arrayElement(photos));

    await render(hbs`<div {{background-image url=this.image}} data-test-selector="testable-one"></div>`);

    let el = find(generateTestSelector('testable-one'));
    assert.ok(
      el.style.backgroundImage.includes(`${this.image}")`),
      'contains the right url',
    );
  });

  test('sets nothing when nothing is passed', async function(assert) {
    this.set('image', null);

    await render(hbs`<div {{background-image url=this.image}} data-test-selector="testable-one"></div>`);

    assert.dom(generateTestSelector('testable-one'))
      .hasStyle({
        backgroundImage: 'none',
      });
  });

  test('it can be disabled', async function(assert) {
    this.set('image', faker.random.arrayElement(photos));

    this.set('enabled', false);

    await render(hbs`<div {{background-image url=this.image enabled=this.enabled}} data-test-selector="testable-one"></div>`);

    assert.dom(generateTestSelector('testable-one'))
      .hasStyle({
        backgroundImage: 'none',
      });

    this.set('enabled', true);
    await settled();

    let el = find(generateTestSelector('testable-one'));
    assert.ok(
      el.style.backgroundImage.includes(`${this.image}")`),
      'contains the right url',
    );
  });
});

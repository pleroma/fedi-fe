import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import sinon from 'sinon';

module('Integration | Component | on-route-changed', function(hooks) {
  setupRenderingTest(hooks);

  test('invokes a handler when the route will change', async function(assert) {
    let routerService = this.owner.lookup('service:router');

    this.set('handler', sinon.fake());

    await render(hbs`<OnRouteChanged @handler={{this.handler}}/>`);

    assert.ok(this.handler.notCalled);

    routerService.trigger('routeWillChange');

    assert.ok(this.handler.calledOnce);

    routerService.trigger('routeWillChange');

    assert.ok(this.handler.callCount, 2);
  });
});

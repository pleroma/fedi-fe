import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Component | in-viewport-notifier', function(hooks) {
  setupRenderingTest(hooks);

  test('accepts splattributes', async function(assert) {
    await render(hbs`<InViewportNotifier data-test-selector="splatter"/>`);

    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('yields into itself', async function(assert) {
    await render(hbs`<InViewportNotifier><div data-test-selector="splatter"></div></InViewportNotifier>`);

    assert.dom(generateTestSelector('splatter')).exists();
  });
});

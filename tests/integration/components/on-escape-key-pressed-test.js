import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, triggerKeyEvent, clearRender } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import sinon from 'sinon';

module('Integration | Component | on-escape-key-pressed', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  test('calls a passed handler when the escape key is pressed', async function(assert) {
    this.set('handler', sinon.fake());

    await render(hbs`<OnEscapeKeyPressed @handler={{this.handler}}/>`);

    assert.equal(this.element.childElementCount, 0, 'renders no element');

    assert.ok(this.handler.notCalled);

    await triggerKeyEvent(document.body, 'keydown', 27);

    assert.ok(this.handler.calledOnce);

    await triggerKeyEvent(document.body, 'keydown', 27);

    assert.ok(this.handler.callCount, 2);

    await clearRender();

    await triggerKeyEvent(document.body, 'keydown', 27);

    assert.ok(this.handler.callCount, 2);
  });

  test('does not pass any arguments to the handler', async function(assert) {
    this.set('handler', sinon.fake());

    await render(hbs`<OnEscapeKeyPressed @handler={{this.handler}}/>`);

    await triggerKeyEvent(document.body, 'keydown', 27);

    assert.ok(this.handler.calledWith());
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  find,
  render,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';

module('Integration | Component | feed-item/actions', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('renders the proper bits', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Actions
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('status-actions-bar')).exists();
    assert.equal(find(generateTestSelector('status-actions-bar')).nodeName, 'UL');

    assert.dom(generateTestSelector('replies-count')).hasText('', 'Does not have a replies count when there are no replies');
    assert.dom(generateTestSelector('replies-count')).hasAria('label', 'no replies', 'has the correct aria label');

    assert.dom(generateTestSelector('reply-action-item')).hasAttribute('title', t('reply'));

    assert.dom(generateTestSelector('reposts-count')).hasText('', 'Does not have a reposts count when there are no reposts');
    assert.dom(generateTestSelector('reposts-count')).hasAria('label', 'no reposts', 'has the correct aria label');

    assert.dom(generateTestSelector('favorites-count')).hasText('', 'Does not have a favorites count when there are no favorites');
    assert.dom(generateTestSelector('favorites-count')).hasAria('label', 'no favorites', 'has the correct aria-label');

    assert.dom(generateTestSelector('favorite-action-item')).doesNotHaveClass('status-actions__item--active', 'the status is not marked as favorited if the signed in user has not favorited it');
    assert.dom(generateTestSelector('favorite-action-item-button')).hasAttribute('title', t('favorite'));

    assert.dom(generateTestSelector('repost-action-item')).doesNotHaveClass('status-actions__item--active', 'the status is not marked as reposted if the signed in user has not reposted it.');
    assert.dom(generateTestSelector('repost-action-item-button')).hasAttribute('title', t('repost'));

    assert.dom(generateTestSelector('status-share-menu-trigger')).hasAttribute('title', t('share'), 'the trigger has the correct title attribute');
    assert.dom(generateTestSelector('status-share-menu-trigger')).hasAria('label', t('openMenu'));
    assert.equal(find(generateTestSelector('status-share-menu-trigger')).nodeName, 'BUTTON');

    await click(generateTestSelector('status-share-menu-trigger'));
    await settled();

    assert.dom(generateTestSelector('status-share-menu')).exists();
    assert.equal(find(generateTestSelector('status-share-menu')).nodeName, 'UL');

    assert.dom(generateTestSelector('close-status-share-menu')).exists();
  });

  test('accepts splattributes', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Actions
      data-test-selector="splattributes"
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('splattributes')).exists();
  });

  module('the share menu', function() {
    test('closes on escape key pressed', async function(assert) {
      let user = this.server.create('user');

      let status = this.server.create('status', {
        account: user,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Actions
        @status={{this.status}}
      />`);

      assert.dom(generateTestSelector('status-share-menu')).doesNotExist();

      await click(generateTestSelector('status-share-menu-trigger'));

      assert.dom(generateTestSelector('status-share-menu')).exists();

      await triggerKeyEvent(document.body, 'keydown', 27);
      await settled();

      assert.dom(generateTestSelector('status-share-menu')).doesNotExist();
    });

    test('closes when clicking the close button', async function(assert) {
      let user = this.server.create('user');

      let status = this.server.create('status', {
        account: user,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('status-share-menu')).doesNotExist();

      await click(generateTestSelector('status-share-menu-trigger'));

      assert.dom(generateTestSelector('status-share-menu')).exists();

      await click(generateTestSelector('close-status-share-menu'));
      await settled();

      assert.dom(generateTestSelector('status-share-menu')).doesNotExist();
    });

    test('closes when copying the status link', async function(assert) {
      let user = this.server.create('user');

      let status = this.server.create('status', {
        account: user,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('status-share-menu')).doesNotExist();

      await click(generateTestSelector('status-share-menu-trigger'));

      assert.dom(generateTestSelector('status-share-menu')).exists();

      await click(generateTestSelector('status-share-menu-item', 'copy-status-button'));
      await settled();

      assert.dom(generateTestSelector('status-share-menu')).doesNotExist();
    });
  });

  module('favorites', function() {
    test('when the status has favorites(favourites)', async function(assert) {
      let user = this.server.create('user');
      let status = this.server.create('status', {
        account: user,
        favouritesCount: 7,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Actions
        @status={{this.status}}
      />`);

      assert.dom(generateTestSelector('favorites-count')).exists();

      assert.dom(generateTestSelector('favorites-count')).hasText(status.favouritesCount.toString(), 'Has a favorites count when there are favorites');
      assert.dom(generateTestSelector('favorites-count')).hasAria('label', `${status.favouritesCount} favorites`, 'has the correct aria-label');

      this.set('status.favouritesCount', 999);

      assert.dom(generateTestSelector('favorites-count')).hasText('999', 'Correctly rounds the favorites count');
      assert.dom(generateTestSelector('favorites-count')).hasAria('label', '999 favorites', 'correctly rounds the aria-label');

      this.set('status.favouritesCount', 1000);

      assert.dom(generateTestSelector('favorites-count')).hasText('1k', 'Correctly rounds the favorites count');
      assert.dom(generateTestSelector('favorites-count')).hasAria('label', '1k favorites', 'correctly rounds the aria-label');

      this.set('status.favouritesCount', 1001);

      assert.dom(generateTestSelector('favorites-count')).hasText('1k', 'Correctly rounds the favorites count');
      assert.dom(generateTestSelector('favorites-count')).hasAria('label', '1k favorites', 'correctly rounds the aria-label');

      this.set('status.favouritesCount', 1200);

      assert.dom(generateTestSelector('favorites-count')).hasText('1.2k', 'Correctly rounds the favorites count');
      assert.dom(generateTestSelector('favorites-count')).hasAria('label', '1.2k favorites', 'correctly rounds the aria-label');

      this.set('status.favouritesCount', 3560);

      assert.dom(generateTestSelector('favorites-count')).hasText('3.5k', 'Correctly rounds the favorites count');
      assert.dom(generateTestSelector('favorites-count')).hasAria('label', '3.5k favorites', 'correctly rounds the aria-label');
    });

    test('when the current user has favorited(favourited)', async function(assert) {
      let user = this.server.create('user');
      let status = this.server.create('status', {
        account: user,
        favourited: true,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Actions
        @status={{this.status}}
      />`);

      assert.dom(generateTestSelector('favorite-action-item')).hasClass('status-actions__item--active');
      assert.dom(generateTestSelector('favorite-action-item-button')).hasAttribute('title', t('unfavorite'));
    });
  });

  module('reposts', function() {
    test('when the current user has resposted(reblogged)', async function(assert) {
      let user = this.server.create('user');
      let status = this.server.create('status', {
        account: user,
        reblogged: true,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      assert.equal(statusFromStore.account.username, user.username);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Actions
        @status={{this.status}}
      />`);

      assert.dom(generateTestSelector('repost-action-item')).hasClass('status-actions__item--active');
      assert.dom(generateTestSelector('repost-action-item-button')).hasAttribute('title', t('undoRepost'));
    });

    test('when the status has reposts(reblogs)', async function(assert) {
      let user = this.server.create('user');
      let status = this.server.create('status', {
        account: user,
        reblogsCount: 5,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      assert.equal(statusFromStore.account.username, user.username);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Actions
        @status={{this.status}}
      />`);

      assert.dom(generateTestSelector('reposts-count')).exists();

      assert.dom(generateTestSelector('reposts-count')).hasText(status.reblogsCount.toString(), 'Has a reposts count when there are reposts');
      assert.dom(generateTestSelector('reposts-count')).hasAria('label', `${status.reblogsCount} reposts`, 'has the correct aria label');

      this.set('status.repostsCount', 999);

      assert.dom(generateTestSelector('reposts-count')).hasText('999', 'Has a reposts count when there are reposts');
      assert.dom(generateTestSelector('reposts-count')).hasAria('label', '999 reposts', 'has the correct aria label');

      this.set('status.repostsCount', 1000);

      assert.dom(generateTestSelector('reposts-count')).hasText('1k', 'Has a reposts count when there are reposts');
      assert.dom(generateTestSelector('reposts-count')).hasAria('label', '1k reposts', 'has the correct aria label');

      this.set('status.repostsCount', 1001);

      assert.dom(generateTestSelector('reposts-count')).hasText('1k', 'Has a reposts count when there are reposts');
      assert.dom(generateTestSelector('reposts-count')).hasAria('label', '1k reposts', 'has the correct aria label');

      this.set('status.repostsCount', 1999);

      assert.dom(generateTestSelector('reposts-count')).hasText('1.9k', 'Has a reposts count when there are reposts');
      assert.dom(generateTestSelector('reposts-count')).hasAria('label', '1.9k reposts', 'has the correct aria label');

      this.set('status.repostsCount', 2200);

      assert.dom(generateTestSelector('reposts-count')).hasText('2.2k', 'Has a reposts count when there are reposts');
      assert.dom(generateTestSelector('reposts-count')).hasAria('label', '2.2k reposts', 'has the correct aria label');
    });
  });
});

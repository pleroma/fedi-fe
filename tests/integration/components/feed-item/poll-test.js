import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  findAll,
  render,
  settled,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';
import { freezeDateAt, unfreezeDate } from 'ember-mockdate-shim';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import { set } from '@ember/object';
import sinon from 'sinon';
import { defer } from 'rsvp';

module('Integration | Component | feed-item/poll', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
    freezeDateAt(new Date('September 9, 2019 17:31:20'));

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  hooks.afterEach(function() {
    unfreezeDate();
  });

  test('voteCount pluralization', async function(assert) {
    let status = this.server.create('status', 'withExpiredPoll');
    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

    assert.dom(generateTestSelector('feed-item-poll')).exists();

    assert.dom(generateTestSelector('poll', 'total-votes')).hasText(t('voteCount', { n: statusFromStore.poll.votesCount }));

    set(statusFromStore.poll, 'votesCount', 0);
    await settled();

    assert.dom(generateTestSelector('poll', 'total-votes')).hasText('0 votes');

    set(statusFromStore.poll, 'votesCount', 1);
    await settled();

    assert.dom(generateTestSelector('poll', 'total-votes')).hasText('1 vote');

    set(statusFromStore.poll, 'votesCount', 2);
    await settled();

    assert.dom(generateTestSelector('poll', 'total-votes')).hasText('2 votes');
  });

  test('passes the poll and the selected options index to the vote arg', async function(assert) {
    let status = this.server.create('status', 'withRunningPoll');
    let store = this.owner.lookup('service:store');
    let spy = sinon.spy();

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);
    this.set('vote', spy);

    await render(hbs`<FeedItem::Poll @feedItem={{this.status}} @vote={{this.vote}}/>`);

    await click(generateTestSelector('poll', 'option'));

    assert.ok(spy.calledWith(statusFromStore.poll, 0));
  });

  module('while the poll is running', async function() {
    test('shows a count down timer', async function(assert) {
      let now = new Date();
      let status = this.server.create('status', 'withRunningPoll');
      let store = this.owner.lookup('service:store');

      status.poll.update({
        expiresAt: now.setMinutes(now.getMinutes() + 10),
      });

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('poll', 'time-remaining'))
        .hasText(this.intl.t('timeLeft', { time: '10m' }));
    });

    test('does not show the end-time or end-date', async function(assert) {
      let status = this.server.create('status', 'withRunningPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('poll', 'end-time')).doesNotExist();

      assert.dom(generateTestSelector('poll', 'end-date')).doesNotExist();
    });

    test('it is accessible', async function(assert) {
      await a11yAudit(this.element);

      assert.ok(true, 'no a11y errors found!');
    });

    test('poll option buttons are enabled', async function(assert) {
      let status = this.server.create('status', 'withRunningPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('poll', 'option')).isEnabled();
    });

    test('does not add a class to the winning option', async function(assert) {
      let pollOption1 = this.server.create('poll-option', {
        votesCount: 6,
      });
      let pollOption2 = this.server.create('poll-option', {
        votesCount: 4,
      });
      let poll = this.server.create('poll', {
        options: [pollOption1, pollOption2],
        expired: false,
      });
      let status = this.server.create('status', {
        poll,
      });
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}} />`);

      assert.dom(generateTestSelector('poll', 'option'))
        .doesNotHaveClass('poll__option--winner');

      assert.dom(findAll(generateTestSelector('poll', 'option'))[1])
        .doesNotHaveClass('poll__option--winner');
    });

    test('long polls while in viewport', async function(assert) {
      let status = this.server.create('status', 'withRunningPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('poll', 'long-poll-notifier'))
        .exists();
    });
  });

  module('when the poll is expired', async function() {
    test('shows an end-date and end-time', async function(assert) {
      let status = this.server.create('status', 'withExpiredPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('feed-item-poll')).exists();

      assert.dom(generateTestSelector('poll', 'end-time')).hasText(this.intl.formatDate(statusFromStore.poll.expiresAt, {
        hour: 'numeric',
        minute: '2-digit',
        hour12: true,
      }));

      assert.dom(generateTestSelector('poll', 'end-date')).hasText(this.intl.formatDate(statusFromStore.poll.expiresAt, {
        month: 'long',
        day: 'numeric',
        year: 'numeric',
      }));
    });

    test('does not show a count-down timer', async function(assert) {
      let status = this.server.create('status', 'withExpiredPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('time-remaining')).doesNotExist();
    });

    test('it is accessible', async function(assert) {
      await a11yAudit(this.element);

      assert.ok(true, 'no a11y errors found!');
    });

    test('disables option buttons', async function(assert) {
      let status = this.server.create('status', 'withExpiredPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('poll', 'option')).isDisabled();
    });

    test('gives a special class to the winning options', async function(assert) {
      let pollOption1 = this.server.create('poll-option', {
        votesCount: 4,
      });
      let pollOption2 = this.server.create('poll-option', {
        votesCount: 6,
      });
      let poll = this.server.create('poll', {
        options: [pollOption1, pollOption2],
        expired: true,
      });
      let status = this.server.create('status', {
        poll,
      });
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}} />`);

      assert.dom(generateTestSelector('poll', 'option'))
        .doesNotHaveClass('poll__option--winner');

      assert.dom(findAll(generateTestSelector('poll', 'option'))[1])
        .hasClass('poll__option--winner');
    });

    test('if the winners are a tie, gives them a special class', async function(assert) {
      let pollOption1 = this.server.create('poll-option', {
        votesCount: 4,
      });
      let pollOption2 = this.server.create('poll-option', {
        votesCount: 4,
      });
      let pollOption3 = this.server.create('poll-option', {
        votesCount: 3,
      });
      let poll = this.server.create('poll', {
        options: [pollOption1, pollOption2, pollOption3],
        expired: true,
      });
      let status = this.server.create('status', {
        poll,
      });
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}} />`);

      assert.dom(generateTestSelector('poll', 'option'))
        .hasClass('poll__option--winner');

      assert.dom(findAll(generateTestSelector('poll', 'option'))[1])
        .hasClass('poll__option--winner');

      assert.dom(findAll(generateTestSelector('poll', 'option'))[2])
        .doesNotHaveClass('poll__option--winner');
    });

    test('if no one voted, does not give any option the winning class', async function(assert) {
      let pollOption1 = this.server.create('poll-option');
      let pollOption2 = this.server.create('poll-option');
      let pollOption3 = this.server.create('poll-option');
      let poll = this.server.create('poll', {
        options: [pollOption1, pollOption2, pollOption3],
        expired: true,
      });
      let status = this.server.create('status', {
        poll,
      });
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}} />`);

      assert.dom(generateTestSelector('poll', 'option'))
        .doesNotHaveClass('poll__option--winner');
      assert.dom(findAll(generateTestSelector('poll', 'option'))[1])
        .doesNotHaveClass('poll__option--winner');
      assert.dom(findAll(generateTestSelector('poll', 'option'))[2])
        .doesNotHaveClass('poll__option--winner');
    });
  });

  module('each poll option', async function() {
    test('properly templates the background gradient', async function(assert) {
      let status = this.server.create('status', 'withExpiredPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('feed-item-poll')).exists();

      assert.dom(generateTestSelector('poll', 'option')).exists({ count: statusFromStore.poll.options.length });

      assert.dom(generateTestSelector('poll', 'percentage-bar'))
        .hasAttribute('aria-hidden', 'true')
        .hasStyle({
          width: '4px',
        });



      assert.dom(findAll(generateTestSelector('poll', 'percentage-bar'))[1])
        .hasAttribute('aria-hidden', 'true')
        .hasStyle({
          width: '4px',
        });

      set(statusFromStore.poll, 'votesCount', 10);
      set(statusFromStore.poll.options.firstObject, 'votesCount', 4);
      await settled();

      assert.equal(document.querySelector(generateTestSelector('poll', 'percentage-bar')).style.width, '40%');

      assert.dom(findAll(generateTestSelector('poll', 'percentage-bar'))[1])
        .hasAttribute('aria-hidden', 'true')
        .hasStyle({
          width: '4px',
        });
    });

    test('templates the option title', async function(assert) {
      let status = this.server.create('status', 'withExpiredPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('feed-item-poll')).exists();

      assert.dom(generateTestSelector('poll', 'option')).exists({ count: statusFromStore.poll.options.length });

      assert.dom(generateTestSelector('poll', 'option-title'))
        .hasText(statusFromStore.poll.options.firstObject.title);
    });

    test('if the poll has votes, show each option\'s percentage', async function(assert) {
      let status = this.server.create('status', 'withExpiredPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('feed-item-poll')).exists();

      assert.dom(generateTestSelector('poll', 'option')).exists({ count: statusFromStore.poll.options.length });

      assert.dom(generateTestSelector('poll', 'option-percent')).doesNotExist();

      set(statusFromStore.poll, 'votesCount', 10);
      set(statusFromStore.poll.options.firstObject, 'votesCount', 4);
      await settled();

      assert.dom(generateTestSelector('poll', 'option-percent'))
        .hasText(t('percent', { n: 40 }));
    });

    test('becomes disabled while saving vote', async function(assert) {
      let status = this.server.create('status', 'withRunningPoll');
      let store = this.owner.lookup('service:store');
      let deferred = defer();

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      this.set('promise', () => deferred.promise);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}} @vote={{this.promise}}/>`);

      await click(generateTestSelector('poll', 'option'));

      assert.dom(generateTestSelector('poll', 'option')).isDisabled();

      deferred.resolve();
    });

    test('gets a special class when the poll is expired', async function(assert) {
      let status = this.server.create('status', 'withExpiredPoll');
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}}/>`);

      assert.dom(generateTestSelector('poll', 'option')).hasClass('poll__option--ended');
    });
  });

  module('when session user has voted', function() {
    test('the option buttons are disabled', async function(assert) {
      let options = this.server.createList('poll-option', 2);
      let poll = this.server.create('poll', {
        options,
        expired: false,
        voted: true,
        ownVotes: [1],
      });
      let status = this.server.create('status', {
        poll,
      });
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}} />`);

      assert.dom(generateTestSelector('poll-option-index', '0')).isDisabled();
      assert.dom(generateTestSelector('poll-option-index', '1')).isDisabled();
    });

    test('the option the user voted for has a special class', async function(assert) {
      let options = this.server.createList('poll-option', 2);
      let poll = this.server.create('poll', {
        options,
        expired: false,
        voted: true,
        ownVotes: [1],
      });
      let status = this.server.create('status', {
        poll,
      });
      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem::Poll @feedItem={{this.status}} />`);

      assert.dom(generateTestSelector('poll-option-index', '0'))
        .doesNotHaveClass('poll__option--voted');

      assert.dom(generateTestSelector('poll-option-index', '1'))
        .hasClass('poll__option--voted');
    });
  });

  test('accepts splattributes', async function(assert) {
    let status = this.server.create('status', 'withExpiredPoll');
    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Poll @feedItem={{this.status}} data-test-selector="splatter"/>`);

    assert.dom(generateTestSelector('splatter')).exists();
  });
});

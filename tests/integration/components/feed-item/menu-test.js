import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Integration | Component | feed-item::menu', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();

    enableFeature('directMessages');

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('when not authenticated', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Menu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-item', 'copy-status')).hasText(t('copyStatusLink'));

    assert.dom(generateTestSelector('feed-item-menu-item', 'share-status')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).doesNotExist('does not show when not authenticated');
    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).doesNotExist('does not show when not authenticated');
    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-user')).doesNotExist('does not show when not authenticated');
    assert.dom(generateTestSelector('feed-item-menu-item', 'block-user')).doesNotExist('does not show when not authenticated');
    assert.dom(generateTestSelector('feed-item-menu-item', 'delete-status')).doesNotExist('does not show when not authenticated');
    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-conversation')).doesNotExist('does not show when not authenticated');
    assert.dom(generateTestSelector('feed-item-menu-item', 'unmute-conversation')).doesNotExist('does not show when not authenticated');
  });

  test('when looking at your own, shows specific options', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Menu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-user')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'block-user')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-conversation')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'unmute-conversation')).doesNotExist();

    assert.dom(generateTestSelector('feed-item-menu-item', 'copy-status')).hasText(t('copyStatusLink'));

    assert.dom(generateTestSelector('feed-item-menu-item', 'share-status')).hasText(t('shareStatus'));

    assert.dom(generateTestSelector('feed-item-menu-item', 'delete-status')).hasText(t('deleteStatus'));
  });

  test('when looking at someone elses', async function(assert) {
    let statusUser = this.server.create('user');
    this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: statusUser,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Menu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-item', 'delete-status')).doesNotExist('does not show when not authenticated');

    assert.dom(generateTestSelector('feed-item-menu-item', 'copy-status')).hasText(t('copyStatusLink'));

    assert.dom(generateTestSelector('feed-item-menu-item', 'share-status')).hasText(t('shareStatus'));

    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).hasText(t('followUser', { handle: status.account.username }));
    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).doesNotExist();

    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-user')).hasText(t('muteUser', { handle: status.account.username }));

    assert.dom(generateTestSelector('feed-item-menu-item', 'block-user')).hasText(t('blockUser', { handle: status.account.username }));

    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-conversation')).hasText(t('muteConversation'));
    assert.dom(generateTestSelector('feed-item-menu-item', 'unmute-conversation')).doesNotExist();
  });

  test('when looking at someone elses and you are already following them', async function(assert) {
    let statusUser = this.server.create('user', 'followed');

    this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: statusUser,
    });

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Menu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).hasText(t('unfollowUser', { handle: status.account.username }));
  });

  test('when looking at someone elses and you have already muted the conversation', async function(assert) {
    let statusUser = this.server.create('user');
    this.server.create('user', 'withToken', 'withSession');

    let statusExtras = this.server.create('status-extra', {
      threadMuted: true,
    });

    let status = this.server.create('status', {
      account: statusUser,
      pleroma: statusExtras,
    });

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Menu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-conversation')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'unmute-conversation')).hasText(t('unmuteConversation'));
  });

  test('when looking at someone elses and you have already muted them', async function(assert) {
    let statusUser = this.server.create('user', 'muted');

    this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: statusUser,
    });

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Menu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-item', 'unmute-user')).hasText(t('unmuteUser', { handle: status.account.username }));
    assert.dom(generateTestSelector('feed-item-menu-item', 'mute-user')).doesNotExist();
  });

  test('when looking at someone elses and you hace already blocked them', async function(assert) {
    let statusUser = this.server.create('user', 'blocked');

    this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: statusUser,
    });

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Menu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-item', 'unblock-user')).hasText(t('unblockUser', { handle: status.account.username }));
    assert.dom(generateTestSelector('feed-item-menu-item', 'block-user')).doesNotExist();
  });
});

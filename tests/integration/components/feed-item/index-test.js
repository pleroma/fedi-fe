import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  find,
  render,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import { setupIntl, t } from 'ember-intl/test-support';
import { freezeDateAt, unfreezeDate } from 'ember-mockdate-shim';
import Helper from '@ember/component/helper';
import sinon from 'sinon';
import faker from 'faker';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Integration | Component | FeedItem', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.owner.setupRouter();
    freezeDateAt(new Date('September 9, 2019 17:31:20'));

    this.server.create('user', 'withToken', 'withSession');
    await this.owner.lookup('service:session').loadCurrentUser();

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  hooks.afterEach(function() {
    unfreezeDate();
  });

  test('it properly renders a status resource', async function(assert) {
    this.routeActions = { onAttachmentClicked: sinon.spy() };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action](...args); };
    }));

    let feeds = this.owner.lookup('service:feeds');
    let publicFeed = feeds.getFeed('public');

    let user = this.server.create('user');
    let createdAt = new Date();
    createdAt.setMinutes(createdAt.getMinutes() - 24);

    let attachments = this.server.createList('attachment', 6);

    let status = this.server.create('status', {
      account: user,
      createdAt: createdAt.toISOString(),
      mediaAttachments: attachments,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    assert.equal(statusFromStore.account.username, user.username);

    this.set('status', statusFromStore);
    this.set('feed', publicFeed);
    this.set('setSize', 24);
    this.set('posInSet', 12);

    await render(hbs`<FeedItem
      @feed={{this.feed}}
      @posinset={{this.posInSet}}
      @setsize={{this.setSize}}
      @feedItem={{this.status}}
    />`);

    assert.dom(generateTestSelector('avatar')).exists();
    assert.dom(generateTestSelector('user-avatar')).exists();

    assert.dom(generateTestSelector('feed-item-display-name')).hasText(user.displayName, 'properly templates the displayName');
    assert.dom(generateTestSelector('feed-item-display-name')).hasAttribute('href', `/account/${user.id}`);

    assert.dom(generateTestSelector('feed-item-username')).hasText(`@${user.username}`, 'properly templates the username adding the @ symbol');
    assert.dom(generateTestSelector('feed-item-username')).hasAttribute('href', `/account/${user.id}`);

    assert.dom(generateTestSelector('feed-item-content')).hasText(`View status thread. ${status.content}`, 'properly templates the status content');
    assert.dom(generateTestSelector('feed-item-attachments')).hasText(`${t('remainingCount', { count: 2 })}`, 'properly templates the status attachments');

    assert.dom(generateTestSelector('user-avatar')).hasAttribute('src', user.avatar);

    assert.dom(generateTestSelector('feed-item-timestamp')).hasText('24m', 'properly templates the status timestamp');
    assert.dom(generateTestSelector('feed-item-timestamp')).hasAria('label', this.intl.formatRelative(statusFromStore.createdAt), 'properly templates the aria-labal');
    assert.dom(generateTestSelector('feed-item-timestamp')).hasAttribute('href', `/notice/${status.id}`, 'properly templates the timestamp link');
    assert.dom(generateTestSelector('feed-item-timestamp')).hasAttribute('title', this.intl.formatDate(statusFromStore.createdAt, {
      weekday: 'short',
      month: 'short',
      day: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
      hour12: true,
    }), 'properly templates the timestamp title');

    assert.dom(generateTestSelector('status')).hasAttribute('tabindex', '0', 'is focusable in normal document flow');

    assert.dom(generateTestSelector('status')).doesNotHaveClass('feed-item--muted');

    assert.dom(generateTestSelector('status')).hasAria('labelledby', `feed-item__header--${status.id}`, 'the status is "labelledby" the status header via id');
    assert.dom(generateTestSelector('feed-item-header')).hasAttribute('id', `feed-item__header--${status.id}`, 'the status header has a unique id');

    assert.dom(generateTestSelector('status')).hasAria('describedby', `feed-item__body--${this.status.id}`, 'the status is "describedby" the status body via id');
    assert.dom(generateTestSelector('feed-item-content')).hasAttribute('id', `feed-item__body--${status.id}`, 'the status body has a unique id');

    assert.dom(generateTestSelector('feed-item-attachments')).exists();
    attachments.slice(0, 4).forEach((attachment) => {
      assert.dom(`${generateTestSelector('feed-item-attachment', attachment.id)} img`).hasAttribute('src', attachment.previewUrl);
      assert.dom(`${generateTestSelector('feed-item-attachment', attachment.id)} img`).hasAttribute('alt', attachment.description);
    });
    assert.dom(generateTestSelector('feed-item-attachments-remaining-count')).hasText(t('remainingCount', { count: attachments.length - 4 }));

    assert.dom(generateTestSelector('feed-item-menu-trigger')).hasAria('label', t('openMenu'));
    assert.equal(find(generateTestSelector('feed-item-menu-trigger')).nodeName, 'BUTTON');

    assert.dom(generateTestSelector('status-actions-bar')).exists();

    assert.dom(generateTestSelector('status')).hasAria('posinset', this.posInSet.toString(), 'properly represents its position within the feed');
    assert.dom(generateTestSelector('status')).hasAria('setsize', this.setSize.toString(), 'properly represents the size of the feed');

    this.set('setSize', 32);
    this.set('posInSet', 4);

    await settled();

    assert.dom(generateTestSelector('status')).hasAria('setsize', this.setSize.toString(), 'the size of the feed attr responds to changes');
    assert.dom(generateTestSelector('status')).hasAria('posinset', this.posInSet.toString(), 'the position of the status within the feed responds to changes');
  });

  test('accepts splattributes', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
      data-test-selector="splatter"
    />`);

    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('when the status is a reply', async function(assert) {
    let user = this.server.create('user');
    let otherStatus = this.server.create('status', {
      account: user,
    });
    let statusExtras = this.server.create('status-extra', {
      inReplyToAccountAcct: user.username,
      conversationId: otherStatus.id,
    });
    let status = this.server.create('status', {
      account: user,
      inReplyToAccountId: user.id,
      inReplyToId: otherStatus.id,
      pleroma: statusExtras,
      repliesCount: 3,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    assert.equal(statusFromStore.account.username, user.username);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-replying-to-text-link')).exists();
    assert.dom(generateTestSelector('feed-item-in-reply-to-link')).exists();

    assert.dom(generateTestSelector('replies-count')).hasText(status.repliesCount.toString());
    assert.dom(generateTestSelector('replies-count')).hasAria('label', `${status.repliesCount} replies`, 'properly pluralizes the count');
    assert.dom(generateTestSelector('feed-item-header')).hasClass('feed-item__header--conversation', 'The status header gets a correct class');
  });

  module('reposts', function() {
    test('when the status is a repost, renders a resposted callout', async function(assert) {
      let user1 = this.server.create('user');
      let user2 = this.server.create('user');
      let status = this.server.create('status', {
        account: user1,
        reblogged: true,
      });

      let repost = this.server.create('status', {
        reblog: status,
        account: user2,
      });

      let store = this.owner.lookup('service:store');

      let repostFromStore = await store.loadRecord('status', repost.id);

      this.set('repost', repostFromStore);

      await this.owner.setupRouter();

      await render(hbs`<FeedItem
        @feedItem={{this.repost}}
      />`);

      assert.dom(`${generateTestSelector('status-reposted-by')} .feed-item__eyebrow-details-wrapper`).hasText(`${user2.displayName} ${t('reposted')}`);
      assert.dom(`${generateTestSelector('status-reposted-by')} a`).hasAttribute('href', `/account/${user2.id}`);
    });

    test('renders the reposted status and not the current status', async function(assert) {
      let user1 = this.server.create('user');
      let user2 = this.server.create('user');
      let status = this.server.create('status', {
        account: user1,
        reblogged: true,
        content: faker.lorem.sentence(),
      });

      let repost = this.server.create('status', {
        reblog: status,
        account: user2,
        content: faker.lorem.sentence(),
      });

      let store = this.owner.lookup('service:store');

      let repostFromStore = await store.loadRecord('status', repost.id);

      this.set('repost', repostFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.repost}}
      />`);

      assert.dom(generateTestSelector('status-id', repost.id)).exists();

      assert.dom(generateTestSelector('feed-item-content')).hasText(`View status thread. ${status.content}`);
    });
  });

  test('the status timestamp formatting', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user');
    let status = this.server.create('status', { account: user });
    let statusFromStore = await store.loadRecord('status', status.id);
    this.set('status', statusFromStore);

    let secondsAgo = new Date();
    secondsAgo.setSeconds(secondsAgo.getSeconds() - 25);

    let minutesAgo = new Date();
    minutesAgo.setMinutes(minutesAgo.getMinutes() - 10);

    let hoursAgo = new Date();
    hoursAgo.setHours(hoursAgo.getHours() - 10);

    let daysAgo = new Date();
    daysAgo.setDate(daysAgo.getDate() - 5);

    let yearsAgo = new Date();
    yearsAgo.setFullYear(yearsAgo.getFullYear() - 5);

    this.set('status.createdAt', secondsAgo);

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-timestamp')).hasText('25s');

    this.set('status.createdAt', minutesAgo);
    await settled();

    assert.dom(generateTestSelector('feed-item-timestamp')).hasText('10m', 'displays relative time in minutes');

    this.set('status.createdAt', hoursAgo);
    await settled();

    assert.dom(generateTestSelector('feed-item-timestamp')).hasText('10h', 'displays relative time in hours');

    this.set('status.createdAt', daysAgo);
    await settled();

    assert.dom(generateTestSelector('feed-item-timestamp')).hasText('Sep 4', 'displays a date when after 24 hrs');

    this.set('status.createdAt', yearsAgo);
    await settled();

    assert.dom(generateTestSelector('feed-item-timestamp')).hasText('Sep 9, 2014', 'displays a date and a year when more than a year ago');
  });

  module('when the status has attachments', function() {
    test('4 or less attachments does not show the remaining count', async function(assert) {
      this.routeActions = { onAttachmentClicked: sinon.spy() };

      this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
        return () => { this.routeActions[action](...args); };
      }));

      let attachments = this.server.createList('attachment', 3);

      let status = this.server.create('status', {
        mediaAttachments: attachments,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-attachments')).exists();
      attachments.forEach((attachment) => {
        assert.dom(`${generateTestSelector('feed-item-attachment', attachment.id)} img`).hasAttribute('src', attachment.previewUrl);
        assert.dom(`${generateTestSelector('feed-item-attachment', attachment.id)} img`).hasAttribute('alt', attachment.description);
      });
      assert.dom(generateTestSelector('feed-item-attachments-remaining-count')).doesNotExist();
    });

    test('4 or more attachments renders the correct remaining count', async function(assert) {
      this.routeActions = { onAttachmentClicked: sinon.spy() };

      this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
        return () => { this.routeActions[action](...args); };
      }));

      let attachments = this.server.createList('attachment', 10);

      let status = this.server.create('status', {
        mediaAttachments: attachments,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-attachments')).exists();
      attachments.slice(0, 4).forEach((attachment) => {
        assert.dom(`${generateTestSelector('feed-item-attachment', attachment.id)} img`).hasAttribute('src', attachment.previewUrl);
        assert.dom(`${generateTestSelector('feed-item-attachment', attachment.id)} img`).hasAttribute('alt', attachment.description);
      });
      assert.dom(generateTestSelector('feed-item-attachments-remaining-count')).hasText(t('remainingCount', { count: attachments.length - 4 }));
    });

    test('no attachments will not render the attachments container', async function(assert) {
      let status = this.server.create('status', {
        mediaAttachments: [],
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-attachments')).doesNotExist();
    });
  });

  module('the menu', function() {
    test('renders the proper bits', async function(assert) {
      let user = this.server.create('user');

      let status = this.server.create('status', {
        account: user,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      await click(generateTestSelector('feed-item-menu-trigger'));
      await settled();

      assert.equal(find(generateTestSelector('feed-item-menu')).nodeName, 'UL');

      assert.dom(generateTestSelector('close-feed-item-menu')).exists();
    });

    test('closes on escape key pressed', async function(assert) {
      let user = this.server.create('user');

      let status = this.server.create('status', {
        account: user,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-menu')).doesNotExist();

      await click(generateTestSelector('feed-item-menu-trigger'));

      assert.dom(generateTestSelector('feed-item-menu')).exists();

      await triggerKeyEvent(document.body, 'keydown', 27);
      await settled();

      assert.dom(generateTestSelector('feed-item-menu')).doesNotExist();
    });

    test('closes when clicking the close button', async function(assert) {
      let user = this.server.create('user');

      let status = this.server.create('status', {
        account: user,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-menu')).doesNotExist();

      await click(generateTestSelector('feed-item-menu-trigger'));

      assert.dom(generateTestSelector('feed-item-menu')).exists();

      await click(generateTestSelector('close-feed-item-menu'));
      await settled();

      assert.dom(generateTestSelector('feed-item-menu')).doesNotExist();
    });

    test('closes when copying the status link', async function(assert) {
      let user = this.server.create('user');

      let status = this.server.create('status', {
        account: user,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-menu')).doesNotExist();

      await click(generateTestSelector('feed-item-menu-trigger'));

      assert.dom(generateTestSelector('feed-item-menu')).exists();

      await click(generateTestSelector('feed-item-menu-item', 'copy-status'));
      await settled();

      assert.dom(generateTestSelector('feed-item-menu')).doesNotExist();
    });
  });

  test('it can optionaly hide the status actions', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);
    this.set('feed', { id: 1 });

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
      @feed={{this.feed}}
      @hideActions={{true}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-trigger')).doesNotExist();

    assert.dom(generateTestSelector('status-actions-bar')).doesNotExist();
  });

  test('it can optionally hide the attachments', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);
    this.set('feed', { id: 1 });

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
      @feed={{this.feed}}
      @hideAttachments={{true}}
    />`);

    assert.dom(generateTestSelector('feed-item-attachments')).doesNotExist();
  });

  test('it can optionally hide both the attachments and actions', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);
    this.set('feed', { id: 1 });

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
      @feed={{this.feed}}
      @hideAttachments={{true}}
      @hideActions={{true}}
    />`);

    assert.dom(generateTestSelector('feed-item-menu-trigger')).doesNotExist();
    assert.dom(generateTestSelector('status-actions-bar')).doesNotExist();
    assert.dom(generateTestSelector('feed-item-attachments')).doesNotExist();
  });

  test('renders the spoiler-text if provided', async function(assert) {
    this.routeActions = { onAttachmentClicked: sinon.spy() };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action](...args); };
    }));

    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
      spoilerText: faker.lorem.sentence(),
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
    />`);

    assert.dom(generateTestSelector('feed-item-content')).hasText(`View status thread. ${status.spoilerText} ${status.content}`);
  });

  module('when status has a poll', function() {
    test('if the feature flag is not enabled, we show nothing', async function(assert) {
      let user = this.server.create('user');
      let poll = this.server.create('poll');
      let status = this.server.create('status', {
        account: user,
        poll,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-poll')).doesNotExist();
    });

    test('renders a poll', async function(assert) {
      await enableFeature('polls');

      let user = this.server.create('user');
      let poll = this.server.create('poll');
      let status = this.server.create('status', {
        account: user,
        poll,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-poll')).exists();
    });

    test('gives a special class to the feed item', async function(assert) {
      await enableFeature('polls');

      let user = this.server.create('user');
      let poll = this.server.create('poll');
      let status = this.server.create('status', {
        account: user,
        poll,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('status'))
        .hasClass('has-poll');
    });
  });

  module('when muted', function() {
    test('gets a special muted class', async function(assert) {
      let user = this.server.create('user');
      let statusExtras = this.server.create('status-extra', {
        threadMuted: true,
      });
      let status = this.server.create('status', {
        account: user,
        pleroma: statusExtras,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      assert.equal(statusFromStore.account.username, user.username);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('status')).hasClass('feed-item--muted');
    });

    test('renders an unmute button ', async function(assert) {
      let user = this.server.create('user');
      let statusExtras = this.server.create('status-extra', {
        threadMuted: true,
      });
      let status = this.server.create('status', {
        account: user,
        pleroma: statusExtras,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      assert.equal(statusFromStore.account.username, user.username);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-unmute')).exists();
      assert.dom(generateTestSelector('feed-item-unmute')).hasAttribute('title', t('unmute'));
    });

    test('does not render actions', async function(assert) {
      let user = this.server.create('user');
      let statusExtras = this.server.create('status-extra', {
        threadMuted: true,
      });
      let status = this.server.create('status', {
        account: user,
        pleroma: statusExtras,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      assert.equal(statusFromStore.account.username, user.username);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('status-actions-bar')).doesNotExist();
    });

    test('does not render attachments', async function(assert) {
      let user = this.server.create('user');
      let statusExtras = this.server.create('status-extra', {
        threadMuted: true,
      });
      let attachments = this.server.createList('attachment', 6);
      let status = this.server.create('status', {
        account: user,
        pleroma: statusExtras,
        mediaAttachments: attachments,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      assert.equal(statusFromStore.account.username, user.username);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-attachments')).doesNotExist();
    });

    test('does not render the created-at date', async function(assert) {
      let user = this.server.create('user');
      let statusExtras = this.server.create('status-extra', {
        threadMuted: true,
      });
      let status = this.server.create('status', {
        account: user,
        pleroma: statusExtras,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      assert.equal(statusFromStore.account.username, user.username);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-timestamp')).doesNotExist();
    });

    test('accepts a showMutedAvatars param that will, even when the feed item is muted, show an avatar', async function(assert) {
      let store = this.owner.lookup('service:store');
      let user = this.server.create('user');
      let statusExtra = this.server.create('status-extra', {
        threadMuted: true,
      });
      let status = this.server.create('status', {
        account: user,
        pleroma: statusExtra,
      });

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);
      this.set('feed', { id: 1 });

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
        @feed={{this.feed}}
        @showMutedAvatars={{true}}
      />`);

      assert.dom(`${generateTestSelector('feed-item-avatar-link')} ${generateTestSelector('muted-avatar')}`)
        .exists();
    });

    test('does not render polls', async function(assert) {
      await enableFeature('polls');

      let user = this.server.create('user');
      let statusExtras = this.server.create('status-extra', {
        threadMuted: true,
      });
      let poll = this.server.create('poll');
      let status = this.server.create('status', {
        account: user,
        pleroma: statusExtras,
        poll,
      });

      let store = this.owner.lookup('service:store');

      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('status', statusFromStore);

      await render(hbs`<FeedItem
        @feedItem={{this.status}}
      />`);

      assert.dom(generateTestSelector('feed-item-poll')).doesNotExist();
    });
  });

  skip('it is accessible', async function(assert) {
    let user = this.server.create('user');
    this.status = this.server.create('status', {
      account: user,
    });

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
    />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  test('passes the correct status to the status-actions-bar (the original status)', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user');
    let status = this.server.create('status', {
      account: user,
    });

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);
    this.set('feed', { id: 1 });

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
      @feed={{this.feed}}
    />`);


    assert.dom(`${generateTestSelector('status-actions-bar')}${generateTestSelector('status-id', statusFromStore.id)}`).exists();
  });

  test('passes the correct status to the feed-item-menu bar (the original status', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user');
    let status = this.server.create('status', {
      account: user,
    });

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);
    this.set('feed', { id: 1 });

    await render(hbs`<FeedItem
      @feedItem={{this.status}}
      @feed={{this.feed}}
    />`);

    await click(generateTestSelector('feed-item-menu-trigger'));

    await settled();

    assert.dom(generateTestSelector('feed-item-menu')).exists();
    assert.dom(`${generateTestSelector('feed-item-menu-item', 'copy-status')}${generateTestSelector('status-id', statusFromStore.id)}`).exists();
  });

  module('when is a notification', function() {
    test('it has the proper parts', async function(assert) {
      let createdAt = new Date();
      let notificationUser = this.server.create('user');

      createdAt.setMinutes(createdAt.getMinutes() - 24);

      this.server.create('notification', {
        // exclude follow, since follows will not show a timestamp
        type: faker.random.arrayElement(['reblog', 'favorite', 'mention']),
        account: notificationUser,
        createdAt,
      });

      let feedsService = this.owner.lookup('service:feeds');
      let notificationsFeed = await feedsService.subscribe('notifications');
      let notification = notificationsFeed.content.firstObject;

      this.set('notification', notification);

      await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);

      assert.dom(generateTestSelector('notification')).exists();
      assert.dom(generateTestSelector('notification-id', notification.id)).exists();
      assert.dom(generateTestSelector('notification')).exists();
      assert.dom(generateTestSelector('notification')).doesNotHaveClass('read', 'Does not have the read class by default');

      assert.dom(generateTestSelector('feed-item-avatar-link')).hasAttribute('href', `/account/${notificationUser.id}`);
      assert.dom(`${generateTestSelector('feed-item-avatar-link')} ${generateTestSelector('user-avatar')}`).hasAttribute('src', notificationUser.avatar);

      assert.dom(generateTestSelector('feed-item-header')).doesNotHaveClass('status__header--conversation');
      assert.dom(generateTestSelector('feed-item-header')).hasAttribute('id', `feed-item__header--${notification.id}`);

      assert.dom(generateTestSelector('feed-item-username')).hasAttribute('href', `/account/${notificationUser.id}`);
      assert.dom(generateTestSelector('feed-item-username')).hasText(t('atHandle', { handle: notificationUser.username }));

      assert.dom(generateTestSelector('feed-item-timestamp')).containsText('24m');
      assert.dom(generateTestSelector('status-status-link')).doesNotExist();

      assert.dom(generateTestSelector('feed-item-content')).hasAttribute('id', `feed-item__body--${notification.id}`);

      assert.dom(generateTestSelector('notification-follow-back-button')).doesNotExist();
    });

    test('gets a special `read` class when the notification has been seen.', async function(assert) {
      let notificationExtra = this.server.create('notification-extra', {
        isSeen: true,
      });
      this.server.create('notification', {
        pleroma: notificationExtra,
      });

      this.server.create('user', 'withToken', 'withSession');

      let feedsService = this.owner.lookup('service:feeds');
      let notificationsFeed = await feedsService.subscribe('notifications');
      let notification = notificationsFeed.content.firstObject;

      this.set('notification', notification);

      await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);
      await settled();

      assert.dom(generateTestSelector('notification')).hasClass('is-read');
    });

    test('when the notification is a repost', async function(assert) {
      let createdAt = new Date();

      let notificationUser = this.server.create('user');

      createdAt.setMinutes(createdAt.getMinutes() - 24);

      let notificationStatus = this.server.create('status', {
        createdAt: createdAt.toISOString(),
      });

      this.server.create('notification', {
        status: notificationStatus,
        account: notificationUser,
        type: 'reblog',
      });

      this.server.create('user', 'withToken', 'withSession');

      let feedsService = this.owner.lookup('service:feeds');
      let notificationsFeed = await feedsService.subscribe('notifications');
      let notification = notificationsFeed.content.firstObject;

      this.set('notification', notification);

      await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);
      await settled();

      assert.dom(generateTestSelector('notification-message')).containsText(t('hasReposted'));

      assert.dom(generateTestSelector('feed-item-timestamp')).hasAttribute('href', `/notice/${notificationStatus.id}`);
      assert.dom(generateTestSelector('feed-item-timestamp')).hasText('24m', 'properly templates the status timestamp');
      assert.dom(generateTestSelector('feed-item-timestamp')).hasAria('label', this.intl.formatRelative(notificationStatus.createdAt), 'properly templates the aria-labal');
      assert.dom(generateTestSelector('feed-item-timestamp')).hasAttribute('title', this.intl.formatDate(notificationStatus.createdAt, {
        weekday: 'short',
        month: 'short',
        day: 'numeric',
        year: 'numeric',
        hour: 'numeric',
        minute: '2-digit',
        hour12: true,
      }), 'properly templates the timestamp title');
    });

    test('when the notification is a favourite', async function(assert) {
      let createdAt = new Date();

      let notificationUser = this.server.create('user');

      createdAt.setMinutes(createdAt.getMinutes() - 24);

      this.server.create('notification', {
        account: notificationUser,
        type: 'favourite',
        createdAt: createdAt.toISOString(),
      });

      this.server.create('user', 'withToken', 'withSession');

      let feedsService = this.owner.lookup('service:feeds');
      let notificationsFeed = await feedsService.subscribe('notifications');
      let notification = notificationsFeed.content.firstObject;

      this.set('notification', notification);

      await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);
      await settled();

      assert.dom(generateTestSelector('notification-message')).containsText(t('hasFavorited'));
      assert.dom(generateTestSelector('notification-message')).containsText('24m');
    });

    module('when the notification is a follow', function() {
      test('and the authed user is not following the notification.account', async function(assert) {
        let createdAt = new Date();

        let notificationUser = this.server.create('user');

        createdAt.setMinutes(createdAt.getMinutes() - 24);

        this.server.create('notification', {
          account: notificationUser,
          type: 'follow',
          createdAt: createdAt.toISOString(),
        });

        this.server.create('user', 'withToken', 'withSession');

        let feedsService = this.owner.lookup('service:feeds');
        let notifications = await feedsService.subscribe('notifications');
        let notification = notifications.content.firstObject;

        this.set('notification', notification);

        await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);
        await settled();

        assert.dom(generateTestSelector('notification-message')).containsText(t('hasFollowed'));
        assert.dom(generateTestSelector('notification-message')).containsText('24m');

        assert.dom(generateTestSelector('feed-item-follow-back-button')).hasText(t('followBack'));

        assert.dom(generateTestSelector('feed-item-unfollow-button')).doesNotExist();
      });

      test('and the authed user is following the notification.account', async function(assert) {
        let createdAt = new Date();

        let notificationUser = this.server.create('user', 'followed');

        createdAt.setMinutes(createdAt.getMinutes() - 24);

        this.server.create('notification', {
          account: notificationUser,
          type: 'follow',
          createdAt: createdAt.toISOString(),
        });

        this.server.create('user', 'withToken', 'withSession');

        let feedsService = this.owner.lookup('service:feeds');
        let notifications = await feedsService.subscribe('notifications');
        let notification = notifications.content.firstObject;

        this.set('notification', notification);

        await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);
        await settled();

        assert.dom(generateTestSelector('notification-message')).containsText(t('hasFollowed'));
        assert.dom(generateTestSelector('notification-message')).containsText('24m');

        assert.dom(generateTestSelector('feed-item-unfollow-button')).hasText(t('unfollow'));

        assert.dom(generateTestSelector('feed-item-follow-back-button')).doesNotExist();
      });
    });

    test('when the notification is a mention', async function(assert) {
      let notificationUser = this.server.create('user');

      this.server.create('notification', {
        account: notificationUser,
        type: 'mention',
      });

      this.server.create('user', 'withToken', 'withSession');

      let feedsService = this.owner.lookup('service:feeds');
      let notificationsFeed = await feedsService.subscribe('notifications');
      let notification = notificationsFeed.content.firstObject;

      this.set('notification', notification);

      await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);
      await settled();

      assert.dom(generateTestSelector('notification-message')).containsText(t('hasMentioned'));
    });

    test('when the notification has an associated status', async function(assert) {
      let notificationUser = this.server.create('user');
      let notificationStatus = this.server.create('status');
      let createdAt = new Date();

      createdAt.setMinutes(createdAt.getMinutes() - 24);

      this.server.create('notification', {
        account: notificationUser,
        status: notificationStatus,
        type: 'follow',
        createdAt: createdAt.toISOString(),
      });

      this.server.create('user', 'withToken', 'withSession');

      let feedsService = this.owner.lookup('service:feeds');
      let notificationsFeed = await feedsService.subscribe('notifications');
      let notification = notificationsFeed.content.firstObject;

      this.set('notification', notification);

      await render(hbs`<FeedItem @feedItem={{this.notification}}/>`);
      await settled();

      assert.dom(generateTestSelector('feed-item-content')).containsText(notificationStatus.content);
    });
  });
});

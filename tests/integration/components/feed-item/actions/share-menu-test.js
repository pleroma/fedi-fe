import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  render,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Integration | Component | feed-item/actions/share-menu', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.owner.setupRouter();

    enableFeature('directMessages');

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('renders the proper bits if not authenticated', async function(assert) {
    let user = await this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Actions::ShareMenu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('status-share-menu-item', 'copy-status-button')).hasText(t('copyStatusLink'));

    assert.dom(generateTestSelector('status-share-menu-item', 'share-status')).doesNotExist();
  });

  test('renders the proper bits if authenticated', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: user,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('status', statusFromStore);

    await render(hbs`<FeedItem::Actions::ShareMenu
      @status={{this.status}}
    />`);

    assert.dom(generateTestSelector('status-share-menu-item', 'share-status')).hasText(t('shareStatus'));
  });
});

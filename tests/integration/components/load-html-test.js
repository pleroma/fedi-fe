import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Component | load-html', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('it loads an html file, stripping out inline styles inline scripts and classes', async function(assert) {
    this.server.get('/instance/panel.html', () => {
      return `
      <div data-test-selector="from-the-server">
        <h3 class="i-should-not-have-classes" data-test-selector="has-classes">Pleroma Sidebar.html</h3>
        <div>
          <p>YEAH PLEROMA</p>
        </div>
        <script data-test-selector="script-element">
          (function() { console.log("bing bong opolis");})();
        </script>
        <script data-test-selector="script-element-other">
          (function() { console.log("bing bong opolis");})();
        </script>
        <style data-test-selector="style-element">
        </style>
        <div>
          <p style="height: 200px;" data-test-selector="has-inline-styles">
            Pleroma is very cool!
            <br>
            <a href="/">I have links</a>
          </p>
          <p style='height: 200px;'>
            <span>I also have inline styles</span>
          </p>
        </div>
        <img data-test-selector="image-1"/>
        <img data-test-selector="image-2">
      </div>
      `;
    }, 200);

    await render(hbs`<LoadHtml @path="instance/panel.html" />`);
    await settled();

    assert.dom(generateTestSelector('from-the-server')).exists();

    assert.dom(generateTestSelector('script-element')).doesNotExist();

    assert.dom(generateTestSelector('script-element-other')).doesNotExist();

    assert.dom(generateTestSelector('style-element')).doesNotExist();

    assert.dom(generateTestSelector('has-inline-styles')).doesNotHaveAttribute('style');

    assert.dom(generateTestSelector('has-classes')).doesNotHaveAttribute('class');

    assert.dom(generateTestSelector('image-1')).doesNotExist();
    assert.dom(generateTestSelector('image-2')).doesNotExist();
  });

  test('when failing it recovers and yields nothing', async function(assert) {
    this.server.get('/instance/panel.html', () => {}, 404);

    await render(hbs`<LoadHtml @path="instance/panel.html" data-test-selector="load-html-component"/>`);
    await settled();

    assert.dom(generateTestSelector('load-html-component')).exists();

    assert.equal(document.querySelector(generateTestSelector('load-html-component')).childElementCount, 0);
    assert.dom(generateTestSelector('load-html-component')).hasText('');
  });
});

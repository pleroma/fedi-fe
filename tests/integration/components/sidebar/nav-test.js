import { module, test } from 'qunit';
import { set } from '@ember/object';
import { setupRenderingTest } from 'ember-qunit';
import { render, find, settled } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import { authenticateSession } from 'ember-simple-auth/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Integration | Component | sidebar/nav', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    this.owner.setupRouter();
    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');
    await this.owner.lookup('service:instances').refreshTask.perform();
  });

  test('its has all the proper pieces', async function(assert) {
    let instance = this.server.schema.instances.first();
    // let feeds = this.owner.lookup('service:feeds');
    let unreadCountsService = this.owner.lookup('service:unreadCounts');
    let user = this.server.create('user');
    let tokenRecord = this.server.create('token', {
      user,
    });

    await render(hbs`<Sidebar::Nav/>`);

    assert.equal(find(generateTestSelector('sidebar-nav')).nodeName, 'NAV');

    assert.dom(generateTestSelector('feed-link-home')).doesNotExist();
    assert.dom(generateTestSelector('feed-link-dms-classic')).doesNotExist();
    assert.dom(generateTestSelector('feed-link-dms-chat')).doesNotExist();
    assert.dom(generateTestSelector('feed-link-notifications')).doesNotExist();

    assert.dom(generateTestSelector('feed-link-public')).hasAttribute('href', '/feeds/public');
    assert.dom(generateTestSelector('feed-link-public')).hasText(instance.title, 'Gets its title from the current application instance');

    assert.dom(generateTestSelector('feed-link-all')).hasAttribute('href', '/feeds/all');
    assert.dom(generateTestSelector('feed-link-all')).hasText(t('wholeKnownNetwork'));

    assert.dom(generateTestSelector('sidebar-nav-search-link')).hasAttribute('href', '/search');
    assert.dom(generateTestSelector('sidebar-nav-search-link')).hasText(t('search'));

    await authenticateSession({
      'token_type': 'Bearer',
      'access_token': tokenRecord.token,
    });

    await settled();

    assert.dom(generateTestSelector('feed-link-home')).hasAttribute('href', '/feeds/home');
    assert.dom(generateTestSelector('feed-link-home')).hasText(t('myFeed'));

    // await this.pauseTest();

    assert.dom(generateTestSelector('feed-link-dms-classic')).hasAttribute('href', '/feeds/direct');
    assert.dom(generateTestSelector('feed-link-dms-classic')).hasText(t('directMessages'));
    assert.dom(generateTestSelector('feed-link-dms-classic')).doesNotHaveClass('menu-list__item-link--badge');
    assert.dom(`${generateTestSelector('feed-link-dms-classic')} .menu-list__item-title`).doesNotHaveAttribute('data-count');
    assert.dom(`${generateTestSelector('feed-link-dms-classic')} .menu-list__item-title`).doesNotHaveAttribute('aria-label');
    assert.dom(`${generateTestSelector('feed-link-dms-classic')} .menu-list__item-title .menu-list__item-title-count`).doesNotExist();

    assert.dom(generateTestSelector('feed-link-dms-chat')).hasAttribute('href', '/messages');
    assert.dom(generateTestSelector('feed-link-dms-chat')).hasText(t('chats'));
    assert.dom(generateTestSelector('feed-link-dms-chat')).doesNotHaveClass('menu-list__item-link--badge');
    assert.dom(`${generateTestSelector('feed-link-dms-chat')} .menu-list__item-title`).doesNotHaveAttribute('data-count');
    assert.dom(`${generateTestSelector('feed-link-dms-chat')} .menu-list__item-title`).doesNotHaveAttribute('aria-label');
    assert.dom(`${generateTestSelector('feed-link-dms-chat')} .menu-list__item-title .menu-list__item-title-count`).doesNotExist();

    assert.dom(generateTestSelector('feed-link-notifications')).hasAttribute('href', '/notifications');
    assert.dom(generateTestSelector('feed-link-notifications')).hasText(t('notifications'));
    assert.dom(generateTestSelector('feed-link-notifications')).doesNotHaveClass('menu-list__item-link--badge');
    assert.dom(`${generateTestSelector('feed-link-notifications')} .menu-list__item-title`).doesNotHaveAttribute('data-count');
    assert.dom(`${generateTestSelector('feed-link-notifications')} .menu-list__item-title`).doesNotHaveAttribute('aria-label');
    assert.dom(`${generateTestSelector('feed-link-notifications')} .menu-list__item-title .menu-list__item-title-count`).doesNotExist();

    assert.dom(generateTestSelector('feed-link-public')).exists();
    assert.dom(generateTestSelector('feed-link-all')).exists();

    set(unreadCountsService, 'unreadNotificationsCount', 3);
    set(unreadCountsService, 'unreadChatCount', 5);
    set(unreadCountsService, 'unreadConversationCount', 7);

    await settled();

    assert.dom(`${generateTestSelector('feed-link-dms-chat')} .menu-list__item-title`).hasAttribute('data-count', '5');
    assert.dom(`${generateTestSelector('feed-link-dms-chat')} .menu-list__item-title`).hasAria('label', `${t('chats')}, ${t('newItems', { n: 5 })}`);
    assert.dom(`${generateTestSelector('feed-link-dms-chat')} .menu-list__item-title .menu-list__item-title-count`).exists();

    assert.dom(generateTestSelector('feed-link-notifications')).hasClass('menu-list__item-link--badge');
    assert.dom(`${generateTestSelector('feed-link-notifications')} .menu-list__item-title`).hasAttribute('data-count', '3');
    assert.dom(`${generateTestSelector('feed-link-notifications')} .menu-list__item-title`).hasAria('label', `${t('notifications')}, ${t('newItems', { n: 3 })}`);
    assert.dom(`${generateTestSelector('feed-link-notifications')} .menu-list__item-title .menu-list__item-title-count`).exists();
  });
});

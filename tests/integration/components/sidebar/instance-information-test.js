import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, find } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | sidebar/instance-information', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
    this.server.create('instance');
  });

  test('its has all the proper pieces', async function(assert) {
    let instances = this.owner.lookup('service:instances');

    await instances.refreshTask.perform();

    await render(hbs`<Sidebar::InstanceInformation/>`);

    assert.equal(find(generateTestSelector('sidebar-instance-information')).nodeName, 'SECTION');
    assert.dom(generateTestSelector('load-html')).exists();
    assert.dom(generateTestSelector('sidebar-about-page-link')).hasAttribute('href', '/about');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';

module('Integration | Component | <Sidebar::SignOut />', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('has all the right parts', async function(assert) {
    await render(hbs`<Sidebar::SignOut />`);

    assert.dom(generateTestSelector('sidebar-profile-sign-out-button')).hasText(t('signOut'));
    assert.dom(generateTestSelector('sidebar-profile-sign-out-button')).hasAttribute('href', '/sign-out');
  });
});

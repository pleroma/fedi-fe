import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, find } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';

module('Integration | Component | <Sidebar::Profile />', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('it does not throw an exception', async function(assert) {
    await render(hbs`<Sidebar::Profile />`);

    assert.ok(this.element);
  });

  test('its containing element is of type section', async function(assert) {
    await render(hbs`<Sidebar::Profile />`);

    assert.equal(find(generateTestSelector('sidebar-profile')).nodeName, 'SECTION');
  });

  test('when authenticated, renders a proper user profile card', async function(assert) {
    await this.owner.setupRouter();

    let user = this.server.create('user', 'withToken', 'withSession');

    await render(hbs`<Sidebar::Profile />`);

    assert.dom(generateTestSelector('profile-card')).exists();
    assert.dom('.profile-card__text').hasText(`${user.displayName} @${user.username}`, 'Has the correct text');
    assert.dom('.profile-card__avatar .avatar__image').hasAttribute('src', user.avatar, 'Templates the correct avatar');

    assert.dom(generateTestSelector('username-link'))
      .hasAttribute('href', `/account/${user.id}`)
      .hasAttribute('title', t('viewProfile'));
  });

  test('it is accessible', async function(assert) {
    await render(hbs`<Sidebar::Profile />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

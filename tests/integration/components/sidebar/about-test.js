import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, find } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Integration | Component | sidebar/about', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('its has all the proper pieces', async function(assert) {
    await render(hbs`<Sidebar::About/>`);

    assert.equal(find(generateTestSelector('sidebar-about')).nodeName, 'SECTION', 'Has the proper tagname');

    assert.dom(generateTestSelector('sidebar-about-link')).hasAttribute('href', '/about', 'Links to the about page');
    assert.dom(generateTestSelector('sidebar-about-link')).hasText(t('about'));
    assert.dom(generateTestSelector('sidebar-settings-link')).doesNotExist();

    this.server.create('user', 'withToken', 'withSession');
    await this.owner.lookup('service:session').loadCurrentUser();

    assert.dom(generateTestSelector('sidebar-settings-link')).exists();
    assert.dom(generateTestSelector('sidebar-settings-link')).hasAttribute('href', '/settings');
  });
});

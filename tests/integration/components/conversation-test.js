import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { freezeDateAt, unfreezeDate } from 'ember-mockdate-shim';

module('Integration | Component | conversation', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
    freezeDateAt(new Date('September 9, 2019 17:31:20'));
  });

  hooks.afterEach(function() {
    unfreezeDate();
  });

  test('it is accessible', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user', 'withToken', 'withSession');
    let createdAt = new Date();
    createdAt.setMinutes(createdAt.getMinutes() - 24);
    let status = this.server.create('status', {
      account: user,
      createdAt: createdAt.toISOString(),
    });
    let conversation = this.server.create('conversation', {
      accounts: this.server.createList('user', 4),
      lastStatus: status,
    });

    let conversationFromStore = await store.loadRecord('conversation', conversation.id);

    this.set('conversation', conversationFromStore);

    await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  test('properly renders a conversation', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user', 'withToken', 'withSession');
    let createdAt = new Date();
    createdAt.setMinutes(createdAt.getMinutes() - 24);
    let status = this.server.create('status', {
      account: user,
      createdAt: createdAt.toISOString(),
    });
    let accounts = this.server.createList('user', 4);
    let conversation = this.server.create('conversation', {
      accounts,
      lastStatus: status,
    });

    let conversationFromStore = await store.loadRecord('conversation', conversation.id);

    this.set('conversation', conversationFromStore);

    await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

    assert.dom(generateTestSelector('conversation')).exists();

    assert.dom(generateTestSelector('conversation', 'participant-avatar')).exists({ count: 2 });
    assert.dom(generateTestSelector('conversation', 'participant-avatar-remaining-count')).hasText(t('remainingCount', { count: 2 }));

    assert.dom(generateTestSelector('conversation', 'participant-display-name')).exists({ count: 4 });

    let conversationHeader = `${accounts.mapBy('displayName').join(', ')} ${t('thisIsAGroupDm')}`;
    assert.dom(generateTestSelector('conversation', 'participant-names')).hasText(conversationHeader);

    assert.dom(generateTestSelector('conversation', 'last-status-content')).hasText(status.content);
    assert.dom(generateTestSelector('conversation', 'last-status-created-at')).hasText('24m');
  });

  test('accepts splattributes', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user', 'withToken', 'withSession');
    let createdAt = new Date();
    createdAt.setMinutes(createdAt.getMinutes() - 24);
    let status = this.server.create('status', {
      account: user,
      createdAt: createdAt.toISOString(),
    });
    let conversation = this.server.create('conversation', {
      accounts: this.server.createList('user', 4),
      lastStatus: status,
    });

    let conversationFromStore = await store.loadRecord('conversation', conversation.id);

    this.set('conversation', conversationFromStore);

    await render(hbs`<Conversation @conversation={{this.conversation}} data-test-selector="splatter"/>`);

    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('when the conversation is not read, it gets a special class', async function(assert) {
    let store = this.owner.lookup('service:store');
    this.server.create('user', 'withToken', 'withSession');
    let conversation = this.server.create('conversation', {
      accounts: this.server.createList('user', 4),
      unread: true,
    });

    let conversationFromStore = await store.loadRecord('conversation', conversation.id);

    this.set('conversation', conversationFromStore);

    await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

    assert.dom(generateTestSelector('conversation'))
      .hasClass('dm-list__dm-item--unread')
      .doesNotHaveClass('dm-list__dm-item--read');
  });

  test('when the conversation is read, it gets a special class', async function(assert) {
    let store = this.owner.lookup('service:store');
    this.server.create('user', 'withToken', 'withSession');
    let conversation = this.server.create('conversation', {
      accounts: this.server.createList('user', 4),
      unread: false,
    });

    let conversationFromStore = await store.loadRecord('conversation', conversation.id);

    this.set('conversation', conversationFromStore);

    await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

    assert.dom(generateTestSelector('conversation'))
      .hasClass('dm-list__dm-item--read')
      .doesNotHaveClass('dm-list__dm-item--unread');
  });

  module('when there are more than 2 recipients', function() {
    test('show a special group dm callout', async function(assert) {
      let store = this.owner.lookup('service:store');
      this.server.create('user', 'withToken', 'withSession');
      let accounts = this.server.createList('user', 4);
      let conversation = this.server.create('conversation', {
        accounts,
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

      let conversationHeader = `${accounts.mapBy('displayName').join(', ')} ${t('thisIsAGroupDm')}`;
      assert.dom(generateTestSelector('conversation', 'participant-names')).hasText(conversationHeader);
    });

    test('only show 2 avatars + a remaining count overlay', async function(assert) {
      let store = this.owner.lookup('service:store');
      this.server.create('user', 'withToken', 'withSession');
      let conversation = this.server.create('conversation', {
        accounts: this.server.createList('user', 4),
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

      assert.dom(generateTestSelector('conversation', 'participant-avatar')).exists({ count: 2 });
      assert.dom(generateTestSelector('conversation', 'participant-avatar-remaining-count')).hasText(t('remainingCount', { count: 2 }));
    });
  });

  module('when there are exactly 2 recipients', function() {
    test('show a special group dm callout', async function(assert) {
      let store = this.owner.lookup('service:store');
      this.server.create('user', 'withToken', 'withSession');
      let accounts = this.server.createList('user', 2);
      let conversation = this.server.create('conversation', {
        accounts,
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      await render(hbs`<Conversation @conversation={{this.conversation}}/>`);


      let conversationHeader = `${accounts.mapBy('displayName').join(', ')} ${t('thisIsAGroupDm')}`;
      assert.dom(generateTestSelector('conversation', 'participant-names')).hasText(conversationHeader);
    });

    test('only show 2 avatars + do not show remaining count overlay', async function(assert) {
      let store = this.owner.lookup('service:store');
      this.server.create('user', 'withToken', 'withSession');
      let conversation = this.server.create('conversation', {
        accounts: this.server.createList('user', 2),
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

      assert.dom(generateTestSelector('conversation', 'participant-avatar')).exists({ count: 2 });
      assert.dom(generateTestSelector('conversation', 'participant-avatar-remaining-count')).doesNotExist();
    });
  });

  module('when there is only 1 recipient', function() {
    test('show the recipients username and display name', async function(assert) {
      let store = this.owner.lookup('service:store');
      this.server.create('user', 'withToken', 'withSession');
      let accounts = this.server.createList('user', 1);
      let conversation = this.server.create('conversation', {
        accounts,
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

      assert.dom(generateTestSelector('conversation', 'participant-names')).doesNotIncludeText(t('thisIsAGroupDm'));

      assert.dom(generateTestSelector('conversation', 'participant-display-name')).hasText(accounts.firstObject.displayName);
      assert.dom(generateTestSelector('conversation', 'participant-username')).hasText(t('atHandle', { handle: accounts.firstObject.username }));
    });

    test('only show 1 avatar + do not show remaining count overlay', async function(assert) {
      let store = this.owner.lookup('service:store');
      this.server.create('user', 'withToken', 'withSession');
      let accounts = this.server.createList('user', 1);
      let conversation = this.server.create('conversation', {
        accounts,
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      await render(hbs`<Conversation @conversation={{this.conversation}}/>`);

      assert.dom(generateTestSelector('conversation', 'participant-avatar')).exists({ count: 1 });
      assert.dom(generateTestSelector('conversation', 'participant-avatar-remaining-count')).doesNotExist();
    });
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  find,
  render,
  resetOnerror,
  settled,
  setupOnerror,
  triggerEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import faker from 'faker';
import sinon from 'sinon';
import Service from '@ember/service';

module('Integration | Component | forgot-password-form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('it renders the proper bits', async function(assert) {
    await render(hbs`<AuthModal::ForgotPasswordForm />`);

    assert.dom(generateTestSelector('forgot-password-form')).exists();
    assert.equal(find(generateTestSelector('forgot-password-form')).nodeName, 'FORM');

    assert.dom(generateTestSelector('forgot-email-input')).hasAttribute('id', 'forgot-email');
    assert.dom(generateTestSelector('forgot-email-input')).doesNotHaveClass('invalid');
    assert.dom(generateTestSelector('forgot-email-input')).hasAttribute('placeholder', t('usernameOrEmailAddress'));
    assert.dom(generateTestSelector('forgot-email-input')).isEnabled();
    assert.dom(generateTestSelector('forgot-email-input')).hasAttribute('autocomplete', 'email');
    assert.dom(generateTestSelector('forgot-email-input')).isRequired();
    assert.dom(generateTestSelector('forgot-email-input')).hasAttribute('type', 'text');
    assert.dom(generateTestSelector('forgot-email-input')).isFocused();

    assert.dom(generateTestSelector('forgot-password-form-submit-button')).isDisabled();
    assert.dom(generateTestSelector('forgot-password-form-submit-button')).hasAttribute('type', 'submit');

    assert.dom(generateTestSelector('forgot-password-form-error')).doesNotExist();
  });

  test('locks down the form while task is running', async function(assert) {

    this.set('requestPasswordResetEmail', {
      isRunning: false,
    });

    await render(hbs`<AuthModal::ForgotPasswordForm @requestPasswordResetEmail={{this.requestPasswordResetEmail}}/>`);

    assert.dom(generateTestSelector('forgot-email-input')).isEnabled();
    assert.dom(generateTestSelector('forgot-password-form-submit-button')).isDisabled();
    assert.dom(generateTestSelector('forgot-password-form-submit-button')).hasText(t('submit'));

    this.set('requestPasswordResetEmail.isRunning', true);

    assert.dom(generateTestSelector('forgot-email-input')).isDisabled();
    assert.dom(generateTestSelector('forgot-password-form-submit-button')).isDisabled();
    assert.dom(generateTestSelector('forgot-password-form-submit-button')).hasText(t('requesting'));
  });

  test('gives correct errors and error classes when the form is invalid', async function(assert) {
    await render(hbs`<AuthModal::ForgotPasswordForm />`);

    assert.dom(generateTestSelector('forgot-password-form-error')).doesNotExist();

    await click(generateTestSelector('forgot-password-form-submit-button'));
    await triggerEvent(generateTestSelector('forgot-password-form'), 'submit');
    await settled();

    assert.dom(generateTestSelector('forgot-password-form-error')).exists();
    assert.dom(generateTestSelector('forgot-password-form-error')).hasText(t('youMustEnterUsernameOrEmail'));
    assert.dom(generateTestSelector('forgot-password-form-submit-button')).isDisabled();
    assert.dom(generateTestSelector('forgot-email-input')).isEnabled();
  });

  test('Never shows errors even if the server fails', async function(assert) {
    let emailAddress = faker.internet.email();

    let authModalServiceStub = Service.extend({
      showForgotPasswordSuccess: sinon.fake(),
    });

    this.owner.register('service:auth-modal', authModalServiceStub);

    setupOnerror(function(err) {
      assert.ok(err);
    });

    await render(hbs`<AuthModal::ForgotPasswordForm />`);

    await fillIn(generateTestSelector('forgot-email-input'), emailAddress);

    this.server.get('/auth/password', () => ({
      message: ['We could not find that email address'],
    }), 404);

    await click(generateTestSelector('forgot-password-form-submit-button'));
    await settled();

    assert.ok(this.owner.lookup('service:auth-modal').showForgotPasswordSuccess.calledOnce);

    resetOnerror();
  });

  test('Attempts to show the success screen when server says success', async function(assert) {
    let user = this.server.create('user');

    let authModalServiceStub = Service.extend({
      showForgotPasswordSuccess: sinon.fake(),
    });

    this.owner.register('service:auth-modal', authModalServiceStub);

    await render(hbs`<AuthModal::ForgotPasswordForm />`);

    await fillIn(generateTestSelector('forgot-email-input'), user.email);

    await click(generateTestSelector('forgot-password-form-submit-button'));
    await settled();

    assert.ok(this.owner.lookup('service:auth-modal').showForgotPasswordSuccess.calledOnce);
  });
});

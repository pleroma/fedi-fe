import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  render,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { authenticateSession } from 'ember-simple-auth/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';
import { set } from '@ember/object';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Integration | Component | Feed::Switcher', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    await this.owner.lookup('service:instances').refreshTask.perform();

    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action](...args); };
    }));
  });

  test('it displays a menu of availale feeds when the chevron is clicked', async function(assert) {
    await this.owner.setupRouter();

    let feedService = this.owner.lookup('service:feeds');
    let feed = feedService.getFeed('public');

    this.set('feed', feed);

    await render(hbs`<Feed::Switcher @feed={{this.feed}}/>`);

    await click(generateTestSelector('feed-menu-trigger'));
    await settled();

    assert.dom(generateTestSelector('feed-switcher-dropdown')).exists();
    assert.dom(generateTestSelector('close-feed-menu')).exists();
  });

  test('the menu items are properly templated', async function(assert) {
    let instance = this.server.schema.instances.first();
    let user = this.server.create('user');
    let tokenRecord = this.server.create('token', {
      user,
    });

    await this.owner.setupRouter();

    let unreadCountsService = this.owner.lookup('service:unreadCounts');

    let feedService = this.owner.lookup('service:feeds');
    let feed = feedService.getFeed('public');

    this.set('feed', feed);

    await render(hbs`<Feed::Switcher @feed={{this.feed}}/>`);

    await click(generateTestSelector('feed-menu-trigger'));
    await settled();

    assert.dom(generateTestSelector('feed-switcher-home-link')).doesNotExist();
    assert.dom(generateTestSelector('feed-switcher-direct-classic-link')).doesNotExist();
    assert.dom(generateTestSelector('feed-switcher-direct-chat-link')).doesNotExist();
    assert.dom(generateTestSelector('feed-switcher-notifications-link')).doesNotExist();

    assert.dom(generateTestSelector('feed-switcher-public-link')).hasAttribute('href', '/feeds/public');
    assert.dom(`${generateTestSelector('feed-switcher-public-link')} .menu-list__item-icon`).hasAttribute('role', 'presentation');
    assert.dom(`${generateTestSelector('feed-switcher-public-link')} .menu-list__item-title`).hasText(instance.title);
    assert.dom(`${generateTestSelector('feed-switcher-public-link')} .menu-list__item-description`).hasText(t('localFeedDescription', { instance: instance.title }));

    assert.dom(generateTestSelector('feed-switcher-all-link')).hasAttribute('href', '/feeds/all');
    assert.dom(`${generateTestSelector('feed-switcher-all-link')} .menu-list__item-icon`).hasAttribute('role', 'presentation');
    assert.dom(`${generateTestSelector('feed-switcher-all-link')} .menu-list__item-title`).hasText(t('wholeKnownNetwork'));
    assert.dom(`${generateTestSelector('feed-switcher-all-link')} .menu-list__item-description`).hasText(t('globalFeedDescription', { instance: instance.title }));

    await authenticateSession({
      'token_type': 'Bearer',
      'access_token': tokenRecord.token,
    });

    await settled();

    assert.dom(generateTestSelector('feed-switcher-home-link')).hasAttribute('href', '/feeds/home');
    assert.dom(`${generateTestSelector('feed-switcher-home-link')} .menu-list__item-icon`).hasAttribute('role', 'presentation');
    assert.dom(`${generateTestSelector('feed-switcher-home-link')} .menu-list__item-title`).hasText(t('myFeed'));
    assert.dom(`${generateTestSelector('feed-switcher-home-link')} .menu-list__item-description`).hasText(t('userFeedDescription'));

    assert.dom(generateTestSelector('feed-switcher-direct-classic-link')).hasAttribute('href', '/feeds/direct');
    assert.dom(`${generateTestSelector('feed-switcher-direct-classic-link')} .menu-list__item-icon`).hasAttribute('role', 'presentation');
    assert.dom(`${generateTestSelector('feed-switcher-direct-classic-link')} .menu-list__item-title`).hasText(t('directMessages'));
    assert.dom(`${generateTestSelector('feed-switcher-direct-classic-link')} .menu-list__item-description`).hasText(t('directMessagesClassicDescription'));

    assert.dom(generateTestSelector('feed-switcher-direct-chat-link')).hasAttribute('href', '/messages');
    assert.dom(`${generateTestSelector('feed-switcher-direct-chat-link')} .menu-list__item-icon`).hasAttribute('role', 'presentation');
    console.log('directMessagesChats', t('directMessagesChats'));
    assert.dom(`${generateTestSelector('feed-switcher-direct-chat-link')} .menu-list__item-title`).hasText(t('chats'));
    console.log('directMessagesChatsDescription', t('directMessagesChatsDescription'));
    assert.dom(`${generateTestSelector('feed-switcher-direct-chat-link')} .menu-list__item-description`).hasText(t('directMessagesChatsDescription'));

    set(unreadCountsService, 'unreadChatCount', 5);
    await settled();

    assert.dom(generateTestSelector('feed-switcher-direct-chat-link')).hasClass('menu-list__item-link--badge');
    assert.dom(`${generateTestSelector('feed-switcher-direct-chat-link')} .menu-list__item-title-count`).hasText(`5 ${t('new')}`);

    set(unreadCountsService, 'unreadConversationCount', 5);
    await settled();

    assert.dom(generateTestSelector('feed-switcher-notifications-link')).hasAttribute('href', '/notifications');
    assert.dom(`${generateTestSelector('feed-switcher-notifications-link')} .menu-list__item-icon`).hasAttribute('role', 'presentation');
    assert.dom(`${generateTestSelector('feed-switcher-notifications-link')} .menu-list__item-title`).hasText(t('notifications'));
    assert.dom(`${generateTestSelector('feed-switcher-notifications-link')} .menu-list__item-description`).hasText(t('notificationFeedDescription'));

    set(unreadCountsService, 'unreadNotificationsCount', 5);
    await settled();

    assert.dom(generateTestSelector('feed-switcher-notifications-link')).hasClass('menu-list__item-link--badge');
    assert.dom(`${generateTestSelector('feed-switcher-notifications-link')} .menu-list__item-title-count`).hasText(`5 ${t('new')}`);
  });

  test('pressing escape will close the feed switcher menu', async function(assert) {
    let feedService = this.owner.lookup('service:feeds');
    let feed = feedService.getFeed('public');

    this.set('feed', feed);

    await render(hbs`<Feed::Switcher @feed={{this.feed}}/>`);

    await click(generateTestSelector('feed-menu-trigger'));
    await settled();

    assert.dom(generateTestSelector('feed-switcher-dropdown')).exists();

    await triggerKeyEvent(document.body, 'keydown', 27);

    assert.dom(generateTestSelector('feed-switcher-dropdown')).doesNotExist();
  });

  test('clicking the close button will close the feed switcher menu', async function(assert) {
    let feedService = this.owner.lookup('service:feeds');
    let feed = feedService.getFeed('public');

    this.set('feed', feed);

    await render(hbs`<Feed::Switcher @feed={{this.feed}}/>`);

    await click(generateTestSelector('feed-menu-trigger'));
    await settled();

    assert.dom(generateTestSelector('feed-switcher-dropdown')).exists();

    await click(generateTestSelector('close-feed-menu'));

    assert.dom(generateTestSelector('feed-switcher-dropdown')).doesNotExist();
  });

  test('if the feed is the notifications feed, and there are unread notifications, it will add a data-attr', async function(assert) {
    let feedService = this.owner.lookup('service:feeds');
    let feed = feedService.getFeed('notifications');
    let unreadCountsService = this.owner.lookup('service:unreadCounts');

    set(unreadCountsService, 'unreadNotificationsCount', 20);

    this.set('feed', feed);

    await render(hbs`<Feed::Switcher @feed={{this.feed}}/>`);

    assert.dom(generateTestSelector('feed-heading')).hasAttribute('data-count', '20');
  });

  test('does not add the data-count attr when not on the notifications feed', async function(assert) {
    let feedService = this.owner.lookup('service:feeds');
    let feed = feedService.getFeed('public');

    this.set('feed', feed);

    await render(hbs`<Feed::Switcher @feed={{this.feed}}/>`);

    assert.dom(generateTestSelector('feed-heading')).doesNotHaveAttribute('data-count');
  });

  test('does not add the data-count attr when not on the notifications feeds unread count is zero', async function(assert) {
    let feedService = this.owner.lookup('service:feeds');
    let feed = feedService.getFeed('notifications');

    this.set('feed', feed);

    await render(hbs`<Feed::Switcher @feed={{this.feed}}/>`);

    assert.dom(generateTestSelector('feed-heading')).doesNotHaveAttribute('data-count');
  });
});

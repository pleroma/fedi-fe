import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import Helper from '@ember/component/helper';

module('Integration | Component | feed/status-form-trigger', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it has all the correct bits', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    await render(hbs`<Feed::StatusFormTrigger/>`);

    assert.dom(generateTestSelector('status-form-trigger')).exists('can be programatically selected');
    assert.dom(generateTestSelector('status-form-trigger')).hasClass('status-form-trigger');

    assert.dom(generateTestSelector('avatar')).exists('renders an avatar');
    assert.dom(generateTestSelector('user-avatar')).hasAttribute('src', user.avatar, 'renders the sessions user into the avatar component');

    assert.dom(generateTestSelector('create-status-form-trigger-text')).exists();
    assert.dom(generateTestSelector('create-status-form-trigger-text')).hasText(t('postAStatus'));
    assert.dom(generateTestSelector('create-status-form-trigger-image')).exists();
    assert.dom(generateTestSelector('create-status-form-trigger-poll')).exists();
  });
});

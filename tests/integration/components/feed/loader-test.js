import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  render,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import { setupIntl, t } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';
import { A } from '@ember/array';

module('Integration | Component | Feed::Loader', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    await this.owner.lookup('service:instances').refreshTask.perform();

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action](...args); };
    }));
  });

  test('if no statuses are passed, we do not render the infinity loader', async function(assert) {
    this.set('feed', {
      id: 'public',
      content: A(),
    });

    await render(hbs`<Feed::Loader @feed={{this.feed}}/>`);

    assert.dom(generateTestSelector('feed-infinity-loader')).doesNotExist();
  });

  skip('when there are feed items, it displayes the infinity loader', async function(assert) {
    this.set('feed', {
      id: 'public',
      content: this.server.createList('status', 5),
      canLoadMore: true,
      subscribedTo: true,
    });

    await render(hbs`<Feed::Loader @feed={{this.feed}} />`);

    assert.dom(generateTestSelector('feed-infinity-loader')).exists();
  });

  test('does not show the feed infinity load when not subscribedTo', async function(assert) {
    this.set('feed', {
      id: 'public',
      content: this.server.createList('status', 5),
      canLoadMore: true,
      subscribedTo: false,
    });

    await render(hbs`<Feed::Loader @feed={{this.feed}} />`);

    assert.dom(generateTestSelector('feed-infinity-loader')).doesNotExist();
  });

  test('if the feed is done loading, optionally renders a "begining of feed" callout', async function(assert) {
    this.set('feed', {
      id: 'public',
      content: this.server.createList('status', 5),
      canLoadMore: false,
    });

    this.set('beginningOfFeedCallout', t('beginningOfFeed'));

    await render(hbs`<Feed::Loader @feed={{this.feed}} @beginningOfFeedCallout={{this.beginningOfFeedCallout}}/>`);

    assert.dom(generateTestSelector('feed-fully-loaded')).hasText(t('beginningOfFeed'));
  });

  skip('it is accessible', async function(assert) {
    await render(hbs`<Feed::Loader />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

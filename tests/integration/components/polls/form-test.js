import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  findAll,
  render,
  settled,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import faker from 'faker';
import sinon from 'sinon';
import config from 'pleroma-pwa/config/environment';

module('Integration | Component | Polls/Form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    let instances = this.owner.lookup('service:instances');
    await instances.refreshTask.perform();
  });

  test('has the proper default dom', async function(assert) {
    await render(hbs`<Polls::Form />`);

    assert.dom(generateTestSelector('polls-form'))
      .exists();

    assert.dom(generateTestSelector('polls-form', 'remove-poll-button'))
      .hasAttribute('type', 'button')
      .hasAttribute('title', t('rmvPoll'))
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'add-poll-option')).doesNotExist();

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .doesNotHaveClass('invalid')
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'day-selector-option'))
      .exists({ count: 8 });

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .doesNotHaveClass('invalid')
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'hour-selector-option'))
      .exists({ count: 24 });

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .doesNotHaveClass('invalid')
      .hasValue((config.APP.DEFAULT_POLL_EXPIRATION / 60).toString())
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'minute-selector-option'))
      .exists({ count: 60 });

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option')).doesNotExist();
  });

  test('accepts splattributes', async function(assert) {
    await render(hbs`<Polls::Form data-test-selector="splatter"/>`);

    assert.dom(generateTestSelector('splatter'))
      .exists();
  });

  test('the polls form renders option inputs for each option passed', async function(assert) {
    this.set('poll', {
      options: new Array(2),
    });

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      />`);

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 2 });
  });

  test('the poll option input has the proper templating', async function(assert) {
    let sentence = faker.lorem.sentence();

    this.set('poll', {
      options: [
        {
          title: sentence,
        },
      ],
    });

    this.set('maxOptionChars', 100);

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @maxOptionChars={{this.maxOptionChars}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .hasAttribute('type', 'text')
      .hasAttribute('id', 'poll-option-0')
      .hasValue(sentence);

    assert.dom(generateTestSelector('polls-form', 'option-input-label'))
      .hasAttribute('for', 'poll-option-0')
      .hasText(t('pollOptionLabel', { num: 1 }));

    assert.dom(generateTestSelector('polls-form', 'option-remaning-char-count'))
      .doesNotHaveClass('is-near-limit')
      .hasText((this.maxOptionChars - sentence.length).toString());
  });

  test('when typing in a poll option input, we update the poll option', async function(assert) {
    let sentence = faker.lorem.sentence();

    this.set('poll', {
      options: [
        {
          title: null,
        },
      ],
    });

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .hasNoValue();

    await fillIn(generateTestSelector('polls-form', 'option-input'), sentence);
    await settled();

    assert.equal(this.poll.options.firstObject.title, sentence);
  });

  test('accepts an argument to lock down the form', async function(assert) {
    this.set('poll', {
      options: [
        { title: null },
        { title: null },
        { title: null },
      ],
    });

    this.set('shouldDisable', false);

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @shouldDisable={{this.shouldDisable}}
      @maxOptionsCount=4
    />`);

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'remove-poll-button'))
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'add-poll-option'))
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .isEnabled();

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
      .isEnabled();

    this.set('shouldDisable', true);
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .isDisabled();

    assert.dom(generateTestSelector('polls-form', 'remove-poll-button'))
      .isDisabled();

    assert.dom(generateTestSelector('polls-form', 'add-poll-option'))
      .isDisabled();

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .isDisabled();

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .isDisabled();

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .isDisabled();

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
      .isDisabled();
  });

  module('when startValidating argument is set', function() {
    test('validates against a maxOptionChars argument', async function(assert) {
      let instances = this.owner.lookup('service:instances');

      this.set('poll', {
        options: [
          { title: faker.lorem.word() },
        ],
      });

      this.set('startValidating', false);

      this.set('maxOptionChars', instances.current.pollLimits.maxOptionChars);

      await render(hbs`<Polls::Form
        @poll={{this.poll}}
        @startValidating={{this.startValidating}}
        @maxOptionChars={{this.maxOptionChars}}
      />`);

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .doesNotHaveClass('text-input__input--error');

      assert.dom(generateTestSelector('polls-form', 'option-error-icon'))
        .doesNotExist();

      await fillIn(generateTestSelector('polls-form', 'option-input'), '#'.repeat(instances.current.pollLimits.maxOptionChars + 1));
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .doesNotHaveClass('text-input__input--error');

      assert.dom(generateTestSelector('polls-form', 'option-error-icon'))
        .doesNotExist();

      this.set('startValidating', true);
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('polls-form', 'option-error-icon'))
        .exists();

      this.set('startValidating', true);
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('polls-form', 'option-error-icon'))
        .exists();
    });

    test('adds invalid class to poll empty poll options', async function(assert) {
      this.set('poll', {
        options: [
          { title: null },
        ],
      });

      this.set('startValidating', false);

      await render(hbs`<Polls::Form
        @poll={{this.poll}}
        @startValidating={{this.startValidating}}
      />`);

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .doesNotHaveClass('text-input__input--error');

      assert.dom(generateTestSelector('polls-form', 'option-error-icon'))
        .doesNotExist();

      this.set('startValidating', true);
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('polls-form', 'option-error-icon'))
        .exists();

      this.set('startValidating', true);
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('polls-form', 'option-error-icon'))
        .exists();
    });
  });

  test('add option button has proper dom', async function(assert) {
    this.set('poll', {
      options: [],
    });

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @maxOptionsCount="3"
    />`);

    assert.dom(generateTestSelector('polls-form', 'add-poll-option'))
      .hasAttribute('type', 'button')
      .hasAttribute('title', t('addAnotherChoice'))
      .hasText(t('addAnotherChoice'))
      .isEnabled()
  });

  test('accepts a click handler for adding new poll options', async function(assert) {
    this.set('spy', sinon.spy());
    this.set('poll', {
      options: [],
    });

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @addPollOption={{this.spy}}
    />`);

    assert.ok(this.spy.notCalled);

    await click(generateTestSelector('polls-form', 'add-poll-option'));

    assert.ok(this.spy.calledOnce);
  });

  module('accepts a maxOptionsCount argument', function() {
    test('hides the add option button when the options length meets the passed max count', async function(assert) {
      this.set('poll', {
        options: [
          { title: null },
          { title: null },
          { title: null },
        ],
      });

      this.set('shouldDisable', false);

      await render(hbs`<Polls::Form
        @poll={{this.poll}}
        @maxOptionsCount="5"
      />`);

      assert.dom(generateTestSelector('polls-form', 'add-poll-option'))
        .exists();

      this.set('poll.options', [
        { title: null },
        { title: null },
        { title: null },
        { title: null },
        { title: null },
      ]);

      assert.dom(generateTestSelector('polls-form', 'add-poll-option'))
        .doesNotExist();
    });
  });

  test('correctly parses the configs default poll expiration', async function(assert) {
    this.set('poll', {
      options: [
        { title: null },
        { title: null },
      ],
    });

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .hasValue((config.APP.DEFAULT_POLL_EXPIRATION / 60).toString());
  });

  test('shows a remove poll option button for each option when there are more than 2 options', async function(assert) {
    this.set('poll', {
      options: [
        { title: null },
        { title: null },
        { title: null },
      ],
    });

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option')).exists({ count: 3 });

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
      .hasAttribute('type', 'button')
      .hasAttribute('title', t('removeChoice'))
      .isEnabled();
  });

  test('When passed a minExpiration and that is larger than the default, we use the min expiration', async function(assert) {
    let minute = 60;
    let hour = 60 * minute;
    let day = 24 * hour;

    this.set('poll', {
      options: [
        { title: null },
        { title: null },
      ],
    });

    // 2 days, 6 hours, 5 minutes
    this.set('minExpiration', day * 2 + hour * 6 + minute * 5);

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @minExpiration={{this.minExpiration}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .hasValue('5');

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .hasValue('6');

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .hasValue('2');
  });

  test('accepts a argument that gets called with the updated duration, in seconds', async function(assert) {
    let minute = 60;
    let hour = 60 * minute;
    let day = 24 * hour;

    this.set('poll', {
      options: [
        { title: null },
        { title: null },
        ],
    });

    this.set('spy', sinon.spy());

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @updatePollExpiration={{this.spy}}
    />`);

    assert.ok(this.spy.notCalled);

    await fillIn(generateTestSelector('polls-form', 'minute-selector'), 0);
    await fillIn(generateTestSelector('polls-form', 'day-selector'), 4);
    await fillIn(generateTestSelector('polls-form', 'hour-selector'), 0);
    await settled();

    assert.ok(this.spy.calledWith(4 * day));

    await fillIn(generateTestSelector('polls-form', 'minute-selector'), 20);
    await fillIn(generateTestSelector('polls-form', 'day-selector'), 0);
    await fillIn(generateTestSelector('polls-form', 'hour-selector'), 4);
    await settled();

    assert.ok(this.spy.calledWith(20 * minute + 4 * hour));

    await fillIn(generateTestSelector('polls-form', 'minute-selector'), 20);
    await fillIn(generateTestSelector('polls-form', 'day-selector'), 4);
    await fillIn(generateTestSelector('polls-form', 'hour-selector'), 4);
    await settled();

    assert.ok(this.spy.calledWith(20 * minute + 4 * hour + 4 * day));
  });

  test('accepts a removePollOption argument that will be called with a poll option to remove', async function(assert) {
    this.set('poll', {
      options: [
        { title: 'a' },
        { title: 'b' },
        { title: 'c' },
      ],
    });

    this.set('spy', sinon.spy());

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @removePollOption={{this.spy}}
    />`);

    assert.ok(this.spy.notCalled);

    await click(findAll(generateTestSelector('polls-form', 'remove-poll-option'))[2]);
    await settled();

    assert.ok(this.spy.calledWith({ title: 'c' }));
  });

  test('accepts an argument to mark the duration inputs invalid', async function(assert) {
    this.set('poll', {
      options: [
        { title: null },
        { title: null },
      ],
    });

    this.set('expiresInError', false);

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @expiresInError={{this.expiresInError}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .doesNotHaveClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .doesNotHaveClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .doesNotHaveClass('invalid');

    this.set('expiresInError', true);

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .hasClass('invalid');
  });

  test('shows a remaining char count as the user fills out an option title', async function(assert) {
    this.set('poll', {
      options: [
        { title: null },
        { title: null },
      ],
    });

    this.set('maxOptionChars', 22);

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @maxOptionChars={{this.maxOptionChars}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'option-remaning-char-count'))
      .hasText(this.maxOptionChars.toString());

    await fillIn(generateTestSelector('polls-form', 'option-input'), '#'.repeat(10));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-remaning-char-count'))
      .hasText((this.maxOptionChars - 10).toString());

    await fillIn(generateTestSelector('polls-form', 'option-input'), '#'.repeat(12));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-remaning-char-count'))
      .hasText((this.maxOptionChars - 12).toString());

    await fillIn(generateTestSelector('polls-form', 'option-input'), '#'.repeat(this.maxOptionChars * 2));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-remaning-char-count'))
      .hasText((this.maxOptionChars * -1).toString());
  });

  test('adds a special class to the input when approaching the char length limit', async function(assert) {
    this.set('poll', {
      options: [
        { title: null },
        { title: null },
      ],
    });

    this.set('maxOptionChars', 22);

    await render(hbs`<Polls::Form
      @poll={{this.poll}}
      @maxOptionChars={{this.maxOptionChars}}
    />`);

    assert.dom(generateTestSelector('polls-form', 'option-remaning-char-count'))
      .doesNotHaveClass('is-near-limit');

    await fillIn(generateTestSelector('polls-form', 'option-input'), '#'.repeat(20));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-remaning-char-count'))
      .hasClass('is-near-limit');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  clearRender,
  render,
  settled,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import faker from 'faker';

module('Integration | Component | body-background-image', function(hooks) {
  setupRenderingTest(hooks);

  test('it gives a background image to the body', async function(assert) {
    this.set('image', faker.image.dataUri(400, 400));

    await render(hbs`<BodyBackgroundImage @url={{this.image}}/>`);

    assert.dom(document.body)
      .hasStyle({
        backgroundImage: `url("${this.image}")`,
      });
  });

  test('does nothing if no url is passed', async function(assert) {
    await render(hbs`<BodyBackgroundImage />`);

    assert.dom(document.body)
      .hasStyle({
        backgroundImage: 'none',
      });
  });

  test('does NOT watch the instance url for updates', async function(assert) {
    let firstImage = faker.image.dataUri(400, 400);

    this.set('image', firstImage);

    await render(hbs`<BodyBackgroundImage @url={{this.image}}/>`);

    assert.dom(document.body)
      .hasStyle({
        backgroundImage: `url("${this.image}")`,
      });

    this.set('image', faker.image.dataUri(400, 400));

    await settled();

    assert.dom(document.body)
      .hasStyle({
        backgroundImage: `url("${firstImage}")`,
      }, 'has the same background image');
  });

  test('removes the background image when un-rendered', async function(assert) {
    this.set('image', faker.image.dataUri(400, 400));

    await render(hbs`<BodyBackgroundImage @url={{this.image}}/>`);

    assert.dom(document.body)
      .hasStyle({
        backgroundImage: `url("${this.image}")`,
      });

    await clearRender();

    assert.dom(document.body)
      .hasStyle({
        backgroundImage: 'none',
      });
  });
});

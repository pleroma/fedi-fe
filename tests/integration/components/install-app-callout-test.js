import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';

module('Integration | Component | install-app-callout', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  test('it renders', async function(assert) {
    await render(hbs`<InstallAppCallout />`);

    assert.dom(this.element).hasText('');

    // Template block usage:
    await render(hbs`
      <InstallAppCallout>
        template block text
      </InstallAppCallout>
    `);

    assert.dom(this.element).hasText('template block text');
  });

  test('it is accessible', async function(assert) {
    await render(hbs`<InstallAppCallout />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Component | avatar', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('it renders a properly formatted avatar', async function(assert) {
    let user = this.server.create('user');
    let store = this.owner.lookup('service:store');

    this.userFromStore = await store.loadRecord('user', user.id);

    await render(hbs`<Avatar @user={{this.userFromStore}}/>`);

    assert.dom(this.element.querySelector('.avatar__image'))
      .hasAttribute('src', this.userFromStore.avatar, 'Has the correct src')
      .hasAttribute('alt', t('avatarFor', { name: t('atHandle', { handle: this.userFromStore.username }) }), 'Has the correct alt');

    assert.dom(generateTestSelector('user-avatar'))
      .hasAttribute('width', '48')
      .hasAttribute('height', '48');
  });

  test('accepts splattributes', async function(assert) {
    await render(hbs`<Avatar data-splatter/>`);

    assert.dom(generateTestSelector('avatar')).hasAttribute('data-splatter');
  });

  test('it optionally accepts a size and will use those as the images with and height', async function(assert) {
    let user = this.server.create('user');
    let store = this.owner.lookup('service:store');

    this.userFromStore = await store.loadRecord('user', user.id);

    await render(hbs`<Avatar @user={{this.userFromStore}} @size="34"/>`);

    assert.dom(generateTestSelector('user-avatar'))
      .hasAttribute('width', '34')
      .hasAttribute('height', '34');
  });

  test('it is accessible', async function(assert) {
    await render(hbs`<Avatar />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  test('it optionally accepts a @muted param to display a generic muted avatar', async function(assert) {
    let user = this.server.create('user');
    let store = this.owner.lookup('service:store');

    this.userFromStore = await store.loadRecord('user', user.id);

    await render(hbs`<Avatar @user={{this.userFromStore}} @muted={{true}}/>`);

    assert.dom(generateTestSelector('muted-avatar'))
      .hasAria('hidden', 'true')
      .hasAttribute('width', '48px')
      .hasAttribute('height', '48px');
  });
});

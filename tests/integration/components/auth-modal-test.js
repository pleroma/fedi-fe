import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  find,
  render,
  settled,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import faker from 'faker';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Integration | Component | auth-modal', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    enableFeature('registration');
  });

  test('it has the right parts', async function(assert) {
    await render(hbs`<AuthModal @currentTab="sign-in"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-form')).exists();
    assert.dom(generateTestSelector('close-modal-button')).exists();
    assert.dom(generateTestSelector('auth-modal-back-button')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-form')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-success')).doesNotExist();
    assert.dom(generateTestSelector('registration-form')).doesNotExist();
    assert.dom(generateTestSelector('lock-body-scroll')).exists();
    assert.dom(generateTestSelector('onboarding-avatar-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
  });

  test('it accepts its reason from the session service', async function(assert) {
    let session = this.owner.lookup('service:session');
    let intl = this.owner.lookup('service:intl');

    await render(hbs`<AuthModal @currentTab="sign-in"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('signIn'));

    let newReasonForAuthentication = faker.lorem.sentence();

    intl.addTranslations('en', {
      newReasonForAuthentication,
    });

    session.set('reasonForAuthenticationKey', 'newReasonForAuthentication');

    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).hasText(newReasonForAuthentication, 'the reason text can be customized');
  });

  test('knows how to show the forgot password form', async function(assert) {
    this.server.create('instance', {
      nodeInfo: this.server.create('node-info'),
    });

    let instancesService = this.owner.lookup('service:instances');
    await instancesService.refreshTask.perform();

    await render(hbs`<AuthModal @currentTab="forgot-password"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).doesNotExist();
    assert.dom(generateTestSelector('sign-in-form')).doesNotExist();
    assert.dom(generateTestSelector('auth-modal-back-button')).exists();
    assert.dom(generateTestSelector('close-modal-button')).exists();
    assert.dom(generateTestSelector('forgot-password-form')).exists();
    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-success')).doesNotExist();
    assert.dom(generateTestSelector('registration-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-avatar-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
  });

  test('knows how to show the forgot password not yet setup callout', async function(assert) {
    let nodeInfo = this.server.create('node-info');

    nodeInfo.update({
      metadata: {
        ...nodeInfo.metaData,
        mailerEnabled: false,
      },
    });

    this.server.create('instance', {
      nodeInfo,
    });

    let instancesService = this.owner.lookup('service:instances');
    await instancesService.refreshTask.perform();

    await render(hbs`<AuthModal @currentTab="forgot-password"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).doesNotExist();
    assert.dom(generateTestSelector('sign-in-form')).doesNotExist();
    assert.dom(generateTestSelector('auth-modal-back-button')).exists();
    assert.dom(generateTestSelector('close-modal-button')).exists();
    assert.dom(generateTestSelector('forgot-password-form')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).exists();
    assert.dom(generateTestSelector('forgot-password-success')).doesNotExist();
    assert.dom(generateTestSelector('registration-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-avatar-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
  });

  test('knows how to show the forgot password success form', async function(assert) {
    await render(hbs`<AuthModal @currentTab="forgot-password-success"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).doesNotExist();
    assert.dom(generateTestSelector('sign-in-form')).doesNotExist();
    assert.dom(generateTestSelector('auth-modal-back-button')).doesNotExist();
    assert.dom(generateTestSelector('close-modal-button')).exists();
    assert.dom(generateTestSelector('forgot-password-form')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-success')).exists();
    assert.dom(generateTestSelector('registration-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-avatar-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
  });

  test('knows how to show the registration form', async function(assert) {
    await render(hbs`<AuthModal @currentTab="register"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).doesNotExist();
    assert.dom(generateTestSelector('sign-in-form')).doesNotExist();
    assert.dom(generateTestSelector('auth-modal-back-button')).exists();
    assert.dom(generateTestSelector('close-modal-button')).exists();
    assert.dom(generateTestSelector('forgot-password-form')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-success')).doesNotExist();
    assert.dom(generateTestSelector('registration-form')).exists();
    assert.dom(generateTestSelector('onboarding-avatar-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
  });

  test('knows how to show the onboarding avatar form', async function(assert) {
    await render(hbs`<AuthModal @currentTab="onboarding-avatar-form"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).doesNotExist();
    assert.dom(generateTestSelector('sign-in-form')).doesNotExist();
    assert.dom(generateTestSelector('auth-modal-back-button')).doesNotExist();
    assert.dom(generateTestSelector('close-modal-button')).exists();
    assert.dom(generateTestSelector('forgot-password-form')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-success')).doesNotExist();
    assert.dom(generateTestSelector('registration-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-avatar-form')).exists();
    assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
  });

  test('knows how to show the onboarding bio form', async function(assert) {
    await render(hbs`<AuthModal @currentTab="onboarding-bio-form"/>`);
    await settled();

    assert.dom(generateTestSelector('sign-in-reason')).doesNotExist();
    assert.dom(generateTestSelector('sign-in-form')).doesNotExist();
    assert.dom(generateTestSelector('auth-modal-back-button')).exists();
    assert.dom(generateTestSelector('close-modal-button')).exists();
    assert.dom(generateTestSelector('forgot-password-form')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).doesNotExist();
    assert.dom(generateTestSelector('forgot-password-success')).doesNotExist();
    assert.dom(generateTestSelector('registration-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-avatar-form')).doesNotExist();
    assert.dom(generateTestSelector('onboarding-bio-form')).exists();
  });

  skip('blocks scroll on the body when open', async function(assert) {
    assert.equal(window.getComputedStyle(document.body, null).getPropertyValue('overflow'), 'visible');

    await render(hbs`
      <div style="height: 200vh;"></div>
      <div data-test-selector="scroll-here"></div>
      <AuthModal />
    `);

    await find(generateTestSelector('scroll-here')).scrollIntoView();

    let authModalService = this.owner.lookup('service:auth-modal');
    await authModalService.showSignInModal();
    await settled();

    assert.equal(window.getComputedStyle(document.body, null).getPropertyValue('overflow'), 'hidden');
  });
});

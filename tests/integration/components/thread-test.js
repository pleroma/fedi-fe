import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';

module('Integration | Component | thread', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();

    this.createAndFetchStatus = async() => {
      let status = this.server.create('status');
      let store = this.owner.lookup('service:store');
      let fetched = await store.loadRecord('status', status.id);
      return fetched;
    };

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it accepts a conversation, and renders it', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();
    let firstLevelReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
    let secondLevelReplies = await Promise.all([...Array(4).keys()].map(this.createAndFetchStatus));

    this.set('conversation', {
      status: parentStatus,
      children: firstLevelReplies.map((status) => {
        return {
          status,
          children: secondLevelReplies.map((status) => {
            return { status, children: [] };
          }),
        };
      }),
    });

    await render(hbs`<Thread @conversation={{this.conversation}}/>`);

    assert.dom(generateTestSelector('status-level', 1)).hasClass('conversation__parent');
    assert.dom(generateTestSelector('status-level', 2)).exists({ count: 1 });
    assert.dom(generateTestSelector('status-level', 3)).exists({ count: 4 });
  });

  module('when not in the active tree', function() {
    test('it will occlude replies after the first 6', async function(assert) {
      let parentStatus = await this.createAndFetchStatus();
      let firstLevelReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
      let secondLevelReplies = await Promise.all([...Array(8).keys()].map(this.createAndFetchStatus));

      this.set('conversation', {
        status: parentStatus,
        children: firstLevelReplies.map((status) => {
          return {
            status,
            children: secondLevelReplies.map((status) => {
              return { status, children: [] };
            }),
          };
        }),
      });

      await render(hbs`<Thread @conversation={{this.conversation}}/>`);

      assert.dom(generateTestSelector('status-level', 3)).exists({ count: 6 });

      assert.dom(generateTestSelector('link-to-remaining')).hasText(t('numMoreReplies', { n: 2 }));
    });

    test('after nested 5 levels, will occlude ALL children', async function(assert) {
      let parentStatus = await this.createAndFetchStatus();
      let firstLevelReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
      let secondLevelReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
      let thirdLevelReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
      let fourthLevelReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
      let fifthLevelReplies = await Promise.all([...Array(6).keys()].map(this.createAndFetchStatus));

      this.set('conversation', {
        status: parentStatus,
        children: firstLevelReplies.map((status) => {
          return {
            status,
            children: secondLevelReplies.map((status) => {
              return {
                status,
                children: thirdLevelReplies.map((status) => {
                  return {
                    status,
                    children: fourthLevelReplies.map((status) => {
                      return {
                        status,
                        children: fifthLevelReplies.map((status) => {
                          return {
                            status,
                            children: [],
                          };
                        }),
                      };
                    }),
                  };
                }),
              };
            }),
          };
        }),
      });

      await render(hbs`<Thread @conversation={{this.conversation}}/>`);

      assert.dom(generateTestSelector('status-level', 5)).exists({ count: 1 });

      assert.dom(generateTestSelector('status-level', 6)).doesNotExist();

      assert.dom(generateTestSelector('child-replies-hidden')).hasText(t('numMoreReplies', { n: 6 }));
    });

    test('it will not show the "see more" callout when there are exactly 6 replies', async function(assert) {
      let parentStatus = await this.createAndFetchStatus();
      let firstLevelReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
      let secondLevelReplies = await Promise.all([...Array(6).keys()].map(this.createAndFetchStatus));

      this.set('conversation', {
        status: parentStatus,
        children: firstLevelReplies.map((status) => {
          return {
            status,
            children: secondLevelReplies.map((status) => {
              return { status, children: [] };
            }),
          };
        }),
      });

      await render(hbs`<Thread @conversation={{this.conversation}}/>`);

      assert.dom(generateTestSelector('grandchild-remaining-count')).doesNotExist();
    });
  });

  test('when the status is in the active-tree, we will not occlude anything', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();
    let childReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
    let grandchildReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
    let greatGrandchildReplies = await Promise.all([...Array(8).keys()].map(this.createAndFetchStatus));

    this.set('conversation', {
      status: parentStatus,
      containsCurrent: true,
      children: childReplies.map((status) => {
        return {
          status,
          containsCurrent: true,
          children: grandchildReplies.map((status) => {
            return {
              status,
              containsCurrent: true,
              children: greatGrandchildReplies.map((status) => {
                return {
                  status,
                  children: [],
                };
              }),
            };
          }),
        };
      }),
    });

    await render(hbs`<Thread @conversation={{this.conversation}}/>`);

    assert.dom(generateTestSelector('status-level', 4)).exists({ count: 8 });
  });

  test('gives a special class to the active status item', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();
    let childReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
    let grandchildReplies = await Promise.all([...Array(1).keys()].map(this.createAndFetchStatus));
    let statusInQuestion = await this.createAndFetchStatus();

    this.set('conversation', {
      status: parentStatus,
      containsCurrent: true,
      children: childReplies.map((status) => {
        return {
          status,
          containsCurrent: true,
          children: grandchildReplies.map((status) => {
            return {
              status,
              containsCurrent: true,
              children: [{
                status: statusInQuestion,
                children: [],
              }],
            };
          }),
        };
      }),
    });

    this.set('centerStatusId', statusInQuestion.id);

    await render(hbs`<Thread @conversation={{this.conversation}} @centerStatusId={{this.centerStatusId}}/>`);

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', statusInQuestion.id)}`).hasClass('feed-item--anchor');
  });

  test('does not highlight the active status if it has no children and is the root', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();

    this.set('conversation', {
      status: parentStatus,
      containsCurrent: true,
      children: [],
    });

    this.set('centerStatusId', parentStatus.id);

    await render(hbs`<Thread @conversation={{this.conversation}} @centerStatusId={{this.centerStatusId}}/>`);

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', parentStatus.id)}`).doesNotHaveClass('feed-item--anchor');
  });

  test('does not give parent calss if there are no children', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();

    this.set('conversation', {
      status: parentStatus,
      containsCurrent: true,
      children: [],
    });
    await render(hbs`<Thread @conversation={{this.conversation}}/>`);

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', parentStatus.id)}`).doesNotHaveClass('conversation__parent');
  });

  test('gives a special class to the parent status', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();
    let childReplies = await Promise.all([...Array(10).keys()].map(this.createAndFetchStatus));

    this.set('conversation', {
      status: parentStatus,
      containsCurrent: true,
      children: childReplies.map((status) => {
        return {
          status,
          children: [],
        };
      }),
    });

    await render(hbs`<Thread @conversation={{this.conversation}}/>`);

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', parentStatus.id)}`).hasClass('conversation__parent');
  });

  test('gives a special class to the conversation children if they have siblings who will be occluded', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();
    let childReplies = await Promise.all([...Array(10).keys()].map(this.createAndFetchStatus));

    this.set('conversation', {
      status: parentStatus,
      children: childReplies.map((status) => {
        return {
          status,
          children: [],
        };
      }),
    });

    await render(hbs`<Thread @conversation={{this.conversation}} />`);

    assert.dom(generateTestSelector('status-level', 2)).exists({ count: 6 });

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', childReplies[0].id)}`).hasClass('conversation__last-sibling-before-cut');
  });

  test('gives a special class to child feed items', async function(assert) {
    let parentStatus = await this.createAndFetchStatus();

    this.set('conversation', {
      status: parentStatus,
      children: [],
    });

    await render(hbs`<Thread @conversation={{this.conversation}} />`);

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', parentStatus.id)}`)
      .hasClass('feed-item__threaded');
  });
});

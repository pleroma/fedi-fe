import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  render,
  settled,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import faker from 'faker';
import { defer } from 'rsvp';
import sinon from 'sinon';
import a11yAudit from 'ember-a11y-testing/test-support/audit';

module('Integration | Component | onboarding::bio-form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('it has the proper dom', async function(assert) {
    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{noop}}
      @onClose={{noop}}
    />`);

    assert.dom(generateTestSelector('bio-form', 'errors-desination'))
      .hasNoText();

    assert.dom(generateTestSelector('bio-form', 'header'))
      .hasText(t('onboardingWriteBio'));

    assert.dom(generateTestSelector('onboarding-bio-form')).exists();

    assert.dom(generateTestSelector('bio-form', 'input'))
      .doesNotHaveClass('textarea__input--error')
      .hasAttribute('cols', '15')
      .hasAttribute('id', 'onboard-bio')
      .hasAttribute('name', 'onboard-bio')
      .hasAttribute('placeholder', t('userSettingsUserBio'))
      .isEnabled();

    assert.dom(generateTestSelector('bio-form', 'input-label'))
      .hasAttribute('for', 'onboard-bio')
      .hasText(t('userSettingsUserBio'));

    assert.dom(generateTestSelector('bio-form', 'remaining-char-count'))
      .doesNotHaveClass('is-near-limit')
      .hasNoText();

    assert.dom(generateTestSelector('bio-form', 'submit-button'))
      .hasAttribute('type', 'submit')
      .isEnabled()
      .hasText(t('finish'));

    assert.dom(generateTestSelector('bio-form', 'skip-button'))
      .hasAttribute('type', 'button')
      .isEnabled()
      .hasText(t('skipThis'));
  });

  test('validates bio length', async function(assert) {
    let sentence = faker.lorem.sentence();
    let instances = this.owner.lookup('service:instances');

    this.server.create('instance');

    this.owner.setupRouter();

    await instances.refreshTask.perform();

    let instance = instances.current;

    instance.userBioLength = sentence.length - 5;

    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{noop}}
      @onClose={{noop}}
    />`);

    assert.dom(generateTestSelector('bio-form', 'remaining-char-count'))
      .doesNotHaveClass('is-near-limit')
      .hasNoText();

    assert.dom(generateTestSelector('bio-form', 'input'))
      .doesNotHaveClass('textarea__input--error');

    await fillIn(generateTestSelector('bio-form', 'input'), sentence);
    await settled();

    assert.dom(generateTestSelector('bio-form', 'remaining-char-count'))
      .hasClass('is-near-limit')
      .hasText('-5');

    assert.dom(generateTestSelector('bio-form', 'input-error-icon')).hasAnyText();

    assert.dom(generateTestSelector('bio-form', 'input'))
      .hasClass('textarea__input--error');

    assert.dom(generateTestSelector('bio-form-error', 'sign-in-error'))
      .hasText(t('yourBioIsTooLong', { max: instance.userBioLength }));
  });

  test('validates bio presence', async function(assert) {
    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{noop}}
      @onClose={{noop}}
    />`);

    await click(generateTestSelector('bio-form', 'submit-button'));
    await settled();

    assert.dom(generateTestSelector('bio-form', 'input-error-icon'))
      .exists();

    assert.dom(generateTestSelector('bio-form', 'input'))
      .hasClass('textarea__input--error');

    assert.dom(generateTestSelector('bio-form-error', 'sign-in-error'))
      .hasText(t('youMustEnterABio'));
  });

  test('adds a class to the bio input when approaching the limit', async function(assert) {
    let sentence = faker.lorem.sentence();
    let instances = this.owner.lookup('service:instances');

    this.server.create('instance');

    this.owner.setupRouter();

    await instances.refreshTask.perform();

    let instance = instances.current;

    instance.userBioLength = sentence.length + 50;

    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{noop}}
      @onClose={{noop}}
    />`);

    assert.dom(generateTestSelector('bio-form', 'remaining-char-count'))
      .doesNotHaveClass('is-near-limit')
      .hasNoText();

    await fillIn(generateTestSelector('bio-form', 'input'), sentence);

    assert.dom(generateTestSelector('bio-form', 'remaining-char-count'))
      .hasClass('is-near-limit')
      .hasText('50');
  });

  test('locks down the form while the bio is saving', async function(assert) {
    let deferred = defer();
    let sentence = faker.lorem.sentence();

    this.set('updateUserBio', () => deferred.promise);

    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{this.updateUserBio}}
      @onClose={{noop}}
    />`);

    await fillIn(generateTestSelector('bio-form', 'input'), sentence);
    await settled();

    await click(generateTestSelector('bio-form', 'submit-button'));

    assert.dom(generateTestSelector('bio-form', 'input')).isDisabled();

    assert.dom(generateTestSelector('bio-form', 'submit-button'))
      .hasText(t('saving'))
      .isDisabled();

    assert.dom(generateTestSelector('bio-form', 'skip-button'))
      .isDisabled();

    deferred.resolve();
  });

  test('on save, closes the auth modal', async function(assert) {
    let sentence = faker.lorem.sentence();

    this.set('spy', sinon.spy());

    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{noop}}
      @onClose={{this.spy}}
    />`);

    await fillIn(generateTestSelector('bio-form', 'input'), sentence);
    await click(generateTestSelector('bio-form', 'submit-button'));

    assert.ok(this.spy.calledOnce);
  });

  test('if the request fails, we show a good error', async function(assert) {
    let sentence = faker.lorem.sentence();

    this.set('throwError', () => {
      throw new Error('There was an error');
    });

    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{this.throwError}}
      @onClose={{noop}}
    />`);

    await fillIn(generateTestSelector('bio-form', 'input'), sentence);
    await click(generateTestSelector('bio-form', 'submit-button'));

    assert.dom(generateTestSelector('bio-form-error', 'sign-in-error'))
      .hasText('There was an error');
  });

  test('if the server returns validation errors, we show them', async function(assert) {
    let sentence = faker.lorem.sentence();
    let errorSentance = faker.lorem.sentence();

    this.set('throwError', () => {
      throw new Error(JSON.stringify({
        errors: {
          bio: [errorSentance],
        },
      }));
    });

    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{this.throwError}}
      @onClose={{noop}}
    />`);

    await fillIn(generateTestSelector('bio-form', 'input'), sentence);
    await click(generateTestSelector('bio-form', 'submit-button'));

    assert.dom(generateTestSelector('bio-form-error', 'sign-in-error'))
      .hasText(errorSentance);
  });

  test('it is accessible', async function(assert) {
    await render(hbs`<AuthModal::Onboarding::BioForm
      @updateUserBio={{noop}}
      @onClose={{noop}}
    />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

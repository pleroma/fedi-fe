import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  render,
  resetOnerror,
  settled,
  setupOnerror,
  triggerEvent,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { defer } from 'rsvp';
import { set } from '@ember/object';
import faker from 'faker';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import config from 'pleroma-pwa/config/environment';
import { avatars } from 'pleroma-pwa/mirage/images';

module('Integration | Component | onboarding::avatar-form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('has the proper dom', async function(assert) {
    this.server.create('user', 'withToken', 'withSession', {
      avatar: config.APP.DEFAULT_AVATAR,
    });

    await render(hbs`<AuthModal::Onboarding::AvatarForm />`);

    assert.dom(generateTestSelector('avatar-form', 'errors-destination')).hasNoText();
    assert.dom(generateTestSelector('avatar-form', 'file-selected-error')).doesNotExist();
    assert.dom(generateTestSelector('avatar-form', 'clear-avatar-error')).doesNotExist();

    assert.dom(generateTestSelector('avatar-form', 'onboard-avatar-header')).hasText(t('congratulations'));

    assert.dom(generateTestSelector('avatar-form', 'file-input'))
      .doesNotHaveAttribute('multiple')
      .hasAttribute('accept', 'image/*')
      .hasAttribute('hidden')
      .hasAttribute('id', 'onboard-avatar-input')
      .hasAttribute('type', 'file')
      .hasClass('visuallyhidden')
      .isEnabled();

    assert.dom(generateTestSelector('avatar-form', 'avatar-uploading')).doesNotExist();

    assert.dom(generateTestSelector('avatar-form', 'avatar-image')).doesNotExist();

    assert.dom(generateTestSelector('avatar-form', 'clear-avatar-button')).doesNotExist();

    assert.dom(generateTestSelector('avatar-form', 'next-button'))
      .hasAttribute('type', 'button')
      .isDisabled()
      .hasText(t('next'));

    assert.dom(generateTestSelector('avatar-form', 'skip-button'))
      .hasAttribute('type', 'button')
      .isEnabled()
      .hasText(t('skipThis'));
  });

  test('does not render the avatar when it is the apps default avatar', async function(assert) {
    this.server.create('user', 'withToken', 'withSession', {
      avatar: config.APP.DEFAULT_AVATAR,
    });

    await render(hbs`<AuthModal::Onboarding::AvatarForm />`);

    assert.dom(generateTestSelector('avatar-form', 'avatar-image')).doesNotExist();
  });

  module('uploading an avatar', function() {
    test('when uploading the image fails, we show a good error', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.create('user', 'withToken', 'withSession');

      this.set('updateAvatar', function() {
        throw new Error(t('errorUpdatingAvatarTryAgain'));
      });

      await render(hbs`<AuthModal::Onboarding::AvatarForm @updateAvatar={{this.updateAvatar}}/>`);

      await triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['abcd'], 'test-image.jpg', { type: 'jpg' })] },
      );
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'file-selected-error'))
        .hasText(`Error: ${t('errorUpdatingAvatarTryAgain')}`);

      resetOnerror();
    });

    test('locks down the form while running', async function(assert) {
      let deferred = defer();
      this.server.create('user', { avatar: null }, 'withToken', 'withSession');

      this.set('updateAvatar', () => deferred.promise);

      await render(hbs`<AuthModal::Onboarding::AvatarForm @updateAvatar={{this.updateAvatar}} />`);

      triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['abcd'], 'test-image.jpg', { type: 'jpg' })] },
      );
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'avatar-uploading')).exists();

      assert.dom(generateTestSelector('avatar-form', 'file-input')).isDisabled();

      assert.dom(generateTestSelector('avatar-form', 'clear-avatar-button')).doesNotExist();

      assert.dom(generateTestSelector('avatar-form', 'next-button'))
        .isDisabled()
        .hasText(t('uploadingAvatar'));

      assert.dom(generateTestSelector('avatar-form', 'skip-button'))
        .isDisabled();

      deferred.resolve();
    });

    test('enables the next button on success', async function(assert) {
      this.server.create('user', 'withAvatar', 'withToken', 'withSession');

      this.set('updateAvatar', () => {});

      await render(hbs`<AuthModal::Onboarding::AvatarForm @updateAvatar={{this.updateAvatar}} />`);

      await triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['abcd'], 'test-image.jpg')] },
      );
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'next-button'))
        .isEnabled()
        .hasText(t('next'));
    });

    test('renders the session users avatar once set', async function(assert) {
      let session = this.owner.lookup('service:session');
      let avatar = faker.random.arrayElement(avatars);
      this.server.create('user', { avatar: null }, 'withToken', 'withSession');

      this.set('updateAvatar', () => {
        set(session.currentUser, 'avatar', avatar);
      });

      await render(hbs`<AuthModal::Onboarding::AvatarForm @updateAvatar={{this.updateAvatar}} />`);

      await triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['abcd'], 'test-image.jpg')] },
      );
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'avatar-image'))
        .hasAttribute('src', avatar);
    });
  });

  module('clearing the avatar', function() {
    test('when an image is uploaded, shows a clear button', async function(assert) {
      let session = this.owner.lookup('service:session');
      this.server.create('user', { avatar: null }, 'withToken', 'withSession');

      this.set('updateAvatar', () => {
        set(session.currentUser, 'avatar', faker.random.arrayElement(avatars));
      });

      await render(hbs`<AuthModal::Onboarding::AvatarForm @updateAvatar={{this.updateAvatar}} />`);

      await triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['abcd'], 'test-image.jpg')] },
      );
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'clear-avatar-button'))
        .isEnabled();
    });

    test('when clearing fails, shows a good error', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.create('user', { avatar: faker.random.arrayElement(avatars) }, 'withToken', 'withSession');

      this.set('clearAvatar', () => {
        throw new Error(t('errorClearingAvatarTryAgain'));
      });

      await render(hbs`<AuthModal::Onboarding::AvatarForm @clearAvatar={{this.clearAvatar}}/>`);

      await click(generateTestSelector('avatar-form', 'clear-avatar-button'));
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'clear-avatar-error'))
        .hasText(`Error: ${t('errorClearingAvatarTryAgain')}`);

      resetOnerror();
    });

    test('clearing the avatar will set its default value', async function(assert) {
      let user = this.server.create('user', { avatar: faker.random.arrayElement(avatars) }, 'withToken', 'withSession');

      this.set('clearAvatar', () => {
        user.update({
          avatar: null,
        });
      });

      await render(hbs`<AuthModal::Onboarding::AvatarForm @clearAvatar={{this.clearAvatar}}/>`);

      await click(generateTestSelector('avatar-form', 'clear-avatar-button'));
      await settled();

      assert.notOk(user.avatar);
    });

    test('locks down the buttons on the form while running', async function(assert) {
      let deferred = defer();
      this.server.create('user', { avatar: faker.random.arrayElement(avatars) }, 'withToken', 'withSession');

      this.set('clearAvatar', () => deferred.promise);

      await render(hbs`<AuthModal::Onboarding::AvatarForm @clearAvatar={{this.clearAvatar}}/>`);

      click(generateTestSelector('avatar-form', 'clear-avatar-button'));
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'clear-avatar-button'))
        .isDisabled();

      assert.dom(generateTestSelector('avatar-form', 'next-button'))
        .isDisabled();

      assert.dom(generateTestSelector('avatar-form', 'skip-button'))
        .isDisabled();

      assert.dom(generateTestSelector('avatar-form', 'file-input'))
        .isDisabled();

      deferred.resolve();
    });
  });

  test('it is accessible', async function(assert) {
    await render(hbs`<AuthModal::Onboarding::AvatarForm />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  test('after a file is selected, we clear the file input\'s value to ensure you can upload same avatar twice', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await render(hbs`<AuthModal::Onboarding::AvatarForm @updateAvatar={{noop}}/>`);

    assert.dom(generateTestSelector('avatar-form', 'file-input'))
      .hasNoValue();

    await triggerEvent(
      generateTestSelector('avatar-form', 'file-input'),
      'change',
      { files: [new File(['abcd'], 'test-image.jpg', { type: 'jpg' })] },
    );
    await settled();

    assert.dom(generateTestSelector('avatar-form', 'file-input'))
      .hasNoValue();
  });
});

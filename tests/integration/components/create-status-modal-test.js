import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import Helper from '@ember/component/helper';

module('Integration | Component | create-status-modal', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.routeActions = { };

    this.server.create('instance');
    await this.owner.lookup('service:instances').refreshTask.perform();

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it has all the proper bits', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    await this.owner.lookup('service:session').loadCurrentUser();

    await render(hbs`<CreateStatusModal @compose="text"/>`);
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();
    assert.dom(generateTestSelector('close-modal-button')).exists('It has a close button');
    assert.dom(generateTestSelector('create-status-form')).exists('it renders the status form');
    assert.dom(generateTestSelector('in-reply-to-status-link')).doesNotExist('only shows this callout when appropriate');
  });

  module('when the status is in reply to another status', function() {
    test('it renders the status card without the actions or attachments', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');
      await this.owner.lookup('service:session').loadCurrentUser();

      let status = this.server.create('status', { account: user });

      let store = this.owner.lookup('service:store');
      let statusFromStore = await store.loadRecord('status', status.id);

      this.set('statusId', statusFromStore.id);

      await render(hbs`<CreateStatusModal @compose="reply" @inReplyToStatusId={{this.statusId}}/>`);
      await settled();

      assert.dom(generateTestSelector('in-reply-to-status-link')).exists();

      assert.dom(generateTestSelector('status-id', status.id)).exists();

      assert.dom(generateTestSelector('feed-item-menu-trigger')).doesNotExist();
      assert.dom(generateTestSelector('status-actions-bar')).doesNotExist();
      assert.dom(generateTestSelector('feed-item-attachments')).doesNotExist();
    });
  });
});

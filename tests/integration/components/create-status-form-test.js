import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  find,
  findAll,
  render,
  resetOnerror,
  settled,
  setupOnerror,
  triggerEvent,
} from '@ember/test-helpers';
import { set } from '@ember/object';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import sinon from 'sinon';
import faker from 'faker';
import { enableFeature } from 'ember-feature-flags/test-support';
import Helper from '@ember/component/helper';
import { durationFromSeconds } from 'pleroma-pwa/utils/duration';

module('Integration | Component | create-status-form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    this.owner.setupRouter();
    await this.owner.lookup('service:instances').refreshTask.perform();

    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');
    enableFeature('statusMedia');

    this.server.create('user', 'withToken', 'withSession');
    await this.owner.lookup('service:session').loadCurrentUser();

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it has all the proper bits', async function(assert) {
    let instance = this.server.schema.instances.first();
    await render(hbs`<CreateStatusForm />`);

    assert.dom(generateTestSelector('create-status-form')).exists();

    assert.dom(generateTestSelector('avatar')).exists('renders the user avatar');

    assert.dom(generateTestSelector('visibility-select')).exists();
    assert.dom(generateTestSelector('visibility-option', 'public')).hasText(t('public'));
    assert.dom(generateTestSelector('visibility-option', 'private')).hasText(t('followersOnly'));
    assert.dom(generateTestSelector('visibility-option', 'directMessageClassic')).hasText(t('directMessage'));
    assert.dom(generateTestSelector('visibility-option', 'directMessageChat')).hasText(t('chat'));

    assert.dom(generateTestSelector('create-status-body')).exists();
    assert.dom(generateTestSelector('create-status-body')).hasAttribute('placeholder', t('statusContentPlaceholder', {
      instance: instance.title,
    }));
    assert.dom(generateTestSelector('create-status-body')).isFocused('auto focuses the input when the form is rendered');

    assert.dom(generateTestSelector('create-status-button')).hasText(t('postStatus'));

    assert.dom(generateTestSelector('polls-form')).doesNotExist();
    assert.dom(generateTestSelector('status-form-poll-toggle')).doesNotHaveClass('is-active');

    assert.dom(generateTestSelector('post-visibility-direct-message')).doesNotExist();
  });

  test('accepts arguments to signal if the user wanted to start the post as a poll', async function(assert) {
    await enableFeature('polls');

    await render(hbs`<CreateStatusForm @compose="poll"/>`);

    assert.dom(generateTestSelector('polls-form')).exists();
    assert.dom(generateTestSelector('status-form-poll-toggle')).hasClass('is-active');
  });

  test('accepts arguments to signal if the user wanted to start the post as a directMessage', async function(assert) {
    await render(hbs`<CreateStatusForm @compose="direct"/>`);

    assert.dom(generateTestSelector('visibility-select')).hasValue('directMessageClassic', 'classic mode should be the default');

    await render(hbs`<CreateStatusForm @compose="direct" @directMode="classic" />`);

    assert.dom(generateTestSelector('visibility-select')).hasValue('directMessageClassic', 'classic mode should be accepted');

    await render(hbs`<CreateStatusForm @compose="direct" @directMode="chat" />`);

    assert.dom(generateTestSelector('visibility-select')).hasValue('directMessageChat', 'chat mode should be accepted');
  });

  test('accepts arguments to signal if the user wanted to start the post with a mention', async function(assert) {
    let word = faker.lorem.word();
    this.set('word', word);
    await render(hbs`<CreateStatusForm @compose="mention" @initialMentions={{array this.word}}/>`);

    assert.equal(find(generateTestSelector('create-status-body')).value, `@${word} `);
  });

  test('accepts multiple initial mentions', async function(assert) {
    this.set('word1', faker.lorem.word());
    this.set('word2', faker.lorem.word());
    await render(hbs`<CreateStatusForm @compose="mention" @initialMentions={{array this.word1 this.word2}}/>`);

    assert.equal(find(generateTestSelector('create-status-body')).value, `${t('atHandle', { handle: this.word1 })} ${t('atHandle', { handle: this.word2 })} `);
  });

  test('the inputs get an invalid class when the form is invalid', async function(assert) {
    await render(hbs`<CreateStatusForm/>`);

    await triggerEvent(generateTestSelector('create-status-form'), 'submit');
    await settled();

    assert.dom(generateTestSelector('create-status-body')).hasClass('invalid');
  });

  test('it displays useful errors', async function(assert) {
    await render(hbs`<CreateStatusForm/>`);

    await triggerEvent(generateTestSelector('create-status-form'), 'submit');
    await settled();

    assert.dom(generateTestSelector('create-status-error')).hasText(t('yourPostMustHaveContent'));
  });

  test('it responds to submit events', async function(assert) {
    this.set('createStatusTask', {
      perform: sinon.fake(),
    });

    await render(hbs`<CreateStatusForm @createStatusTask={{this.createStatusTask}}/>`);

    assert.ok(this.createStatusTask.perform.notCalled);

    await triggerEvent(generateTestSelector('create-status-form'), 'submit');

    assert.ok(this.createStatusTask.perform.calledOnce);
  });

  module('when the post task is running', function() {
    test('locks down the form', async function(assert) {
      this.set('createStatusTask', {
        isRunning: false,
      });

      await render(hbs`<CreateStatusForm @createStatusTask={{this.createStatusTask}}/>`);

      assert.dom(generateTestSelector('create-status-body')).isEnabled();
      assert.dom(generateTestSelector('create-status-button')).isDisabled('is disabled when there is no content');
      assert.dom(generateTestSelector('create-status-button')).hasText(t('postStatus'));

      this.set('createStatusTask.isRunning', true);

      await settled();

      assert.dom(generateTestSelector('create-status-body')).isDisabled('disables the status body input');
      assert.dom(generateTestSelector('create-status-button')).isDisabled('disables the status form button');
      assert.dom(generateTestSelector('status-form-poll-toggle')).isDisabled('disables the toggle poll button');
      assert.dom(generateTestSelector('visibility-select')).isDisabled('disables the visibility select');
      assert.dom(generateTestSelector('create-status-button')).hasText(t('postingStatus'));
      assert.dom(generateTestSelector('create-status-form', 'file-input-element')).isDisabled('disables the image upload input');
    });

    test('locks down the poll option inputs', async function(assert) {
      await enableFeature('polls');

      this.set('createStatusTask', {
        isRunning: false,
      });

      await render(hbs`<CreateStatusForm @createStatusTask={{this.createStatusTask}}/>`);

      await click(generateTestSelector('status-form-poll-toggle'));
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input')).isEnabled();

      assert.dom(generateTestSelector('polls-form', 'remove-poll-button'))
        .isEnabled();

      assert.dom(generateTestSelector('polls-form', 'day-selector'))
        .isEnabled();

      assert.dom(generateTestSelector('polls-form', 'hour-selector'))
        .isEnabled();

      assert.dom(generateTestSelector('polls-form', 'minute-selector'))
        .isEnabled();

      this.set('createStatusTask.isRunning', true);
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .isDisabled();

      assert.dom(generateTestSelector('polls-form', 'remove-poll-button'))
        .isDisabled();

      assert.dom(generateTestSelector('polls-form', 'day-selector'))
        .isDisabled();

      assert.dom(generateTestSelector('polls-form', 'hour-selector'))
        .isDisabled();

      assert.dom(generateTestSelector('polls-form', 'minute-selector'))
        .isDisabled();
    });
  });

  test('clicking the form button will attempt to submit the form', async function(assert) {
    let fake = sinon.fake();

    this.set('createStatusTask', {
      perform(event) {
        event.preventDefault();
        event.stopPropagation();
        fake();
      },
    });

    await render(hbs`<CreateStatusForm @createStatusTask={{this.createStatusTask}}/>`);
    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());

    assert.ok(fake.notCalled);

    await click(generateTestSelector('create-status-button'));

    assert.ok(fake.calledOnce);
  });

  test('When saving fails, it shows good errors', async function(assert) {
    let error = 'The server is down';

    this.server.post('/api/v1/statuses', () => ({ error }), 500);

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());

    await click(generateTestSelector('create-status-button'));

    assert.dom(generateTestSelector('create-status-error')).hasText(error);
  });

  test('server errors do not disable the input', async function(assert) {
    let error = 'The server is down';

    this.server.post('/api/v1/statuses', () => ({ error }), 500);

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());

    await click(generateTestSelector('create-status-button'));

    assert.dom(generateTestSelector('create-status-error')).hasText(error);

    assert.dom(generateTestSelector('create-status-button')).isEnabled();
  });

  test('displays a remaing char count', async function(assert) {
    let sentence = faker.lorem.sentence();
    let instance = this.server.create('instance');
    let intl = this.owner.lookup('service:intl');

    let instances = this.owner.lookup('service:instances');
    await instances.refreshTask.perform();

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), sentence);
    await settled();

    assert.dom(generateTestSelector('post-remaining-char-count')).hasText(intl.formatNumber((instance.maxTootChars - sentence.length)));

    await fillIn(generateTestSelector('create-status-body'), sentence + sentence);
    await settled();

    assert.dom(generateTestSelector('post-remaining-char-count')).hasText(intl.formatNumber((instance.maxTootChars - (sentence.length + sentence.length))));
  });

  test('gets the char count from the instance max-toot-chars if it can', async function(assert) {
    let sentence = 'Only the young can say They\'re free to fly away';

    let intl = this.owner.lookup('service:intl');
    let instances = this.owner.lookup('service:instances');
    let instance = instances.current;

    set(instance, 'maxTootChars', 20);

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), sentence);
    await settled();

    assert.dom(generateTestSelector('post-remaining-char-count')).hasText(intl.formatNumber((instance.maxTootChars - sentence.length)));
  });

  test('displays good errors if the stautus length exceeds the limit', async function(assert) {
    let sentence = faker.lorem.sentence();

    let instances = this.owner.lookup('service:instances');
    let instance = instances.current;

    set(instance, 'maxTootChars', sentence.length - 1);

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), sentence);
    await click(generateTestSelector('create-status-button'));

    assert.dom(generateTestSelector('create-status-error')).hasText(t('yourPostIsTooLong', { max: sentence.length - 1 }));
    assert.dom(generateTestSelector('create-status-body')).hasClass('invalid');
  });

  test('adds a class to char count getting close to the max-toot-chars <100 chars remain', async function(assert) {
    let sentence = faker.lorem.sentence();

    let instances = this.owner.lookup('service:instances');
    let instance = instances.current;

    set(instance, 'maxTootChars', sentence.length + 50);

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), sentence);

    assert.dom(generateTestSelector('create-status-info')).hasClass('is-near-limit');
  });

  test('displays errors eagerly without submiting the form', async function(assert) {
    let sentence = faker.lorem.sentence();

    let instances = this.owner.lookup('service:instances');
    let instance = instances.current;

    set(instance, 'maxTootChars', sentence.length - 1);

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), sentence);

    assert.dom(generateTestSelector('create-status-error')).hasText(t('yourPostIsTooLong', { max: sentence.length - 1 }));
    assert.dom(generateTestSelector('create-status-body')).hasClass('invalid');
  });

  test('disables the form when submitting', async function(assert) {
    this.createStatusTask = {
      perform() {},
      isRunning: false,
    };

    await render(hbs`<CreateStatusForm @createStatusTask={{this.createStatusTask}}/>`);

    assert.dom(generateTestSelector('create-status-body')).isEnabled();
    assert.dom(generateTestSelector('create-status-button')).isDisabled('button is disabled when there is no content');
    assert.dom(generateTestSelector('visibility-select')).isEnabled();
    assert.dom(generateTestSelector('create-status-form', 'file-input-element')).isEnabled();
    assert.dom(generateTestSelector('status-form-poll-toggle')).isEnabled();

    this.set('createStatusTask.isRunning', true);

    assert.dom(generateTestSelector('create-status-body')).isDisabled();
    assert.dom(generateTestSelector('create-status-button')).isDisabled();
    assert.dom(generateTestSelector('visibility-select')).isDisabled();
    assert.dom(generateTestSelector('create-status-form', 'file-input-element')).isDisabled();
    assert.dom(generateTestSelector('status-form-poll-toggle')).isDisabled();
  });

  test('disables the submit button when the form is invalid', async function(assert) {
    let sentence = faker.lorem.sentence();
    await render(hbs`<CreateStatusForm/>`);

    assert.dom(generateTestSelector('create-status-button')).isDisabled('Button is disabled when there is no status body');

    await fillIn(generateTestSelector('create-status-body'), sentence);
    await settled();

    assert.dom(generateTestSelector('create-status-button')).isEnabled();

    await fillIn(generateTestSelector('create-status-body'), '');
    await settled();

    assert.dom(generateTestSelector('create-status-button')).isDisabled();
  });

  test('does not disable the submit button when there are server errors', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.post('/api/v1/statuses', () => ({
      message: ['The server is down'],
    }), 500);

    await render(hbs`<CreateStatusForm/>`);

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());

    await click(generateTestSelector('create-status-button'));

    assert.dom(generateTestSelector('create-status-button')).isEnabled();

    resetOnerror();
  });

  test('disables the submit button when there is no content (regarless of changeset isPristine or inValid)', async function(assert) {
    await render(hbs`<CreateStatusForm/>`);

    assert.dom(generateTestSelector('create-status-button')).isDisabled();
  });

  test('the submit button becomes undisabled once a status body has been entered', async function(assert) {
    await render(hbs`<CreateStatusForm/>`);

    assert.dom(generateTestSelector('create-status-button')).isDisabled();

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());

    assert.dom(generateTestSelector('create-status-button')).isEnabled();
  });

  test('We validate that all poll options are input and individually mark them invalid', async function(assert) {
    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    let statusContent = faker.lorem.sentence();
    let pollOptionOne = faker.lorem.sentence();

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    await fillIn(generateTestSelector('create-status-body'), statusContent);
    await fillIn(generateTestSelector('polls-form', 'option-input'), pollOptionOne);

    await settled();

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-error')).hasText(t('pollOptionsEmpty'));
    assert.dom(generateTestSelector('polls-form', 'option-input')).doesNotHaveClass('text-input__input--error');
    assert.dom(findAll(generateTestSelector('polls-form', 'option-input'))[1]).hasClass('text-input__input--error');
  });

  test('We validate each poll options length against and individually mark them invalid', async function(assert) {
    let instance = this.owner.lookup('service:instances');

    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    let statusContent = faker.lorem.sentence();
    let pollOptionOne = faker.lorem.sentence();
    let pollOptionTwo = '#'.repeat(instance.current.pollLimits.maxOptionChars + 1);

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    await fillIn(generateTestSelector('create-status-body'), statusContent);
    await fillIn(generateTestSelector('polls-form', 'option-input'), pollOptionOne);
    await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[1], pollOptionTwo);

    await settled();

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-error')).hasText(t('pollOptionTooLong', { max: instance.current.pollLimits.maxOptionChars }));
    assert.dom(generateTestSelector('polls-form', 'option-input')).doesNotHaveClass('text-input__input--error');
    assert.dom(findAll(generateTestSelector('polls-form', 'option-input'))[1]).hasClass('text-input__input--error');
  });

  test('We validate poll option title uniqueness', async function(assert) {
    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    let statusContent = faker.lorem.sentence();
    let sentence = faker.lorem.sentence();

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    await fillIn(generateTestSelector('create-status-body'), statusContent);
    await fillIn(generateTestSelector('polls-form', 'option-input'), sentence);
    await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[1], sentence.toUpperCase());

    await settled();

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-error')).hasText(t('pollOptionsUnique'));
    assert.dom(generateTestSelector('polls-form', 'option-input')).doesNotHaveClass('text-input__input--error');
  });

  test('You can add a poll', async function(assert) {
    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    assert.dom(generateTestSelector('polls-form')).doesNotExist();

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    assert.dom(generateTestSelector('polls-form')).exists();
  });

  test('You can remove a poll', async function(assert) {
    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    assert.dom(generateTestSelector('polls-form')).doesNotExist();

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    assert.dom(generateTestSelector('polls-form')).exists();

    await click(generateTestSelector('polls-form', 'remove-poll-button'));
    await settled();

    assert.dom(generateTestSelector('polls-form')).doesNotExist();
  });

  test('You can add options to a poll', async function(assert) {
    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 2 });

    await click(generateTestSelector('polls-form', 'add-poll-option'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 3 });
  });

  test('You can remove options from a poll', async function(assert) {
    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 2 });

    await click(generateTestSelector('polls-form', 'add-poll-option'));
    await settled();

    await click(generateTestSelector('polls-form', 'add-poll-option'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 4 });

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
      .exists({ count: 4 });

    await click(generateTestSelector('polls-form', 'remove-poll-option'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 3 });

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
      .exists({ count: 3 });

    await click(generateTestSelector('polls-form', 'remove-poll-option'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 2 });

    assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
      .doesNotExist();
  });

  test('we validate the max number of options', async function(assert) {
    let instances = this.owner.lookup('service:instances');
    await enableFeature('polls');

    set(instances.current.pollLimits, 'maxOptions', 3);

    await render(hbs`<CreateStatusForm/>`);

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 2 });

    // Do not await these clicks, we are trying to add more than the validation allows.
    click(generateTestSelector('polls-form', 'add-poll-option'));
    click(generateTestSelector('polls-form', 'add-poll-option'));
    await settled();

    assert.dom(generateTestSelector('polls-form', 'option-input'))
      .exists({ count: 4 });

    assert.dom(generateTestSelector('polls-form', 'add-poll-option'))
      .doesNotExist();

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
    await fillIn(generateTestSelector('polls-form', 'option-input'), faker.lorem.sentence());
    await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[1], faker.lorem.sentence());
    await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[2], faker.lorem.sentence());
    await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[3], faker.lorem.sentence());

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-error'))
      .hasText(t('pollHasTooManyOptions', { max: 3 }));

    assert.dom(generateTestSelector('polls-form', 'option-input')).hasClass('text-input__input--error');
    assert.dom(findAll(generateTestSelector('polls-form', 'option-input'))[1]).hasClass('text-input__input--error');
    assert.dom(findAll(generateTestSelector('polls-form', 'option-input'))[2]).hasClass('text-input__input--error');
    assert.dom(findAll(generateTestSelector('polls-form', 'option-input'))[3]).hasClass('text-input__input--error');
  });

  test('we validate the min expiration times of the poll', async function(assert) {
    let oneDay = 24 * 60 * 60;
    let instances = this.owner.lookup('service:instances');
    await enableFeature('polls');

    set(instances.current.pollLimits, 'minExpiration', oneDay);

    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
    await fillIn(generateTestSelector('polls-form', 'option-input'), faker.lorem.sentence());
    await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[1], faker.lorem.sentence());
    await fillIn(generateTestSelector('polls-form', 'day-selector'), '0');
    await fillIn(generateTestSelector('polls-form', 'hour-selector'), '2');
    await settled();

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-error'))
      .hasText(this.intl.t('pollDurationTooShort', { min: this.intl.t('hoursMinutesSeconds', durationFromSeconds(oneDay)) }));

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .hasClass('invalid');
  });

  test('we validate the max expiration times of the poll', async function(assert) {
    let oneDay = 24 * 60 * 60;
    let instances = this.owner.lookup('service:instances');

    set(instances.current.pollLimits, 'maxExpiration', oneDay);

    await enableFeature('polls');

    await render(hbs`<CreateStatusForm/>`);

    await click(generateTestSelector('status-form-poll-toggle'));
    await settled();

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
    await fillIn(generateTestSelector('polls-form', 'option-input'), faker.lorem.sentence());
    await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[1], faker.lorem.sentence());
    await fillIn(generateTestSelector('polls-form', 'day-selector'), '2');
    await settled();

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-error'))
      .hasText(this.intl.t('pollDurationTooLong', { max: this.intl.t('hoursMinutesSeconds', durationFromSeconds(oneDay)) }));

    assert.dom(generateTestSelector('polls-form', 'day-selector'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'hour-selector'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('polls-form', 'minute-selector'))
      .hasClass('invalid');
  });

  skip('We validate that you cannot submit a status with a poll AND media items', async function() {
    // Write this when the media-uploads + polls are complete
  });
});

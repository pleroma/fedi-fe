import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  render,
  settled,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { freezeDateAt, unfreezeDate } from 'ember-mockdate-shim';
import faker from 'faker';
import Helper from '@ember/component/helper';

module('Integration | Component | direct-message/feed', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    freezeDateAt(new Date('September 9, 2019 17:31:20'));
    this.owner.setupRouter();

    this.routeActions = { };
    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  hooks.afterEach(function() {
    unfreezeDate();
  });

  module('With new chats api', function() {
    test('it renders the proper dom', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let otherUser = this.server.create('user');

      let store = this.owner.lookup('service:store');

      let [chat] = this.server.createList('chat', 5);
      this.server.createList('chat-message', 5, { chat, account: otherUser });

      let chatFromStore = await store.loadRecord('chat', chat.id);

      this.set('chat', chatFromStore);

      await render(hbs`<DirectMessage::Feed @chat={{this.chat}}/>`);

      assert.dom(generateTestSelector('direct-message-feed'))
        .hasAttribute('data-direct-message-feed');
    });

    test('it renders message fragments for each status in the feed', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let otherUser = this.server.create('user');

      let chat = this.server.create('chat');
      this.server.createList('chat-message', 5, { chat, account: otherUser });

      let store = this.owner.lookup('service:store');
      let chatFromStore = await store.loadRecord('chat', chat.id);

      this.set('chat', chatFromStore);

      await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
      await settled();

      assert.dom(generateTestSelector('message-fragment')).exists({ count: 5 });
    });

    module('each message fragment', function() {
      test('when sent by the current sessions user', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');

        let createdAt = new Date();
        createdAt.setMinutes(createdAt.getMinutes() - 24);

        let chat = this.server.create('chat');
        let message = this.server.create('chat-message', {
          chat,
          account: user,
          createdAt,
        });

        let store = this.owner.lookup('service:store');
        let chatFromStore = await store.loadRecord('chat', chat.id);

        this.set('chat', chatFromStore);

        await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
        await settled();

        assert.dom(generateTestSelector('message-fragment'))
          .hasClass('message-fragment--sent')
          .doesNotHaveClass('message-fragment--received');

        assert.dom(generateTestSelector('message-fragment', 'sender-avatar-link')).doesNotExist();
        assert.dom(generateTestSelector('message-fragment', 'sender-avatar')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'content'))
          .hasClass('message-fragment__content--sent')
          .doesNotHaveClass('message-fragment__content--received');

        assert.dom(generateTestSelector('message-fragment', 'text'))
          .hasText(message.content);

        assert.dom(generateTestSelector('message-fragment', 'attachments-container')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'meta'))
          .hasClass('message-fragment__meta--sent')
          .doesNotHaveClass('message-fragment__meta--received');

        assert.dom(generateTestSelector('message-fragment', 'created-at'))
          .hasText('24m');

        assert.dom(generateTestSelector('message-fragment', 'author')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'delete-item-button')).doesNotExist();

        await click(generateTestSelector('message-fragment'));
        await settled();

        assert.dom(generateTestSelector('message-fragment', 'delete-item-button'))
          .hasText(t('deleteMessage'));

        await click(generateTestSelector('message-fragment'));
        await settled();

        assert.dom(generateTestSelector('message-fragment', 'delete-item-button')).doesNotExist();
      });

      test('when not sent by the current sessions user', async function(assert) {
        this.server.create('user', 'withToken', 'withSession');

        let createdAt = new Date();
        createdAt.setMinutes(createdAt.getMinutes() - 24);

        let otherUser = this.server.create('user');

        let chat = this.server.create('chat');
        let message = this.server.create('chat-message', { chat, account: otherUser, createdAt });

        let store = this.owner.lookup('service:store');
        let chatFromStore = await store.loadRecord('chat', chat.id);

        this.set('chat', chatFromStore);

        await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
        await settled();

        assert.dom(generateTestSelector('message-fragment'))
          .doesNotHaveClass('message-fragment--sent')
          .hasClass('message-fragment--received');

        assert.dom(generateTestSelector('message-fragment', 'sender-avatar-link'))
          .hasAttribute('href', `/account/${otherUser.id}`);

        assert.dom(generateTestSelector('message-fragment', 'sender-avatar')).exists();

        assert.dom(generateTestSelector('message-fragment', 'content'))
          .doesNotHaveClass('message-fragment__content--sent')
          .hasClass('message-fragment__content--received');

        assert.dom(generateTestSelector('message-fragment', 'text'))
          .hasText(message.content);

        assert.dom(generateTestSelector('message-fragment', 'attachments-container')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'meta'))
          .doesNotHaveClass('message-fragment__meta--sent')
          .hasClass('message-fragment__meta--received');

        assert.dom(generateTestSelector('message-fragment', 'created-at'))
          .hasText('24m');

        assert.dom(generateTestSelector('message-fragment', 'author'))
          .hasText(otherUser.displayName);
      });
    });

    test('when the status has attachments', async function(assert) {
      let content = faker.lorem.sentence();
      let user = this.server.create('user', 'withToken', 'withSession');

      let createdAt = new Date();
      createdAt.setMinutes(createdAt.getMinutes() - 24);

      let attachment = this.server.create('attachment', { description: content });

      let chat = this.server.create('chat');
      this.server.create('chat-message', {
        chat,
        attachment,
        account: user,
        createdAt,
      });

      let store = this.owner.lookup('service:store');
      let chatFromStore = await store.loadRecord('chat', chat.id);

      this.set('chat', chatFromStore);

      await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
      await settled();

      assert.dom(generateTestSelector('message-fragment', 'attachments-container')).exists();

      // TODO: Fix when able to attach multiple attachments again.
      assert.dom(generateTestSelector('message-fragment-attachment', 'button')).exists({ count: 1 });
      assert.dom(generateTestSelector('message-fragment-attachment', 'image')).exists({ count: 1 });

      assert.dom(`${generateTestSelector('message-fragment-attachment-id', attachment.id)} ${generateTestSelector('message-fragment-attachment', 'image')}`)
        .hasAttribute('src', attachment.previewUrl)
        .hasAttribute('alt', attachment.description)
        .hasAttribute('loading', 'lazy');

      // TODO: Fix when able to attach multiple attachments again.
      // assert.dom(generateTestSelector('message-fragment-attachment', 'remaining-count'))
      //   .hasText(t('remainingCount', { count: 2 }));
    });

    test('it is accessible', async function(assert) {
      let content = faker.lorem.sentence();
      let user = this.server.create('user', 'withToken', 'withSession');

      let createdAt = new Date();
      createdAt.setMinutes(createdAt.getMinutes() - 24);

      let attachments = this.server.createList('attachment', 6);

      let statusExtra = this.server.create('status-extra', {
        content: {
          'text/plain': content,
        },
      });

      this.server.create('status', {
        account: user,
        createdAt: createdAt.toISOString(),
        mediaAttachments: attachments,
        pleroma: statusExtra,
      });

      let feedsService = this.owner.lookup('service:feeds');
      let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');

      let feed = DirectMessageFeed.create({
        conversationId: '1',
      });

      feedsService.registerFeed(feed);

      await feedsService.subscribe(feed.id);

      this.set('feed', feed);

      await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);

      await a11yAudit(this.element);

      assert.ok(true, 'no a11y errors found!');
    });

    // test('if the feed can load more items, it shows the infinity loader', async function(assert) {
    //   this.server.createList('status', 5);
    //
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //     canLoadMore: false,
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).doesNotExist();
    //   assert.dom(generateTestSelector('loading-spinner')).doesNotExist();
    //
    //   set(feed, 'canLoadMore', true);
    //   await settled();
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).exists();
    //   assert.dom(generateTestSelector('loading-spinner')).exists();
    // });

    // test('while the feed is loading', async function(assert) {
    //   this.server.createList('status', 5);
    //
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //     isLoading: false,
    //     canLoadMore: true,
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).exists();
    //   assert.dom(generateTestSelector('enque-new-items-sentinal')).exists();
    //
    //   set(feed, 'isLoading', true);
    //   await settled();
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).doesNotExist();
    // });

    // test('when the feed is empty', async function(assert) {
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //
    //   assert.dom(generateTestSelector('empty-feed-callout')).exists();
    // });

    // test('if the feed has new items', async function(assert) {
    //   this.server.createList('status', 15);
    //
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //   await settled();
    //
    //   scrollToTop(find(generateTestSelector('direct-message-feed')));
    //
    //   set(feed, 'newItems', this.server.createList('status', 6));
    //   await settled();
    //
    //   assert.dom(generateTestSelector('sync-new-items-button'))
    //     .hasText(t('numNewStatuses', { n: 6 }));
    // });
  });

  module('With OLD Dms api', function() {
    skip('it renders the proper dom', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let otherUser = this.server.create('user');

      let store = this.owner.lookup('service:store');

      let [chat] = this.server.createList('chat', 5);
      this.server.createList('chat-message', 5, { chat, account: otherUser });

      let chatFromStore = await store.loadRecord('chat', chat.id);

      // load user into store manually
      // await store.loadRecord('user', otherUser.id);

      // let DirectMessageFeed = this.owner.factoryFor('feed:chats');
      //
      // let feed = DirectMessageFeed.create({
      //   conversationId: conversation.id,
      // });
      //
      // feedsService.registerFeed(feed);
      //
      // await feedsService.subscribe(feed.id);

      this.set('chat', chatFromStore);
      // this.set('conversation', conversationFromStore);

      await render(hbs`<DirectMessage::Feed @chat={{this.chat}}/>`);

      assert.dom(generateTestSelector('direct-message-feed'))
        .hasAttribute('data-direct-message-feed');
    });

    skip('it renders message fragments for each status in the feed', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let otherUser = this.server.create('user');

      let chat = this.server.create('chat');
      this.server.createList('chat-message', 5, { chat, account: otherUser });

      let store = this.owner.lookup('service:store');
      let chatFromStore = await store.loadRecord('chat', chat.id);

      // let feedsService = this.owner.lookup('service:feeds');
      //
      // let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
      //
      // let feed = DirectMessageFeed.create({
      //   conversationId: '1',
      // });
      //
      // feedsService.registerFeed(feed);
      //
      // await feedsService.subscribe(feed.id);

      this.set('chat', chatFromStore);

      await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
      await settled();

      assert.dom(generateTestSelector('message-fragment')).exists({ count: 5 });
    });

    module('each message fragment', function() {
      skip('when sent by the current sessions user', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');

        let createdAt = new Date();
        createdAt.setMinutes(createdAt.getMinutes() - 24);

        // let otherUser = this.server.create('user');

        let chat = this.server.create('chat');
        let message = this.server.create('chat-message', {
          chat,
          account: user,
          createdAt,
        });

        // let status = this.server.create('status', {
        //   account: user,
        //   createdAt: createdAt.toISOString(),
        //   visibility: 'direct',
        // });

        // let feedsService = this.owner.lookup('service:feeds');
        // let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
        //
        // let feed = DirectMessageFeed.create({
        //   conversationId: '1',
        // });

        // feedsService.registerFeed(feed);
        //
        // await feedsService.subscribe(feed.id);

        let store = this.owner.lookup('service:store');
        let chatFromStore = await store.loadRecord('chat', chat.id);

        this.set('chat', chatFromStore);

        await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
        await settled();

        assert.dom(generateTestSelector('message-fragment'))
          .hasClass('message-fragment--sent')
          .doesNotHaveClass('message-fragment--received');

        assert.dom(generateTestSelector('message-fragment', 'sender-avatar-link')).doesNotExist();
        assert.dom(generateTestSelector('message-fragment', 'sender-avatar')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'content'))
          .hasClass('message-fragment__content--sent')
          .doesNotHaveClass('message-fragment__content--received');

        assert.dom(generateTestSelector('message-fragment', 'text'))
          .hasText(message.content);

        assert.dom(generateTestSelector('message-fragment', 'attachments-container')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'meta'))
          .hasClass('message-fragment__meta--sent')
          .doesNotHaveClass('message-fragment__meta--received');

        assert.dom(generateTestSelector('message-fragment', 'created-at'))
          .hasText('24m');

        assert.dom(generateTestSelector('message-fragment', 'author')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'delete-item-button')).doesNotExist();

        await click(generateTestSelector('message-fragment'));
        await settled();

        assert.dom(generateTestSelector('message-fragment', 'delete-item-button'))
          .hasText(t('deleteMessage'));

        await click(generateTestSelector('message-fragment'));
        await settled();

        assert.dom(generateTestSelector('message-fragment', 'delete-item-button')).doesNotExist();
      });

      skip('when not sent by the current sessions user', async function(assert) {
        this.server.create('user', 'withToken', 'withSession');

        let createdAt = new Date();
        createdAt.setMinutes(createdAt.getMinutes() - 24);

        let otherUser = this.server.create('user');

        let chat = this.server.create('chat');
        let message = this.server.create('chat-message', { chat, account: otherUser, createdAt });

        // let status = this.server.create('status', {
        //   createdAt: createdAt.toISOString(),
        //   account: otherUser,
        //   visibility: 'direct',
        // });

        // let feedsService = this.owner.lookup('service:feeds');
        // let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
        //
        // let feed = DirectMessageFeed.create({
        //   conversationId: '1',
        // });
        //
        // feedsService.registerFeed(feed);
        //
        // await feedsService.subscribe(feed.id);

        let store = this.owner.lookup('service:store');
        let chatFromStore = await store.loadRecord('chat', chat.id);

        this.set('chat', chatFromStore);

        await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
        await settled();

        assert.dom(generateTestSelector('message-fragment'))
          .doesNotHaveClass('message-fragment--sent')
          .hasClass('message-fragment--received');

        assert.dom(generateTestSelector('message-fragment', 'sender-avatar-link'))
          .hasAttribute('href', `/account/${otherUser.id}`);

        assert.dom(generateTestSelector('message-fragment', 'sender-avatar')).exists();

        assert.dom(generateTestSelector('message-fragment', 'content'))
          .doesNotHaveClass('message-fragment__content--sent')
          .hasClass('message-fragment__content--received');

        assert.dom(generateTestSelector('message-fragment', 'text'))
          .hasText(message.content);

        assert.dom(generateTestSelector('message-fragment', 'attachments-container')).doesNotExist();

        assert.dom(generateTestSelector('message-fragment', 'meta'))
          .doesNotHaveClass('message-fragment__meta--sent')
          .hasClass('message-fragment__meta--received');

        assert.dom(generateTestSelector('message-fragment', 'created-at'))
          .hasText('24m');

        assert.dom(generateTestSelector('message-fragment', 'author'))
          .hasText(otherUser.displayName);
      });
    });

    skip('when the status has attachments', async function(assert) {
      let content = faker.lorem.sentence();
      let user = this.server.create('user', 'withToken', 'withSession');

      let createdAt = new Date();
      createdAt.setMinutes(createdAt.getMinutes() - 24);

      let attachment = this.server.create('attachment', { description: content });

      // let statusExtra = this.server.create('status-extra', {
      //   content: {
      //     'text/plain': content,
      //   },
      // });

      // let status = this.server.create('status', {
      //   account: user,
      //   createdAt: createdAt.toISOString(),
      //   mediaAttachments: attachments,
      //   pleroma: statusExtra,
      //   visibility: 'direct',
      // });

      let chat = this.server.create('chat');
      this.server.create('chat-message', {
        chat,
        attachment,
        account: user,
        createdAt,
      });

      // let feedsService = this.owner.lookup('service:feeds');
      // let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
      //
      // let feed = DirectMessageFeed.create({
      //   conversationId: '1',
      // });
      //
      // feedsService.registerFeed(feed);
      //
      // await feedsService.subscribe(feed.id);

      let store = this.owner.lookup('service:store');
      let chatFromStore = await store.loadRecord('chat', chat.id);

      this.set('chat', chatFromStore);

      await render(hbs`<DirectMessage::Feed @chat={{this.chat}} />`);
      await settled();

      assert.dom(generateTestSelector('message-fragment', 'attachments-container')).exists();

      // TODO: Fix when able to attach multiple attachments again.
      assert.dom(generateTestSelector('message-fragment-attachment', 'button')).exists({ count: 1 });
      assert.dom(generateTestSelector('message-fragment-attachment', 'image')).exists({ count: 1 });

      assert.dom(`${generateTestSelector('message-fragment-attachment-id', attachment.id)} ${generateTestSelector('message-fragment-attachment', 'image')}`)
        .hasAttribute('src', attachment.previewUrl)
        .hasAttribute('alt', attachment.description)
        .hasAttribute('loading', 'lazy');

      // TODO: Fix when able to attach multiple attachments again.
      // assert.dom(generateTestSelector('message-fragment-attachment', 'remaining-count'))
      //   .hasText(t('remainingCount', { count: 2 }));
    });

    skip('it is accessible', async function(assert) {
      let content = faker.lorem.sentence();
      let user = this.server.create('user', 'withToken', 'withSession');

      let createdAt = new Date();
      createdAt.setMinutes(createdAt.getMinutes() - 24);

      let attachments = this.server.createList('attachment', 6);

      let statusExtra = this.server.create('status-extra', {
        content: {
          'text/plain': content,
        },
      });

      this.server.create('status', {
        account: user,
        createdAt: createdAt.toISOString(),
        mediaAttachments: attachments,
        pleroma: statusExtra,
      });

      let feedsService = this.owner.lookup('service:feeds');
      let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');

      let feed = DirectMessageFeed.create({
        conversationId: '1',
      });

      feedsService.registerFeed(feed);

      await feedsService.subscribe(feed.id);

      this.set('feed', feed);

      await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);

      await a11yAudit(this.element);

      assert.ok(true, 'no a11y errors found!');
    });

    // test('if the feed can load more items, it shows the infinity loader', async function(assert) {
    //   this.server.createList('status', 5);
    //
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //     canLoadMore: false,
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).doesNotExist();
    //   assert.dom(generateTestSelector('loading-spinner')).doesNotExist();
    //
    //   set(feed, 'canLoadMore', true);
    //   await settled();
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).exists();
    //   assert.dom(generateTestSelector('loading-spinner')).exists();
    // });

    // test('while the feed is loading', async function(assert) {
    //   this.server.createList('status', 5);
    //
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //     isLoading: false,
    //     canLoadMore: true,
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).exists();
    //   assert.dom(generateTestSelector('enque-new-items-sentinal')).exists();
    //
    //   set(feed, 'isLoading', true);
    //   await settled();
    //
    //   assert.dom(generateTestSelector('feed-infinity-loader')).doesNotExist();
    // });

    // test('when the feed is empty', async function(assert) {
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //
    //   assert.dom(generateTestSelector('empty-feed-callout')).exists();
    // });

    // test('if the feed has new items', async function(assert) {
    //   this.server.createList('status', 15);
    //
    //   let feedsService = this.owner.lookup('service:feeds');
    //   let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    //
    //   let feed = DirectMessageFeed.create({
    //     conversationId: '1',
    //   });
    //
    //   feedsService.registerFeed(feed);
    //
    //   await feedsService.subscribe(feed.id);
    //
    //   this.set('feed', feed);
    //
    //   await render(hbs`<DirectMessage::Feed @feed={{this.feed}} />`);
    //   await settled();
    //
    //   scrollToTop(find(generateTestSelector('direct-message-feed')));
    //
    //   set(feed, 'newItems', this.server.createList('status', 6));
    //   await settled();
    //
    //   assert.dom(generateTestSelector('sync-new-items-button'))
    //     .hasText(t('numNewStatuses', { n: 6 }));
    // });
  });
});

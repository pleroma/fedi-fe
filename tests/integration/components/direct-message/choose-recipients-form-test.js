import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  findAll,
  render,
  settled,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import faker from 'faker';
import Helper from '@ember/component/helper';

module('Integration | Component | direct-message/choose-recipients-form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    await this.owner.setupRouter();

    this.server.create('user', 'withToken', 'withSession');

    await this.owner.lookup('service:session').loadCurrentUser();

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it renders', async function(assert) {
    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    assert.dom(generateTestSelector('recipient')).doesNotExist();

    assert.dom(generateTestSelector('recipient-search'))
      .hasAttribute('placeholder', t('searchForPeopleAndMessages'))
      .hasAria('label', t('searchForPeopleAndMessages'))
      .doesNotHaveClass('invalid')
      .isFocused();

    assert.dom(generateTestSelector('compose-message-button'))
      .isDisabled()
      .hasText(t('next'));

    assert.dom(generateTestSelector('user-card')).doesNotExist();

    assert.dom(generateTestSelector('loading-recent-messages')).doesNotExist();

    assert.dom(generateTestSelector('recent-conversation')).doesNotExist();

    assert.dom(generateTestSelector('no-messages-callout')).exists();

    assert.dom(generateTestSelector('see-all-direct-messages-link')).doesNotExist();
  });

  test('accepts splattributes', async function(assert) {
    await render(hbs`<DirectMessage::ChooseRecipientsForm data-test-selector="splatter"/>`);

    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('renders a list of users a search is entered, shows at most 10 results', async function(assert) {
    let query = faker.lorem.word();

    let users = this.server.createList('user', 100, {
      displayName: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);
    await settled();

    assert.dom(generateTestSelector('user-card')).exists();

    let count = findAll(generateTestSelector('user-card')).length;

    assert.ok(
      count === 10 || count === 9,
      'num of results should be 10 or 9; there is a small chance the current session user could be excluded from results',
    );

    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)}`)
      .hasText(`${users[5].displayName} ${t('atHandle', { handle: users[5].username })}`)
      .hasAttribute('for', `recipient-${users[5].id}`);

    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)} ${generateTestSelector('user-card', 'checkbox')}`)
      .hasAttribute('id', `recipient-${users[5].id}`)
      .hasAttribute('type', 'checkbox');
  });

  test('in classic mode clicking on a user will add it to recipients list', async function(assert) {
    let query = faker.lorem.word();

    let users = this.server.createList('user', 3, {
      displayName: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    assert.dom(generateTestSelector('user-card')).exists({ count: 3 });

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[1].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[1].id)}`).exists();

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[0].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[0].id)}`).exists();

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[2].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[2].id)}`).exists();
  });

  test('in chat mode clicking on a user will add it to recipients list', async function(assert) {
    let query = faker.lorem.word();

    let users = this.server.createList('user', 3, {
      displayName: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm @directMode="chat" />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    assert.dom(generateTestSelector('user-card')).exists({ count: 3 });

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[1].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[1].id)}`).exists();

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[0].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[0].id)}`).doesNotExist('cannot add more than one user for now');

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[2].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[2].id)}`).doesNotExist('cannot add more than one user for now');
  });

  test('recipients can be removed individually', async function(assert) {
    let query = faker.lorem.word();
    let users = this.server.createList('user', 3, {
      displayName: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[1].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[1].id)}`).hasText(t('atHandle', { handle: users[1].username }));

    await click(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[1].id)} ${generateTestSelector('remove-recipient-button')}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[1].id)}`).doesNotExist();
  });

  module('recent conversations', function() {
    test('in classic mode when you have some, it will display the first three', async function(assert) {
      this.server.createList('conversation', 5);

      await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

      assert.dom(generateTestSelector('recent-conversation')).exists({ count: 3 });
      assert.dom(generateTestSelector('loading-recent-messages')).doesNotExist();
      assert.dom(generateTestSelector('no-messages-callout')).doesNotExist();
      assert.dom(generateTestSelector('see-all-direct-messages-link')).hasText(t('seeAllChats'));
    });

    test('in chat mode when you have some, it will display the first three', async function(assert) {
      this.server.createList('chat-message', 5);

      await render(hbs`<DirectMessage::ChooseRecipientsForm @directMode="chat" />`);

      assert.dom(generateTestSelector('recent-conversation')).exists({ count: 3 });
      assert.dom(generateTestSelector('loading-recent-messages')).doesNotExist();
      assert.dom(generateTestSelector('no-messages-callout')).doesNotExist();
      assert.dom(generateTestSelector('see-all-direct-messages-link')).hasText(t('seeAllChats'));
    });

    test('in classic mode when you dont have any, it will display a nice callout', async function(assert) {
      await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

      assert.dom(generateTestSelector('recent-conversation')).doesNotExist();
      assert.dom(generateTestSelector('loading-recent-messages')).doesNotExist();
      assert.dom(generateTestSelector('no-messages-callout')).hasText(t('youHaveNoChats'));
      assert.dom(generateTestSelector('see-all-direct-messages-link')).doesNotExist();
    });

    test('in chat mode when you dont have any, it will display a nice callout', async function(assert) {
      await render(hbs`<DirectMessage::ChooseRecipientsForm @directMode="chat" />`);

      assert.dom(generateTestSelector('recent-conversation')).doesNotExist();
      assert.dom(generateTestSelector('loading-recent-messages')).doesNotExist();
      assert.dom(generateTestSelector('no-messages-callout')).hasText(t('youHaveNoChats'));
      assert.dom(generateTestSelector('see-all-direct-messages-link')).doesNotExist();
    });
  });

  test('while search is running, it disables the search button', async function(assert) {
    this.set('search', {
      isRunning: true,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm @search={{this.search}}/>`);

    assert.dom(generateTestSelector('compose-message-button')).isDisabled();
  });

  test('if searching yields no users, we show a good message + recent direct messages', async function(assert) {
    let query = faker.lorem.word();

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    assert.dom(generateTestSelector('user-card')).doesNotExist();
    assert.dom(generateTestSelector('empty-user-results')).hasText(t('noUsersMatchedQuery'));
  });

  test('the next button will enable when recipients have been selected', async function(assert) {
    let query = faker.lorem.word();

    let users = this.server.createList('user', 20, {
      username: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[1].id)}`);
    await settled();

    assert.dom(generateTestSelector('compose-message-button')).isEnabled();
  });

  test('it is accessible', async function(assert) {
    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  test('accepts properties to pre-populate the recipients list', async function(assert) {
    let otherUser = this.server.create('user');

    this.set('otherUser', otherUser);

    await render(hbs`<DirectMessage::ChooseRecipientsForm @initialParticipants={{array this.otherUser.id}}/>`);

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', otherUser.id)}`).hasText(t('atHandle', { handle: otherUser.username }));
  });

  test('does not display the sessions user as a valid recipient', async function(assert) {
    let query = faker.lorem.word();

    let sessionUser = this.server.schema.users.first();
    sessionUser.update({ username: query });

    this.server.createList('user', 100, {
      displayName: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', sessionUser.id)}`).doesNotExist();
  });

  test('cannot add the sessions user as a recipient', async function(assert) {
    let sessionUser = this.server.create('user', 'withToken', 'withSession');

    this.set('sessionUserId', sessionUser.id);

    await render(hbs`<DirectMessage::ChooseRecipientsForm @initialParticipants={{array this.sessionUserId}}/>`);

    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', sessionUser.id)}`).doesNotExist();
  });

  test('adds a checkbox next to users when selected', async function(assert) {
    let query = faker.lorem.word();
    this.server.create('user', 'withToken', 'withSession', {
      username: query,
    });

    let users = this.server.createList('user', 100, {
      displayName: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[0].id)} ${generateTestSelector('user-card', 'checkbox')}`).isNotChecked();
    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[1].id)} ${generateTestSelector('user-card', 'checkbox')}`).isNotChecked();
    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[2].id)} ${generateTestSelector('user-card', 'checkbox')}`).isNotChecked();
    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[3].id)} ${generateTestSelector('user-card', 'checkbox')}`).isNotChecked();
    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[4].id)} ${generateTestSelector('user-card', 'checkbox')}`).isNotChecked();

    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)} ${generateTestSelector('user-card', 'checkbox')}`).isChecked();

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)} ${generateTestSelector('user-card', 'checkbox')}`).isNotChecked();
  });

  test('clicking on a previously added user will remove that user from the recipients', async function(assert) {
    let query = faker.lorem.word();
    this.server.create('user', 'withToken', 'withSession', {
      username: query,
    });

    let users = this.server.createList('user', 100, {
      displayName: query,
    });

    await render(hbs`<DirectMessage::ChooseRecipientsForm />`);

    await fillIn(generateTestSelector('recipient-search'), query);

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[5].id)}`).doesNotExist();

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[5].id)}`).exists();

    await click(`${generateTestSelector('user-card')}${generateTestSelector('user-id', users[5].id)}`);
    await settled();

    assert.dom(`${generateTestSelector('recipient')}${generateTestSelector('user-id', users[5].id)}`).doesNotExist();
  });
});

import { module, skip, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  findAll,
  render,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import faker from 'faker';
import { setBreakpoint } from 'ember-responsive/test-support';
import Helper from '@ember/component/helper';

module('Integration | Component | direct-message', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it renders', async function(assert) {
    let store = this.owner.lookup('service:store');

    let user = this.server.create('user', 'withToken', 'withSession');

    let chat = this.server.create('chat');
    this.server.create('chat-message', {
      chat,
      account: user,
    });

    let chatFromStore = await store.loadRecord('chat', chat.id);

    this.set('chat', chatFromStore);

    await render(hbs`<DirectMessage @chat={{this.chat}} @participants={{array this.chat.account}} />`);

    assert.dom(generateTestSelector('direct-message')).exists();
    assert.dom(generateTestSelector('direct-message-form')).exists();
    assert.dom(generateTestSelector('direct-message-feed')).exists();

    assert.dom(generateTestSelector('direct-message', 'reload-messages-loader')).exists();

    // TODO: Re-enable when multiple participants are possible.
    // assert.dom(generateTestSelector('direct-message', 'heading')).hasText(`${chatFromStore.account.displayName} ${t('remainingCount', { count: 6 })}`);

    // assert.equal(findAll(generateTestSelector('direct-message', 'recipient-link')).length, 5);
    // assert.dom(generateTestSelector('direct-message', 'remaining-count')).hasText(t('remainingCount', { count: 2 }));
  });

  test('when messaging 1 person, it displays the recipients displayName', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let otherUser = this.server.create('user');

    let chat = this.server.create('chat', {
      account: otherUser,
    });

    let store = this.owner.lookup('service:store');
    let chatFromStore = await store.loadRecord('chat', chat.id);

    this.set('chat', chatFromStore);

    await render(hbs`<DirectMessage @chat={{this.chat}} @participants={{array this.chat.account}} />`);

    assert.dom(generateTestSelector('direct-message', 'heading')).hasText(otherUser.displayName);
    assert.equal(findAll(generateTestSelector('direct-message', 'recipient-link')).length, 1);
    assert.dom(generateTestSelector('direct-message', 'remaining-count')).doesNotExist();
  });

  skip('when messaging 2 people, it displays both the recipients displayName\'s', async function(assert) {
    let store = this.owner.lookup('service:store');
    let DirectMessageFeed = this.owner.factoryFor('feed:directMessage');
    let feeds = this.owner.lookup('service:feeds');
    this.server.create('user', 'withToken', 'withSession');
    let accounts = this.server.createList('user', 2, {
      displayName: faker.name.firstName().substring(0, 24),
    });
    let conversation = this.server.create('conversation', {
      accounts,
    });

    let conversationFromStore = await store.loadRecord('conversation', conversation.id);

    let feed = feeds.registerFeed(DirectMessageFeed.create({
      conversationId: conversation.id,
    }));

    await feeds.subscribe(feed.id);

    this.set('conversation', conversationFromStore);
    this.set('feed', feed);

    await render(hbs`<DirectMessage @conversation={{this.conversation}} @feed={{this.feed}}/>`);

    assert.dom(generateTestSelector('direct-message', 'heading')).hasText(`${accounts.firstObject.displayName}, ${accounts.lastObject.displayName}`);
    assert.equal(findAll(generateTestSelector('direct-message', 'recipient-link')).length, 2);
    assert.dom(generateTestSelector('direct-message', 'remaining-count')).doesNotExist();
  });

  skip('when messaging 2 or more people, it displays the the first users name plus a remaining count', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });
    let conversation = this.server.create('conversation', {
      lastStatus: status,
      accounts: this.server.createList('user', 7, {
        displayName: faker.name.firstName().substring(0, 24),
      }),
    });

    let conversationFromStore = await store.loadRecord('conversation', conversation.id);

    this.set('conversation', conversationFromStore);

    await render(hbs`<DirectMessage @conversation={{this.conversation}}/>`);

    assert.dom(generateTestSelector('direct-message', 'heading')).hasText(`${conversationFromStore.accounts.firstObject.displayName} ${t('remainingCount', { count: 6 })}`);
  });

  module('avatar occlusion', function() {
    skip('on mobile, we only show 3 avatars + a remaining count for any more than that', async function(assert) {
      let store = this.owner.lookup('service:store');
      let user = this.server.create('user', 'withToken', 'withSession');
      let status = this.server.create('status', {
        account: user,
      });
      let conversation = this.server.create('conversation', {
        accounts: this.server.createList('user', 7),
        lastStatus: status,
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      setBreakpoint('small');

      await render(hbs`<DirectMessage @conversation={{this.conversation}}/>`);

      assert.equal(findAll(generateTestSelector('direct-message', 'recipient-link')).length, 3);
      assert.dom(generateTestSelector('direct-message', 'remaining-count')).hasText(t('remainingCount', { count: 4 }));
    });

    skip('On desktop we show the first 5 recipients + a remaining count for any more than that', async function(assert) {
      let store = this.owner.lookup('service:store');
      let user = this.server.create('user', 'withToken', 'withSession');
      let status = this.server.create('status', {
        account: user,
      });
      let conversation = this.server.create('conversation', {
        accounts: this.server.createList('user', 7),
        lastStatus: status,
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      this.set('conversation', conversationFromStore);

      setBreakpoint('large');

      await render(hbs`<DirectMessage @conversation={{this.conversation}}/>`);

      assert.equal(findAll(generateTestSelector('direct-message', 'recipient-link')).length, 5);
      assert.dom(generateTestSelector('direct-message', 'remaining-count')).hasText(t('remainingCount', { count: 2 }));
    });
  });

  test('truncates all displayName\'s in the header at 24chars', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let [otherUser] = this.server.createList('user', 1, {
      displayName: '#'.repeat(30),
    });

    let chat = this.server.create('chat', {
      account: otherUser,
    });

    let store = this.owner.lookup('service:store');
    let chatFromStore = await store.loadRecord('chat', chat.id);

    this.set('chat', chatFromStore);

    await render(hbs`<DirectMessage @chat={{this.chat}} @participants={{array this.chat.account}} />`);

    assert.dom(generateTestSelector('direct-message', 'heading')).hasText(`${otherUser.displayName.substring(0, 24)}...`);
  });

  test('it is accessible', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let otherUser = this.server.create('user');

    let chat = this.server.create('chat', {
      account: otherUser,
    });

    this.server.createList('chat-message', 5, {
      chat,
      account: otherUser,
    });

    let store = this.owner.lookup('service:store');
    let chatFromStore = await store.loadRecord('chat', chat.id);

    this.set('chat', chatFromStore);

    await render(hbs`<DirectMessage @chat={{this.chat}} @participants={{array this.chat.account}} />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

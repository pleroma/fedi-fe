import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  render,
  settled,
  triggerEvent,
  triggerKeyEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import sinon from 'sinon';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import faker from 'faker';
import { defer } from 'rsvp';
import { enableFeature } from 'ember-feature-flags/test-support';
import humanizeFileSize from 'pleroma-pwa/utils/humanize-file-size';

module('Integration | Component | direct-message/form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
    this.owner.setupRouter();
    enableFeature('statusMedia');
  });

  test('it renders', async function(assert) {
    await render(hbs`<DirectMessage::Form />`);

    assert.dom(generateTestSelector('create-direct-message-form')).exists();

    assert.dom(generateTestSelector('create-direct-message', 'file-input-element'))
      .hasAttribute('type', 'file')
      .isNotDisabled();

    assert.dom(generateTestSelector('create-direct-message', 'message-body-input'))
      .doesNotHaveClass('invalid')
      .hasAttribute('aria-label')
      .hasAttribute('rows', '1')
      .isFocused()
      .isNotDisabled();

    assert.dom(generateTestSelector('create-direct-message', 'submit-button'))
      .hasAttribute('aria-label', t('submit'))
      .hasAttribute('type', 'submit')
      .isDisabled();
  });

  test('accepts an array of initialLinks that it uses to pre-populate the status content', async function(assert) {
    this.initialLinks = [
      faker.internet.url(),
      faker.internet.url(),
    ];

    await render(hbs`<DirectMessage::Form
      @initialLinks={{this.initialLinks}}
    />`);
    await settled();

    assert.dom(generateTestSelector('create-direct-message', 'message-body-input'))
      .hasValue(`${this.initialLinks.join(' ')} `);
  });

  test('displays good errors if the stautus length exceeds the limit', async function(assert) {
    let sentence = faker.lorem.sentence();

    let instance = this.server.schema.instances.first();
    instance.update('maxTootChars', sentence.length - 1);
    await this.owner.lookup('service:instances').refreshTask.perform();

    await render(hbs`<DirectMessage::Form />`);
    await settled();

    await fillIn(generateTestSelector('create-direct-message', 'message-body-input'), sentence);
    await click(generateTestSelector('create-direct-message', 'submit-button'));

    assert.dom(generateTestSelector('create-direct-message', 'compose-wrapper'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('create-direct-message', 'error'))
      .hasText(t('yourPostIsTooLong', { max: instance.maxTootChars }));
  });

  test('displays good errors when you have entered nothing, and submit the form', async function(assert) {
    await render(hbs`<DirectMessage::Form />`);

    await triggerKeyEvent(generateTestSelector('create-direct-message', 'message-body-input'),
      'keydown',
      13,
      {
        metaKey: true,
      });
    await settled();

    assert.dom(generateTestSelector('create-direct-message', 'error'))
      .hasText(t('yourPostMustHaveContent'));

    assert.dom(generateTestSelector('create-direct-message', 'compose-wrapper'))
      .hasClass('invalid');

    assert.dom(generateTestSelector('create-direct-message', 'submit-button'))
      .isDisabled();
  });

  test('locks down the form when saving', async function(assert) {
    let deferred = defer();

    this.set('promise', () => deferred.promise);

    await render(hbs`<DirectMessage::Form
      @createDirectMessage={{this.promise}}
    />`);

    await fillIn(generateTestSelector('create-direct-message', 'message-body-input'), faker.lorem.sentence());
    await click(generateTestSelector('create-direct-message', 'submit-button'));

    assert.dom(generateTestSelector('create-direct-message', 'file-input-element'))
      .isDisabled();

    assert.dom(generateTestSelector('create-direct-message', 'message-body-input'))
      .isDisabled();

    assert.dom(generateTestSelector('create-direct-message', 'submit-button'))
      .isDisabled();

    assert.dom(generateTestSelector('create-direct-message', 'sending-spinner'))
      .hasAttribute('alt', t('sendingMessage'))
      .hasAttribute('aria-busy', 'true')
      .hasAttribute('height', '24')
      .hasAttribute('role', 'alert')
      .hasAttribute('width', '24');

    deferred.resolve();
    await settled();
  });

  test('you can submit the form with cmd+enter', async function(assert) {
    this.set('spy', sinon.spy());

    await render(hbs`<DirectMessage::Form
      @createDirectMessage={{this.spy}}
    />`);
    await settled();

    await fillIn(generateTestSelector('create-direct-message', 'message-body-input'), faker.lorem.sentence());
    await triggerKeyEvent(generateTestSelector('create-direct-message', 'message-body-input'),
      'keydown',
      13,
      {
        metaKey: true,
      });

    assert.ok(this.spy.calledOnce);
  });

  test('you can submit the form with ctrl+enter', async function(assert) {
    this.set('spy', sinon.spy());

    await render(hbs`<DirectMessage::Form
      @createDirectMessage={{this.spy}}
    />`);
    await settled();

    await fillIn(generateTestSelector('create-direct-message', 'message-body-input'), faker.lorem.sentence());
    await triggerKeyEvent(generateTestSelector('create-direct-message', 'message-body-input'),
      'keydown',
      13,
      {
        ctrlKey: true,
      });

    assert.ok(this.spy.calledOnce);
  });

  test('it is accessible', async function(assert) {
    await render(hbs`<DirectMessage::Form />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  test('it accepts splattributes', async function(assert) {
    await render(hbs`<DirectMessage::Form
      data-test-selector="splatter"
    />`);

    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('displays good errors when uploading a file that is too large', async function(assert) {
    let instances = this.owner.lookup('service:instances');

    await instances.refreshTask.perform();

    await render(hbs`<DirectMessage::Form />`);

    await triggerEvent(
      generateTestSelector('create-direct-message', 'file-input-element'),
      'change',
      { files: [new File(['a'.repeat(instances.current.uploadLimit + 1)], 'test-image.jpg', { type: 'jpg' })] },
    );
    await settled();

    assert.dom(generateTestSelector('create-direct-message', 'error'))
      .hasText(t('fileTooLarge', { max: humanizeFileSize(instances.current.uploadLimit) }));
  });

  test('clears file size errors when submitting so that you can submit the form', async function(assert) {
    // Given that I have selected a file that is too big
    // When I enter in text instead
    // Then I will be able to send the message

    let instances = this.owner.lookup('service:instances');

    await instances.refreshTask.perform();

    await render(hbs`<DirectMessage::Form @createDirectMessage={{noop}} />`);

    await triggerEvent(
      generateTestSelector('create-direct-message', 'file-input-element'),
      'change',
      { files: [new File(['a'.repeat(instances.current.uploadLimit + 1)], 'test-image.jpg', { type: 'jpg' })] },
    );
    await settled();

    assert.dom(generateTestSelector('create-direct-message', 'error'))
      .hasText(t('fileTooLarge', { max: humanizeFileSize(instances.current.uploadLimit) }));

    await fillIn(generateTestSelector('create-direct-message', 'message-body-input'), faker.lorem.sentence());
    await settled();

    await click(generateTestSelector('create-direct-message', 'submit-button'));
    await settled();

    assert.dom(generateTestSelector('create-direct-message', 'error'))
      .doesNotExist();
  });
});

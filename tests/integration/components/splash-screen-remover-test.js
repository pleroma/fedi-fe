import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Component | splash-screen-remover', function(hooks) {
  setupRenderingTest(hooks);

  test('when rendered, removes an element with the id splash-screen from the dom', async function(assert) {

    await render(hbs`
      <div id="splash-screen" data-test-selector="splash-screen"></div>
      <SplashScreenRemover />
    `);

    assert.dom(generateTestSelector('splash-screen')).doesNotExist();
  });
});

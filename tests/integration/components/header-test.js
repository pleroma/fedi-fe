import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, click, settled } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Integration | Component | header', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
    this.owner.setupRouter();
  });

  test('has all the right parts', async function(assert) {
    let instances = this.owner.lookup('service:instances');
    let offCanvasNav = this.owner.lookup('service:offCanvasNav');

    await instances.refreshTask.perform();

    await render(hbs`<Header/>`);

    assert.dom(generateTestSelector('header-title')).exists();
    assert.dom(`${generateTestSelector('header-title')} .header__logo`).hasAttribute('src', instances.current.thumbnail);
    assert.dom(`${generateTestSelector('header-title')} .header__logo`).hasAttribute('alt', '');
    assert.dom(`${generateTestSelector('header-title')} .header__title-text`).hasText(instances.current.title);

    assert.dom(generateTestSelector('header-title-link')).hasAttribute('href', '/feeds/public', 'The title is a link back to the home page');

    assert.dom(generateTestSelector('header-nav')).exists();

    assert.dom(generateTestSelector('header-menu-toggle')).hasAria('controls', 'menu');
    assert.dom(generateTestSelector('header-menu-toggle')).hasAria('haspopup', 'true');
    assert.dom(generateTestSelector('header-menu-toggle')).hasAria('label', t('openMenu'));
    assert.dom(generateTestSelector('header-menu-toggle')).hasAttribute('tabindex', '0');

    assert.dom(generateTestSelector('header-search')).exists();

    assert.dom(generateTestSelector('header-menu-toggle')).hasAria('expanded', 'false');

    await click(generateTestSelector('header-menu-toggle'));

    await settled();

    await offCanvasNav.open();

    await settled();

    assert.dom(generateTestSelector('header-menu-toggle')).hasAria('expanded', 'true');
  });

  test('accepts splattributes', async function(assert) {
    await render(hbs`<Header data-test-selector="splatter"/>`);
    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('it is accessible', async function(assert) {
    let instances = this.owner.lookup('service:instances');

    await instances.refreshTask.perform();

    await render(hbs`<Header />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  fillIn,
  find,
  render,
  settled,
  setupOnerror,
  resetOnerror,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';

module('Integration | Component | search-suggestions-overlay', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('yields a trigger identifier', async function(assert) {
    await render(hbs`
      <SearchSuggestionsOverlay as |overlay|>
        <div data-test-selector="has-identifier">{{overlay.triggerIdentifier}}</div>
      </SearchSuggestionsOverlay>
    `);

    assert.dom(generateTestSelector('has-identifier'))
      .includesText('-trigger');
  });

  test('if it fails to fetch results, elegantly fails and does nothing', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.get('/api/v2/search', function() {}, 500);

    let query = 'test';

    await render(hbs`
      <SearchSuggestionsOverlay as |overlay|>
        <input
          data-test-selector="input"
          {{on "input" (queue
            (action (get (service "search") "setQuery") value="target.value")
            overlay.onQueryUpdated
          )}}
        >
      </SearchSuggestionsOverlay>
    `);

    await fillIn(generateTestSelector('input'), `#${query}`);
    await settled();

    assert.dom(generateTestSelector('search-suggestions-overlay', 'content'))
      .doesNotExist();

    resetOnerror();
  });

  module('the results panel', function() {
    test('shows at most 3 hashtag results', async function(assert) {
      let query = 'test';

      for (let step = 0; step < 5; step++) {
        this.server.create('hashtag', {
          name: `${query} ${step}`,
        });
      }

      await render(hbs`
        <SearchSuggestionsOverlay as |overlay|>
          <input
            data-test-selector="input"
            {{on "input" (queue
              (action (get (service "search") "setQuery") value="target.value")
              overlay.onQueryUpdated
            )}}
          >
        </SearchSuggestionsOverlay>
      `);

      await fillIn(generateTestSelector('input'), `#${query}`);
      await settled();

      assert.dom(generateTestSelector('search-suggestions-overlay', 'hashtag'))
        .exists({
          count: 3,
        });
    });

    test('hashtag results have the proper templating', async function(assert) {
      let hashtag = this.server.create('hashtag', {
        name: 'test',
      });

      await this.owner.setupRouter();

      await render(hbs`
        <SearchSuggestionsOverlay as |overlay|>
          <input
            data-test-selector="input"
            {{on "input" (queue
              (action (get (service "search") "setQuery") value="target.value")
              overlay.onQueryUpdated
            )}}
          >
        </SearchSuggestionsOverlay>
      `);

      await fillIn(generateTestSelector('input'), '#test');
      await settled();

      assert.dom(generateTestSelector('search-suggestions-overlay', 'hashtag'))
        .hasAttribute('href', `/tag/${hashtag.name}`)
        .hasText('#test')
    });

    test('links to the hashtag tab of the search results screen', async function(assert) {
      let query = 'test';

      this.server.create('hashtag', {
        name: query,
      });

      await this.owner.setupRouter();

      await render(hbs`
        <SearchSuggestionsOverlay as |overlay|>
          <input
            data-test-selector="input"
            {{on "input" (queue
              (action (get (service "search") "setQuery") value="target.value")
              overlay.onQueryUpdated
            )}}
          >
        </SearchSuggestionsOverlay>
      `);

      await fillIn(generateTestSelector('input'), `#${query}`);
      await settled();

      assert.dom(generateTestSelector('search-suggestions-overlay', 'all-hashtags-link'))
        .hasAttribute('href', `/search/${encodeURIComponent(`#${query}`)}/hashtags`)
        .hasText(t('seeAllHashtags'));
    });

    test('shows at most 5 account results', async function(assert) {
      let query = 'test';

      this.server.createList('user', 10, {
        username: query,
      });

      await render(hbs`
        <SearchSuggestionsOverlay as |overlay|>
          <input
            data-test-selector="input"
            {{on "input" (queue
              (action (get (service "search") "setQuery") value="target.value")
              overlay.onQueryUpdated
            )}}
          >
        </SearchSuggestionsOverlay>
      `);

      await fillIn(generateTestSelector('input'), `#${query}`);
      await settled();

      assert.dom(generateTestSelector('profile-card'))
        .exists({
          count: 5,
        });
    });

    test('links to the accounts tab of the search results screen', async function(assert) {
      let query = 'test';

      this.server.create('user', {
        username: query,
      });

      await this.owner.setupRouter();

      await render(hbs`
        <SearchSuggestionsOverlay as |overlay|>
          <input
            data-test-selector="input"
            {{on "input" (queue
              (action (get (service "search") "setQuery") value="target.value")
              overlay.onQueryUpdated
            )}}
          >
        </SearchSuggestionsOverlay>
      `);

      await fillIn(generateTestSelector('input'), `#${query}`);
      await settled();

      assert.dom(generateTestSelector('search-suggestions-overlay', 'all-people-link'))
        .hasAttribute('href', `/search/${encodeURIComponent(`#${query}`)}/people`)
        .hasText(t('seeAllPeople'));
    });

    test('is accessible', async function(assert) {
      await render(hbs`
        <SearchSuggestionsOverlay as |overlay|>
          <input
            data-test-selector="input"
            {{on "input" (queue
              (action (get (service "search") "setQuery") value="target.value")
              overlay.onQueryUpdated
            )}}
          >
        </SearchSuggestionsOverlay>
      `);

      await fillIn(generateTestSelector('input'), '#'.repeat(3));
      await settled();

      await a11yAudit(find(generateTestSelector('search-suggestions-overlay', 'content')));

      assert.ok(true, 'no a11y errors found!');
    });
  });
});

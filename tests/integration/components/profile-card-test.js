import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  render,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';

module('Integration | Component | profile-card', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('it has the right parts', async function(assert) {
    let user = this.server.create('user');
    let store = this.owner.lookup('service:store');
    let userFromStore = await store.loadRecord('user', user.id);

    this.set('user', userFromStore);

    await render(hbs`<ProfileCard @user={{this.user}}/>`);

    assert.dom(generateTestSelector('profile-card')).exists();
    assert.dom(generateTestSelector('user-id', userFromStore.id)).exists();

    assert.dom(generateTestSelector('avatar-link')).hasAttribute('href', `/account/${userFromStore.id}`);
    assert.dom(generateTestSelector('user-avatar')).exists();

    assert.dom(generateTestSelector('profile-card', 'displayName')).hasText(userFromStore.displayName);
    assert.dom(generateTestSelector('profile-card', 'username')).hasText(t('atHandle', { handle: userFromStore.username }));

    assert.dom(generateTestSelector('profile-card-action', 'unfollow')).doesNotExist();
    assert.dom(generateTestSelector('profile-card-action', 'follow')).doesNotExist();
  });

  test('shows a follow button if the session user is not following the context user', async function(assert) {
    // a "current user" context is necessary for understanding how button should be shown
    this.server.create('user', 'withToken', 'withSession');

    let user = this.server.create('user');
    let store = this.owner.lookup('service:store');
    let userFromStore = await store.loadRecord('user', user.id);

    this.set('user', userFromStore);

    await render(hbs`<ProfileCard @user={{this.user}} @includeActions={{true}}/>`);

    assert.dom(generateTestSelector('profile-card-action', 'follow')).hasText(`${t('follow')} ${t('atHandle', { handle: user.username })}`);
  });

  test('shows an unfollow button if the sessions current user is following the context user', async function(assert) {
    // a "current user" context is necessary for understanding how button should be shown
    this.server.create('user', 'withToken', 'withSession');

    let user = this.server.create('user', 'followed');
    let store = this.owner.lookup('service:store');
    let userFromStore = await store.loadRecord('user', user.id);

    this.set('user', userFromStore);

    await render(hbs`<ProfileCard @user={{this.user}} @includeActions={{true}}/>`);

    assert.dom(generateTestSelector('profile-card-action', 'unfollow')).hasText(`${t('unfollow')} ${t('atHandle', { handle: user.username })}`);
  });

  test('it accepts splattributes', async function(assert) {
    await render(hbs`<ProfileCard data-test-selector="splatter"/>`);

    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('it is accessible', async function(assert) {
    let user = this.server.create('user');
    let store = this.owner.lookup('service:store');
    let userFromStore = await store.loadRecord('user', user.id);

    this.set('user', userFromStore);

    await render(hbs`<ProfileCard @user={{this.user}}/>`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

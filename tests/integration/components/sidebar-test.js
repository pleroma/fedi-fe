import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  render,
  click,
  settled,
  find,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import { authenticateSession, currentSession } from 'ember-simple-auth/test-support';
import { setBreakpoint } from 'ember-responsive/test-support';

module('Integration | Component | sidebar', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
    setBreakpoint('large');
  });

  test('it has all the correct parts', async function(assert) {
    let user = this.server.create('user');
    let tokenRecord = this.server.create('token', {
      user,
    });

    await render(hbs`<Sidebar/>`);

    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAria('controls', 'menu');
    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAria('expanded', 'false');
    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAria('haspopup', 'true');
    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAria('label', t('closeMenu'));
    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAttribute('tabindex', '0');

    assert.equal(find(generateTestSelector('account-cta')).nodeName, 'SECTION', 'has the proper tagname');
    assert.dom(generateTestSelector('account-cta')).hasAttribute('tabindex', '-1');

    assert.dom(generateTestSelector('account-cta-create-account-button')).exists();
    assert.dom(generateTestSelector('account-cta-create-account-button')).hasText(t('createAccount'));

    assert.dom(generateTestSelector('account-cta-sign-in-button')).exists();
    assert.dom(generateTestSelector('account-cta-sign-in-button')).hasText(t('signIn'));

    assert.dom(generateTestSelector('sidebar-nav')).exists();
    assert.dom(generateTestSelector('sidebar-about')).exists();
    assert.dom(generateTestSelector('sidebar-instance-information')).exists();
    assert.dom(generateTestSelector('sidebar-sign-up')).exists();
    assert.dom(generateTestSelector('sidebar-sign-in')).exists();
    assert.dom(generateTestSelector('sidebar-profile')).doesNotExist();
    assert.dom(generateTestSelector('sidebar-sign-out')).doesNotExist();

    assert.dom(generateTestSelector('sidebar-underlay')).exists();

    await authenticateSession({
      'token_type': 'Bearer',
      'access_token': tokenRecord.token,
    });
    await settled();

    assert.ok(currentSession().isAuthenticated);

    assert.dom(generateTestSelector('sidebar-sign-out')).exists();
    assert.dom(generateTestSelector('sidebar-profile')).exists();
    assert.dom(generateTestSelector('sidebar-sign-up')).doesNotExist();
    assert.dom(generateTestSelector('sidebar-sign-in')).doesNotExist();
    assert.dom(generateTestSelector('account-cta')).doesNotExist();
    assert.dom(generateTestSelector('sidebar-underlay')).exists();
  });

  test('has correct attrs when opening/closing', async function(assert) {
    let offCanvasNavService = this.owner.lookup('service:off-canvas-nav');

    await render(hbs`<Sidebar />`);

    assert.dom(generateTestSelector('sidebar')).hasAttribute('id', 'menu');

    // Its closed
    assert.dom(generateTestSelector('sidebar')).hasAttribute('hidden');
    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAria('expanded', 'false');

    // Open it
    offCanvasNavService.open();

    await settled();

    assert.dom(generateTestSelector('sidebar')).doesNotHaveAttribute('hidden');
    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAria('expanded', 'true');

    // Close it
    await click(generateTestSelector('sidebar-menu-toggle'));

    await settled();

    assert.notOk(offCanvasNavService.isOpen);
    assert.ok(offCanvasNavService.isClosed);

    assert.dom(generateTestSelector('sidebar')).hasAttribute('hidden');
    assert.dom(generateTestSelector('sidebar-menu-toggle')).hasAria('expanded', 'false');
  });

  test('accepts splattributes', async function(assert) {
    await render(hbs`<Sidebar data-test-selector="splattribute"/>`);
    assert.dom(generateTestSelector('splattribute')).hasAttribute('id', 'menu');
  });

  skip('it is accessible', async function(assert) {
    await render(hbs`<Sidebar />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  test('renders a overlay button, that when clicked, will close the sidebar', async function(assert) {
    let offCanvasNavService = this.owner.lookup('service:off-canvas-nav');

    await render(hbs`<Sidebar />`);

    assert.dom(generateTestSelector('sidebar-underlay')).exists();

    offCanvasNavService.open();

    await click(generateTestSelector('sidebar-underlay'));

    assert.ok(offCanvasNavService.isClosed);
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  render,
  settled,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Component | load-record', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('it loads a record', async function(assert) {
    let user = this.server.create('user');

    this.set('userId', user.id);

    await render(hbs`<LoadRecord @modelName="user" @modelId="{{this.userId}}" as |isRunning account|>
      <div data-test-selector="user-id">{{account.id}}</div>
    </LoadRecord>`);
    await settled();

    assert.dom(generateTestSelector('user-id')).hasText(`${user.id}`);
  });
});

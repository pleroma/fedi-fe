import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  render,
  settled,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import faker from 'faker';
import { setupIntl, t } from 'ember-intl/test-support';

module('Integration | Component | toast-renderer', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('it accepts splattributes', async function(assert) {
    await render(hbs`<ToastRenderer data-test-selector="splattribute"/>`);

    assert.dom(generateTestSelector('splattribute')).exists();
  });

  test('accepts toasts', async function(assert) {
    let testToastCopy = faker.lorem.sentence();

    this.set('toastService', this.owner.lookup('service:toast'));

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    this.toastService.notify({
      message: testToastCopy,
    });

    await settled();

    assert.dom(generateTestSelector('toast-message')).hasText(testToastCopy);

    assert.dom(generateTestSelector('toast-type', 'success')).exists({ count: 1 });

    this.toastService.notify({
      message: testToastCopy,
    });

    this.toastService.notify({
      message: testToastCopy,
    });

    await settled();

    assert.dom(generateTestSelector('toast-type', 'success')).exists({ count: 3 });
  });

  test('allows users to dismiss toasts by clicking a button', async function(assert) {
    let testToastCopy = faker.lorem.sentence();

    this.set('toastService', this.owner.lookup('service:toast'));

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    this.toastService.notify({
      type: 'error',
      message: testToastCopy,
    });

    this.toastService.notify({
      type: 'error',
      message: testToastCopy,
    });

    await settled();

    assert.dom(generateTestSelector('toast-type', 'error')).exists({ count: 2 });

    await click(generateTestSelector('dismiss-toast-button'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'error')).exists({ count: 1 });

    await click(generateTestSelector('dismiss-toast-button'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'error')).doesNotExist();
  });

  test('toasts will render by type', async function(assert) {
    this.set('toasts', [
      {
        type: 'test-type',
        message: faker.lorem.sentence(),
      },
      {
        type: 'test-type-2',
        message: faker.lorem.sentence(),
      },
    ]);

    await render(hbs`<ToastRenderer @toasts={{this.toasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'test-type')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-type', 'test-type-2')).exists({ count: 1 });
  });

  test('can render a error toast type', async function(assert) {
    let message = faker.lorem.sentence();
    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'error',
      message,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'error')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(message);
    assert.dom(generateTestSelector('dismiss-toast-button')).hasAttribute('title', t('dismissToast'));
  });

  test('can render a newStatus custom type', async function(assert) {
    let user = this.server.create('user');

    let status = this.server.create('status', {
      account: user,
    });

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'newStatus',
      newStatus: status,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'new-status')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('yourStatusWasPosted'));
    assert.dom(generateTestSelector('new-status-toast-link')).hasText(t('view'));
    assert.dom(generateTestSelector('new-status-toast-link')).hasAttribute('href', `/notice/${status.id}`);
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
  });

  test('can render a userBlocked custom type', async function(assert) {
    let user = this.server.create('user');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'userBlocked',
      user,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-blocked')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('userBlocked', { username: user.username }));
    assert.dom(generateTestSelector('user-blocked-unblock-button')).hasText(t('unblock'));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
  });

  test('can render a userUnblocked custom type', async function(assert) {
    let user = this.server.create('user');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'userUnblocked',
      user,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-unblocked')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('userUnblocked', { username: user.username }));
    assert.dom(generateTestSelector('user-unblocked-block-button')).hasText(t('block'));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
  });

  test('can render a userMuted custom type', async function(assert) {
    let user = this.server.create('user');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'userMuted',
      user,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-muted')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('userMuted', { username: user.username }));
    assert.dom(generateTestSelector('user-muted-unmute-button')).hasText(t('unmute'));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
  });

  test('can render a userUnmuted custom type', async function(assert) {
    let user = this.server.create('user');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'userUnmuted',
      user,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-unmuted')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('userUnmuted', { username: user.username }));
    assert.dom(generateTestSelector('user-unmuted-mute-button')).hasText(t('mute'));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
  });

  test('can render a replyStatus custom type', async function(assert) {
    await this.owner.setupRouter();

    let originalStatus = this.server.create('status');
    let status = this.server.create('status', {
      inReplyToId: originalStatus.id,
    });

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'newReply',
      newStatus: status,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'new-reply')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('yourReplyWasPosted'));
    assert.dom(generateTestSelector('new-reply-toast-link')).hasAttribute('href', `/notice/${status.id}`);
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
  });

  test('can render a userFollowed custom type', async function(assert) {
    let user = this.server.create('user');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'userFollowed',
      user,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-followed')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('nowFollowing', { username: user.username }));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
    assert.dom(generateTestSelector('user-followed-unfollow-button')).hasText(t('unfollow'));
  });

  test('can render a userUnfollowed custom type', async function(assert) {
    let user = this.server.create('user');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'userUnfollowed',
      user,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-unfollowed')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('userUnfollowed', { username: user.username }));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
    assert.dom(generateTestSelector('user-unfollowed-follow-button')).hasText(t('follow'));
  });

  test('can render a conversationMuted custom type', async function(assert) {
    let status = this.server.create('status');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'conversationMuted',
      status,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'conversation-muted')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('conversationMuted'));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
    assert.dom(generateTestSelector('conversation-muted-unmute-button')).hasText(t('unmute'));
  });

  test('can render a conversationUnmuted custom type', async function(assert) {
    let status = this.server.create('status');

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'conversationUnmuted',
      status,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'conversation-unmuted')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('conversationUnmuted'));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
    assert.dom(generateTestSelector('conversation-unmuted-mute-button')).hasText(t('mute'));
  });

  test('can render a statusReposted custom type', async function(assert) {
    let user = this.server.create('user');

    let newStatus = this.server.create('status', {
      account: user,
    });

    this.set('toastService', this.owner.lookup('service:toast'));

    this.toastService.notify({
      type: 'statusReposted',
      newStatus,
    });

    await render(hbs`<ToastRenderer @toasts={{this.toastService.activeToasts}}/>`);

    await settled();

    assert.dom(generateTestSelector('toast-type', 'status-reposted')).exists({ count: 1 });
    assert.dom(generateTestSelector('toast-message')).hasText(t('statusReposted'));
    assert.dom(generateTestSelector('dismiss-toast-button')).doesNotExist();
    assert.dom(generateTestSelector('status-reposted-toast-link'))
      .hasAttribute('href', `/notice/${newStatus.id}`)
      .hasText(t('view'));
  });
});

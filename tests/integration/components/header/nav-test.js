import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import { authenticateSession } from 'ember-simple-auth/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import { set } from '@ember/object';

module('Integration | Component | header-nav', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    this.owner.setupRouter();
    await this.owner.lookup('service:instances').refreshTask.perform();
    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');
  });

  test('it renders a properly formatted header', async function(assert) {
    let instance = this.server.schema.instances.first();
    let user = this.server.create('user');
    let tokenRecord = this.server.create('token', {
      user,
    });

    await render(hbs`<Header::Nav />`);

    assert.dom(generateTestSelector('header-nav-mobile-home-link')).doesNotExist();
    assert.dom(generateTestSelector('header-nav-home-medium')).doesNotExist();

    assert.dom(generateTestSelector('header-nav-search')).hasAria('label', t('search'));
    assert.dom(generateTestSelector('header-nav-search')).hasAttribute('href', '/search');

    assert.dom(generateTestSelector('header-nav-public')).hasAria('label', instance.title);
    assert.dom(generateTestSelector('header-nav-public')).hasAttribute('href', '/feeds/public');

    assert.dom(generateTestSelector('header-nav-all')).hasAria('label', t('wholeKnownNetwork'));
    assert.dom(generateTestSelector('header-nav-all')).hasAttribute('href', '/feeds/all');

    assert.dom(generateTestSelector('header-nav-direct-messages-classic')).doesNotExist();
    assert.dom(generateTestSelector('header-nav-direct-messages-chat')).doesNotExist();
    assert.dom(generateTestSelector('header-nav-notifications')).doesNotExist();

    await authenticateSession({
      'token_type': 'Bearer',
      'access_token': tokenRecord.token,
    });

    await settled();

    assert.dom(generateTestSelector('header-nav-home-medium')).hasAria('label', t('myFeed'));
    assert.dom(generateTestSelector('header-nav-home-medium')).hasAttribute('href', '/feeds/home');

    assert.dom(generateTestSelector('header-nav-public')).hasAria('label', instance.title, 'gets its aria-label from the current instance title');
    assert.dom(generateTestSelector('header-nav-public')).hasAttribute('href', '/feeds/public');

    assert.dom(generateTestSelector('header-nav-direct-messages-classic')).hasAria('label', t('directMessagesClassic'));
    assert.dom(generateTestSelector('header-nav-direct-messages-classic')).hasAttribute('href', '/feeds/direct');

    assert.dom(generateTestSelector('header-nav-direct-messages-chat')).hasAria('label', t('directMessagesChats'));
    assert.dom(generateTestSelector('header-nav-direct-messages-chat')).hasAttribute('href', '/messages');

    assert.dom(generateTestSelector('header-nav-notifications')).hasAria('label', t('notifications'));
    assert.dom(generateTestSelector('header-nav-notifications')).hasAttribute('href', '/notifications');

    assert.dom(generateTestSelector('header-nav-search')).hasAria('label', t('search'));
    assert.dom(generateTestSelector('header-nav-search')).hasAttribute('href', '/search');
  });

  test('accepts splattributes', async function(assert) {
    await render(hbs`<Header data-test-selector="splatter"/>`);

    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('feed-links with unread counts get a --badge class', async function(assert) {
    let unreadCountsService = this.owner.lookup('service:unreadCounts');

    this.server.create('user', 'withToken', 'withSession');

    await render(hbs`<Header />`);

    assert.dom(generateTestSelector('header-nav-notifications')).doesNotHaveClass('header-nav__item-link--badge');
    assert.dom(generateTestSelector('header-nav-direct-messages-chat')).doesNotHaveClass('header-nav__item-link--badge');

    set(unreadCountsService, 'unreadNotificationsCount', 5);

    await settled();

    assert.dom(generateTestSelector('header-nav-notifications')).hasClass('header-nav__item-link--badge');
    assert.dom(generateTestSelector('header-nav-direct-messages-chat')).doesNotHaveClass('header-nav__item-link--badge');

    set(unreadCountsService, 'unreadNotificationsCount', 0);
    set(unreadCountsService, 'unreadChatCount', 5);
    set(unreadCountsService, 'unreadConversationCount', 5);

    await settled();

    assert.dom(generateTestSelector('header-nav-notifications')).doesNotHaveClass('header-nav__item-link--badge');
    assert.dom(generateTestSelector('header-nav-direct-messages-chat')).hasClass('header-nav__item-link--badge');
  });
});

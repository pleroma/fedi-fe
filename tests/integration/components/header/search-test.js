import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  render,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import faker from 'faker';

module('Integration | Component | header-search', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    await this.owner.lookup('service:instances').refreshTask.perform();
  });

  test('it renders a properly formatted search bar', async function(assert) {
    let instance = this.server.schema.instances.first();
    await render(hbs`<Header::Search />`);

    assert.dom(generateTestSelector('header-search-input'))
      .hasAttribute('aria-label',  t('searchPlaceholder', { instance: instance.title }), 'has the correct placeholder')
      .hasAttribute('autocomplete', 'off')
      .hasAttribute('data-ebd-id')
      .hasAttribute('id', 'header-search-input')
      .hasAttribute('placeholder', t('searchPlaceholder', { instance: instance.title }), 'has the correct placeholder')
      .hasAttribute('type', 'text')
      .hasNoValue();

    assert.dom(generateTestSelector('header-search'))
      .doesNotHaveClass('header__search--hidden');
  });

  test('accepts its value from the search service', async function(assert) {
    let query = faker.lorem.word();
    let searchService = this.owner.lookup('service:search');
    searchService.setQuery(query);

    await render(hbs`<Header::Search />`);

    assert.dom(generateTestSelector('header-search-input'))
      .hasValue(query);
  });

  test('accepts splattributes', async function(assert) {
    await render(hbs`<Header::Search data-test-selector="splatter"/>`);
    assert.dom(generateTestSelector('splatter')).exists();
  });

  test('it is accessible', async function(assert) {
    let instances = this.owner.lookup('service:instances');

    await instances.refreshTask.perform();

    await render(hbs`<Header::Search />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });

  skip('captures focus on / pressed', async function(assert) {
    await render(hbs`<Header::Search />`);

    assert.dom(generateTestSelector('header-search-input')).isNotFocused();

    await triggerKeyEvent(document, 'keydown', 93);
    await settled();

    assert.dom(generateTestSelector('header-search-input')).isFocused();
  });

  test('when entering more than 3 chars, shows the suggestions overlay', async function(assert) {
    await render(hbs`<Header::Search />`);

    assert.dom(generateTestSelector('search-suggestions-overlay', 'content'))
      .doesNotExist();

    await fillIn(generateTestSelector('header-search-input'), '##');
    await settled();

    assert.dom(generateTestSelector('search-suggestions-overlay', 'content'))
      .doesNotExist();

    await fillIn(generateTestSelector('header-search-input'), '###');
    await settled();

    assert.dom(generateTestSelector('search-suggestions-overlay', 'content'))
      .exists();

    await fillIn(generateTestSelector('header-search-input'), '##');
    await settled();

    assert.dom(generateTestSelector('search-suggestions-overlay', 'content'))
      .doesNotExist();
  });

  test('shows a clear query button, that when clicked, will clear the current query', async function(assert) {
    await render(hbs`<Header::Search />`);

    assert.dom(generateTestSelector('clear-query-button'))
      .doesNotExist();

    await fillIn(generateTestSelector('header-search-input'), '##');
    await settled();

    assert.dom(generateTestSelector('clear-query-button'))
      .exists();

    await click(generateTestSelector('clear-query-button'));
    await settled();

    assert.dom(generateTestSelector('clear-query-button'))
      .doesNotExist();

    assert.dom(generateTestSelector('header-search-input'))
      .hasNoValue();
  });
});

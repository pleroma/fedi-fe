import { module, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  render,
  settled,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupIntl } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { resolve } from 'rsvp';
import sinon from 'sinon';

module('Integration | Component | settings/push-notifications', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    await this.owner.lookup('service:instances').refreshTask.perform();
  });

  hooks.afterEach(function() {
    navigator.serviceWorker = null;
  });

  skip('if push notifications are not supported by the browser, we show nothing', async function() {});

  skip('disables the settings form when there is no subscription', async function(assert) {
    await this.server.create('user', 'withToken', 'withSession');

    await render(hbs`<Settings::PushNotifications
      @subscription={{false}}
    />`);
    await settled();

    assert.dom(generateTestSelector('push-notifications-toggle'))
      .hasAttribute('id', 'push-notifications')
      .hasAttribute('type', 'checkbox')
      .isEnabled()
      .isNotChecked();

    assert.dom(generateTestSelector('push-notifications-follow'))
      .hasAttribute('id', 'push-notifications-follow')
      .hasAttribute('type', 'checkbox')
      .isDisabled()
      .isNotChecked();

    assert.dom(generateTestSelector('push-notifications-favorite'))
      .hasAttribute('id', 'push-notifications-favorite')
      .hasAttribute('type', 'checkbox')
      .isDisabled()
      .isNotChecked();

    assert.dom(generateTestSelector('push-notifications-repost'))
      .hasAttribute('id', 'push-notifications-repost')
      .hasAttribute('type', 'checkbox')
      .isDisabled()
      .isNotChecked();

    assert.dom(generateTestSelector('push-notifications-mention'))
      .hasAttribute('id', 'push-notifications-mention')
      .hasAttribute('type', 'checkbox')
      .isDisabled()
      .isNotChecked();

    assert.dom(generateTestSelector('push-notifications-poll'))
      .hasAttribute('id', 'push-notifications-poll')
      .hasAttribute('type', 'checkbox')
      .isDisabled()
      .isNotChecked();

    assert.dom(generateTestSelector('save-push-notification-settings'))
      .hasAttribute('type', 'submit')
      .isDisabled();
  });

  skip('enables the form when a subscription exists', async function(assert) {
    await this.server.create('user', 'withSubscription', 'withToken', 'withSession');

    // TODO: This can be better.
    Object.defineProperty(navigator, 'serviceWorker', {
      value: {
        register: () => {},
        ready: resolve({
          pushManager: {
            getSubscription() {
              return {};
            },
          },
        }),
      },
    });

    let pushNotifications = this.owner.lookup('service:push-notifications');

    await pushNotifications.existingSubscriptionTask.perform();

    let subscription = pushNotifications.existingSubscriptionTask.last.value.apiSubscription;

    this.set('subscription', subscription);

    await render(hbs`<Settings::PushNotifications
      @subscription={{this.subscription}}
    />`);
    await settled();

    assert.dom(generateTestSelector('push-notifications-toggle'))
      .hasAttribute('id', 'push-notifications')
      .hasAttribute('type', 'checkbox')
      .isEnabled()
      .isChecked();

    assert.dom(generateTestSelector('push-notifications-follow'))
      .hasAttribute('id', 'push-notifications-follow')
      .hasAttribute('type', 'checkbox')
      .isEnabled()
      .isChecked();

    assert.dom(generateTestSelector('push-notifications-favorite'))
      .hasAttribute('id', 'push-notifications-favorite')
      .hasAttribute('type', 'checkbox')
      .isEnabled()
      .isChecked();

    assert.dom(generateTestSelector('push-notifications-repost'))
      .hasAttribute('id', 'push-notifications-repost')
      .hasAttribute('type', 'checkbox')
      .isEnabled()
      .isChecked();

    assert.dom(generateTestSelector('push-notifications-mention'))
      .hasAttribute('id', 'push-notifications-mention')
      .hasAttribute('type', 'checkbox')
      .isEnabled()
      .isChecked();

    assert.dom(generateTestSelector('push-notifications-poll'))
      .hasAttribute('id', 'push-notifications-poll')
      .hasAttribute('type', 'checkbox')
      .isEnabled()
      .isChecked();

    assert.dom(generateTestSelector('save-push-notification-settings'))
      .hasAttribute('type', 'submit')
      .isDisabled();
  });

  skip('checking the toggle will attempt to subscribe you', async function(assert) {
    await this.server.create('user', 'withToken', 'withSession');

    let pushNotifications = this.owner.lookup('service:push-notifications');

    sinon.stub(pushNotifications.subscribe, 'perform');

    await render(hbs`<Settings::PushNotifications
      @subscription={{false}}
    />`);

    assert.ok(pushNotifications.subscribe.perform.notCalled);

    await click(generateTestSelector('push-notifications-toggle'));

    assert.ok(pushNotifications.subscribe.perform.calledOnce);
  });

  skip('unchecking the form will attempt to unsubscribe', async function(assert) {
    await this.server.create('user', 'withSubscription', 'withToken', 'withSession');

    // TODO: This can be better.
    Object.defineProperty(navigator, 'serviceWorker', {
      value: {
        register: () => {},
        ready: resolve({
          pushManager: {
            getSubscription() {
              return {};
            },
          },
        }),
      },
    });

    let pushNotifications = this.owner.lookup('service:push-notifications');

    await pushNotifications.existingSubscriptionTask.perform();

    let subscription = pushNotifications.existingSubscriptionTask.last.value.apiSubscription;

    this.set('subscription', subscription);

    sinon.stub(pushNotifications.unsubscribe, 'perform');

    await render(hbs`<Settings::PushNotifications
      @subscription={{this.subscription}}
    />`);
    await settled();

    assert.ok(pushNotifications.unsubscribe.perform.notCalled);

    await click(generateTestSelector('push-notifications-toggle'));

    assert.ok(pushNotifications.unsubscribe.perform.calledOnce);
  });

  skip('modifying the form will allow you to save new settings', async function(assert) {
    await this.server.create('user', 'withSubscription', 'withToken', 'withSession');

    // TODO: This can be better.
    Object.defineProperty(navigator, 'serviceWorker', {
      value: {
        register: () => {},
        ready: resolve({
          pushManager: {
            getSubscription() {
              return {};
            },
          },
        }),
      },
    });

    let pushNotifications = this.owner.lookup('service:push-notifications');

    await pushNotifications.existingSubscriptionTask.perform();

    let subscription = pushNotifications.existingSubscriptionTask.last.value.apiSubscription;

    this.set('subscription', subscription);

    sinon.stub(pushNotifications.updatePushSubscription, 'perform');

    await render(hbs`<Settings::PushNotifications
      @subscription={{this.subscription}}
    />`);
    await settled();

    await click(generateTestSelector('push-notifications-follow'));

    await click(generateTestSelector('push-notifications-mention'));

    assert.dom(generateTestSelector('save-push-notification-settings'))
      .isEnabled();

    assert.ok(pushNotifications.updatePushSubscription.notCalled);

    await click(generateTestSelector('save-push-notification-settings'));

    assert.ok(pushNotifications.updatePushSubscription.calledWith({
      follow: false,
      favourite: true,
      reblog: true,
      mention: false,
      poll: true,
    }));
  });
});

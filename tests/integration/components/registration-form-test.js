import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  find,
  render,
  resetOnerror,
  settled,
  setupOnerror,
  triggerEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import faker from 'faker';
import { Promise } from 'rsvp';

module('Integration | Component | registration-form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    this.owner.setupRouter();
    await this.owner.lookup('service:instances').refreshTask.perform();
  });

  test('it has the right parts', async function(assert) {
    await render(hbs`<AuthModal::RegistrationForm />`);

    assert.dom(generateTestSelector('registration-form')).exists();

    assert.dom(generateTestSelector('register-username-label'))
      .hasText(t('registerUsernameLabel'))
      .hasAttribute('for', 'register-username');

    assert.dom(generateTestSelector('username-input'))
      .doesNotHaveClass('text-input__input--error')
      .hasAttribute('id', 'register-username')
      .hasAttribute('name', 'register-username')
      .hasAttribute('placeholder', t('registerUsernamePlaceholder'))
      .hasAttribute('type', 'text')
      .isFocused()
      .isEnabled()
      .isRequired();

    assert.dom(generateTestSelector('username-tooltip'))
      .hasAttribute('data-position', 'bottom center')
      .hasClass('tooltip')
      .hasAttribute('role', 'tooltip')
      .hasAttribute('tabindex', '0');

    assert.dom(generateTestSelector('username-hint'))
      .hasText(t('registerUsernameHint'));

    assert.dom(generateTestSelector('display-name-label'))
      .hasAttribute('for', 'register-displayname')
      .hasText(t('registerDisplaynameLabel'));

    assert.dom(generateTestSelector('display-name-input'))
      .doesNotHaveClass('text-input__input--error')
      .hasAttribute('id', 'register-displayname')
      .hasAttribute('name', 'register-displayname')
      .hasAttribute('placeholder', t('registerDisplaynamePlaceholder'))
      .hasAttribute('type', 'text')
      .isEnabled()
      .isRequired();

    assert.dom(generateTestSelector('display-name-tooltip'))
      .hasAttribute('data-position', 'bottom center')
      .hasClass('tooltip')
      .hasAttribute('role', 'tooltip')
      .hasAttribute('tabindex', '0');

    assert.dom(generateTestSelector('email-input'))
      .doesNotHaveClass('text-input__input--error')
      .hasAttribute('autocomplete', 'email')
      .hasAttribute('id', 'register-email')
      .hasAttribute('name', 'register-email')
      .hasAttribute('placeholder', t('emailAddress'))
      .hasAttribute('type', 'email')
      .isEnabled()
      .isRequired();

    assert.dom(generateTestSelector('email-label'))
      .hasAttribute('for', 'register-email')
      .hasText(t('emailAddress'));

    assert.dom(generateTestSelector('password-input'))
      .doesNotHaveClass('text-input__input--error')
      .hasAttribute('autocomplete', 'password')
      .hasAttribute('id', 'register-password')
      .hasAttribute('name', 'register-password')
      .hasAttribute('type', 'password')
      .isEnabled()
      .isRequired();

    assert.dom(generateTestSelector('password-label'))
      .hasAttribute('for', 'register-password')
      .hasText(t('password'));

    assert.dom(generateTestSelector('password-confirmation-input'))
      .doesNotHaveClass('text-input__input--error')
      .hasAttribute('autocomplete', 'password')
      .hasAttribute('id', 'register-password-confirmation')
      .hasAttribute('name', 'register-password-confirmation')
      .hasAttribute('placeholder', t('confirmPassword'))
      .hasAttribute('type', 'password')
      .isEnabled()
      .isRequired();

    assert.dom(generateTestSelector('password-confirmation-label'))
      .hasAttribute('for', 'register-password-confirmation')
      .hasText(t('confirmPassword'));

    assert.dom(generateTestSelector('captcha-image'))
      .hasAttribute('alt', t('captcha'))
      .hasAttribute('src');

    assert.dom(generateTestSelector('registration-captcha-input'))
      .hasAttribute('id', 'registration-captcha')
      .hasAttribute('name', 'registration-captcha')
      .hasAttribute('placeholder', t('captcha'))
      .hasAttribute('type', 'text')
      .isEnabled()
      .isRequired();

    assert.dom(generateTestSelector('captcha-label'))
      .hasAttribute('for', 'registration-captcha')
      .hasText(t('captcha'));

    assert.dom(generateTestSelector('captcha-hint'))
      .hasText(t('registerCaptcha'));

    assert.dom(generateTestSelector('register-button'))
      .hasAttribute('type', 'submit')
      .isEnabled()
      .hasText(t('register'));

    assert.dom(generateTestSelector('sign-in-error')).doesNotExist();
  });

  test('when registation is running', async function(assert) {
    this.set('setIsRunning', () => new Promise(() => {}));

    await render(hbs`<AuthModal::RegistrationForm
      @blockExit={{noop}}
      @allowExit={{noop}}
      @onRegistered={{noop}}
      @registerUser={{this.setIsRunning}}
    />`);

    assert.dom(generateTestSelector('register-button')).isEnabled();
    assert.dom(generateTestSelector('username-input')).isEnabled();
    assert.dom(generateTestSelector('email-input')).isEnabled();
    assert.dom(generateTestSelector('password-input')).isEnabled();
    assert.dom(generateTestSelector('password-confirmation-input')).isEnabled();
    assert.dom(generateTestSelector('display-name-input')).isEnabled();
    assert.dom(generateTestSelector('registration-captcha-input')).isEnabled();

    let password = faker.internet.password();
    await fillIn(generateTestSelector('username-input'), faker.lorem.word());
    await fillIn(generateTestSelector('email-input'), faker.internet.email());
    await fillIn(generateTestSelector('password-input'), password);
    await fillIn(generateTestSelector('password-confirmation-input'), password);
    await fillIn(generateTestSelector('display-name-input'), faker.internet.userName());
    await fillIn(generateTestSelector('registration-captcha-input'), faker.internet.userName());

    await click(generateTestSelector('register-button'));
    await settled();

    assert.dom(generateTestSelector('register-button')).isDisabled();
    assert.dom(generateTestSelector('username-input')).isDisabled();
    assert.dom(generateTestSelector('email-input')).isDisabled();
    assert.dom(generateTestSelector('password-input')).isDisabled();
    assert.dom(generateTestSelector('password-confirmation-input')).isDisabled();
    assert.dom(generateTestSelector('display-name-input')).isDisabled();
    assert.dom(generateTestSelector('registration-captcha-input')).isDisabled();

    assert.dom(generateTestSelector('register-button')).hasText(t('registering'));
  });

  module('when the changeset is invalid', function() {
    test('required fields', async function(assert) {
      this.set('setIsRunning', () => new Promise(() => {}));

      await render(hbs`<AuthModal::RegistrationForm
        @blockExit={{noop}}
        @allowExit={{noop}}
        @onRegistered={{noop}}
        @registerUser={{this.setIsRunning}}
      />`);

      await click(generateTestSelector('register-button'));
      await triggerEvent(generateTestSelector('registration-form'), 'submit');
      await settled();

      assert.dom(generateTestSelector('username-input'))
        .isEnabled()
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('email-input'))
        .isEnabled()
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('password-input'))
        .isEnabled()
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('password-confirmation-input'))
        .isEnabled()
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('display-name-input'))
        .isEnabled()
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('registration-error'))
        .containsText(t('youMustConfirmYourPassword'))
        .containsText(t('youMustEnterDisplayName'))
        .containsText(t('youMustEnterEmail'))
        .containsText(t('youMustEnterPassword'))
        .containsText(t('youMustEnterUsername'))
        .doesNotContainText(t('yourEmailAddressNotValid'));
    });

    test('shows an error when the password and confirm password do not match', async function(assert) {
      await render(hbs`<AuthModal::RegistrationForm
        @blockExit={{noop}}
        @allowExit={{noop}}
        @onRegistered={{noop}}
        @registerUser={{noop}}
      />`);

      await fillIn(generateTestSelector('password-input'), faker.random.word());
      await fillIn(generateTestSelector('password-confirmation-input'), faker.random.word());

      await click(generateTestSelector('register-button'));
      await triggerEvent(generateTestSelector('registration-form'), 'submit');
      await settled();

      assert.dom(generateTestSelector('password-confirmation-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('registration-error'))
        .containsText(t('yourConfirmPasswordDoesNotMatch'))
        .doesNotContainText(t('youMustConfirmYourPassword'));
    });

    test('email input will validate that the email format is correct', async function(assert) {
      await render(hbs`<AuthModal::RegistrationForm
        @blockExit={{noop}}
        @allowExit={{noop}}
        @onRegistered={{noop}}
        @registerUser={{noop}}
      />`);

      let password = faker.lorem.word();
      await fillIn(generateTestSelector('email-input'), faker.lorem.word());
      await fillIn(generateTestSelector('username-input'), faker.lorem.word());
      await fillIn(generateTestSelector('password-input'), password);
      await fillIn(generateTestSelector('password-confirmation-input'), password);
      await fillIn(generateTestSelector('display-name-input'), faker.internet.userName());
      await fillIn(generateTestSelector('registration-captcha-input'), faker.internet.userName());

      await click(generateTestSelector('register-button'));
      await triggerEvent(generateTestSelector('registration-form'), 'submit');
      await settled();

      assert.dom(generateTestSelector('email-input')).hasClass('text-input__input--error');

      assert.dom(generateTestSelector('registration-error'))
        .containsText(t('invalidEmail'))
        .doesNotContainText(t('youMustEnterEmail'));
    });

    test('ensure that you cannot submit a username or display name that is just empty spaces', async function(assert) {
      await render(hbs`<AuthModal::RegistrationForm
        @blockExit={{noop}}
        @allowExit={{noop}}
        @onRegistered={{noop}}
        @registerUser={{noop}}
      />`);

      let password = faker.lorem.word();

      await fillIn(generateTestSelector('username-input'), '   ');
      await fillIn(generateTestSelector('display-name-input'), '   ');

      await fillIn(generateTestSelector('email-input'), faker.lorem.word());
      await fillIn(generateTestSelector('password-input'), password);
      await fillIn(generateTestSelector('password-confirmation-input'), password);
      await fillIn(generateTestSelector('registration-captcha-input'), faker.internet.userName());

      await click(generateTestSelector('register-button'));
      await triggerEvent(generateTestSelector('registration-form'), 'submit');
      await settled();

      assert.dom(generateTestSelector('username-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('display-name-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('registration-error'))
        .containsText(t('youMustEnterUsername'))
        .containsText(t('youMustEnterDisplayName'));
    });

    test('ensure that you cannot submit a username with non url safe chars', async function(assert) {
      await render(hbs`<AuthModal::RegistrationForm
        @blockExit={{noop}}
        @allowExit={{noop}}
        @onRegistered={{noop}}
        @registerUser={{noop}}
      />`);

      let password = faker.lorem.word();

      await fillIn(generateTestSelector('username-input'), 'lkj (kj)');

      await fillIn(generateTestSelector('display-name-input'), faker.lorem.word());
      await fillIn(generateTestSelector('email-input'), faker.internet.email());
      await fillIn(generateTestSelector('password-input'), password);
      await fillIn(generateTestSelector('password-confirmation-input'), password);
      await fillIn(generateTestSelector('registration-captcha-input'), faker.lorem.word());

      await click(generateTestSelector('register-button'));
      await triggerEvent(generateTestSelector('registration-form'), 'submit');
      await settled();

      assert.dom(generateTestSelector('username-input'))
        .hasClass('text-input__input--error');

      assert.dom(generateTestSelector('registration-error'))
        .containsText(t('yourUsernameContainsInvalidCharacters'));
    });
  });

  test('when there are server errors', async function(assert) {
    let password = faker.internet.password();

    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.set('throw', () => {
      throw new Error(JSON.stringify({
        errors: {
          username: ['is taken'],
        },
      }));
    });

    await render(hbs`
      <AuthModal::RegistrationForm
        @blockExit={{noop}}
        @allowExit={{noop}}
        @onRegistered={{noop}}
        @registerUser={{this.throw}}
      />
    `);

    let previousCaptchaUrl = find(generateTestSelector('captcha-image')).getAttribute('src');

    await fillIn(generateTestSelector('username-input'), faker.lorem.word());
    await fillIn(generateTestSelector('email-input'), faker.internet.email());
    await fillIn(generateTestSelector('password-input'), password);
    await fillIn(generateTestSelector('password-confirmation-input'), password);
    await fillIn(generateTestSelector('display-name-input'), faker.internet.userName());
    await fillIn(generateTestSelector('registration-captcha-input'), faker.internet.userName());

    await click(generateTestSelector('register-button'));
    await settled();

    assert.dom(generateTestSelector('username-input')).isEnabled();
    assert.dom(generateTestSelector('email-input')).isEnabled();
    assert.dom(generateTestSelector('password-input')).isEnabled();
    assert.dom(generateTestSelector('password-confirmation-input')).isEnabled();
    assert.dom(generateTestSelector('display-name-input')).isEnabled();
    assert.dom(generateTestSelector('registration-captcha-input')).isEnabled();

    assert.dom(generateTestSelector('registration-error')).hasText('Username is taken');

    let newCaptchaUrl = find(generateTestSelector('captcha-image')).getAttribute('src');

    assert.dom(generateTestSelector('registration-captcha-input')).hasNoText();

    assert.notEqual(previousCaptchaUrl, newCaptchaUrl);

    resetOnerror();
  });

  test('if the server returns validation errors for nickname we alias it to displayName errors', async function(assert) {
    let password = faker.internet.password();

    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.set('throw', () => {
      throw new Error(JSON.stringify({
        errors: {
          nickname: ['Nickname is invalid'],
        },
      }));
    });

    await render(hbs`
      <AuthModal::RegistrationForm
        @blockExit={{noop}}
        @allowExit={{noop}}
        @onRegistered={{noop}}
        @registerUser={{this.throw}}
      />
    `);

    await fillIn(generateTestSelector('username-input'), faker.lorem.word());
    await fillIn(generateTestSelector('email-input'), faker.internet.email());
    await fillIn(generateTestSelector('password-input'), password);
    await fillIn(generateTestSelector('password-confirmation-input'), password);
    await fillIn(generateTestSelector('display-name-input'), faker.internet.userName());
    await fillIn(generateTestSelector('registration-captcha-input'), faker.internet.userName());

    await click(generateTestSelector('register-button'));
    await settled();

    assert.dom(generateTestSelector('display-name-input'))
      .hasClass('text-input__input--error');

    assert.dom(generateTestSelector('registration-error'))
      .hasText(`${t('displayName')} is invalid`);

    resetOnerror();
  });

  test('clicking on the image, will render a new captcha', async function(assert) {
    await render(hbs`<AuthModal::RegistrationForm />`);
    await settled();

    let firstImage = find(generateTestSelector('captcha-image')).getAttribute('src');

    await click(generateTestSelector('captcha-image'));
    await settled();

    let secondImage = find(generateTestSelector('captcha-image')).getAttribute('src');

    assert.notEqual(firstImage, secondImage);
  });
});

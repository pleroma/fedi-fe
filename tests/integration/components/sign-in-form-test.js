import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  fillIn,
  render,
  resetOnerror,
  settled,
  setupOnerror,
  triggerEvent,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';

module('Integration | Component | sign-in-form', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('has a complete template', async function(assert) {
    await render(hbs`<AuthModal::SignInForm />`);

    assert.dom(generateTestSelector('username-label')).exists();
    assert.dom(generateTestSelector('username-label')).hasAttribute('for', 'username');
    assert.dom(generateTestSelector('username-label')).hasText(t('usernameOrEmail'));

    assert.dom(generateTestSelector('username-input')).exists();
    assert.dom(generateTestSelector('username-input')).hasAttribute('placeholder', t('usernameOrEmail'));
    assert.dom(generateTestSelector('username-input')).hasAttribute('autocomplete', 'email');
    assert.dom(generateTestSelector('username-input')).hasAttribute('id', 'username');
    assert.dom(generateTestSelector('username-input')).isRequired();
    assert.dom(generateTestSelector('username-input')).isFocused('should try and autofocus after render');
    assert.dom(generateTestSelector('username-input')).doesNotHaveAttribute('aria-invalid');

    assert.dom(generateTestSelector('password-label')).exists();
    assert.dom(generateTestSelector('password-label')).hasAttribute('for', 'password');
    assert.dom(generateTestSelector('password-label')).hasText(t('password'));

    assert.dom(generateTestSelector('password-input')).exists();
    assert.dom(generateTestSelector('password-input')).hasAttribute('placeholder', t('passwordInputPlaceholder'));
    assert.dom(generateTestSelector('password-input')).hasAttribute('autocomplete', 'current-password');
    assert.dom(generateTestSelector('password-input')).hasAttribute('id', 'password');
    assert.dom(generateTestSelector('password-input')).isRequired();
    assert.dom(generateTestSelector('password-input')).doesNotHaveAttribute('aria-invalid');

    assert.dom(generateTestSelector('forgot-password-link')).hasText(t('forgotPassword'));

    assert.dom(generateTestSelector('register-button')).hasText(t('signUp'));

    assert.dom(generateTestSelector('sign-in-button')).exists();
  });

  test('while signing in, all inputs and buttons are disabled', async function(assert) {
    this.authenticateTask = {
      perform() {},
      isRunning: false,
    };

    await render(hbs`<AuthModal::SignInForm @authenticateTask={{this.authenticateTask}}/>`);

    assert.dom(generateTestSelector('sign-in-button')).isEnabled();
    assert.dom(generateTestSelector('username-input')).isEnabled();
    assert.dom(generateTestSelector('password-input')).isEnabled();

    this.set('authenticateTask.isRunning', true);

    assert.dom(generateTestSelector('sign-in-button')).isDisabled();
    assert.dom(generateTestSelector('username-input')).isDisabled();
    assert.dom(generateTestSelector('password-input')).isDisabled();
  });

  test('incomplete form will show helpful errors from changeset', async function(assert) {
    await render(hbs`<AuthModal::SignInForm />`);

    await triggerEvent(generateTestSelector('sign-in-form'), 'submit');

    await settled();

    assert.dom(generateTestSelector('sign-in-error')).hasText(`${t('youMustEnterUsernameOrEmail')} ${t('youMustEnterPassword')}`);

    await fillIn(generateTestSelector('username-input'), 'badusername');
    await fillIn(generateTestSelector('password-input'), 'badpassword');

    await settled();

    assert.dom(generateTestSelector('sign-in-error')).doesNotExist();
  });

  test('inputs will get aria-invalid attributes when corresponding values are invalid', async function(assert) {
    await render(hbs`<AuthModal::SignInForm />`);

    await triggerEvent(generateTestSelector('sign-in-form'), 'submit');

    await settled();

    assert.dom(generateTestSelector('username-input'))
      .hasAria('invalid', '');
    assert.dom(generateTestSelector('password-input'))
      .hasAria('invalid', '');
  });

  test('incomplete form will disable the submit button once first clicked, and not before', async function(assert) {
    await render(hbs`<AuthModal::SignInForm />`);

    assert.dom(generateTestSelector('sign-in-button')).isEnabled();

    await fillIn(generateTestSelector('username-input'), 'badusername');

    assert.dom(generateTestSelector('sign-in-button')).isEnabled();

    await triggerEvent(generateTestSelector('sign-in-form'), 'submit');

    await settled();

    assert.dom(generateTestSelector('sign-in-button')).isDisabled();
  });

  test('server errors will be shown as errors', async function(assert) {
    let user = this.server.create('user');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    await render(hbs`<AuthModal::SignInForm />`);

    await fillIn(generateTestSelector('username-input'), user.username);
    await fillIn(generateTestSelector('password-input'), 'badpassword');

    await click(generateTestSelector('sign-in-button'));
    await settled();

    assert.dom(generateTestSelector('sign-in-error')).hasText('Invalid credentials');

    resetOnerror();
  });

  test('server errors will clear when modifying the changeset', async function(assert) {
    let user = this.server.create('user');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    await render(hbs`<AuthModal::SignInForm />`);

    await fillIn(generateTestSelector('username-input'), user.username);
    await fillIn(generateTestSelector('password-input'), 'badpassword');

    await click(generateTestSelector('sign-in-button'));
    await settled();

    assert.dom(generateTestSelector('sign-in-error')).exists();

    await fillIn(generateTestSelector('username-input'), 'badusername');

    assert.dom(generateTestSelector('sign-in-error')).doesNotExist();

    resetOnerror();
  });

  test('server errors will NOT disable the submit button, only invalid changesets', async function(assert) {
    let user = this.server.create('user');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    await render(hbs`<AuthModal::SignInForm />`);

    await fillIn(generateTestSelector('username-input'), user.username);
    await fillIn(generateTestSelector('password-input'), 'badpassword');

    await click(generateTestSelector('sign-in-button'));
    await settled();

    assert.dom(generateTestSelector('sign-in-button')).isEnabled();

    resetOnerror();
  });

  skip('it is accessible', async function(assert) {
    await render(hbs`<AuthModal::SignInForm />`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  find,
  render,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import sinon from 'sinon';
import Helper from '@ember/component/helper';

module('Integration | Component | attachment-gallery-modal/gallery', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(async function() {
    this.server.create('instance');
    this.owner.setupRouter();
    await this.owner.lookup('service:instances').refreshTask.perform();

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it has all the right parts', async function(assert) {
    let user = this.server.create('user');
    let attachments = this.server.createList('attachment', 5);
    let status = this.server.create('status', {
      account: user,
      mediaAttachments: attachments,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    let centerAttachmentId = attachments[2].id;

    this.set('mediaAttachments', statusFromStore.mediaAttachments);
    this.set('centerAttachmentId', centerAttachmentId);
    this.set('onCenterChanged', sinon.fake());
    this.set('subject', statusFromStore);
    this.set('subjectType', 'status');

    await render(hbs`<AttachmentGalleryModal::Gallery
      @subject={{this.subject}}
      @subjectType={{this.subjectType}}
      @attachments={{this.mediaAttachments}}
      @centerAttachmentId={{this.centerAttachmentId}}
      @onCenterChanged={{this.onCenterChanged}}
    />`);

    assert.dom(generateTestSelector('attachment-gallery')).exists();
    assert.dom(generateTestSelector('attachment-gallery-slide')).exists({ count: 5 + 2 }); // For the occlusion radius
    let parentEl = find(generateTestSelector('gallery-item', centerAttachmentId)).parentElement.parentElement.parentElement.parentElement;
    assert.dom(parentEl).hasClass('status-gallery__slide--active');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import hbs from 'htmlbars-inline-precompile';
import {
  click,
  render,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import Helper from '@ember/component/helper';

module('Integration | Component | attachment-gallery-modal', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();

    this.server.create('instance');

    this.routeActions = { };

    this.owner.register('helper:route-action', Helper.helper(([action, ...args]) => {
      return () => { this.routeActions[action] && this.routeActions[action](...args); };
    }));
  });

  test('it has all the right bits', async function(assert) {
    let user = this.server.create('user');
    let attachments = this.server.createList('attachment', 5);
    let status = this.server.create('status', {
      account: user,
      mediaAttachments: attachments,
    });

    let centerAttachmentId = attachments[2].id;

    this.set('centerAttachmentId', centerAttachmentId);
    this.set('subjectId', status.id);

    await render(hbs`
      <AttachmentGalleryModal
        @subjectId={{this.subjectId}}
        @centerAttachmentId={{this.centerAttachmentId}}
      />
    `);

    assert.dom(generateTestSelector('attachment-gallery-modal')).exists();
    assert.dom(generateTestSelector('attachment-gallery-close-modal-button')).exists();
    assert.dom(generateTestSelector('attachment-gallery')).exists();
    assert.dom(generateTestSelector('lock-body-scroll')).exists();

    assert.dom(generateTestSelector('attachment-gallery-button-previous')).exists();
    assert.dom(generateTestSelector('attachment-gallery-button-next')).exists();
    assert.dom(generateTestSelector('status-actions-bar')).exists();
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`3 /  ${attachments.length}`);
  });

  test('it can be closed by button click', async function(assert) {
    let user = this.server.create('user');
    let attachments = this.server.createList('attachment', 5);
    let status = this.server.create('status', {
      account: user,
      mediaAttachments: attachments,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    let centerAttachmentId = attachments[2].id;

    this.set('centerAttachmentId', centerAttachmentId);
    this.set('status', statusFromStore);

    await render(hbs`
      <AttachmentGalleryModal
        @subjectId={{this.status.id}}
        @centerAttachmentId={{this.centerAttachmentId}}
      />
    `);

    assert.dom(generateTestSelector('attachment-gallery-modal')).exists();

    await click(generateTestSelector('attachment-gallery-close-modal-button'));

    assert.dom(generateTestSelector('attachment-gallery-modal')).doesNotExist();
  });

  test('it can be closed by escape key press', async function(assert) {
    let user = this.server.create('user');
    let attachments = this.server.createList('attachment', 5);
    let status = this.server.create('status', {
      account: user,
      mediaAttachments: attachments,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    let centerAttachmentId = attachments[2].id;

    this.set('centerAttachmentId', centerAttachmentId);
    this.set('status', statusFromStore);

    await render(hbs`
      <AttachmentGalleryModal
        @subjectId={{this.status.id}}
        @centerAttachmentId={{this.centerAttachmentId}}
      />
    `);

    assert.dom(generateTestSelector('attachment-gallery-modal')).exists();

    await triggerKeyEvent(document.body, 'keydown', 27);
    await settled();

    assert.dom(generateTestSelector('attachment-gallery-modal')).doesNotExist();
  });

  test('does not show the pagination / next-prev buttons when there is only 1 item', async function(assert) {
    let user = this.server.create('user');
    let attachments = this.server.createList('attachment', 1);
    let status = this.server.create('status', {
      account: user,
      mediaAttachments: attachments,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    let centerAttachmentId = attachments[0].id;

    this.set('mediaAttachments', statusFromStore.mediaAttachments);
    this.set('centerAttachmentId', centerAttachmentId);
    this.set('status', statusFromStore);

    await render(hbs`<AttachmentGalleryModal
      @subjectId={{this.status.id}}
      @centerAttachmentId={{this.centerAttachmentId}}
    />`);

    assert.dom(generateTestSelector('attachment-gallery-button-previous')).doesNotExist();
    assert.dom(generateTestSelector('attachment-gallery-button-next')).doesNotExist();
    assert.dom(generateTestSelector('attachment-gallery-pagination')).doesNotExist();
  });

  test('when open, prevents scroll on the body', async function(assert) {
    let user = this.server.create('user');
    let attachments = this.server.createList('attachment', 5);
    let status = this.server.create('status', {
      account: user,
      mediaAttachments: attachments,
    });

    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    let centerAttachmentId = attachments[2].id;

    this.set('centerAttachmentId', centerAttachmentId);
    this.set('status', statusFromStore);

    assert.equal(window.getComputedStyle(document.body, null).getPropertyValue('overflow'), 'visible');

    await render(hbs`
      <AttachmentGalleryModal
        @subjectId={{this.status.id}}
        @centerAttachmentId={{this.centerAttachmentId}}
      />
    `);
    await settled();

    assert.equal(window.getComputedStyle(document.body, null).getPropertyValue('overflow'), 'hidden');
  });

  test('accepts a subject type to override the model type the modal will fetch to show itself', async function(assert) {
    await this.server.create('user', 'withToken', 'withSession');

    let otherUser = this.server.create('user');

    let attachment = this.server.create('attachment');
    let chat = this.server.create('chat');
    let chatMessage = this.server.create('chat-message', { chat, account: otherUser, attachment });

    let chatActions = this.owner.lookup('service:chat-actions');
    let chatMessageFromStore = await chatActions.getChatMessage.perform(chat.id, chatMessage.id);

    this.set('centerAttachmentId', chatMessageFromStore.attachment);
    this.set('subject', chatMessageFromStore);
    this.set('subjectType', 'chat-message');

    await render(hbs`
      <AttachmentGalleryModal
        @subjectId={{this.subject.id}}
        @subjectType={{this.subjectType}}
        @centerAttachmentId={{this.centerAttachmentId}}
      />
    `);
    await settled();

    assert.dom(generateTestSelector('attachment-gallery-modal')).exists();
    assert.dom(generateTestSelector('attachment-gallery')).exists();
    assert.dom(`${generateTestSelector('attachment-gallery-slide')} img`)
      .hasAttribute('src', attachment.url);
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | forgot-password-success', function(hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en');

  test('has the proper bits', async function(assert) {
    await render(hbs`<AuthModal::ForgotPasswordSuccess />`);

    assert.dom(generateTestSelector('forgot-password-success')).exists();

    assert.dom(`${generateTestSelector('forgot-password-success')} ${generateTestSelector('close-modal-link')}`).exists();
  });
});

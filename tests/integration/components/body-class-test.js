import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import faker from 'faker';

module('Integration | Component | body-class', function(hooks) {
  setupRenderingTest(hooks);

  test('it adds a class to the body when rendered', async function(assert) {
    let classList = faker.lorem.word();

    this.set('shouldRender', false);
    this.set('classList', classList);

    await render(hbs`
      {{#if this.shouldRender}}
        <BodyClass @class={{this.classList}}/>
      {{/if}}
    `);

    assert.dom(document.body).doesNotHaveClass(classList);

    this.set('shouldRender', true);

    assert.dom(document.body).hasClass(classList);

    this.set('shouldRender', false);

    assert.dom(document.body).doesNotHaveClass(classList);
  });
});

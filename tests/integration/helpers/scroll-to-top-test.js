import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  render,
  click,
  find,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | scroll-to-top', function(hooks) {
  setupRenderingTest(hooks);

  test('it returns a function, that when called, scrolls to the top o\' the page', async function(assert) {
    await render(hbs`
      <div style="height: 500vh;">Spacer</div>
      <button {{on "click" (scroll-to-top)}} data-test-selector="scroll-to-top-button"></button>
      `);

    await find(generateTestSelector('scroll-to-top-button')).scrollIntoView();

    await click(generateTestSelector('scroll-to-top-button'));

    assert.equal(document.body.scrollTop, 0);
  });
});

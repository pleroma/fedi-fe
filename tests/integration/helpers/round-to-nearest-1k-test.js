import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | round-to-nearest-1k', function(hooks) {
  setupRenderingTest(hooks);

  test('properly rounds numbers', async function(assert) {
    this.set('inputValue', '500');

    await render(hbs`{{round-to-nearest-1k inputValue}}`);

    assert.equal(this.element.textContent.trim(), '500', 'under 1k return');

    this.set('inputValue', '999');

    assert.equal(this.element.textContent.trim(), '999', 'under 1k return');

    this.set('inputValue', '1000');

    assert.equal(this.element.textContent.trim(), '1k');

    this.set('inputValue', '1001');

    assert.equal(this.element.textContent.trim(), '1k');

    this.set('inputValue', '1999');

    assert.equal(this.element.textContent.trim(), '1.9k');

    this.set('inputValue', '1200');

    assert.equal(this.element.textContent.trim(), '1.2k');

    this.set('inputValue', '2000');

    assert.equal(this.element.textContent.trim(), '2k');

    this.set('inputValue', '39480');

    assert.equal(this.element.textContent.trim(), '39.4k');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled, click } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import sinon from 'sinon';

module('Integration | Helper | stop-propagation', function(hooks) {
  setupRenderingTest(hooks);

  test('currys a function and called stopPropagation on that first arg', async function(assert) {
    this.set('handler', sinon.fake());
    await render(hbs`<button {{on "click" (stop-propagation this.handler)}} data-test-selector="button"></button>`);
    await click(generateTestSelector('button'));
    await settled();

    assert.ok(this.handler.getCall(0).args[0].propagationStopped, true);
  });
});

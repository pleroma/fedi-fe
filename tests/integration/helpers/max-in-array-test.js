import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Helper | max-in-array', function(hooks) {
  setupRenderingTest(hooks);

  test('it accepts an array of numbers and spreads it againts Math.max', async function(assert) {
    this.set('arrayOfNumbers', [1,2,3,4,5]);

    await render(hbs`{{max-in-array this.arrayOfNumbers}}`);

    assert.equal(this.element.textContent.trim(), '5');
  });
});

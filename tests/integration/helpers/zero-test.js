import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Helper | zero', function(hooks) {
  setupRenderingTest(hooks);

  test('it returns a zero', async function(assert) {
    await render(hbs`<div data-test-selector="should-have-zero">{{zero}}</div>`);

    assert.dom(generateTestSelector('should-have-zero')).hasText('0');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import a11yAudit from 'ember-a11y-testing/test-support/audit';

module('Integration | Helper | html-safe', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    this.set('inputValue', '1234');

    await render(hbs`{{html-safe inputValue}}`);

    assert.equal(this.element.textContent.trim(), '1234');
  });

  test('it is accessible', async function(assert) {
    this.set('inputValue', '1234');

    await render(hbs`{{html-safe inputValue}}`);

    await a11yAudit(this.element);

    assert.ok(true, 'no a11y errors found!');
  });
});

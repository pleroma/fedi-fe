import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { freezeDateAt, unfreezeDate } from 'ember-mockdate-shim';

module('Integration | Helper | format-relative-short', function(hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function() {
    freezeDateAt(new Date('September 19, 2019 17:31:20'));
  });

  hooks.afterEach(function() {
    unfreezeDate();
  });

  test('it returns all the proper bits', async function(assert) {
    this.set('inputValue', null);

    await render(hbs`{{format-relative-short inputValue}}`);

    assert.equal(this.element.textContent.trim(), '', 'does nothing when nothing is passed');

    // --

    let secondsAgo = new Date();
    secondsAgo.setSeconds(secondsAgo.getSeconds() - 15);
    this.set('inputValue', secondsAgo);

    await settled();

    assert.dom(this.element).hasText('15s');

    // --

    let minutesAgo = new Date();
    minutesAgo.setMinutes(minutesAgo.getMinutes() - 15);
    this.set('inputValue', minutesAgo);

    await settled();

    assert.dom(this.element).hasText('15m');

    // --

    let hoursAgo = new Date();
    hoursAgo.setHours(hoursAgo.getHours() - 1);
    this.set('inputValue', hoursAgo);

    await settled();

    assert.dom(this.element).hasText('1h');

    // --

    let hourAndTwentyNineMinutesAgo = new Date();
    hourAndTwentyNineMinutesAgo.setHours(hourAndTwentyNineMinutesAgo.getHours() - 1);
    hourAndTwentyNineMinutesAgo.setMinutes(hourAndTwentyNineMinutesAgo.getMinutes() - 29);
    this.set('inputValue', hourAndTwentyNineMinutesAgo);

    await settled();

    assert.dom(this.element).hasText('1h');

    // --

    let hourAndAHalfAgo = new Date();
    hourAndAHalfAgo.setHours(hourAndAHalfAgo.getHours() - 1);
    hourAndAHalfAgo.setMinutes(hourAndAHalfAgo.getMinutes() - 31);
    this.set('inputValue', hourAndAHalfAgo);

    await settled();

    assert.dom(this.element).hasText('2h');

    // --

    let almostYesterday = new Date();
    almostYesterday.setHours(almostYesterday.getHours() - 23);
    this.set('inputValue', almostYesterday);

    await settled();

    assert.dom(this.element).hasText('23h');

    // --

    let yesterday = new Date();
    yesterday.setHours(yesterday.getHours() - 24);
    this.set('inputValue', yesterday);

    await settled();

    assert.dom(this.element).hasText('Sep 18');

    // --

    let lastMonth = new Date();
    lastMonth.setMonth(lastMonth.getMonth() - 1);
    this.set('inputValue', lastMonth);

    await settled();

    assert.dom(this.element).hasText('Aug 19');

    // --

    let lastYear = new Date();
    lastYear.setFullYear(lastYear.getFullYear() - 1);
    this.set('inputValue', lastYear);

    await settled();

    assert.dom(this.element).hasText('Sep 19, 2018', 'only show the year if its not this year');

    // --

    let tenSecondsFromNow = new Date();
    tenSecondsFromNow.setSeconds(tenSecondsFromNow.getSeconds() + 10);
    this.set('inputValue', tenSecondsFromNow);

    await settled();

    assert.dom(this.element).hasText('10s');

    // --

    let fiveMinutesFromNow = new Date();
    fiveMinutesFromNow.setMinutes(fiveMinutesFromNow.getMinutes() + 5);
    this.set('inputValue', fiveMinutesFromNow);

    await settled();

    assert.dom(this.element).hasText('5m');

    // --

    let fiveAndAHalfMinutesFromNow = new Date();
    fiveAndAHalfMinutesFromNow.setMinutes(fiveAndAHalfMinutesFromNow.getMinutes() + 5);
    fiveAndAHalfMinutesFromNow.setSeconds(fiveAndAHalfMinutesFromNow.getSeconds() + 31);
    this.set('inputValue', fiveAndAHalfMinutesFromNow);

    await settled();

    assert.dom(this.element).hasText('6m');

    // --

    let threeHoursFromNow = new Date();
    threeHoursFromNow.setHours(threeHoursFromNow.getHours() + 3);
    this.set('inputValue', threeHoursFromNow);

    await settled();

    assert.dom(this.element).hasText('3h');

    // --

    let twoDaysFromNow = new Date();
    twoDaysFromNow.setHours(twoDaysFromNow.getHours() + 48);
    this.set('inputValue', twoDaysFromNow);

    await settled();

    assert.dom(this.element).hasText('2d');
  });
});

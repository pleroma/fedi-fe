import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  render,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import sinon from 'sinon';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Helper | debounce', function(hooks) {
  setupRenderingTest(hooks);

  test('it will debounce the passed function', async function(assert) {
    this.set('func', sinon.fake());

    await render(hbs`<button data-test-selector="the-button" {{on "click" (debounce this.func)}}></button>`);

    click(generateTestSelector('the-button'));
    await click(generateTestSelector('the-button'));

    assert.ok(this.func.calledOnce);
  });
});

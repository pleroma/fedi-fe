import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Helper | full-url-for', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.owner.setupRouter();
  });

  test('it returns the full url for a ember-data model', async function(assert) {
    let status = this.server.create('status');
    let store = this.owner.lookup('service:store');

    let statusFromStore = await store.loadRecord('status', status.id);

    this.set('statusFromStore', statusFromStore);

    await render(hbs`{{full-url-for "status.details" this.statusFromStore.id}}`);

    let content = this.element.textContent.trim();

    let url = new URL(content);

    assert.equal(url.pathname, `/notice/${status.id}`);

    assert.ok(url.origin, 'it has an origin');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  click,
  render,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Helper | focus', function(hooks) {
  setupRenderingTest(hooks);

  test('it returns a function that accepts an element, when called, focuses that element', async function(assert) {
    await render(hbs`
      {{#let (ember-object) as |hash|}}
        <input data-test-selector="focusable" {{ref hash "input"}}>
        <button {{on "click" (fn (focus) hash.input)}} data-test-selector="clickable"></button>
      {{/let}}
    `);

    assert.dom(generateTestSelector('focusable')).isNotFocused();

    await click(generateTestSelector('clickable'));

    assert.dom(generateTestSelector('focusable')).isFocused();
  });
});

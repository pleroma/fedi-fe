import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  render,
  click,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | scroll-to-bottom', function(hooks) {
  setupRenderingTest(hooks);

  test('it returns a function, that when called, scrolls to the bottom o\' the window', async function(assert) {
    await render(hbs`
    <button {{on "click" (scroll-to-bottom "[data-scroll-me]")}} data-test-selector="scroll-to-bottom-button" data-scroll-me></button>
      <div style="height: 500vh;">Spacer</div>
    `);

    await click(generateTestSelector('scroll-to-bottom-button'));

    assert.equal(document.body.scrollTop, document.body.scrollHeight - document.body.clientHeight);
  });
});

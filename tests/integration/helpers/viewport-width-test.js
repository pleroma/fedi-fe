import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setBreakpoint } from 'ember-responsive/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Helper | viewport-width', function(hooks) {
  setupRenderingTest(hooks);

  test('it returns the viewport-width', async function(assert) {
    setBreakpoint('large');
    let oldInnerWidth = window.innerWidth;

    window.innerWidth = '1010';

    await render(hbs`<div data-test-selector="should-have-viewport-width">{{viewport-width}}</div>`);

    assert.dom(generateTestSelector('should-have-viewport-width')).hasText('1010');

    window.innerWidth = oldInnerWidth;
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | to-bool', function(hooks) {
  setupRenderingTest(hooks);

  test('it will coerce a number to boolean', async function(assert) {
    this.set('inputValue', null);

    await render(hbs`{{to-bool inputValue}}`);

    assert.equal(this.element.textContent.trim(), 'false');

    this.set('inputValue', 23);

    assert.equal(this.element.textContent.trim(), 'true');

    this.set('inputValue', false);

    assert.equal(this.element.textContent.trim(), 'false');

    this.set('inputValue', true);

    assert.equal(this.element.textContent.trim(), 'true');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, click } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import sinon from 'sinon';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Integration | Helper | prevent-default', function(hooks) {
  setupRenderingTest(hooks);

  test('curries a passed function calling preventDefault and StopPropagation on the first arg (event) of passed function', async function(assert) {
    this.set('handler', sinon.spy());

    await render(hbs`<button {{on "click" (prevent-default handler)}} data-test-selector="clicker-mc-clickerson"></button>`);

    await click(generateTestSelector('clicker-mc-clickerson'));

    assert.ok(this.handler.args[0][0].defaultPrevented);
  });
});

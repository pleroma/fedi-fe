import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | camel-to-sentence-case', function(hooks) {
  setupRenderingTest(hooks);

  test('it properly transforms the string to sentence case', async function(assert) {
    this.set('inputValue', 'camelCase');

    await render(hbs`{{camel-to-sentence-case inputValue}}`);

    assert.equal(this.element.textContent.trim(), 'Camel Case');
  });
});

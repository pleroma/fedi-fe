import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { helper } from '@ember/component/helper';
import { typeOf } from '@ember/utils';
import faker from 'faker';

module('Integration | Helper | ember-object', function(hooks) {
  setupRenderingTest(hooks);

  test('it returns an ember object', async function(assert) {
    this.owner.register('helper:type-of', helper(function emberObjectHelper([thing]) {
      return typeOf(thing);
    }));

    await render(hbs`
      {{#let (ember-object) as |context|}}
        <div data-test-selector="type-of-object">
          {{type-of context}}
        </div>
      {{/let}}
    `);

    assert.dom(generateTestSelector('type-of-object')).hasText('instance');
  });

  test('can set default props', async function(assert) {
    let word = faker.lorem.word();

    this.set('word', word);

    await render(hbs`
      {{#let (ember-object word=this.word) as |context|}}
        <div data-test-selector="test">
          {{context.word}}
        </div>
      {{/let}}
    `);

    assert.dom(generateTestSelector('test')).hasText(word);
  });
});

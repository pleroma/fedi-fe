import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Helper | truncate', function(hooks) {
  setupRenderingTest(hooks);

  test('truncates strings passed, adding an ellipsis', async function(assert) {
    this.set('string', '#'.repeat(10));

    await render(hbs`{{truncate this.string 5}}`);

    assert.equal(this.element.textContent.trim(), `${'#'.repeat(5)}...`);
  });

  test('Does not truncate if it does not need to', async function(assert) {
    this.set('string', '#'.repeat(10));

    await render(hbs`{{truncate this.string 20}}`);

    assert.equal(this.element.textContent.trim(), '#'.repeat(10));
  });

  test('support being curried from the outside in', async function(assert) {
    this.set('ary', [
      '#'.repeat(5),
      '#'.repeat(15),
      '#'.repeat(30),
    ]);

    await render(hbs`{{join ", " (map (truncate 5) this.ary)}}`);

    assert.equal(this.element.textContent.trim(), `${'#'.repeat(5)}, ${'#'.repeat(5)}..., ${'#'.repeat(5)}...`);
  });
});

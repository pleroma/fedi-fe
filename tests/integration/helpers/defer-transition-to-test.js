import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import { click, render } from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import sinon from 'sinon';
import Service from '@ember/service';

module('Integration | Helper | defer-transition-to', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('it accepts transition arguments at invocation time', async function(assert) {
    this.owner.setupRouter();
    let transitionToFake = sinon.fake();

    this.owner.register(
      'service:router',
      class SomeService extends Service {
        transitionTo = transitionToFake;
      },
    );

    await render(hbs`
      <input
        {{on "click" (action (defer-transition-to) value="target.value")}}
        value="abc"
        data-test-selector="input"
      >
    `);

    await click(generateTestSelector('input'));

    assert.ok(transitionToFake.calledOnceWithExactly('abc'), 'gets correct arg at invocation time');
  });
});

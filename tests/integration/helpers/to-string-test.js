import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | to-string', function(hooks) {
  setupRenderingTest(hooks);

  test('turns things into strings', async function(assert) {
    this.set('inputValue', 1234);

    await render(hbs`{{to-string inputValue}}`);

    assert.equal(this.element.textContent.trim(), '1234');

    this.set('inputValue', 'bing');

    assert.equal(this.element.textContent.trim(), 'bing');

    this.set('inputValue', true);

    assert.equal(this.element.textContent.trim(), 'true');

    this.set('inputValue', false);

    assert.equal(this.element.textContent.trim(), 'false');
  });
});

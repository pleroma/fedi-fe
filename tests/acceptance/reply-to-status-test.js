import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  fillIn,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import faker from 'faker';
import sinon from 'sinon';

module('Acceptance | replying to a status', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('can reply to a status [happy path]', async function(assert) {
    let statusContent = faker.lorem.sentence();
    let user = this.server.create('user', 'withToken', 'withSession');
    this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/public');

    assert.dom(generateTestSelector('reply-action-item')).exists();

    await click(generateTestSelector('reply-action-item'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));

    assert.equal(currentURL(), '/feeds/public');

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-content')}`).hasText(`View status thread. ${statusContent}`);
  });

  module('closing the modal before saving', function() {
    module('having edited the form', function() {
      test('will show a confirm modal', async function(assert) {
        let newConfirm = sinon.fake();
        let oldConfirm = window.confirm;
        let user = this.server.create('user', 'withToken', 'withSession');

        window.confirm = newConfirm;

        this.server.create('status', {
          account: user,
        });

        await dangerouslyVisit('/feeds/home');

        await click(generateTestSelector('reply-action-item'));
        await settled();

        assert.ok(window.confirm.notCalled);

        await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
        await triggerKeyEvent(generateTestSelector('create-status-body'), 'keyup', 71); // Trigger the G key press

        await click(generateTestSelector('close-modal-button'));
        await settled();

        assert.ok(window.confirm.calledOnce);

        window.confirm = oldConfirm;
      });

      test('clicking confirm will allow you to proceed from where you came', async function(assert) {
        let newConfirm = sinon.fake.returns(true);
        let oldConfirm = window.confirm;
        let user = this.server.create('user', 'withToken', 'withSession');

        window.confirm = newConfirm;

        this.server.create('status', {
          account: user,
        });

        await dangerouslyVisit('/feeds/home');

        await click(generateTestSelector('reply-action-item'));
        await settled();

        assert.ok(window.confirm.notCalled);

        await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
        await triggerKeyEvent(generateTestSelector('create-status-body'), 'keyup', 71); // Trigger the G key press

        await click(generateTestSelector('close-modal-button'));
        await settled();

        assert.ok(window.confirm.calledOnce);

        window.confirm = oldConfirm;
      });

      test('clicking cancel will keep you where you are (with the modal open)', async function(assert) {
        let newConfirm = sinon.fake.returns(true);
        let oldConfirm = window.confirm;
        let user = this.server.create('user', 'withToken', 'withSession');

        window.confirm = newConfirm;

        this.server.create('status', {
          account: user,
        });

        await dangerouslyVisit('/feeds/home');

        await click(generateTestSelector('reply-action-item'));
        await settled();

        assert.ok(window.confirm.notCalled);

        await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
        await triggerKeyEvent(generateTestSelector('create-status-body'), 'keyup', 71); // Trigger the G key press

        await click(generateTestSelector('close-modal-button'));
        await settled();

        assert.ok(window.confirm.calledOnce);

        window.confirm = oldConfirm;
      });
    });

    test('without editing anything should take you back where you started', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      this.server.create('status', {
        account: user,
      });

      await dangerouslyVisit('/feeds/home');

      await click(generateTestSelector('reply-action-item'));
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();

      await click(generateTestSelector('close-modal-button'));
      await settled();

      assert.equal(currentURL(), '/feeds/home');
    });
  });

  test('newly added statuses will be added to the top of the corresponding list', async function(assert) {
    let statusContent = faker.lorem.sentence();
    let user = this.server.create('user', 'withToken', 'withSession');
    this.server.createList('status', 20);
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('reply-action-item')}`);
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));

    assert.equal(currentURL(), '/feeds/all');

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-content')}`).hasText(`View status thread. ${statusContent}`);
  });

  test('will create a status that is a reply and shows the reply callout', async function(assert) {
    let statusContent = faker.lorem.sentence();
    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/home');

    assert.dom(generateTestSelector('reply-action-item')).exists();

    await click(generateTestSelector('reply-action-item'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.equal(currentURL(), '/feeds/home');

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-in-reply-to-link')}`)
      .hasAttribute('href', `/account/${user.id}`)
      .hasText(t('atHandle', { handle: user.username }));

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-replying-to-text-link')}`)
      .hasAttribute('href', `/notice/${status.id}`)
      .hasText(t('replyingTo'));
  });

  test('it shows a toast for the new status which contains a link to the new status', async function(assert) {
    let statusContent = faker.lorem.sentence();
    let user = this.server.create('user', 'withToken', 'withSession');
    this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/home');

    assert.dom(generateTestSelector('reply-action-item')).exists();

    await click(generateTestSelector('reply-action-item'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'new-reply')).exists();
    assert.dom(generateTestSelector('new-reply-toast-link')).hasAttribute('href', '/notice/2');
  });

  test('it will increment the replyCount on the original status', async function(assert) {
    let statusContent = faker.lorem.sentence();
    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/home');

    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('replies-count')}`).hasText('');
    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('replies-count')}`).hasAria('label', t('numReplies', { n: 0 }));

    assert.dom(generateTestSelector('reply-action-item')).exists();

    await click(generateTestSelector('reply-action-item'));
    await settled();

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('replies-count')}`).hasText('1');
    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('replies-count')}`).hasAria('label', t('numReplies', { n: 1 }));
  });

  test('it populates the form with the mentions of the thread participants', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let statusAuthor = this.server.create('user');
    let mentions = this.server.createList('mention', 1);
    let mentionedUser = mentions[0];

    this.server.create('status', {
      account: statusAuthor,
      mentions,
    });

    await dangerouslyVisit('/feeds/public');

    assert.dom(generateTestSelector('reply-action-item')).exists();

    await click(generateTestSelector('reply-action-item'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    await click(generateTestSelector('create-status-button'));

    assert.equal(currentURL(), '/feeds/public');

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-content')}`).hasText(`View status thread. @${statusAuthor.acct} @${mentionedUser.acct}`);
  });
});

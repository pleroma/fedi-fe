import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  fillIn,
  find,
  settled,
  triggerEvent,
  triggerKeyEvent,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import faker from 'faker';
import sinon from 'sinon';
import { dataURLToBlob } from 'blob-util';

module('Acceptance | create status', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');

    enableFeature('statusMedia');
  });

  test('create a new status (happy-path)', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusContent = faker.lorem.sentence();

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('create-status-form-trigger-text'));
    await settled();

    assert.dom(generateTestSelector('create-status-form')).exists();

    assert.dom(generateTestSelector('create-status-body')).exists();

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));

    assert.equal(currentURL(), '/feeds/all');

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-content')}`).hasText(`View status thread. ${statusContent}`);
  });

  test('create a new status with only media (happy-path)', async function(assert) {
    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all?compose=text');

    await triggerEvent(
      generateTestSelector('create-status-form', 'file-input-element'),
      'change',
      { files: [file] },
    );

    await click(generateTestSelector('create-status-button'));

    assert
      .dom('[data-test-feed-item-attachment] img')
      .hasAttribute('src', smiley, 'new status with only media is added');
  });

  test('create a new status with media and status content (happy-path)', async function(assert) {
    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    this.server.create('user', 'withToken', 'withSession');

    let statusContent = faker.lorem.sentence();

    await dangerouslyVisit('/feeds/all?compose=text');

    await triggerEvent(
      generateTestSelector('create-status-form', 'file-input-element'),
      'change',
      { files: [file] },
    );

    await fillIn(
      generateTestSelector('create-status-body'),
      statusContent,
    );

    await click(generateTestSelector('create-status-button'));

    assert
      .dom(generateTestSelector('feed-item-content'))
      .hasText(`View status thread. ${statusContent}`, 'content is added');

    assert
      .dom('[data-test-feed-item-attachment] img')
      .hasAttribute('src', smiley, 'media is added');
  });

  test('when clicking the poll trigger, it transitions to a post route in a poll context', async function(assert) {
    enableFeature('polls');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');
    await click(generateTestSelector('create-status-form-trigger-poll'));

    assert.dom(generateTestSelector('create-status-modal')).exists();
    assert.dom(generateTestSelector('polls-form')).exists();
    assert.dom(generateTestSelector('status-form-poll-toggle')).hasClass('is-active');
  });

  test('when clicking the image trigger, it transitions to a post route in a image upload context', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');
    await click(generateTestSelector('create-status-form-trigger-image'));

    assert.dom(generateTestSelector('create-status-modal')).exists();
  });

  module('status visibility', function() {
    // todo('can post a status with visibility private', function() {});
    //
    // todo('can post a status with visibility public', function() {});
    //
    // todo('can post a status with visibility direct', function() {});
  });

  module('closing the modal before saving', function() {
    module('having edited the form', function() {
      test('will show a confirm modal', async function(assert) {
        let newConfirm = sinon.fake();
        let oldConfirm = window.confirm;

        window.confirm = newConfirm;

        this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit('/feeds/all');

        await click(generateTestSelector('create-status-form-trigger-text'));

        assert.ok(window.confirm.notCalled);

        await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
        await triggerKeyEvent(generateTestSelector('create-status-body'), 'keyup', 71); // Trigger the G key press

        await click(generateTestSelector('close-modal-button'));
        await settled();

        assert.ok(window.confirm.calledOnce);

        window.confirm = oldConfirm;
      });

      test('clicking confirm will allow you to proceed from where you came', async function(assert) {
        let newConfirm = sinon.fake.returns(true);
        let oldConfirm = window.confirm;

        window.confirm = newConfirm;

        this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit('/feeds/all');
        await click(generateTestSelector('create-status-form-trigger-text'));

        await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
        await triggerKeyEvent(generateTestSelector('create-status-body'), 'keyup', 71); // Trigger the G key press

        await click(generateTestSelector('close-modal-button'));
        await settled();

        assert.equal(currentURL(), '/feeds/all', 'returns you from whence you came');

        window.confirm = oldConfirm;
      });

      test('clicking cancel will keep you where you are (with the modal open)', async function(assert) {
        let newConfirm = sinon.fake.returns(false);
        let oldConfirm = window.confirm;

        window.confirm = newConfirm;

        this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit('/feeds/all');
        await click(generateTestSelector('create-status-form-trigger-text'));

        await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
        await triggerKeyEvent(generateTestSelector('create-status-body'), 'keyup', 71); // Trigger the G key press

        await click(generateTestSelector('close-modal-button'));
        await settled();

        assert.dom(generateTestSelector('create-status-modal')).exists();

        window.confirm = oldConfirm;
      });
    });

    test('without editing anything should take you back where you started', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/feeds/all');
      await click(generateTestSelector('create-status-form-trigger-text'));

      assert.dom(generateTestSelector('create-status-modal')).exists();

      await click(generateTestSelector('close-modal-button'));
      await settled();

      assert.equal(currentURL(), '/feeds/all');
    });
  });

  test('when not authenticated, displays the correct reason', async function(assert) {
    await dangerouslyVisit('/feeds/all');
    await settled();

    let applicationRoute = this.owner.lookup('route:application');
    await applicationRoute.send('createStatus', 'text');
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();

    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToCreateStatus'));
  });

  test('it shows errors', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');
    await click(generateTestSelector('create-status-form-trigger-text'));
    await settled();

    assert.dom(generateTestSelector('create-status-error')).hasNoText(); // elements exists, but is empty
    assert.dom(generateTestSelector('create-status-body')).doesNotHaveClass('invalid');

    await click(generateTestSelector('create-status-button'));
    await triggerEvent(generateTestSelector('create-status-form'), 'submit');
    await settled();

    assert.dom(generateTestSelector('create-status-error')).exists('Shows errors after interaction');
    assert.dom(generateTestSelector('create-status-error')).hasText(t('yourPostMustHaveContent'));
    assert.dom(generateTestSelector('create-status-body')).hasClass('invalid');

    await fillIn(generateTestSelector('create-status-body'), faker.lorem.sentence());
    await settled();

    assert.dom(generateTestSelector('create-status-error')).hasNoText(); // Clears errors when form becomes valid
    assert.dom(generateTestSelector('create-status-body')).doesNotHaveClass('invalid');
  });

  test('newly added statuses will be added to the top of the corresponding list', async function(assert) {
    let statusContent = faker.lorem.sentence();
    let user = this.server.create('user', 'withToken', 'withSession');
    this.server.createList('status', 40, {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');
    await settled();

    find(generateTestSelector('feed-infinity-loader')).scrollIntoView();
    await settled();

    await triggerEvent(generateTestSelector('create-status-form-trigger-text'), 'click'); // ember-test-helpers click() will focus the input, which scrolls the page... we dont want that for this test.
    await settled();

    document.querySelector(generateTestSelector('create-status-body')).value = statusContent;
    await triggerEvent(generateTestSelector('create-status-body'), 'input');
    await settled();
    await triggerEvent(generateTestSelector('create-status-button'), 'click');

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-content')}`).hasText(`View status thread. ${statusContent}`);
  });

  test('it will create a status from the current sessions users', async function(assert) {
    // Mostly a test for the mirage server.
    let user = this.server.create('user', 'withToken', 'withSession');
    let statusContent = faker.lorem.sentence();

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('create-status-form-trigger-text'));

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));

    assert.dom(`${generateTestSelector('status')} ${generateTestSelector('feed-item-avatar-link')}`).hasAttribute('href', `/account/${user.id}`);
  });

  test('it shows a toast for the new status which contains a link to the status details', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let statusContent = faker.lorem.sentence();

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('create-status-form-trigger-text'));

    await fillIn(generateTestSelector('create-status-body'), statusContent);

    await click(generateTestSelector('create-status-button'));

    assert.dom(generateTestSelector('toast-type', 'new-status')).exists();

    assert.dom(generateTestSelector('new-status-toast-link')).hasAttribute('href', '/notice/1');
  });

  test('is shows a initiate create status button in the main viewport when authenticated', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('post-status-button')).exists();
  });

  test('clicking the floating post-status-button will open the create status modal', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('post-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();
  });

  module('create status query params', function() {
    test('opens when the compose param is set', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/feeds/all?compose=text');

      assert.dom(generateTestSelector('create-status-form')).exists();
    });

    test('on a successful creation, clears pertinant params', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');
      let statusContent = faker.lorem.sentence();

      await dangerouslyVisit('/feeds/all?compose=text');

      assert.dom(generateTestSelector('create-status-form')).exists();

      await fillIn(generateTestSelector('create-status-body'), statusContent);

      await click(generateTestSelector('create-status-button'));

      assert.equal(currentURL(), '/feeds/all');
    });
  });
});

import { module, skip, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  currentURL,
  fillIn,
  find,
  resetOnerror,
  settled,
  setupOnerror,
  triggerEvent,
  typeIn,
} from '@ember/test-helpers';
import faker from 'faker';
import { dataURLToBlob } from 'blob-util';
import config from 'pleroma-pwa/config/environment';
const { APP: { testApiBaseUrl } } = config;

module('Acceptance | direct messages', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.isEnabled('directMessages'), false, 'direct messages feature should be off at first');

    // Enable it for tests.
    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');
    enableFeature('statusDetails');
    enableFeature('statusMedia');
    enableFeature('userProfiles');
  });

  test('happy path with classic', async function(assert) {
    let message = faker.lorem.sentence();
    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-classic'));
    await settled();

    // Should open the create status modal.
    let url = new URL(`${testApiBaseUrl}${currentURL()}`);
    let queryParams = Object.fromEntries(url.searchParams);
    assert.equal(queryParams.compose, 'direct');
    assert.equal(queryParams.directMode, 'classic');
    assert.equal(queryParams['initial-mentions'], `["${recipient.acct}"]`);
    assert.equal(queryParams['initial-recipients'], `["${recipient.id}"]`);

    await typeIn(generateTestSelector('create-status-body'), message, { delay: 1 });

    assert.dom(generateTestSelector('create-status-body')).hasValue(`@${recipient.acct} ${message}`);

    await click(generateTestSelector('create-status-button'));
    await settled();

    await click(generateTestSelector('new-status-toast-link'));

    assert.dom(generateTestSelector('feed-item-content')).hasText(`@${recipient.acct} ${message}`);
  });

  test('happy path with chat', async function(assert) {
    let message = faker.lorem.sentence();
    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-chat'));
    await settled();

    assert.equal(currentRouteName(), 'message.chat', 'should be on the right route');
    assert.equal(currentURL(), `/message/chat/${recipient.id}`);

    await fillIn(generateTestSelector('create-direct-message', 'message-body-input'), message);
    await settled();

    await click(generateTestSelector('create-direct-message', 'submit-button'));
    await settled();

    assert.dom(generateTestSelector('direct-message', 'heading')).hasText(recipient.displayName);

    assert.dom(generateTestSelector('message-fragment', 'text')).hasText(message);

    assert.dom(generateTestSelector('create-direct-message', 'message-body-input')).hasNoText();
  });

  module('the choose-recipients modal', async function() {
    skip('clicking the see all recent messages button will transition you to the messages route and close the create status modal', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      this.server.createList('conversation', 5);

      await dangerouslyVisit('feeds/all');

      await click(generateTestSelector('create-status-form-trigger-text'));
      await settled();

      await fillIn(generateTestSelector('visibility-select'), 'direct');
      await settled()

      await click(generateTestSelector('see-all-direct-messages-link'));
      await settled();

      assert.equal(currentRouteName(), 'messages');
      assert.dom(generateTestSelector('create-status-modal')).doesNotExist();
    });
  });

  skip('you can open the status gallery modal by clicking on a dms attachment', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    let attachments = this.server.createList('attachment', 6);

    let attachmentInQuestion = attachments[1];

    let status = this.server.create('status', {
      account: user,
      mediaAttachments: attachments,
      visibility: 'direct',
    });

    let conversation = this.server.create('conversation');

    await dangerouslyVisit(`/message/${conversation.id}`);

    assert.dom(`${generateTestSelector('message-fragment')}${generateTestSelector('status-id', status.id)}`).exists();

    assert.dom(generateTestSelector('message-fragment-attachment', 'button')).exists({ count: 4 });

    await click(generateTestSelector('message-fragment-attachment-id', attachmentInQuestion.id));
    await settled();

    assert.dom(generateTestSelector('attachment-gallery-modal')).exists();
    assert.dom(generateTestSelector('attachment-gallery')).exists();
    assert.equal(currentURL(), `/message/${conversation.id}?gallery-attachment=${attachmentInQuestion.id}&gallery-status=${status.id}`);
    assert.dom(generateTestSelector('status-actions-bar')).doesNotExist();
  });

  module('the messages route', function() {
    test('has a floating button to start a new direct message', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/messages');

      assert.dom(generateTestSelector('start-new-direct-message-button-floater')).hasText(t('newChat'));

      await click(generateTestSelector('start-new-direct-message-button-floater'));

      assert.dom(generateTestSelector('create-status-modal')).exists();
      assert.dom(generateTestSelector('choose-recipients-form')).exists();
    });

    test('you cannot access direct messages routes while unauthenticated', async function(assert) {
      await dangerouslyVisit('/messages');

      assert.equal(currentRouteName(), 'sign-in');

      assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToSeeYourDirectMessages'));
    });

    test('the messages route displays a list of recent direct messages', async function(assert) {
      this.server.createList('chat-message', 5);

      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('messages');

      assert.dom(generateTestSelector('recent-conversation')).exists({ count: 5 });
    });

    test('the messages route displays a button to start a new direct message', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('messages');

      assert.dom(generateTestSelector('start-new-direct-message-button-floater')).exists();
    });

    test('when you have no conversations, it shows a nice message', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/messages');

      assert.dom(generateTestSelector('feed-fully-loaded')).hasText(t('beginningOfConversations'));
    });

    skip('clicking on a recent direct message will open that conversation up in the message details route', async function() {

    });

    skip('you cannot access direct messages routes while unauthenticated', async function() {

    });

    skip('we will show at most 40 search results', function() {

    });

    skip('when you have not chosen a recipient, the button is disabled', function() {

    });

    skip('selecting multiple recipients will nav you to the compose page with the proper query params for recipients sent', async function() {

    });
  });

  module('the message details route', function() {
    test('gives the body a special class', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let conversation = this.server.create('conversation');

      await dangerouslyVisit(`/message/${conversation.id}`);

      assert.dom(document.body).hasClass('alternative-scroll');
    });
  });

  module('the message compose route', function() {
    skip('gives the body a special class', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');
      let otherUser = this.server.create('user');

      await dangerouslyVisit(`/message/compose?participants=${JSON.stringify([otherUser.id])}`);

      assert.dom(document.body).hasClass('alternative-scroll');
    });
  });

  skip('you can reply to someone elses dm', async function() {

  });

  skip('the direct messages content is a feed, and will long poll + show new statuses as they are created', async function() {

  });

  skip('submitting a new direct message will attempt to keep the content scrolled at the bottom', async function() {

  });

  skip('when a new direct message appears from someone else, and you are not scrolled back up, we keep you scrolled at the bottom', async function() {

  });

  skip('when a new direct message appears from someone else, and you are scrolled up, we will not scroll you down to see the message, we will enque it like other feeds', async function() {

  });

  skip('when the search endpoint fails, we show an error toast', async function() {

  });

  test('with chat, when creating a DM fails, we show an error toast', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    let error = faker.lorem.sentence();
    let message = faker.lorem.sentence();
    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    this.server.post('/api/v1/pleroma/chats/:id/messages', function() {
      return { error };
    }, 500);

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-chat'));
    await settled();

    assert.equal(currentRouteName(), 'message.chat');

    await fillIn(generateTestSelector('create-direct-message', 'message-body-input'), message);
    await settled();

    await click(generateTestSelector('create-direct-message', 'submit-button'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'error')).exists();

    assert.dom(generateTestSelector('toast-message'))
      .hasText(error);

    resetOnerror();
  });

  test('with classic, when creating a DM fails, we show an error toast', async function(assert) {
    let error = faker.lorem.sentence();
    let message = faker.lorem.sentence();
    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    this.server.post('/api/v1/statuses', function() {
      return { error };
    }, 500);

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-classic'));
    await settled();

    await typeIn(generateTestSelector('create-status-body'), message, { delay: 1 });
    await click(generateTestSelector('create-status-button'));
    await settled();

    assert.dom(generateTestSelector('create-status-error'))
      .hasText(error);
  });

  test('happy path with chat with only media', async function(assert) {
    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-chat'));
    await settled();

    await triggerEvent(
      generateTestSelector('create-direct-message', 'file-input-element'),
      'change',
      { files: [file] },
    );

    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .exists('pending attachments should be visible');

    await click(generateTestSelector('create-direct-message', 'submit-button'));

    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .doesNotExist('no pending attachments should be visible');

    assert
      .dom(generateTestSelector('message-fragment-attachment', 'image'))
      .hasAttribute('src', smiley, 'new DM with only media is added');
  });

  test('happy path with classic with only media', async function(assert) {
    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-classic'));
    await settled();

    await triggerEvent(
      generateTestSelector('create-status-form', 'file-input-element'),
      'change',
      { files: [file] },
    );

    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .exists('pending attachments should be visible');

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .doesNotExist('no pending attachments should be visible');

    await click(generateTestSelector('new-status-toast-link'));

    let attachment = find('[data-test-feed-item-attachment]');

    assert
      .dom(attachment)
      .exists();

    assert
      .dom(attachment.querySelector('img'))
      .hasAttribute('src', smiley, 'new DM with only media is added');
  });

  test('happy path with chat with media and status content', async function(assert) {
    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    let message = faker.lorem.sentence();
    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-chat'));
    await settled();

    await triggerEvent(
      generateTestSelector('create-direct-message', 'file-input-element'),
      'change',
      { files: [file] },
    );

    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .exists('pending attachments should be visible');

    await fillIn(
      generateTestSelector('create-direct-message', 'message-body-input'),
      message,
    );

    await click(generateTestSelector('create-direct-message', 'submit-button'));

    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .doesNotExist('no pending attachments should be visible');

    assert
      .dom(generateTestSelector('message-fragment-attachment', 'image'))
      .hasAttribute('src', smiley, 'new DM with only media is added');

    assert
      .dom(generateTestSelector('message-fragment', 'text'))
      .hasText(message);
  });

  test('happy path with classic with media and status content', async function(assert) {
    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    let message = faker.lorem.sentence();
    let recipient = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${recipient.id}`);
    await click(generateTestSelector('user-details-header', 'direct-message-button-classic'));
    await settled();

    await triggerEvent(
      generateTestSelector('create-status-form', 'file-input-element'),
      'change',
      { files: [file] },
    );

    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .exists('pending attachments should be visible');

    await typeIn(
      generateTestSelector('create-status-body'),
      message,
      { delay: 1 },
    );

    await click(generateTestSelector('create-status-button'));
    await settled();

    assert
      .dom(generateTestSelector('media-attachments'))
      .doesNotExist('no pending attachments should be visible');

    await click(generateTestSelector('new-status-toast-link'));
    await settled();

    let attachment = find('[data-test-feed-item-attachment]');

    assert
      .dom(attachment.querySelector('img'))
      .hasAttribute('src', smiley, 'new DM with only media is added');

    assert
      .dom(generateTestSelector('feed-item-content'))
      .hasText(`@${recipient.acct} ${message}`);
  });
});

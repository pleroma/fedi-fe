import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  currentRouteName,
  find,
  settled,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import { setBreakpoint } from 'ember-responsive/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import selectChoose from 'pleroma-pwa/tests/helpers/select-choose';

module('Acceptance | settings route', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('the default page should be profile', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings');

    assert.equal(currentRouteName(), 'settings.profile');
  });

  test('choosing select options should navigate properly', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    enableFeature('userSettings');

    setBreakpoint('small');

    await dangerouslyVisit('/settings');

    let dropdown = find(generateTestSelector('settings-options'));

    selectChoose(dropdown, 'settings.notifications');
    await settled();
    assert.equal(currentRouteName(), 'settings.notifications');

    selectChoose(dropdown, 'settings.security');
    await settled();
    assert.equal(currentRouteName(), 'settings.security');

    selectChoose(dropdown, 'settings.blocks');
    await settled();
    assert.equal(currentRouteName(), 'settings.blocks');

    selectChoose(dropdown, 'settings.mutes');
    await settled();
    assert.equal(currentRouteName(), 'settings.mutes');
  });
});

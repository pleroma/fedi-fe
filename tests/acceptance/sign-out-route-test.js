import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { currentRouteName, settled } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { currentSession } from 'ember-simple-auth/test-support';

module('Acceptance | sign out route', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('visiting /sign-out should invalidate the session', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all')

    assert.ok(currentSession().isAuthenticated);

    await dangerouslyVisit('/sign-out');

    assert.notOk(currentSession().isAuthenticated);
  });

  test('visiting /sign-out should transition to home page', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/sign-out');
    await settled();

    assert.equal(currentRouteName(), 'feed');
  });
});

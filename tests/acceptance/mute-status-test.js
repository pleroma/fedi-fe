import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  settled,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';

module('Acceptance | mute status', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('you can mute a status/conversation from the status menu', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let user2 = this.server.create('user');
    let statusExtra = this.server.create('status-extra', {
      threadMuted: false,
    });
    let statusFromOtherUser = this.server.create('status', {
      account: user2,
      pleroma: statusExtra,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'mute-conversation'));
    await settled();

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', statusFromOtherUser.id);

    assert.ok(statusFromStore.pleroma.threadMuted);
  });

  test('muting a conversation will fire a conversationMuted toast', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let user2 = this.server.create('user');
    let statusExtra = this.server.create('status-extra', {
      threadMuted: false,
    });
    let statusFromOtherUser = this.server.create('status', {
      account: user2,
      pleroma: statusExtra,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'mute-conversation'));
    await settled();

    assert.dom(generateTestSelector('toast-message')).hasText(t('conversationMuted'));
  });

  test('you can unmute a conversation from the conversationMuted toast', async function(assert) {
    let statusFromStore;
    let store = this.owner.lookup('service:store');
    this.server.create('user', 'withToken', 'withSession');

    let user2 = this.server.create('user');
    let statusExtra = this.server.create('status-extra', {
      threadMuted: false,
    });
    let statusFromOtherUser = this.server.create('status', {
      account: user2,
      pleroma: statusExtra,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'mute-conversation'));
    await settled();

    statusFromStore = await store.loadRecord('status', statusFromOtherUser.id);

    assert.ok(statusFromStore.pleroma.threadMuted);

    await click(generateTestSelector('conversation-muted-unmute-button'));

    statusFromStore = await store.loadRecord('status', statusFromOtherUser.id);

    assert.notOk(statusFromStore.pleroma.threadMuted);
  });

  test('you can unmute a conversation from the status menu', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let user2 = this.server.create('user');
    let statusExtra = this.server.create('status-extra', {
      threadMuted: true,
    });
    let statusFromOtherUser = this.server.create('status', {
      account: user2,
      pleroma: statusExtra,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-unmute')}`);
    await settled();

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', statusFromOtherUser.id);

    assert.notOk(statusFromStore.pleroma.threadMuted);
  });

  test('unmuting a conversation will throw a conversationUnmuted toast', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let user2 = this.server.create('user');
    let statusExtra = this.server.create('status-extra', {
      threadMuted: true,
    });
    let statusFromOtherUser = this.server.create('status', {
      account: user2,
      pleroma: statusExtra,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-unmute')}`);
    await settled();

    assert.dom(generateTestSelector('toast-message')).hasText(t('conversationUnmuted'));
  });

  test('you can mute a conversation from the conversationUnmuted toast', async function(assert) {
    let statusFromStore;
    let store = this.owner.lookup('service:store');
    this.server.create('user', 'withToken', 'withSession');

    let user2 = this.server.create('user');
    let statusExtra = this.server.create('status-extra', {
      threadMuted: true,
    });
    let statusFromOtherUser = this.server.create('status', {
      account: user2,
      pleroma: statusExtra,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-unmute')}`);
    await settled();

    statusFromStore = await store.loadRecord('status', statusFromOtherUser.id);

    assert.notOk(statusFromStore.pleroma.threadMuted);

    await click(generateTestSelector('conversation-unmuted-mute-button'));

    statusFromStore = await store.loadRecord('status', statusFromOtherUser.id);

    assert.ok(statusFromStore.pleroma.threadMuted);
  });

  test('when not authenticated, attempting to mute will show a modal with the proper reason', async function(assert) {
    let statusActions = this.owner.lookup('service:status-actions');

    let user = this.server.create('user');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await statusActions.muteConversation.perform(status);
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToMuteConversations'));
  });

  test('when not authenticated, attempting to unmute will show a modal with the proper reason', async function(assert) {
    let statusActions = this.owner.lookup('service:status-actions');

    let user = this.server.create('user');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await statusActions.unmuteConversation.perform(status);
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToUnmuteConversations'));
  });

  test('muted statuses get a special class', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let account = this.server.create('user', 'muted');

    let status = this.server.create('status', {
      account,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)}`)
      .hasClass('feed-item--muted');
  });

  test('you can unmute a status from its collapsed (muted) state', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let account = this.server.create('user');
    let statusExtra = this.server.create('status-extra', {
      threadMuted: true,
    });

    let status = this.server.create('status', {
      account,
      pleroma: statusExtra,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-unmute')}`);
    await settled();

    assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)}`)
      .doesNotHaveClass('feed-item--muted');
  });
});

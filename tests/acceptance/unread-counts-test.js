import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { settled } from '@ember/test-helpers';
import sinon from 'sinon';

module('Acceptance | unread counts', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('the application should start watching the unreadCounts immediately after authentication', async function(assert) {
    let unreadCountsService = this.owner.lookup('service:unreadCounts');
    let startWatchingSpy = sinon.spy(unreadCountsService, 'startWatching');

    await dangerouslyVisit('/');

    assert.ok(startWatchingSpy.notCalled);

    this.server.create('user', 'withToken', 'withSession');
    await dangerouslyVisit('/');

    await settled();

    assert.ok(startWatchingSpy.calledOnce);
  });

  test('stops watching unreadCounts when you become unauthenticated', async function(assert) {
    this.server.create('user');
    let unreadCountsService = this.owner.lookup('service:unreadCounts');
    let stopWatchingSpy = sinon.spy(unreadCountsService, 'stopWatching');

    await dangerouslyVisit('/');

    assert.ok(stopWatchingSpy.notCalled);

    this.server.create('user', 'withToken', 'withSession');
    await dangerouslyVisit('/');

    await settled();

    await dangerouslyVisit('/sign-out');

    assert.ok(stopWatchingSpy.calledOnce);
  });
});

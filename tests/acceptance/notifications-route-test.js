import { module, skip } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  find,
  findAll,
  settled,
  waitUntil,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import promiseEachSeries from 'p-each-series';
import { setupIntl } from 'ember-intl/test-support';
import config from 'pleroma-pwa/config/environment';

module('Acceptance | notifications routes', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  // TODO: Write these acceptance tests
  // Future Greg, I am skipping these for now as I am running out of time.

  hooks.beforeEach(function() {
    this.server.create('instance');

    this.testInfiniteScroll = async(assert, max, groupsOf = config.APP.FEED_PAGE_SIZE, selector = 'notification') => {
      let initialSet = findAll(generateTestSelector(selector)).length;
      let loops = new Array(Math.ceil((max - initialSet) / groupsOf));

      await settled();

      await promiseEachSeries(loops, async(element, index) => {
        if (index === 0) {
          assert.dom(generateTestSelector(selector)).exists({ count: initialSet });
        } else {
          assert.dom(generateTestSelector(selector)).exists({ count: Math.min((index * groupsOf) + initialSet, max) });
        }

        await settled();

        if (find(generateTestSelector('feed-infinity-loader'))) {
          assert.dom(generateTestSelector('feed-infinity-loader')).exists();

          find(generateTestSelector('feed-infinity-loader')).scrollIntoView();
        } else {
          assert.dom(generateTestSelector('feed-fully-loaded'));
        }

        await settled();

        await waitUntil(() => {
          return findAll(generateTestSelector(selector)).length === Math.min((index + 1) * groupsOf + initialSet, max);
        });
      });
    };
  });

  skip('the notifications route will default to its sub tab of ', async function() {

  });

  skip('you can navigate through different filtered notification views', async function() {

  });

  skip('the all notifications route renders ALL of the notifications', async function() {

  });

  skip('the all notifications route will infinite scroll', async function(assert) {
    assert.timeout(60000);

    this.server.createList('notification', 75);

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/notifications');

    await this.testInfiniteScroll(assert, 75);
  });

  skip('the new follows tab renders all of the follow notifications', async function() {

  });

  skip('you can follow back from a follow notification', async function() {

  });

  skip('you can unfollow from a follow notification', async function() {

  });

  skip('the mentions route renders a status card with a mentioned you callout', async function() {
    // the card has the correct mention date.
  });

  skip('the mention notification card renders a link to the user who mentioned you', async function() {

  });

  skip('the reposts tab renders statuses of type repost', async function() {

  });

  skip('the favorites tab renders statuses of type favorite', async function() {

  });

  skip('favorite notification has a link to the user who favorited your status', async function() {

  });
});

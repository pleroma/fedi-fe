import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  currentURL,
  fillIn,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import { invalidateSession } from 'ember-simple-auth/test-support';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import signIn from 'pleroma-pwa/tests/helpers/sign-in';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import config from 'pleroma-pwa/config/environment';
import { setBreakpoint } from 'ember-responsive/test-support';

module('Acceptance | auth routing', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    setBreakpoint('large');
    this.server.create('instance');
    this.user = this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    });
  });

  hooks.afterEach(function() {
    this.user.destroy();
  });

  test('Becoming unauthenticated, while on a authenticated route will kick you out to the root url', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/home');

    assert.equal(currentURL(), '/feeds/home');

    let applicationRoute = this.owner.lookup('route:application');

    // We need to stub this because the default ESA is to hard reload the window
    applicationRoute.sessionInvalidated = function() {
      this.transitionTo(config.rootURL);
    };

    await invalidateSession({
      'token_type': 'Bearer',
      'access_token': 123456789,
    });

    await settled();

    assert.equal(currentURL(), '/feeds/all');
  });

  test('Attempting to visit a unauthenticated route while authenticated will kick you out to the authenticated route', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/home');

    await dangerouslyVisit('/sign-in');

    assert.equal(currentURL(), '/feeds/home');
  });

  test('The sign-in route renders the sign-in-form', async function(assert) {
    await dangerouslyVisit('/sign-in');
    assert.equal(currentRouteName(), 'sign-in');
    assert.dom(generateTestSelector('sign-in-form')).exists();
  });

  test('The sign-in modal renders the sign-in-form', async function(assert) {
    await dangerouslyVisit('/feeds/all');
    await dangerouslyVisit('/feeds/home');
    await settled();
    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-form')).exists();
  });

  test('routes can specify an authenticated reasonKey which will be shown on the sign in modal and page', async function(assert) {
    await dangerouslyVisit('/feeds/all');
    await dangerouslyVisit('/notifications');
    await settled();
    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-form')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToSeeYourNotifications'));
  });

  test('it will clear the reason for authentication when abandoning sign in', async function(assert) {
    await dangerouslyVisit('/feeds/all');

    await dangerouslyVisit('/notifications');

    assert.dom(generateTestSelector('sign-in-form'));
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToSeeYourNotifications'));

    await click(generateTestSelector('close-modal-button'));
    await settled();

    await click(generateTestSelector('account-cta-sign-in-button'));

    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('signIn'));
  });

  test('it will return focus to the trigger when abandoning sign in', async function(assert) {
    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('account-cta-sign-in-button'));
    await settled();

    await click(generateTestSelector('close-modal-button'));
    await settled();

    assert.dom(generateTestSelector('account-cta-sign-in-button')).isFocused();
  });

  module('auth modal', function() {
    test('it closes when the close button is clicked', async function(assert) {
      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('account-cta-sign-in-button'));
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();

      await click(generateTestSelector('close-modal-button'));

      assert.dom(generateTestSelector('auth-modal')).doesNotExist();
    });

    test('it closes when the escape key is pressed', async function(assert) {
      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('account-cta-sign-in-button'));
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();

      await triggerKeyEvent(document.body, 'keydown', 27);

      assert.dom(generateTestSelector('auth-modal')).doesNotExist();
    });
  });

  module('when attempting to navigate to an authenticated route', function() {
    module('from direct navigation on first page load', function() {
      test('will send you to the sign-in route', async function(assert) {
        await dangerouslyVisit('/feeds/home');

        assert.equal(currentRouteName(), 'sign-in');

        assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToSeeThatPage'), 'The sign-in route shows helpful text as to why you are here');
      });

      test('sign in success, will direct you to your home feed', async function(assert) {
        await dangerouslyVisit('/feeds/home');
        assert.equal(currentRouteName(), 'sign-in');
        await signIn(this.user);
        assert.equal(currentURL(), '/feeds/home');
      });

      test('sign in failure, will keep you in the sign in route', async function(assert) {
        await dangerouslyVisit('/feeds/home');

        assert.equal(currentRouteName(), 'sign-in');

        await fillIn(generateTestSelector('username-input'), this.user.username);
        await fillIn(generateTestSelector('password-input'), 'bad password');
        await click(generateTestSelector('sign-in-button'));

        assert.equal(currentRouteName(), 'sign-in');
      });
    });

    module('from inside the app', function() {
      test('sign in success, allows you to pass through to your destination route', async function(assert) {
        await dangerouslyVisit('/feeds/all');

        await dangerouslyVisit('feeds/home');

        await settled();

        assert.equal(currentURL(), '/feeds/all?auth=sign-in');

        assert.dom(generateTestSelector('auth-modal')).exists();

        await signIn(this.user);
        await settled();

        assert.equal(currentURL(), '/feeds/home');
      });

      test('it will clear the redirect route when abandoning sign in', async function(assert) {
        let user = this.server.create('user');

        await dangerouslyVisit('/feeds/all');

        await dangerouslyVisit('/notifications');

        assert.dom(generateTestSelector('sign-in-form'));

        await click(`${generateTestSelector('auth-modal')} ${generateTestSelector('close-modal-button')}`);
        await settled();

        await click(generateTestSelector('account-cta-sign-in-button'));

        assert.dom(generateTestSelector('sign-in-form'));

        await signIn(user);

        assert.equal(currentURL(), '/feeds/all');
        assert.notOk(currentRouteName() === 'notifications');
      });

      test('sign in failure, keeps you on the page with the modal open', async function(assert) {
        await dangerouslyVisit('/feeds/all');

        await dangerouslyVisit('/feeds/home');

        await settled();

        assert.equal(currentURL(), '/feeds/all?auth=sign-in');

        assert.dom(generateTestSelector('auth-modal')).exists();

        await fillIn(generateTestSelector('username-input'), this.user.username);
        await fillIn(generateTestSelector('password-input'), 'bad password');
        await click(generateTestSelector('sign-in-button'));

        assert.equal(currentURL(), '/feeds/all?auth=sign-in');

        assert.dom(generateTestSelector('auth-modal')).exists();
      });

      test('sign in abandonment, returns you to the page you were on before forcing authentication', async function(assert) {
        await dangerouslyVisit('/feeds/all');

        await dangerouslyVisit('/feeds/home');

        await settled();

        assert.equal(currentURL(), '/feeds/all?auth=sign-in');

        assert.dom(generateTestSelector('auth-modal')).exists();

        await click(generateTestSelector('close-modal-button'));

        assert.equal(currentURL(), '/feeds/all');

        assert.dom(generateTestSelector('auth-modal')).doesNotExist();
      });
    });
  });
});

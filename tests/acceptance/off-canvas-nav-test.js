import { module, test, skip } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setBreakpoint } from 'ember-responsive/test-support';

module('Acceptance | off canvas nav', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
    this.user = this.server.create('user');
    this.server.createList('status', 10, {
      account: this.user,
    });
  });

  hooks.afterEach(async function() {
    let offCanvasNav = this.owner.lookup('service:off-canvas-nav');
    offCanvasNav.close();
    await settled();
  });

  test('opens and closes', async function(assert) {
    let offCanvasNav = this.owner.lookup('service:off-canvas-nav');

    await dangerouslyVisit('/feeds/public');

    assert.ok(offCanvasNav.isClosed, 'The sidebar is closed');
    assert.notOk(offCanvasNav.isOpen, 'The sidebar is closed');

    assert.dom(generateTestSelector('sidebar')).hasAttribute('hidden');

    await click(generateTestSelector('header-menu-toggle'));

    await settled();

    assert.ok(offCanvasNav.isOpen, 'The sidebar is open');
    assert.notOk(offCanvasNav.isClosed);

    assert.dom(generateTestSelector('sidebar')).doesNotHaveAttribute('hidden');

    await click(generateTestSelector('sidebar-menu-toggle'));

    await settled();

    assert.ok(offCanvasNav.isClosed, 'The sidebar is closed');
    assert.notOk(offCanvasNav.isOpen, 'The sidebar is closed');

    assert.dom(generateTestSelector('sidebar')).hasAttribute('hidden');
  });

  test('clicking a link in the sidebar will close the off canvas nav', async function(assert) {
    let offCanvasNav = this.owner.lookup('service:off-canvas-nav');

    await dangerouslyVisit('/feeds/public');

    await click(generateTestSelector('header-menu-toggle'));

    await settled();

    assert.ok(offCanvasNav.isOpen, 'The sidebar is open');
    assert.dom(generateTestSelector('sidebar')).doesNotHaveAttribute('hidden');

    await click(generateTestSelector('feed-link-all'));

    await settled();

    assert.equal(currentURL(), '/feeds/all');

    assert.ok(offCanvasNav.isClosed, 'The sidebar is closed');
    assert.dom(generateTestSelector('sidebar')).hasAttribute('hidden');
  });

  test('once opened, closes on escape key pressed', async function(assert) {
    let offCanvasNav = this.owner.lookup('service:off-canvas-nav');

    await dangerouslyVisit('/feeds/public');

    await click(generateTestSelector('header-menu-toggle'));

    await settled();

    assert.ok(offCanvasNav.isOpen, 'The sidebar is open');
    assert.dom(generateTestSelector('sidebar')).doesNotHaveAttribute('hidden');

    await triggerKeyEvent(document.body, 'keydown', 27);

    await settled();

    assert.ok(offCanvasNav.isClosed, 'The sidebar is closed');
    assert.dom(generateTestSelector('sidebar')).hasAttribute('hidden');
  });

  skip('it gives focus to the first <section> element of the sidebar when opened', async function(assert) {
    await dangerouslyVisit('/feeds/public');

    await click(generateTestSelector('header-menu-toggle'));

    await settled();

    assert.dom(`${generateTestSelector('sidebar')} section:first-of-type`).isFocused('will push focus the first section element of the menu when opened');
  });

  test('on mobile, when open, prevents scroll on the body', async function(assert) {
    setBreakpoint('small');

    await dangerouslyVisit('/feeds/public');

    assert.equal(window.getComputedStyle(document.body, null).getPropertyValue('overflow'), 'visible');

    await click(generateTestSelector('header-menu-toggle'));

    await settled();

    assert.equal(window.getComputedStyle(document.body, null).getPropertyValue('overflow'), 'hidden');
  });
});

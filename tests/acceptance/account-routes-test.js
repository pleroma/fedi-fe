import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  currentURL,
  find,
  settled,
  triggerKeyEvent,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { enableFeature } from 'ember-feature-flags/test-support';
import faker from 'faker';
import config from 'pleroma-pwa/config/environment';
const { APP: { testApiBaseUrl } } = config;

module('Acceptance | account routes', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('userProfiles'), false);

    // Enable it for tests.
    enableFeature('userProfiles');
  });

  test('You can see your own profile', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${user.id}`);

    assert.equal(currentRouteName(), 'account.statuses');

    assert.dom(generateTestSelector('user-details-header')).exists();

    assert.dom(generateTestSelector('user-details-header', 'username')).hasText(t('atHandle', { handle: user.username }));
  });

  test('You can see others profile', async function(assert) {
    let otherUser = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${otherUser.id}`);

    assert.equal(currentRouteName(), 'account.statuses');

    assert.dom(generateTestSelector('user-details-header')).exists();

    assert.dom(generateTestSelector('user-details-header', 'username')).hasText(t('atHandle', { handle: otherUser.username }));
  });

  test('You can see others profile with username', async function(assert) {
    let otherUser = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${otherUser.username}`);

    assert.equal(currentRouteName(), 'account.statuses');

    assert.dom(generateTestSelector('user-details-header')).exists();

    assert.dom(generateTestSelector('user-details-header', 'username')).hasText(t('atHandle', { handle: otherUser.username }));
  });

  test('Default to the account.statuses route', async function(assert) {
    let user = this.server.create('user');

    await dangerouslyVisit(`/account/${user.id}`);

    assert.equal(currentRouteName(), 'account.statuses');
  });

  test('Clicking the users avatar will render an image lightbox', async function(assert) {
    let user = this.server.create('user');

    await dangerouslyVisit(`/account/${user.id}`);

    assert.equal(currentRouteName(), 'account.statuses');

    assert.dom(generateTestSelector('user-details-header', 'avatar-lightbox-trigger')).exists();
    assert.equal(find(generateTestSelector('user-details-header', 'avatar-lightbox-trigger')).nodeName, 'BUTTON');
    assert.dom(generateTestSelector('user-details-header', 'avatar')).exists();

    await click(generateTestSelector('user-details-header', 'avatar-lightbox-trigger'));

    assert.dom(generateTestSelector('avatar-lightbox')).exists();
    assert.dom(generateTestSelector('lock-body-scroll')).exists();

    await click(generateTestSelector('avatar-lightbox', 'close-button'));

    assert.dom(generateTestSelector('avatar-lightbox')).doesNotExist();

    await click(generateTestSelector('user-details-header', 'avatar-lightbox-trigger'));

    assert.dom(generateTestSelector('avatar-lightbox')).exists();

    await triggerKeyEvent(document.body, 'keydown', 27);
    await settled();

    assert.dom(generateTestSelector('avatar-lightbox')).doesNotExist();
  });

  test('If the user in question has blocked the session user, we show a blocked screen', async function(assert) {
    let user = this.server.create('user', 'blockedBy');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${user.id}`);

    assert.dom(generateTestSelector('blocked-profile')).exists();
  });

  test('We do not show a blocked screen when the user in question has not blocked the session user', async function(assert) {
    let user = this.server.create('user');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit(`/account/${user.id}`);

    assert.dom(generateTestSelector('blocked-profile')).doesNotExist();
  });

  module('When viewing your own', function() {
    module('The user details header', function() {
      test('renders a working sign-out button', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');
        let session = this.owner.lookup('service:session');

        await dangerouslyVisit(`/account/${user.id}`);

        assert.dom(generateTestSelector('user-details-header', 'sign-out-button')).hasText(t('signOut'));

        await click(generateTestSelector('user-details-header', 'sign-out-button'));
        await settled();

        assert.notOk(session.isAuthenticated);
      });

      test('has the proper user templating', async function(assert) {
        let statusesCount = faker.random.number();
        let followersCount = faker.random.number();
        let followingCount = faker.random.number();
        let userBio = faker.lorem.paragraph();

        let user = this.server.create('user', {
          statusesCount,
          followersCount,
          followingCount,
          note: userBio,
        }, 'withToken', 'withSession');

        await dangerouslyVisit(`/account/${user.id}`);

        assert.dom(generateTestSelector('user-details-header', 'displayName')).hasText(user.displayName);

        assert.dom(generateTestSelector('user-details-header', 'username')).hasText(t('atHandle', { handle: user.username }));

        assert.dom(generateTestSelector('user-details-header', 'status-count')).hasText(`${t('statusesCAPS')}${t('colon')} ${statusesCount}`);

        assert.dom(generateTestSelector('user-details-header', 'followers-count')).hasText(`${t('followersCAPS')}${t('colon')} ${followersCount}`);

        assert.dom(generateTestSelector('user-details-header', 'following-count')).hasText(`${t('followingCAPS')}${t('colon')} ${followingCount}`);

        assert.dom(generateTestSelector('user-details-header', 'bio')).hasText(userBio);
      });

      test('we render a link to the user settings route', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit(`/account/${user.id}`);

        assert.dom(generateTestSelector('user-details-header', 'settings-link')).hasAttribute('title', t('userSettings'));

        await click(generateTestSelector('user-details-header', 'settings-link'));

        assert.equal(currentRouteName(), 'settings.profile');
      });

      test('you cannot see the button to mention a user', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit(`/account/${user.id}`);

        assert.dom(generateTestSelector('user-details-header', 'mention-button')).doesNotExist();
      });

      test('you cannot see the user details header menu', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit(`/account/${user.id}`);

        assert.dom(generateTestSelector('user-details-header', 'menu-trigger')).doesNotExist();
      });

      test('you cannot see the direct message button', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit(`/account/${user.id}`);

        assert.dom(generateTestSelector('user-details-header', 'direct-message-button')).doesNotExist();
      });

      test('We do not show the follow/unfollow buttons', async function(assert) {
        let user = this.server.create('user', 'withToken', 'withSession');

        await dangerouslyVisit(`/account/${user.id}`);

        assert.dom(generateTestSelector('user-details-header', 'unfollow-button')).doesNotExist();
        assert.dom(generateTestSelector('user-details-header', 'follow-button')).doesNotExist();
      });
    });

    test('Account subroute navbar has all the proper links', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}`);

      assert.dom(generateTestSelector('account-statuses-link')).hasText(t('statuses'));
      await click(generateTestSelector('account-statuses-link'));
      assert.equal(currentRouteName(), 'account.statuses');

      assert.dom(generateTestSelector('account-following-link')).hasText(t('following'));
      await click(generateTestSelector('account-following-link'));
      assert.equal(currentRouteName(), 'account.following');

      assert.dom(generateTestSelector('account-followers-link')).hasText(t('followers'));
      await click(generateTestSelector('account-followers-link'));
      assert.equal(currentRouteName(), 'account.followers');

      assert.dom(generateTestSelector('account-media-link')).hasText(t('media'));
      await click(generateTestSelector('account-media-link'));
      assert.equal(currentRouteName(), 'account.media');

      assert.dom(generateTestSelector('account-favorites-link')).hasText(t('favorites'));
      await click(generateTestSelector('account-favorites-link'));
      assert.equal(currentRouteName(), 'account.favorites');
    });
  });

  module('When viewing someone elses', function() {
    module('The user details header', function() {
      test('has the proper user templating', async function(assert) {
        let statusesCount = faker.random.number();
        let followersCount = faker.random.number();
        let followingCount = faker.random.number();
        let userBio = faker.lorem.paragraph();

        this.server.create('user', 'withToken', 'withSession');

        let otherUser = this.server.create('user', {
          statusesCount,
          followersCount,
          followingCount,
          note: userBio,
        });

        await dangerouslyVisit(`/account/${otherUser.id}`);

        assert.dom(generateTestSelector('user-details-header', 'displayName')).hasText(otherUser.displayName);

        assert.dom(generateTestSelector('user-details-header', 'username')).hasText(t('atHandle', { handle: otherUser.username }));

        assert.dom(generateTestSelector('user-details-header', 'status-count')).hasText(`${t('statusesCAPS')}${t('colon')} ${statusesCount}`);

        assert.dom(generateTestSelector('user-details-header', 'followers-count')).hasText(`${t('followersCAPS')}${t('colon')} ${followersCount}`);

        assert.dom(generateTestSelector('user-details-header', 'following-count')).hasText(`${t('followingCAPS')}${t('colon')} ${followingCount}`);

        assert.dom(generateTestSelector('user-details-header', 'bio')).hasText(userBio);
      });

      test('renders a follow button if you are not following that person', async function(assert) {
        this.server.create('user', 'withToken', 'withSession');

        let otherUser = this.server.create('user');

        await dangerouslyVisit(`/account/${otherUser.id}`);

        assert.dom(generateTestSelector('user-details-header', 'follow-button')).hasText(t('follow'));

        await click(generateTestSelector('user-details-header', 'follow-button'));

        assert.dom(generateTestSelector('user-details-header', 'follow-button')).doesNotExist();
        assert.dom(generateTestSelector('user-details-header', 'unfollow-button')).exists();
      });

      test('renders a button to initiate a status with the user mentioned', async function(assert) {
        this.server.create('user', 'withToken', 'withSession');

        let otherUser = this.server.create('user');

        await dangerouslyVisit(`/account/${otherUser.id}`);

        assert.dom(generateTestSelector('user-details-header', 'mention-button')).hasText(t('mention'));

        await click(generateTestSelector('user-details-header', 'mention-button'));

        assert.dom(generateTestSelector('create-status-modal')).exists();

        assert.equal(find(generateTestSelector('create-status-body')).value, `${t('atHandle', { handle: otherUser.username })} `);
      });

      test('renders a direct message button that will initiate the direct message modal', async function(assert) {
        this.server.create('user', 'withToken', 'withSession');

        let otherUser = this.server.create('user');

        await enableFeature('directMessages');
        await enableFeature('showDirectMessagesClassic');

        await dangerouslyVisit(`/account/${otherUser.id}`);

        assert.dom(generateTestSelector('user-details-header', 'direct-message-button-chat')).hasText(t('messageChat'));

        await click(generateTestSelector('user-details-header', 'direct-message-button-chat'));
        assert.equal(currentURL(), `/message/chat/${otherUser.id}`);

        await dangerouslyVisit(`/account/${otherUser.id}`);

        assert.dom(generateTestSelector('user-details-header', 'direct-message-button-classic')).hasText(t('messageClassic'));

        await click(generateTestSelector('user-details-header', 'direct-message-button-classic'));
        await settled();

        let url = new URL(`${testApiBaseUrl}${currentURL()}`);
        let queryParams = Object.fromEntries(url.searchParams);
        assert.equal(queryParams.compose, 'direct');
        assert.equal(queryParams.directMode, 'classic');
        assert.equal(queryParams['initial-mentions'], `["${otherUser.acct}"]`);
        assert.equal(queryParams['initial-recipients'], `["${otherUser.id}"]`);
      });

      module('when authenticated', function() {
        test('renders a unfollow button if you are following that person', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');

          let otherUser = this.server.create('user', 'followed');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          assert.dom(generateTestSelector('user-details-header', 'unfollow-button')).hasText(t('unfollow'));

          await click(generateTestSelector('user-details-header', 'unfollow-button'));

          assert.dom(generateTestSelector('user-details-header', 'unfollow-button')).doesNotExist();
          assert.dom(generateTestSelector('user-details-header', 'follow-button')).exists();
        });
      });

      module('when not authenticated', function() {
        test('renders a remote follow button for local users', async function(assert) {
          let url = faker.internet.url();

          // no session needed because we are testing unauthenticated stuff.
          let user = this.server.create('user', {
            url,
          });

          await dangerouslyVisit(`/account/${user.id}`);

          let parsedUrl = new URL(url);
          let remoteFollowUrl = `${parsedUrl.protocol}//${parsedUrl.host}/main/ostatus`;

          assert.dom(generateTestSelector('user-details-header', 'remote-follow-form'))
            .hasAttribute('method', 'POST')
            .hasAttribute('action', remoteFollowUrl);

          assert.dom(generateTestSelector('user-details-header-remote-follow', 'nickname-input'))
            .hasAttribute('type', 'hidden')
            .hasAttribute('name', 'nickname')
            .hasAttribute('value', user.username);

          assert.dom(generateTestSelector('user-details-header-remote-follow', 'profile-input'))
            .hasAttribute('type', 'hidden')
            .hasAttribute('name', 'profile')
            .hasAttribute('value', '');

          assert.dom(generateTestSelector('user-details-header-remote-follow', 'button'))
            .hasText(t('remoteFollow'))
            .hasAttribute('type', 'submit');
        });

        test('renders a link to the users profile of non local users', async function(assert) {
          let user = this.server.create('user', {
            acct: `${faker.lorem.word()}@${faker.internet.domainName()}`,
          });

          await dangerouslyVisit(`/account/${user.id}`);

          assert.dom(generateTestSelector('user-details-header', 'remote-user-link'))
            .hasAttribute('href', user.url)
            .hasText(t('viewAccount'));
        });
      });

      module('header menu', function() {
        test('trigger has the correct props', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');
          let otherUser = this.server.create('user');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          assert.dom(generateTestSelector('user-details-header', 'menu-trigger'))
            .hasAria('label', t('openMenu'));
          assert.equal(find(generateTestSelector('user-details-header', 'menu-trigger')).nodeName, 'BUTTON');
        });

        test('content has the correct props', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');
          let otherUser = this.server.create('user');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header', 'menu')).exists();
          assert.equal(find(generateTestSelector('user-details-header', 'menu')).nodeName, 'UL');
        });

        test('you can block from the menu', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');
          let otherUser = this.server.create('user');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'block-button')).hasText(t('blockUser', { handle: otherUser.username }));

          await click(generateTestSelector('user-details-header-menu', 'block-button'));
          await settled();

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'block-button')).doesNotExist();
          assert.dom(generateTestSelector('user-details-header-menu', 'unblock-button')).exists();
        });

        test('you can unblock from the menu', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');
          let otherUser = this.server.create('user', 'blocked');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'unblock-button')).hasText(t('unblockUser', { handle: otherUser.username }));

          await click(generateTestSelector('user-details-header-menu', 'unblock-button'));
          await settled();

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'unblock-button')).doesNotExist();
          assert.dom(generateTestSelector('user-details-header-menu', 'block-button')).exists();
        });

        test('you can mute from the menu', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');
          let otherUser = this.server.create('user');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'mute-button')).hasText(t('muteUser', { handle: otherUser.username }));

          await click(generateTestSelector('user-details-header-menu', 'mute-button'));

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'mute-button')).doesNotExist();
          assert.dom(generateTestSelector('user-details-header-menu', 'unmute-button')).exists();
        });

        test('you can unmute from the menu', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');
          let otherUser = this.server.create('user', 'muted');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'unmute-button')).hasText(t('unmuteUser', { handle: otherUser.username }));

          await click(generateTestSelector('user-details-header-menu', 'unmute-button'));

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'unmute-button')).doesNotExist();
          assert.dom(generateTestSelector('user-details-header-menu', 'mute-button')).exists();
        });

        test('you can report a user from the menu', async function(assert) {
          this.server.create('user', 'withToken', 'withSession');
          let otherUser = this.server.create('user');

          await dangerouslyVisit(`/account/${otherUser.id}`);

          await click(generateTestSelector('user-details-header', 'menu-trigger'));

          assert.dom(generateTestSelector('user-details-header-menu', 'report-button')).doesNotExist();

          // Ensure feature is NOT enabled by default
          let featuresService = this.owner.lookup('service:features');
          assert.equal(featuresService.get('reportUsers'), false);

          // Enable it for test.
          enableFeature('reportUsers');
          await settled();

          assert.dom(generateTestSelector('user-details-header-menu', 'report-button')).hasText(t('reportUser', { handle: otherUser.username }));
        });
      });
    });

    test('Account subroute navbar has all the proper links', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let user = this.server.create('user');

      await dangerouslyVisit(`/account/${user.id}`);

      assert.dom(generateTestSelector('account-statuses-link')).hasText(t('statuses'));
      await click(generateTestSelector('account-statuses-link'));
      assert.equal(currentRouteName(), 'account.statuses');

      assert.dom(generateTestSelector('account-following-link')).hasText(t('following'));
      await click(generateTestSelector('account-following-link'));
      assert.equal(currentRouteName(), 'account.following');

      assert.dom(generateTestSelector('account-followers-link')).hasText(t('followers'));
      await click(generateTestSelector('account-followers-link'));
      assert.equal(currentRouteName(), 'account.followers');


      assert.dom(generateTestSelector('account-media-link')).hasText(t('media'));
      await click(generateTestSelector('account-media-link'));
      assert.equal(currentRouteName(), 'account.media');

      assert.dom(generateTestSelector('account-favorites-link')).hasText(t('favorites'));
      await click(generateTestSelector('account-favorites-link'));
      assert.equal(currentRouteName(), 'account.favorites');
    });
  });

  module('Account Statuses Route', function() {
    test('will render a list of that users statuses', async function(assert) {
      let user = this.server.create('user');

      this.server.createList('status', 10, {
        account: user,
      });

      await dangerouslyVisit(`/account/${user.id}/statuses`);

      assert.dom(generateTestSelector('status')).exists({ count: 10 });
    });

    test('opening the status gallery modal will preserve scroll position', async function(assert) {
      let user = this.server.create('user');

      this.server.createList('status', 20, {
        account: user,
      });

      let attachments = this.server.createList('attachment', 6);

      let status = this.server.create('status', {
        mediaAttachments: attachments,
        account: user,
      });

      await dangerouslyVisit('/feeds/all');

      find(generateTestSelector('status-id', status.id)).scrollIntoView();

      let currentScroll = document.getElementById('ember-testing-container').scrollY;

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
      await settled();

      assert.dom(generateTestSelector('attachment-gallery-modal')).exists();

      assert.equal(currentScroll, document.getElementById('ember-testing-container').scrollY, 'Scroll position is preserved');
    });

    test('opening the create status modal from a reply will preserve scroll position', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      this.server.createList('status', 20, {
        account: user,
      });

      let status = this.server.create('status', {
        account: user,
      });

      await dangerouslyVisit('/feeds/all');

      find(generateTestSelector('status-id', status.id)).scrollIntoView();

      let currentScroll = document.getElementById('ember-testing-container').scrollY;

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('reply-action-item')}`);
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();

      assert.equal(currentScroll, document.getElementById('ember-testing-container').scrollY, 'Scroll position is preserved');
    });

    test('if the user has no statuses, we show a nice callout', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/statuses`);

      assert.dom(generateTestSelector('no-statuses-callout')).hasText(t('userHasNoStatuses', { username: user.username }));
    });
  });

  module('Account Following Route', function() {
    test('will render a list of users', async function(assert) {
      this.server.createList('user', 7);
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/following`);

      assert.dom(generateTestSelector('profile-card')).exists({ count: 8 });
    });

    test('you can follow users from here', async function(assert) {
      let otherUser = this.server.create('user');

      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/following`);

      assert.dom(generateTestSelector('profile-card-action', 'follow')).hasText(`${t('follow')} ${t('atHandle', { handle: otherUser.username })}`);
      assert.dom(generateTestSelector('profile-card-action', 'unfollow')).doesNotExist();

      await click(generateTestSelector('profile-card-action', 'follow'));
      await settled();

      assert.dom(generateTestSelector('profile-card-action', 'unfollow')).exists();
      assert.dom(generateTestSelector('profile-card-action', 'unfollow')).hasText(`${t('unfollow')} ${t('atHandle', { handle: otherUser.username })}`);
      assert.dom(generateTestSelector('profile-card-action', 'follow')).doesNotExist();
    });

    test('you can unfollow users from here', async function(assert) {
      let otherUser = this.server.create('user', 'followed');

      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/following`);

      assert.dom(generateTestSelector('profile-card-action', 'follow')).doesNotExist();
      assert.dom(generateTestSelector('profile-card-action', 'unfollow')).hasText(`${t('unfollow')} ${t('atHandle', { handle: otherUser.username })}`);

      await click(generateTestSelector('profile-card-action', 'unfollow'));
      await settled();

      assert.dom(generateTestSelector('profile-card-action', 'unfollow')).doesNotExist();
      assert.dom(generateTestSelector('profile-card-action', 'follow')).exists();
      assert.dom(generateTestSelector('profile-card-action', 'follow')).hasText(`${t('follow')} ${t('atHandle', { handle: otherUser.username })}`);
    });

    test('if the user is not following anyone, we show a nice callout', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/following`);

      assert.dom(generateTestSelector('following-none-callout')).hasText(t('notFollowingAnyone', { username: user.username }));
    });
  });

  module('Account Followers Route', function() {
    test('will render a list of users', async function(assert) {
      this.server.createList('user', 7);
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/followers`);

      assert.dom(generateTestSelector('profile-card')).exists({ count: 8 });
    });

    test('you can follow users from here', async function(assert) {
      this.server.createList('user', 8);
      let otherUser = this.server.create('user');
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/followers`);

      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'follow')}`).hasText(`${t('follow')} ${t('atHandle', { handle: otherUser.username })}`);
      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'unfollow')}`).doesNotExist();

      await click(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'follow')}`);
      await settled();

      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'unfollow')}`).hasText(`${t('unfollow')} ${t('atHandle', { handle: otherUser.username })}`);
      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'follow')}`).doesNotExist();
    });

    test('you can unfollow users from here', async function(assert) {
      this.server.createList('user', 8);

      let otherUser = this.server.create('user', 'followed');

      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/followers`);

      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'follow')}`).doesNotExist();
      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'unfollow')}`).hasText(`${t('unfollow')} ${t('atHandle', { handle: otherUser.username })}`);

      await click(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'unfollow')}`);
      await settled();

      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'unfollow')}`).doesNotExist();
      assert.dom(`${generateTestSelector('profile-card')}${generateTestSelector('user-id', otherUser.id)} ${generateTestSelector('profile-card-action', 'follow')}`).hasText(`${t('follow')} ${t('atHandle', { handle: otherUser.username })}`);
    });

    test('if there are no followers, we show a nice callout', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/followers`);

      assert.dom(generateTestSelector('no-followers-callout')).hasText(t('noOneFollowingYet', { username: user.username }));
    });
  });

  module('Account Media Route', function() {
    test('will render a list of statuses from that user which contain attachments', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');
      let attachments = this.server.createList('attachment', 6);

      this.server.createList('status', 10, {
        mediaAttachments: attachments,
        account: user,
      });

      await dangerouslyVisit(`/account/${user.id}/media`);

      assert.dom(generateTestSelector('status')).exists({ count: 10 });
    });

    test('opening the status gallery modal will preserve scroll position', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');
      let attachments = this.server.createList('attachment', 6);

      this.server.createList('status', 20, {
        mediaAttachments: attachments,
        account: user,
      });

      let status = this.server.create('status', {
        mediaAttachments: attachments,
        account: user,
      });

      await dangerouslyVisit(`/account/${user.id}/media`);

      find(generateTestSelector('status-id', status.id)).scrollIntoView();

      let currentScroll = document.getElementById('ember-testing-container').scrollY;

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
      await settled();

      assert.dom(generateTestSelector('attachment-gallery-modal')).exists();

      assert.equal(currentScroll, document.getElementById('ember-testing-container').scrollY, 'Scroll position is preserved');
    });

    test('opening the create status modal from a reply will preserve scroll position', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');
      let attachments = this.server.createList('attachment', 6);

      this.server.createList('status', 20, {
        mediaAttachments: attachments,
        account: user,
      });

      let status = this.server.create('status', {
        mediaAttachments: attachments,
        account: user,
      });

      await dangerouslyVisit(`/account/${user.id}/media`);

      find(generateTestSelector('status-id', status.id)).scrollIntoView();

      let currentScroll = document.getElementById('ember-testing-container').scrollY;

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('reply-action-item')}`);
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();

      assert.equal(currentScroll, document.getElementById('ember-testing-container').scrollY, 'Scroll position is preserved');
    });

    test('if the user has no media, we show a nice callout', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/media`);

      assert.dom(generateTestSelector('no-media-callout')).hasText(t('noMediaYet', { username: user.username }));
    });
  });

  module('Account Favorites Route', function() {
    test('you can access the favorites route when unauthenticated', async function(assert) {
      let user = this.server.create('user');

      await dangerouslyVisit(`/account/${user.id}/favorites`);

      assert.equal(currentRouteName(), 'account.favorites');

      assert.dom(generateTestSelector('no-favorites-callout')).hasText(t('noFavorites', { username: user.username }));
    });

    test('you can access the favorites route of another user', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');
      let otherUser = this.server.create('user');

      await dangerouslyVisit(`/account/${otherUser.id}/favorites`);
      await settled();

      assert.dom(generateTestSelector('no-favorites-callout')).hasText(t('noFavorites', { username: otherUser.username }));
    });

    test('will render a list of statuses the user has favorited', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      this.server.createList('status', 10, {
        account: user,
      });

      await dangerouslyVisit(`/account/${user.id}/favorites`);

      assert.dom(generateTestSelector('status')).exists({ count: 10 });
    });

    test('opening the status gallery modal will preserve scroll position', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      this.server.createList('status', 20, {
        account: user,
      });

      let attachments = this.server.createList('attachment', 6);

      let status = this.server.create('status', {
        mediaAttachments: attachments,
        account: user,
      });

      await dangerouslyVisit(`/account/${user.id}/favorites`);

      find(generateTestSelector('status-id', status.id)).scrollIntoView();

      let currentScroll = document.getElementById('ember-testing-container').scrollY;

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
      await settled();

      assert.dom(generateTestSelector('attachment-gallery-modal')).exists();

      assert.equal(currentScroll, document.getElementById('ember-testing-container').scrollY, 'Scroll position is preserved');
    });

    test('opening the create status modal from a reply will preserve scroll position', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      this.server.createList('status', 20, {
        account: user,
      });

      let attachments = this.server.createList('attachment', 6);

      let status = this.server.create('status', {
        mediaAttachments: attachments,
        account: user,
      });

      await dangerouslyVisit(`/account/${user.id}/favorites`);

      find(generateTestSelector('status-id', status.id)).scrollIntoView();

      let currentScroll = document.getElementById('ember-testing-container').scrollY;

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('reply-action-item')}`);
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();

      assert.equal(currentScroll, document.getElementById('ember-testing-container').scrollY, 'Scroll position is preserved');
    });

    test('if the user has no favorites, we show a nice callout', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit(`/account/${user.id}/favorites`);

      assert.dom(generateTestSelector('no-favorites-callout')).hasText(t('noFavorites', { username: user.username }));
    });
  });
});

import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  fillIn,
  settled,
  triggerEvent,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { enableFeature } from 'ember-feature-flags/test-support';
import faker from 'faker';

module('Acceptance | search', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure search feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('search'), false);

    // Enable it for tests.
    enableFeature('search');
  });

  test('the clear button works', async function(assert) {
    await dangerouslyVisit('/search');
    await fillIn(generateTestSelector('main-search-input'), 'whatever');
    await click(generateTestSelector('main-search-clear'));
    assert.dom(generateTestSelector('main-search-input')).hasValue('');
  });

  test('header search is hidden when showing search results', async function(assert) {
    await dangerouslyVisit('/search');
    await fillIn(generateTestSelector('main-search-input'), 'whatever');
    await click(generateTestSelector('main-search-submit'));
    assert
      .dom(generateTestSelector('header-search'))
      .doesNotExist('header search should be hidden');
  });

  test('a simple search shows at least one status', async function(assert) {
    this.server.createList('status', 5);
    let status = this.server.schema.statuses.first();
    let word = status.content.match(/\s([^\s]+)\s/i)[1];
    await dangerouslyVisit('/search');
    await fillIn(generateTestSelector('main-search-input'), word);
    await click(generateTestSelector('main-search-submit'));
    assert
      .dom(generateTestSelector('search-result-status'))
      .exists('at least one status shows');
  });

  module('header search', function() {
    test('a simple search shows at least one status', async function(assert) {
      this.server.createList('status', 5);
      let status = this.server.schema.statuses.first();
      let word = status.content.match(/\s([^\s]+)\s/i)[1];
      await dangerouslyVisit('/');
      await fillIn(generateTestSelector('header-search-input'), word);
      await triggerEvent(generateTestSelector('header-search'), 'submit');
      assert
        .dom(generateTestSelector('search-result-status'))
        .exists('at least one status shows');
      assert.equal(1, 1);
    });
  });

  test('we push focus to the search result tab with results', async function(assert) {
    let content = faker.random.word().toLowerCase();

    this.server.createList('status', 3, { content });

    await dangerouslyVisit('/');
    await fillIn(generateTestSelector('header-search-input'), content);
    await triggerEvent(generateTestSelector('header-search'), 'submit');
    await settled();

    assert.equal(currentRouteName(), 'search.results.statuses', 'focus the statuses sub route when result contains statuses');

    this.server.db.emptyData();

    this.server.createList('user', 3, { username: content });

    await dangerouslyVisit('/');
    await fillIn(generateTestSelector('header-search-input'), content);
    await triggerEvent(generateTestSelector('header-search'), 'submit');
    await settled();

    assert.equal(currentRouteName(), 'search.results.people', 'focus the people route when the result does not contain statuses and DOES contain accounts');

    this.server.db.emptyData();

    this.server.createList('hashtag', 3, { name: content });

    await dangerouslyVisit('/');
    await fillIn(generateTestSelector('header-search-input'), content);
    await triggerEvent(generateTestSelector('header-search'), 'submit');
    await settled();

    assert.equal(currentRouteName(), 'search.results.hashtags', 'focus the hashtags subroute when the results do not contain people or statuses but DO contain hashtags');

    this.server.db.emptyData();

    await dangerouslyVisit('/');
    await fillIn(generateTestSelector('header-search-input'), content);
    await triggerEvent(generateTestSelector('header-search'), 'submit');
    await settled();

    assert.equal(currentRouteName(), 'search.results.statuses', 'focus the statuses tab with empty result sets');

    this.server.db.emptyData();
  });

  test('we only push focus to the first populated tab on the FIRST navigation within the search routes', async function(assert) {
    let content = faker.random.word().toLowerCase();

    this.server.createList('user', 3, { username: content });

    await dangerouslyVisit('/');
    await fillIn(generateTestSelector('header-search-input'), content);
    await triggerEvent(generateTestSelector('header-search'), 'submit');
    await settled();

    assert.equal(currentRouteName(), 'search.results.people', 'focus the people route when the result does not contain statuses and DOES contain accounts');

    this.server.db.emptyData();

    await click(generateTestSelector('search-results-statuses-link'));
    await settled();

    assert.equal(currentRouteName(), 'search.results.statuses');

    this.server.createList('user', 3, { username: content });

    await fillIn(generateTestSelector('main-search-input'), content);
    await triggerEvent(generateTestSelector('main-search-submit'), 'submit');

    assert.equal(currentRouteName(), 'search.results.statuses', 'does not push focus on the second request within the search routes');
  });
});

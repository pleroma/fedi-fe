import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { setupOnerror, resetOnerror, currentRouteName } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Acceptance | network errors', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('403 (forbidden) network error should result in the error route with forbidden language', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    let otherUser = this.server.create('user');

    await dangerouslyVisit('/feeds/home');

    this.server.get('/api/v1/accounts/:id', {}, 403);

    await dangerouslyVisit(`/account/${otherUser.id}`);

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('not-authenticated-error')).exists();

    resetOnerror();
  });

  test('401 (unauthorized) network errors should result in the error route with forbidden language', async function(assert) {
    this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    let otherUser = this.server.create('user');

    await dangerouslyVisit('/feeds/home');

    this.server.get('/api/v1/accounts/:id', {}, 401);

    await dangerouslyVisit(`/account/${otherUser.id}`);

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('not-authenticated-error')).exists();

    resetOnerror();
  });

  test('404 (not found) network errors should result in the error route with not found language', async function(assert) {
    this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    let otherUser = this.server.create('user');

    this.server.get('/api/v1/accounts/:id', {}, 404);

    await dangerouslyVisit('/feeds/home');

    await dangerouslyVisit(`/account/${otherUser.id}`);

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('not-found-error')).exists();

    resetOnerror();
  });

  test('500 (server error) network errors should result in the error route with server error language', async function(assert) {
    this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    let otherUser = this.server.create('user');

    this.server.get('/api/v1/accounts/:id', {}, 500);

    await dangerouslyVisit('/feeds/home');

    await dangerouslyVisit(`/account/${otherUser.id}`);

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('server-error')).exists();

    resetOnerror();
  });
});

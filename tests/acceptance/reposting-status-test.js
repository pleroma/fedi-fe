import { module, test, skip } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  resetOnerror,
  settled,
  setupOnerror,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Acceptance | reposting a status', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
    this.owner.setupRouter();
  });

  skip('can repost a status', async function(assert) {
    let store = this.owner.lookup('service:store');
    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/home');

    assert.ok(!this.server.db.statuses.find(status.id).reblogged);
    assert.dom(generateTestSelector('repost-action-item-button')).hasAttribute('title', t('repost'));

    assert.dom(generateTestSelector('repost-action-item-button')).exists();

    await click(generateTestSelector('repost-action-item-button'));
    await settled();

    assert.ok(this.server.db.statuses.find(status.id).reblogged);
    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('repost-action-item-button')}`).hasAttribute('title', t('undoRepost'));
    assert.dom(generateTestSelector('toast-message')).hasText(t('statusReposted'));

    let repost = this.server.db.statuses.findBy({ reblogId: status.id });
    let respostFromStore = await store.loadRecord('status', repost.id);

    assert.dom(`${generateTestSelector('status-reposted-by')} a`).hasAttribute('href', `/account/${respostFromStore.account.id}`);
    assert.dom(`${generateTestSelector('status-reposted-by')} a`).hasText(t('displayNameReposted', { displayName: respostFromStore.account.displayName }));
  });

  skip('can unrepost a status', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/home');

    assert.notOk(this.server.db.statuses.find(status.id).reblogged);

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('repost-action-item-button')}`);
    await settled();

    assert.ok(this.server.db.statuses.find(status.id).reblogged);

    assert.dom(generateTestSelector('status-id', status.id)).exists();
    assert.dom(generateTestSelector('status-id', this.server.db.statuses.find(status.id).reblogId)).exists();

    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('repost-action-item-button')}`).hasAttribute('title', t('undoRepost'));

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('repost-action-item-button')}`);
    await settled();

    assert.notOk(this.server.db.statuses.find(status.id).reblogged);
    assert.dom(generateTestSelector('status-id', this.server.db.statuses.find(status.id).reblogId)).doesNotExist();
    assert.dom(generateTestSelector('repost-action-item-button')).hasAttribute('title', t('repost'));
    assert.dom(generateTestSelector('toast-message')).hasText(t('statusUnreposted'));
  });

  test('will show the sign in modal with an appropriate reason if not signed in', async function(assert) {
    let user = this.server.create('user');

    this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('repost-action-item-button'));
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToRepostStatus'));

    assert.equal(currentURL(), '/feeds/all?auth=sign-in');
  });

  skip('will show a special toast with a view link', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/home');

    assert.ok(!this.server.db.statuses.find(status.id).reblogged);

    assert.dom(generateTestSelector('repost-action-item-button')).hasAttribute('title', t('repost'));

    assert.dom(generateTestSelector('repost-action-item-button')).exists();

    await click(generateTestSelector('repost-action-item-button'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'status-reposted')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('statusReposted'));
    assert.dom(generateTestSelector('status-reposted-toast-link'))
      .hasAttribute('href', `/status/${status.reblogId}`);
  });

  test('will show a toast on server error with the correct verbiage', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.post('/api/v1/statuses/:status_id/reblog', {}, 500);

    let user = this.server.create('user', 'withToken', 'withSession');

    this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('repost-action-item-button'));
    await settled();

    assert.dom(generateTestSelector('toast-message')).hasText(t('errorRepostingTryAgain'));

    resetOnerror();
  });
});

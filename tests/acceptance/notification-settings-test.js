import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { currentRouteName, click } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | notification-settings', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure search feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('userSettings'), false);

    // Enable it for tests.
    enableFeature('userSettings');
  });

  test('the submit button is disabled without any changes', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/notifications');

    assert
      .dom(generateTestSelector('save-notification-settings'))
      .isDisabled();
  });

  test('the checkbox logic works as expected', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/notifications');

    assert
      .dom(generateTestSelector('block-notifications-from-strangers'))
      .isChecked();

    await click(generateTestSelector('block-notifications-from-strangers'));

    assert
      .dom(generateTestSelector('block-notifications-from-strangers'))
      .isNotChecked();

    assert
      .dom(generateTestSelector('hide-notification-contents'))
      .isChecked();

    await click(generateTestSelector('hide-notification-contents'));

    assert
      .dom(generateTestSelector('hide-notification-contents'))
      .isNotChecked();
  });

  test('save works as expected', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/notifications');

    await click(generateTestSelector('block-notifications-from-strangers'));
    await click(generateTestSelector('hide-notification-contents'));

    await click(generateTestSelector('save-notification-settings'));

    assert.dom(generateTestSelector('toast-type', 'success')).hasText(t('notificationSettingsHaveBeenSaved'));
    assert
      .dom(generateTestSelector('save-notification-settings'))
      .isDisabled();
    assert.equal(currentRouteName(), 'settings.notifications', 'not redirected away');
  });
});

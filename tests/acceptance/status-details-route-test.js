import { module, test, skip } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { currentRouteName } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | status details route', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure statusDetails feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('statusDetails'), false);

    // Enable it for tests.
    enableFeature('statusDetails');
  });

  test('You can see the status details', async function(assert) {
    let status = this.server.create('status');

    await dangerouslyVisit(`/notice/${status.id}`);

    assert.equal(currentRouteName(), 'status.details');

    assert.dom(generateTestSelector('status-id', status.id)).exists();
  });

  skip('will scroll to the center item', async function() {

  });

  skip('Viewing visibility:direct statuses', async function() {
    // when not signed in or not the persons in the conversation
  });

  skip('Viewing visibility:private or followers-only statuses', async function() {
    // when not signed in or not a follower of the person
  });

  skip('Viewing visibility:private or followers-only statuses', async function() {
    // when not signed in or not a follower of the person
  });

  skip('When status does not exist, error is thrown and we end up on the error page with not found language', async function() {

  });

  skip('You cannot see private statuses when not signed in', async function() {

  });

  skip('When a new status is created, will add it to the conversation', async function() {

  });

  skip('When a status is deleted, will remove it from the conversation', async function() {

  });

  skip('Will long poll the status conversation', async function() {

  });

  skip('Will stop long-polling when exiting the route', async function() {

  });
});

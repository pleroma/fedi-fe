import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  fillIn,
  resetOnerror,
  settled,
  setupOnerror,
  triggerEvent,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import register from 'pleroma-pwa/tests/helpers/register';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import faker from 'faker';
import humanizeFileSize from 'pleroma-pwa/utils/humanize-file-size';

module('Acceptance | onboarding', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');

    enableFeature('registration');
  });

  module('avatar upload', function() {
    test('after registration, we show the avatar upload form', async function(assert) {
      await dangerouslyVisit('/feeds/public');

      await register();

      assert.dom(generateTestSelector('onboarding-avatar-form')).exists();
    });

    test('you can close this avatar form without being blocked', async function(assert) {
      await dangerouslyVisit('/feeds/public');

      await register();

      assert.dom(generateTestSelector('onboarding-avatar-form')).exists();

      await click(generateTestSelector('close-modal-button'));

      assert.dom(generateTestSelector('onboarding-avatar-form')).doesNotExist();
      assert.dom(generateTestSelector('auth-modal')).doesNotExist();
    });

    test('we do not show the back button when rendering the avatar form', async function(assert) {
      await dangerouslyVisit('/feeds/public');

      await register();

      assert.dom(generateTestSelector('onboarding-avatar-form')).exists();

      assert.dom(generateTestSelector('auth-modal-back-button')).doesNotExist();
    });

    test('uploading an avatar will update the session users avatar', async function(assert) {
      let session = this.owner.lookup('service:session');

      await dangerouslyVisit('/feeds/public');

      await register();

      assert.notOk(session.currentUser.avatar);

      assert.dom(generateTestSelector('onboarding-avatar-form')).exists();

      await triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['abcd'], 'test-image.jpg', { type: 'jpg' })] },
      );
      await settled();

      assert.ok(session.currentUser.avatar);

      assert.dom(generateTestSelector('avatar-form', 'avatar-image')).hasAttribute('src', session.currentUser.avatar);
    });

    test('you can clear your avatar', async function(assert) {
      let session = this.owner.lookup('service:session');

      await dangerouslyVisit('/feeds/public');

      await register();

      await triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['abcd'], 'test-image.jpg', { type: 'jpg' })] },
      );
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'clear-avatar-button')).exists();

      await click(generateTestSelector('avatar-form', 'clear-avatar-button'));
      await settled();

      assert.notOk(session.currentUser.avatar);
    });

    test('validates the file size agains the session', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      let session = this.owner.lookup('service:session');
      let instances = this.owner.lookup('service:instances');

      await instances.refreshTask.perform();

      await dangerouslyVisit('/feeds/public');

      await register();

      assert.notOk(session.currentUser.avatar);

      assert.dom(generateTestSelector('onboarding-avatar-form')).exists();

      await triggerEvent(
        generateTestSelector('avatar-form', 'file-input'),
        'change',
        { files: [new File(['a'.repeat(instances.current.avatarUploadLimit + 1)], 'test-image.jpg', { type: 'jpg' })] },
      );
      await settled();

      assert.dom(generateTestSelector('avatar-form', 'file-selected-error'))
        .hasText(`Error: ${t('fileTooLarge', { max: humanizeFileSize(instances.current.avatarUploadLimit) })}`);

      resetOnerror();
    });
  });

  module('bio form', function() {
    test('you can update your bio after registration', async function(assert) {
      await dangerouslyVisit('/feeds/public');

      await register();

      await click(generateTestSelector('avatar-form', 'skip-button'));

      assert.dom(generateTestSelector('onboarding-bio-form')).exists();
    });

    test('you can close the bio form without being blocked', async function(assert) {
      await dangerouslyVisit('/feeds/public');

      await register();

      await click(generateTestSelector('avatar-form', 'skip-button'));

      assert.dom(generateTestSelector('onboarding-bio-form')).exists();

      await click(generateTestSelector('close-modal-button'));

      assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
      assert.dom(generateTestSelector('auth-modal')).doesNotExist();
    });

    test('the back button will appear, and take you back to the avatar form', async function(assert) {
      await dangerouslyVisit('/feeds/public');

      await register();

      await click(generateTestSelector('avatar-form', 'skip-button'));

      assert.dom(generateTestSelector('onboarding-bio-form')).exists();

      assert.dom(generateTestSelector('auth-modal-back-button')).exists();

      await click(generateTestSelector('auth-modal-back-button'));

      assert.dom(generateTestSelector('onboarding-avatar-form')).exists();
      assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();
    });

    test('updating a bio will update the sessions current user', async function(assert) {
      let sentence = faker.lorem.sentence();
      let session = this.owner.lookup('service:session');

      await dangerouslyVisit('/feeds/public');

      await register();

      await click(generateTestSelector('avatar-form', 'skip-button'));

      assert.dom(generateTestSelector('onboarding-bio-form')).exists();

      await fillIn(generateTestSelector('bio-form', 'input'), sentence);
      await click(generateTestSelector('bio-form', 'submit-button'));
      await settled();

      assert.dom(generateTestSelector('onboarding-bio-form')).doesNotExist();

      assert.equal(session.currentUser.bio, sentence);
    });
  });

  module('from the stand-alone sign-up page', async function() {
    test('after registration, takes you to the home feed with the onboarding avatar form open', async function(assert) {
      await dangerouslyVisit('/sign-up');

      await register();

      assert.dom(generateTestSelector('onboarding-avatar-form')).exists();

      assert.dom(generateTestSelector('auth-modal')).exists();

      assert.equal(currentURL(), '/feeds/home?auth=onboarding-avatar-form');
    });
  });
});

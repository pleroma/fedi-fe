import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  resetOnerror,
  settled,
  setupOnerror,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Acceptance | favoriting a status', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('can favorite a status', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    assert.ok(!this.server.db.statuses.find(status.id).favourited);
    assert.dom(generateTestSelector('favorite-action-item-button')).hasAttribute('title', t('favorite'));

    assert.dom(generateTestSelector('favorite-action-item-button')).exists();

    await click(generateTestSelector('favorite-action-item-button'));
    await settled();

    assert.ok(this.server.db.statuses.find(status.id).favourited);
    assert.dom(generateTestSelector('favorite-action-item-button')).hasAttribute('title', t('unfavorite'));
  });

  test('can unfavorite a status', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    let status = this.server.create('status', {
      account: user,
      favourited: true,
    });

    await dangerouslyVisit('/feeds/all');

    assert.ok(this.server.db.statuses.find(status.id).favourited);
    assert.dom(generateTestSelector('favorite-action-item-button')).hasAttribute('title', t('unfavorite'));

    assert.dom(generateTestSelector('favorite-action-item-button')).exists();

    await click(generateTestSelector('favorite-action-item-button'));
    await settled();

    assert.ok(!this.server.db.statuses.find(status.id).favourited);
    assert.dom(generateTestSelector('favorite-action-item-button')).hasAttribute('title', t('favorite'));
  });

  test('will show the sign in modal with an appropriate reason if not signed in', async function(assert) {
    let user = this.server.create('user');

    this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('favorite-action-item-button'));
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToFavoriteStatus'));

    assert.equal(currentURL(), '/feeds/all?auth=sign-in');
  });

  test('will show a toast on server error with the correct verbiage', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.post('/api/v1/statuses/:status_id/favourite', {}, 500);

    let user = this.server.create('user', 'withToken', 'withSession');

    this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('favorite-action-item-button'));
    await settled();

    assert.dom(generateTestSelector('toast-message')).hasText(t('errorFavoritingTryAgain'));

    resetOnerror();
  });
});

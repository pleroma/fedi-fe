import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { click, currentURL, settled, currentRouteName } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { set } from '@ember/object';
import { currentSession } from 'ember-simple-auth/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | sidebar', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');
  });

  test('when authenticated you can click on the users profile card module to nav to their account details', async function(assert) {
    this.user = this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('username-link'))
      .hasAttribute('href', `/account/${this.user.id}`)
      .hasAttribute('title', t('viewProfile'));

    await click(generateTestSelector('username-link'));

    assert.equal(currentRouteName(), 'account.statuses');

    assert.equal(currentURL(), `/account/${this.user.id}/statuses`);
  });

  test('becoming authenticated, will reveal the private feed links', async function(assert) {
    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('sidebar-nav')).exists();

    assert.dom(generateTestSelector('feed-link-public')).exists();
    assert.dom(generateTestSelector('feed-link-all')).exists();

    this.user = this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('feed-link-home')).exists();
    assert.dom(generateTestSelector('feed-link-dms-classic')).exists();
    assert.dom(generateTestSelector('feed-link-dms-chat')).exists();
    assert.dom(generateTestSelector('feed-link-notifications')).exists();
    assert.dom(generateTestSelector('feed-link-public')).exists();
    assert.dom(generateTestSelector('feed-link-all')).exists();
  });

  test('when authenticated, sidebar items will display a unread count when the feeds unread count is set', async function(assert) {
    this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('sidebar-nav')).exists();

    let unreadCountsService = this.owner.lookup('service:unreadCounts');

    set(unreadCountsService, 'unreadNotificationsCount', 10);

    await settled();

    assert.dom(generateTestSelector('feed-link-notifications')).hasClass('menu-list__item-link--badge');
    assert.dom(`${generateTestSelector('feed-link-notifications')} .menu-list__item-title`).hasAttribute('data-count', '10');
  });

  test('when authenticated, will reveal a sign-out link', async function(assert) {
    this.server.create('user', {
      password: 'p@55w0rd',
      username: 'John',
    }, 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('sidebar-profile-sign-out-button')).hasAttribute('href', '/sign-out');

    await click(generateTestSelector('sidebar-profile-sign-out-button'));
    await settled();

    assert.notOk(currentSession().isAuthenticated);
  });

  test('attempting authentication will close the sidebar', async function(assert) {
    let offCanvasNavService = this.owner.lookup('service:off-canvas-nav');

    await dangerouslyVisit('/feeds/all');

    await offCanvasNavService.open();
    assert.ok(offCanvasNavService.isOpen);
    await click(generateTestSelector('account-cta-create-account-button'));
    assert.ok(offCanvasNavService.isClosed);

    await click(generateTestSelector('close-modal-button'));

    await offCanvasNavService.open();
    assert.ok(offCanvasNavService.isOpen);
    await click(generateTestSelector('account-cta-sign-in-button'));
    assert.ok(offCanvasNavService.isClosed);

    await click(generateTestSelector('close-modal-button'));

    await offCanvasNavService.open();
    assert.ok(offCanvasNavService.isOpen);
    await click(generateTestSelector('sidebar-sign-in'));
    assert.ok(offCanvasNavService.isClosed);

    await click(generateTestSelector('close-modal-button'));

    await offCanvasNavService.open();
    assert.ok(offCanvasNavService.isOpen);
    await click(generateTestSelector('sidebar-sign-up'));
    assert.ok(offCanvasNavService.isClosed);
  });
});

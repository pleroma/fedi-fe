import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  resetOnerror,
  settled,
  setupOnerror,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | blocking users', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
    enableFeature('userProfiles');
  });

  module('blocking a user', function() {
    test('shows a toast', async function(assert) {
      let user1 = this.server.create('user', 'withToken', 'withSession');
      this.server.createList('status', 5, {
        account: user1,
      });

      let user2 = this.server.create('user');
      let statusFromOtherUser = this.server.create('status', {
        account: user2,
      });

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'block-user'));
      await settled();

      assert.dom(generateTestSelector('toast-message')).hasText(t('userBlocked', { username: user2.username }));
    });

    test('removes that users statuses from feeds', async function(assert) {
      let user1 = this.server.create('user', 'withToken', 'withSession');
      this.server.createList('status', 5, {
        account: user1,
      });

      let user2 = this.server.create('user');
      let statusFromOtherUser = this.server.create('status', {
        account: user2,
      });

      await dangerouslyVisit('/feeds/all');

      assert.dom(generateTestSelector('status-id', statusFromOtherUser.id)).exists();

      await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'block-user'));
      await settled();

      assert.dom(generateTestSelector('status-id', statusFromOtherUser.id)).doesNotExist();
    });

    test('shows the sign in modal with a good reason when attempting and not signed in', async function(assert) {
      let userActionsService = this.owner.lookup('service:user-actions');

      let user = this.server.create('user');
      this.server.createList('status', 5, {
        account: user,
      });

      await dangerouslyVisit('/feeds/all');

      await userActionsService.blockUser.perform(user);
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();
      assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToBlockUser'));
    });

    test('shows good errors when blocking fails', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.post('/api/v1/accounts/:account_id/block', {}, 500);

      let user1 = this.server.create('user', 'withToken', 'withSession');
      this.server.createList('status', 5, {
        account: user1,
      });

      let user2 = this.server.create('user');
      let statusFromOtherUser = this.server.create('status', {
        account: user2,
      });

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'block-user'));
      await settled();

      assert.dom(generateTestSelector('toast-type', 'error')).exists({ count: 1 });
      assert.dom(generateTestSelector('toast-message')).hasText(t('errorBlockingUserTryAgain'));

      resetOnerror();
    });

    test('you can unblock a user from the user blocked toast', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let user2 = this.server.create('user');

      await dangerouslyVisit(`/account/${user2.id}`);

      await click(generateTestSelector('user-details-header', 'menu-trigger'));
      await click(generateTestSelector('user-details-header-menu', 'block-button'));

      assert.dom(generateTestSelector('toast-message')).hasText(t('userBlocked', { username: user2.username }));

      await click(generateTestSelector('user-blocked-unblock-button'));
      await settled();

      assert.dom(generateTestSelector('toast-type', 'user-unblocked')).exists();
      assert.dom(generateTestSelector('toast-message')).hasText(t('userUnblocked', { username: user2.username }));
    });
  });

  module('unblocking a user', function() {
    test('shows the sign in modal with a good reason when attempting and not signed in', async function(assert) {
      let userActionsService = this.owner.lookup('service:user-actions');

      let user = this.server.create('user');

      await dangerouslyVisit('/feeds/all');

      await userActionsService.unblockUser.perform(user);
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();
      assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToUnblockUser'));
    });

    test('shows good errors when blocking fails', async function(assert) {
      let userActionsService = this.owner.lookup('service:user-actions');

      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.post('/api/v1/accounts/:account_id/unblock', {}, 500);

      this.server.create('user', 'withToken', 'withSession');

      let user2 = this.server.create('user');

      await dangerouslyVisit('/feeds/all');

      await userActionsService.unblockUser.perform(user2);
      await settled();

      assert.dom(generateTestSelector('toast-type', 'error')).exists({ count: 1 });
      assert.dom(generateTestSelector('toast-message')).hasText(t('errorUnblockingUserTryAgain'));

      resetOnerror();
    });

    test('you can block a user from the user unblocked toast', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let user2 = this.server.create('user');

      await dangerouslyVisit(`/account/${user2.id}`);

      await click(generateTestSelector('user-details-header', 'menu-trigger'));
      await click(generateTestSelector('user-details-header-menu', 'block-button'));
      await settled();

      await click(generateTestSelector('user-blocked-unblock-button'));
      await settled();

      await click(generateTestSelector('user-unblocked-block-button'));
      await settled();

      assert.dom(generateTestSelector('toast-type', 'user-blocked')).exists();
    });
  });
});

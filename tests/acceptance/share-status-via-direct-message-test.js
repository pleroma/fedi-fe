import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  find,
  settled,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import selectChoose from 'pleroma-pwa/tests/helpers/select-choose';

module('Acceptance | share status via direct message', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.isEnabled('directMessages'), false, 'direct messages feature should be off at first');

    // Enable it for tests.
    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');
  });

  test('you can share a status via classic direct message from the feed-item header', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let otherUser = this.server.create('user');
    let status = this.server.create('status', {
      account: otherUser,
    });

    // fake a recent conversation with another person
    let thirdUser = this.server.create('user');
    this.server.create('conversation', { accounts: [thirdUser] });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'share-status'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    assert.dom(generateTestSelector('visibility-select')).hasValue('directMessageClassic');

    let textarea = find(generateTestSelector('create-status-body'));

    assert.dom(textarea).exists('textarea exists');
    assert.ok(textarea.value.includes('@ '), 'should have plain at symbol');
    assert.ok(textarea.value.includes(`/notice/${status.id}`), 'should link to given status');
    assert.ok(textarea.selectionStart, 1, 'selection should start right after at symbol');
    assert.ok(textarea.selectionEnd, 1, 'selection should end right after at symbol');
  });

  test('you can share a status via chat direct message from the feed-item header', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let otherUser = this.server.create('user');
    let status = this.server.create('status', {
      account: otherUser,
    });

    // fake a recent chat with another person
    let thirdUser = this.server.create('user');
    let chat = this.server.create('chat', { account: thirdUser });
    this.server.create('chat-message', { chat });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'share-status'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    let dropdown = find(generateTestSelector('visibility-select'));
    assert.dom(dropdown).hasValue('directMessageClassic');
    selectChoose(dropdown, 'directMessageChat');
    await settled();

    // resume recent conversation to share link
    await click(generateTestSelector('recent-conversation'));
    await settled();

    assert.equal(currentRouteName(), 'message.chat');

    assert.ok(find(generateTestSelector('create-direct-message', 'message-body-input')).value.includes(`/notice/${status.id}`));
  });

  test('you can share a status via direct message from the feed-item actions share menu', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let otherUser = this.server.create('user');
    let status = this.server.create('status', {
      account: otherUser,
    });

    // fake a recent conversation with another person
    let thirdUser = this.server.create('user');
    this.server.create('conversation', { accounts: [thirdUser] });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)} ${generateTestSelector('status-share-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('status-share-menu')).exists();

    await click(generateTestSelector('status-share-menu-item', 'share-status'));
    await settled();

    assert.dom(generateTestSelector('create-status-modal')).exists();

    assert.dom(generateTestSelector('visibility-select')).hasValue('directMessageClassic');

    let textarea = find(generateTestSelector('create-status-body'));

    assert.dom(textarea).exists('textarea exists');
    assert.ok(textarea.value.includes('@ '), 'should have plain at symbol');
    assert.ok(textarea.value.includes(`/notice/${status.id}`), 'should link to given status');
    assert.ok(textarea.selectionStart, 1, 'selection should start right after at symbol');
    assert.ok(textarea.selectionEnd, 1, 'selection should end right after at symbol');
  });
});

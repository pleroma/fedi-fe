import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { setupOnerror, resetOnerror, settled, currentRouteName } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Acceptance | error routing', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('visiting a route that requires authentication when not signed in should result in the error page with non authenticated language', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    await dangerouslyVisit('/notifications');

    assert.equal(currentRouteName(), 'sign-in');

    resetOnerror();
  });

  test('when server returns 404(not found) should result in the error page with not found language', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.get('/api/v1/accounts/:id', () => ({
      message: ['We could not find this user'],
    }), 404);

    await dangerouslyVisit('/account/blah_blah');

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('not-found-error')).exists();

    resetOnerror();
  });

  test('when server returns 500(server error) should result in the error page with server error language', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.get('/api/v1/accounts/:id', () => ({
      message: ['The serve is down!'],
    }), 500);

    await dangerouslyVisit('/account/blah_blah');

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('server-error')).exists();
  });

  test('when server returns 403(forbidden) should result in the error page with server forbidden language', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');
    let otherUser = this.server.create('user');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.get('/api/v1/accounts/:id', () => ({
      message: ['Not authenticated'],
    }), 403);

    await dangerouslyVisit(`/account/${otherUser.id}`);

    await settled();

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('not-authenticated-error')).exists();
  });

  test('when visiting a route that does not exist we should see not found language', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    await dangerouslyVisit('/alabamo-slamo-ramo');

    assert.equal(currentRouteName(), 'error');

    assert.dom(generateTestSelector('not-found-error')).exists();
  });
});

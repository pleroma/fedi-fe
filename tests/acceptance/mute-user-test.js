import { module, skip, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  resetOnerror,
  settled,
  setupOnerror,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | mute user', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
    enableFeature('userProfiles');
  });

  module('muting a user', function() {
    test('shows a toast', async function(assert) {
      let user1 = this.server.create('user', 'withToken', 'withSession');
      this.server.createList('status', 5, {
        account: user1,
      });

      let user2 = this.server.create('user');
      let statusFromOtherUser = this.server.create('status', {
        account: user2,
      });

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'mute-user'));
      await settled();

      assert.dom(generateTestSelector('toast-message')).hasText(t('userMuted', { username: user2.username }));
    });

    skip('from a feed that hidesMutedStatuses, it removes that users statuses in feeds', async function(assert) {
      let user1 = this.server.create('user', 'withToken', 'withSession');
      this.server.createList('status', 5, {
        account: user1,
      });

      let user2 = this.server.create('user');
      let statusFromOtherUser = this.server.create('status', {
        account: user2,
      });

      await dangerouslyVisit('/feeds/home');

      assert.dom(generateTestSelector('status-id', statusFromOtherUser.id)).exists();

      await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'mute-user'));
      await settled();

      assert.dom(generateTestSelector('status-id', statusFromOtherUser.id)).doesNotExist();
    });

    test('from a feed that allows muted status, collapses users statuses in feeds', async function(assert) {
      let user1 = this.server.create('user', 'withToken', 'withSession');
      this.server.createList('status', 5, {
        account: user1,
      });

      let user2 = this.server.create('user');
      let statusFromOtherUser = this.server.create('status', {
        account: user2,
      });

      await dangerouslyVisit('/feeds/all');

      assert.dom(generateTestSelector('status-id', statusFromOtherUser.id)).exists();

      await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'mute-user'));
      await settled();

      assert.dom(generateTestSelector('status-id', statusFromOtherUser.id)).hasClass('feed-item--muted');
    });

    test('shows the sign in modal with a good reason when attempting and not signed in', async function(assert) {
      let userActionsService = this.owner.lookup('service:user-actions');

      let user = this.server.create('user');
      this.server.createList('status', 5, {
        account: user,
      });

      await dangerouslyVisit('/feeds/all');

      await userActionsService.muteUser.perform(user);
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();
      assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToMuteUser'));
    });

    test('shows good errors when muting fails', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.post('/api/v1/accounts/:account_id/mute', {}, 500);

      let user1 = this.server.create('user', 'withToken', 'withSession');
      this.server.createList('status', 5, {
        account: user1,
      });

      let user2 = this.server.create('user');
      let statusFromOtherUser = this.server.create('status', {
        account: user2,
      });

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status-id', statusFromOtherUser.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'mute-user'));
      await settled();

      assert.dom(generateTestSelector('toast-type', 'error')).exists({ count: 1 });
      assert.dom(generateTestSelector('toast-message')).hasText(t('errorMutingUserTryAgain', { username: user2.username }));

      resetOnerror();
    });

    test('you can unmute a user from the user muted toast', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let user2 = this.server.create('user');

      await dangerouslyVisit(`/account/${user2.id}`);

      await click(generateTestSelector('user-details-header', 'menu-trigger'));
      await click(generateTestSelector('user-details-header-menu', 'mute-button'));

      assert.dom(generateTestSelector('toast-message')).hasText(t('userMuted', { username: user2.username }));

      await click(generateTestSelector('user-muted-unmute-button'));
      await settled();

      assert.dom(generateTestSelector('toast-type', 'user-unmuted')).exists();
      assert.dom(generateTestSelector('toast-message')).hasText(t('userUnmuted', { username: user2.username }));
    });

    test('statuses from muted users get a special class', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');
      let account = this.server.create('user', 'muted');

      let status = this.server.create('status', {
        account,
      });

      await dangerouslyVisit('/feeds/all');

      assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)}`)
        .hasClass('feed-item--muted');
    });

    test('you can unmute a user from its collapsed (muted) state', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');
      let account = this.server.create('user', 'muted');

      let status = this.server.create('status', {
        account,
      });

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-unmute')}`);
      await settled();

      assert.dom(`${generateTestSelector('status')}${generateTestSelector('status-id', status.id)}`)
        .doesNotHaveClass('feed-item--muted');
    });
  });

  module('unmuting a user', function() {
    test('shows the sign in modal with a good reason when attempting and not signed in', async function(assert) {
      let userActionsService = this.owner.lookup('service:user-actions');

      let user = this.server.create('user');

      await dangerouslyVisit('/feeds/all');

      await userActionsService.unmuteUser.perform(user);
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();
      assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToUnmuteUser'));
    });

    test('shows good errors when unmuting fails', async function(assert) {
      let userActionsService = this.owner.lookup('service:user-actions');

      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.post('/api/v1/accounts/:account_id/unmute', {}, 500);

      this.server.create('user', 'withToken', 'withSession');

      let user2 = this.server.create('user');

      await dangerouslyVisit('/feeds/all');

      await userActionsService.unmuteUser.perform(user2);
      await settled();

      assert.dom(generateTestSelector('toast-type', 'error')).exists({ count: 1 });
      assert.dom(generateTestSelector('toast-message')).hasText(t('errorUnmutingUserTryAgain', { username: user2.username }));

      resetOnerror();
    });
  });
});

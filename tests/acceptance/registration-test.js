import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import register from 'pleroma-pwa/tests/helpers/register';
import { setupMirage } from 'ember-cli-mirage/test-support';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  currentURL,
  fillIn,
  resetOnerror,
  settled,
  setupOnerror,
} from '@ember/test-helpers';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import faker from 'faker';
import sinon from 'sinon';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | Registration', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');

    enableFeature('registration');
  });

  test('you can access the registration form from the sign-in modal', async function(assert) {
    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('account-cta')} ${generateTestSelector('account-cta-sign-in-button')}`);
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-form')).exists();
    await click(`${generateTestSelector('sign-in-form')} ${generateTestSelector('register-button')}`);
    await settled();

    assert.dom(generateTestSelector('registration-form')).exists();
  });

  test('you can access the registration form from the sidebar account cta', async function(assert) {
    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('account-cta')} ${generateTestSelector('account-cta-create-account-button')}`);
    await settled();

    assert.dom(generateTestSelector('registration-form')).exists();
  });

  test('you can access the registration form from the sign-up route', async function(assert) {
    await dangerouslyVisit('/sign-up');

    assert.dom(generateTestSelector('registration-form')).exists();
  });

  module('success', function() {
    test('happy path', async function(assert) {
      await register();

      assert.equal(currentURL(), '/feeds/home?auth=onboarding-avatar-form');
    });

    test('if registration occurs on a non authenticated page, it keeps you where you are, but signed in', async function(assert) {
      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('account-cta-create-account-button'));
      await settled();

      await register();

      assert.equal(currentURL(), '/feeds/all?auth=onboarding-avatar-form');
    });

    test('if registration occurs while doing something that requires authentication, allows you to pass through, but signed in', async function(assert) {
      this.server.createList('status', 5);

      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('favorite-action-item-button'));
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();

      await click(generateTestSelector('register-button'));
      await settled();

      assert.dom(generateTestSelector('registration-form')).exists();

      await register();

      assert.equal(currentURL(), '/feeds/all?auth=onboarding-avatar-form');
    });
  });

  module('failure', function() {
    test('Will display errors when the form is incomplete', async function(assert) {
      await register({});

      assert.dom(generateTestSelector('registration-error')).exists();

      assert.dom(generateTestSelector('registration-error'))
        .containsText(t('youMustEnterDisplayName'))
        .containsText(t('youMustEnterEmail'))
        .containsText(t('youMustEnterPassword'))
        .containsText(t('youMustEnterUsername'))
        .doesNotContainText(t('yourEmailAddressNotValid'));
    });

    test('Will display errors if server returns errors', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      let error = 'too much information';

      this.server.post('/api/v1/accounts', { error }, 403);

      await register();

      assert.equal(currentRouteName(), 'sign-up');

      assert.dom(generateTestSelector('registration-error')).hasText(error);

      resetOnerror();
    });
  });

  module('abandoning create user', function() {
    module('having edited the form', function() {
      test('will show a confirm modal', async function(assert) {
        let newConfirm = sinon.fake();
        let oldConfirm = window.confirm;

        window.confirm = newConfirm;

        await dangerouslyVisit('/sign-up');

        fillIn(generateTestSelector('username-input'), faker.lorem.word());
        await settled();

        await click(generateTestSelector('close-modal-button'));

        assert.ok(window.confirm.calledOnce);

        window.confirm = oldConfirm;
      });

      test('clicking confirm should allow you to exit', async function(assert) {
        let newConfirm = sinon.fake.returns(true);
        let oldConfirm = window.confirm;

        window.confirm = newConfirm;

        await dangerouslyVisit('/sign-up');

        fillIn(generateTestSelector('username-input'), faker.lorem.word());
        await settled();

        await click(generateTestSelector('close-modal-button'));

        assert.equal(currentURL(), '/feeds/all');

        window.confirm = oldConfirm;
      });

      test('clicking cancel will return you to the form when canceling abandonment', async function(assert) {
        let newConfirm = sinon.fake.returns(false);
        let oldConfirm = window.confirm;

        window.confirm = newConfirm;

        await dangerouslyVisit('/sign-up');

        fillIn(generateTestSelector('username-input'), faker.lorem.word());
        await settled();

        await click(generateTestSelector('close-modal-button'));

        assert.equal(currentURL(), '/sign-up?auth=register');

        window.confirm = oldConfirm;
      });
    });

    test('without editing anything should take you back where you started', async function(assert) {
      await dangerouslyVisit('/sign-up');

      await dangerouslyVisit('/feeds/public');

      assert.equal(currentURL(), '/feeds/public');
    });
  });
});

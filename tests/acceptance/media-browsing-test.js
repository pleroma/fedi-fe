import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  find,
  resetOnerror,
  settled,
  setupOnerror,
  triggerKeyEvent,
  waitUntil,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | media browsing', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');

    enableFeature('directMessages');
    enableFeature('showDirectMessagesClassic');
    enableFeature('statusDetails');
    enableFeature('statusMedia');
    enableFeature('userProfiles');
  });

  test('attempting to view a status gallery and the status does not exist, we land on the parent feed route', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    await dangerouslyVisit('/feeds/all?gallery-attachment=fake-id&gallery-subject=fake-status-id');
    await settled();

    assert.equal(currentURL(), '/feeds/all');

    resetOnerror();
  });

  test('should open the gallery into the correct image', async function(assert) {
    let attachments = this.server.createList('attachment', 6);
    let status = this.server.create('status', {
      mediaAttachments: attachments,
    });
    let attachmentId = attachments[2].id;

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachmentId)}`);
    await settled();

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachmentId}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-modal')).exists();
    assert.dom(generateTestSelector('attachment-gallery')).exists();

    let parentEl = find(generateTestSelector('gallery-item', attachmentId)).parentElement.parentElement.parentElement.parentElement;
    assert.dom(parentEl).hasClass('status-gallery__slide--active');
  });

  test('it updates the url while navigating', async function(assert) {
    let attachments = this.server.createList('attachment', 6);
    let status = this.server.create('status', {
      mediaAttachments: attachments,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
    await settled();

    assert.dom(generateTestSelector('attachment-gallery-modal'));
    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[2].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);

    await click(generateTestSelector('attachment-gallery-button-next'));
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);

    await click(generateTestSelector('attachment-gallery-button-next'));
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[4].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[4].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
  });

  test('it loops through gallery items', async function(assert) {
    let attachments = this.server.createList('attachment', 6);
    let status = this.server.create('status', {
      mediaAttachments: attachments,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
    await settled();

    assert.dom(generateTestSelector('attachment-gallery-modal'));

    await click(generateTestSelector('attachment-gallery-button-next'));
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);

    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`4 /  ${attachments.length}`);

    await click(generateTestSelector('attachment-gallery-button-next'));
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[4].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[4].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`5 /  ${attachments.length}`);

    await click(generateTestSelector('attachment-gallery-button-next'));
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[5].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[5].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`6 /  ${attachments.length}`);

    await click(generateTestSelector('attachment-gallery-button-next'));
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[0].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[0].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`1 /  ${attachments.length}`);

    await click(generateTestSelector('attachment-gallery-button-previous'));
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[5].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[5].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`6 /  ${attachments.length}`);
  });

  test('it returns to previous route when closed', async function(assert) {
    let attachments = this.server.createList('attachment', 6);
    let status = this.server.create('status', {
      mediaAttachments: attachments,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
    await settled();

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[2].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-modal'));

    await click(generateTestSelector('attachment-gallery-close-modal-button'));
    await settled();

    assert.equal(currentURL(), '/feeds/all');
  });

  test('closes on escape key press', async function(assert) {
    let attachments = this.server.createList('attachment', 6);
    let status = this.server.create('status', {
      mediaAttachments: attachments,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
    await settled();

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[2].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-modal'));

    await triggerKeyEvent(document.body, 'keydown', 27);
    await settled();

    assert.equal(currentURL(), '/feeds/all');
  });

  test('the gallery can be navigated with arrow keys', async function(assert) {
    assert.timeout(100000);
    let attachments = this.server.createList('attachment', 6);
    let status = this.server.create('status', {
      mediaAttachments: attachments,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-attachment', attachments[2].id)}`);
    await settled();

    await triggerKeyEvent(document.body, 'keydown', 39);
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);

    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`4 /  ${attachments.length}`);

    await triggerKeyEvent(document.body, 'keydown', 39);
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[4].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[4].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`5 /  ${attachments.length}`);

    await triggerKeyEvent(document.body, 'keydown', 37);
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[3].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`4 /  ${attachments.length}`);

    await triggerKeyEvent(document.body, 'keydown', 37);
    await settled();

    await waitUntil(() => {
      return currentURL() === `/feeds/all?gallery-attachment=${attachments[2].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`;
    });

    assert.equal(currentURL(), `/feeds/all?gallery-attachment=${attachments[2].id}&gallery-subject=${status.id}&gallery-subject-type=${'status'}`);
    assert.dom(generateTestSelector('attachment-gallery-pagination')).hasText(`3 /  ${attachments.length}`);
  });

  module('On the chat details route', function() {
    test('opens the modal correctly', async function(assert) {
      let user = await this.server.create('user', 'withToken', 'withSession');

      let otherUser = this.server.create('user');
      let attachment = this.server.create('attachment');
      let chat = this.server.create('chat', { account: user });
      let chatMessage = this.server.create('chat-message', { chat, account: otherUser, attachment });

      assert.equal(chatMessage.chatId, chat.id);

      await dangerouslyVisit(`/message/chat/${chat.id}`);

      assert.dom(`${generateTestSelector('message-fragment')}${generateTestSelector('item-id', chatMessage.id)}`).exists();

      await click(`${generateTestSelector('message-fragment-attachment', 'button')}${generateTestSelector('message-fragment-attachment-id', attachment.id)}`);
      await settled();

      assert.equal(currentURL(), `/message/chat/${chat.id}?gallery-attachment=${attachment.id}&gallery-subject=${chatMessage.id}&gallery-subject-type=${'chat-message'}`);
      assert.dom(generateTestSelector('attachment-gallery-modal')).exists();
      assert.dom(generateTestSelector('attachment-gallery')).exists();
      let parentEl = find(generateTestSelector('gallery-item', attachment.id)).parentElement.parentElement.parentElement.parentElement;
      assert.dom(parentEl).hasClass('status-gallery__slide--active');
    });

    test('it returns to previous route when closed', async function(assert) {
      let user = await this.server.create('user', 'withToken', 'withSession');

      let otherUser = this.server.create('user');
      let attachment = this.server.create('attachment');
      let chat = this.server.create('chat', { account: user });
      let chatMessage = this.server.create('chat-message', { chat, account: otherUser, attachment });

      assert.equal(chatMessage.chatId, chat.id);

      await dangerouslyVisit(`/message/chat/${chat.id}`);

      assert.dom(`${generateTestSelector('message-fragment')}${generateTestSelector('item-id', chatMessage.id)}`).exists();

      await click(`${generateTestSelector('message-fragment-attachment', 'button')}${generateTestSelector('message-fragment-attachment-id', attachment.id)}`);
      await settled();

      assert.equal(currentURL(), `/message/chat/${chat.id}?gallery-attachment=${attachment.id}&gallery-subject=${chatMessage.id}&gallery-subject-type=${'chat-message'}`);
      assert.dom(generateTestSelector('attachment-gallery-modal'));

      await click(generateTestSelector('attachment-gallery-close-modal-button'));
      await settled();

      assert.equal(currentURL(), `/message/chat/${chat.id}`);
    });

    test('closes on escape key press', async function(assert) {
      let user = await this.server.create('user', 'withToken', 'withSession');

      let otherUser = this.server.create('user');
      let attachment = this.server.create('attachment');
      let chat = this.server.create('chat', { account: user });
      let chatMessage = this.server.create('chat-message', { chat, account: otherUser, attachment });

      assert.equal(chatMessage.chatId, chat.id);

      await dangerouslyVisit(`/message/chat/${chat.id}`);

      assert.dom(`${generateTestSelector('message-fragment')}${generateTestSelector('item-id', chatMessage.id)}`).exists();

      await click(`${generateTestSelector('message-fragment-attachment', 'button')}${generateTestSelector('message-fragment-attachment-id', attachment.id)}`);
      await settled();

      assert.equal(currentURL(), `/message/chat/${chat.id}?gallery-attachment=${attachment.id}&gallery-subject=${chatMessage.id}&gallery-subject-type=${'chat-message'}`);
      assert.dom(generateTestSelector('attachment-gallery-modal'));

      await triggerKeyEvent(document.body, 'keydown', 27);
      await settled();

      assert.equal(currentURL(), `/message/chat/${chat.id}`);
    });
  });

});

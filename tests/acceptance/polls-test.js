import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  fillIn,
  findAll,
  resetOnerror,
  settled,
  setupOnerror,
} from '@ember/test-helpers';
import faker from 'faker';
import sinon from 'sinon';
import { set } from '@ember/object';

module('Acceptance | poll', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure polls feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('polls'), false);

    // Enable it for tests.
    enableFeature('polls');
  });

  module('creating', function() {
    test('you can create a poll (happy path)', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('create-status-form-trigger-poll'));
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();
      assert.dom(generateTestSelector('polls-form')).exists();

      let statusContent = faker.lorem.sentence();
      let pollOptionOne = faker.lorem.sentence();
      let pollOptionTwo = faker.lorem.sentence();

      await fillIn(generateTestSelector('create-status-body'), statusContent);
      await fillIn(generateTestSelector('polls-form', 'option-input'), pollOptionOne);
      await fillIn(findAll(generateTestSelector('polls-form', 'option-input'))[1], pollOptionTwo);
      await settled();

      await click(generateTestSelector('create-status-button'));
      await settled();

      assert.dom(generateTestSelector('status')).exists();
      assert.dom(findAll(`${generateTestSelector('status')} ${generateTestSelector('poll', 'option')}`)[0]).hasText(pollOptionOne);
      assert.dom(findAll(`${generateTestSelector('status')} ${generateTestSelector('poll', 'option')}`)[1]).hasText(pollOptionTwo);

      assert.dom(generateTestSelector('toast-type', 'new-status')).exists();
      assert.dom(generateTestSelector('toast-message')).hasText(t('yourStatusWasPosted'));
    });

    test('we default 2 poll options', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('create-status-form-trigger-poll'));
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();
      assert.dom(generateTestSelector('polls-form')).exists();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 2 });
    });

    test('cannot add more than the max number of options', async function(assert) {
      let instances = this.owner.lookup('service:instances');

      await instances.refreshTask.perform();

      set(instances.current.pollLimits, 'maxOptions', 5);

      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('create-status-form-trigger-poll'));
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();
      assert.dom(generateTestSelector('polls-form')).exists();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 2 });

      await click(generateTestSelector('polls-form', 'add-poll-option'));
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 3 });

      await click(generateTestSelector('polls-form', 'add-poll-option'));
      await settled();

      await click(generateTestSelector('polls-form', 'add-poll-option'));
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 5 });

      assert.dom(generateTestSelector('polls-form', 'add-poll-option')).doesNotExist();
    });

    test('you can remove options from a poll until reaching 2', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('create-status-form-trigger-poll'));
      await settled();

      assert.dom(generateTestSelector('create-status-modal')).exists();
      assert.dom(generateTestSelector('polls-form')).exists();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 2 });

      await click(generateTestSelector('polls-form', 'add-poll-option'));
      await settled();
      await click(generateTestSelector('polls-form', 'add-poll-option'));
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 4 });

      assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
        .exists({ count: 4 });

      await click(generateTestSelector('polls-form', 'remove-poll-option'));
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 3 });

      assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
        .exists({ count: 3 });

      await click(generateTestSelector('polls-form', 'remove-poll-option'));
      await settled();

      assert.dom(generateTestSelector('polls-form', 'option-input'))
        .exists({ count: 2 });

      assert.dom(generateTestSelector('polls-form', 'remove-poll-option'))
        .doesNotExist();
    });
  });

  module('voting', function() {
    test('attempting to vote on a poll while not logged in will open the sign-in modal with a good error', async function(assert) {
      let status = this.server.create('status', 'withRunningPoll');

      await dangerouslyVisit('/feeds/all');

      assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-poll')}`).exists();

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('poll', 'option')}`);
      await settled();

      assert.dom(generateTestSelector('auth-modal')).exists();
      assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToVote'));
    });

    test('voting on a running poll throws a toast', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');
      let status = this.server.create('status', 'withRunningPoll');

      await dangerouslyVisit('/feeds/all');

      assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-poll')}`).exists();

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('poll', 'option')}`);
      await settled();

      assert.dom(generateTestSelector('toast-type', 'success')).exists();
      assert.dom(generateTestSelector('toast-message')).hasText(t('voteSaved'));
    });

    test('if there is a generic server error, we show a toast', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.create('user', 'withToken', 'withSession');
      let status = this.server.create('status', 'withRunningPoll');

      this.server.post('/api/v1/polls/:poll_id/votes', function() {}, 400);

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('poll', 'option')}`);
      await settled();

      assert.dom(generateTestSelector('toast-type', 'error')).exists();
      assert.dom(generateTestSelector('toast-message')).hasText(t('errorVotingTryAgain'));

      resetOnerror();
    });

    test('if there is a special, 422 error, we will pass through the message to an error toast', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      this.server.create('user', 'withToken', 'withSession');
      let status = this.server.create('status', 'withRunningPoll');
      let errorMessage = faker.lorem.sentence();

      this.server.post('/api/v1/polls/:poll_id/votes', function() {
        return {
          error: errorMessage,
        };
      }, 422);

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('poll', 'option')}`);
      await settled();

      assert.dom(generateTestSelector('toast-type', 'error')).exists();
      assert.dom(generateTestSelector('toast-message')).hasText(errorMessage);

      resetOnerror();
    });

    test('you cannot vote on a expired poll', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');
      let status = this.server.create('status', 'withExpiredPoll');
      let spy = sinon.spy();

      this.server.post('/api/v1/polls/:poll_id/votes', spy, 200);

      await dangerouslyVisit('/feeds/all');

      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('poll', 'option')}`);
      await settled();

      assert.ok(spy.notCalled);
    });
  });
});

import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  settled,
  setupOnerror,
  resetOnerror,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';

module('Acceptance | follow/unfollow users', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('when signed in, you can follow a user from the status card menu', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user');
    let status = this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('status-id', status.id)).exists();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'follow-user'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-followed')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('nowFollowing', { username: statusUser.username }));
  });

  test('you can unfollow a user from the follow toast', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user');
    let status = this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('status-id', status.id)).exists();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'follow-user'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-followed')).exists();

    await click(generateTestSelector('user-followed-unfollow-button'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-unfollowed')).exists();
  });

  test('following a user update the relationship and follow/unfollow copy', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user');
    let status = this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('status-id', status.id)).exists();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'follow-user'));
    await settled();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).exists();
  });

  test('unfollowing a user will show a toast', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user', 'followed');
    let status = this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('status-id', status.id)).exists();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'unfollow-user'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-unfollowed')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('userUnfollowed', { username: statusUser.username }));
  });

  test('you can follow a user from the unfollow toast', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user', 'followed');
    let status = this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('status-id', status.id)).exists();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'unfollow-user'));
    await settled();

    await click(generateTestSelector('user-unfollowed-follow-button'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'user-followed')).exists();
  });

  test('unfollowing a user will update the relationship and the follow/unfollow copy', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user', 'followed');

    let status = this.server.create('status', {
      accountId: statusUser.id,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('status-id', status.id)).exists();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'unfollow-user')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'unfollow-user'));
    await settled();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).exists();
  });

  test('failure in following will show a good error', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user');
    let status = this.server.create('status', {
      account: statusUser,
    });

    this.server.post('/api/v1/accounts/:account_id/follow', {}, 500);

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'follow-user'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'error')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('errorFollowingUserTryAgain', { username: statusUser.username }));

    resetOnerror();
  });

  test('failure in unfollowing will show a good error', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user', 'followed');

    let status = this.server.create('status', {
      account: statusUser,
    });

    this.server.post('/api/v1/accounts/:account_id/unfollow', {}, 500);

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'unfollow-user'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'error')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('errorUnfollowingUserTryAgain', { username: statusUser.username }));

    resetOnerror();
  });

  test('attempting to follow while unauthenticated will show the auth modal with a good reason', async function(assert) {
    let statusUser = this.server.create('user');

    this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    let userActionsService = this.owner.lookup('service:user-actions');

    await userActionsService.followUser.perform(statusUser);
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToFollowUser'));
  });

  test('attempting to unfollow while unauthenticated will show the auth modal with a good reason', async function(assert) {
    let statusUser = this.server.create('user');

    this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    let userActionsService = this.owner.lookup('service:user-actions');

    await userActionsService.unfollowUser.perform(statusUser);
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToUnfollowUser'));
  });

  test('the menu should close when following/unfolling from the status menu', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let statusUser = this.server.create('user');
    let status = this.server.create('status', {
      account: statusUser,
    });

    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('status-id', status.id)).exists();

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    assert.dom(generateTestSelector('feed-item-menu')).exists();
    assert.dom(generateTestSelector('feed-item-menu-item', 'follow-user')).exists();

    await click(generateTestSelector('feed-item-menu-item', 'follow-user'));
    await settled();

    assert.dom(generateTestSelector('feed-item-menu')).doesNotExist();
  });
});

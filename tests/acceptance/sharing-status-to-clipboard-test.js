import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  settled,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import promiseEachSeries from 'p-each-series';
import RSVP from 'rsvp';
import sinon from 'sinon';

module('Acceptance | sharing status to clipboard', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('should copy the context statuses url to the clipboard', async function(assert) {
    let statuses = this.server.createList('status', 10);

    await dangerouslyVisit('feeds/all');

    await promiseEachSeries(statuses, async(status) => {
      await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);

      await settled();

      await click(generateTestSelector('feed-item-menu-item', 'copy-status'));

      let statusUrl = new URL(`/notice/${status.id}`, window.location.origin);

      assert.equal(window.testClipboardText, statusUrl);
    });
  });

  test('should fire a toast when the status is on your clipboard', async function(assert) {
    let user = this.server.create('user');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);

    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'copy-status'));

    assert.dom(generateTestSelector('toast-type', 'success')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('statusUrlCopiedToClipboard'));
  });

  test('should fire a error toast when it fails to copy to your clipboard', async function(assert) {
    let user = this.server.create('user');
    let status = this.server.create('status', {
      account: user,
    });

    let clipboardService = this.owner.lookup('service:clipboard');

    sinon.replaceGetter(clipboardService, 'copy', function() { return RSVP.reject(); });

    await dangerouslyVisit('feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);

    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'copy-status'));

    assert.dom(generateTestSelector('toast-type', 'error')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('errorCopyingStatusUrlToClipboard'));
  });
});

import { module } from 'qunit';
import test from 'ember-sinon-qunit/test-support/test';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { currentRouteName } from '@ember/test-helpers';

module('Acceptance | sign in route', function(hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks, 'en');
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('has the right parts', async function(assert) {
    await dangerouslyVisit('/sign-in');

    assert.equal(currentRouteName(), 'sign-in');

    assert.dom(generateTestSelector('sign-in-form')).exists();
  });
});

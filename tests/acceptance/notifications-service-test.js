import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { settled, findAll } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import faker from 'faker';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Acceptance | toast service', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('pushing toasts into the service will display them', async function(assert) {
    let testToastCopy = faker.lorem.sentence();

    await dangerouslyVisit('/');

    let toastService = this.owner.lookup('service:toast');

    toastService.notify({
      message: testToastCopy,
    });

    await settled();

    assert.ok(toastService.activeToasts.length === 1);
    assert.equal(toastService.activeToasts[0].payload.message, testToastCopy);
  });

  test('will only display the most recent toast', async function(assert) {
    let testToastCopy1 = faker.lorem.sentence();
    let testToastCopy2 = faker.lorem.sentence();

    await dangerouslyVisit('/');

    let toastService = this.owner.lookup('service:toast');

    toastService.notify({
      message: testToastCopy1,
    });

    toastService.notify({
      message: testToastCopy2,
    });

    await settled();

    assert.ok(findAll(generateTestSelector('toast-type', 'success')).length, 1);
    assert.dom(generateTestSelector('toast-type', 'success')).hasText(testToastCopy2);
  });
});

import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { setupOnerror, resetOnerror } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';

module('Acceptance | Default language', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('the application should accept its language from the pleroma instance', async function(assert) {
    let intlService = this.owner.lookup('service:intl');

    intlService.addTranslations('ar-AE', {
      bingBong: 'بنج بونج',
    });

    this.server.get('/api/v1/instance', () => ({
      languages: ['AR-AE'], // arabic from the emirates
    }), 200);

    await dangerouslyVisit('/');

    assert.equal(intlService.primaryLocale, 'ar-ae', 'The applications language will accept its language from the pleroma instance');
  });

  test('the application should fallback to en when the pleroma instance languages is an empty set', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    let intlService = this.owner.lookup('service:intl');

    this.server.get('/api/v1/instance', () => ({
      languages: [],
    }), 200);

    await dangerouslyVisit('/');

    assert.equal(intlService.primaryLocale, 'en', 'The applications language will fall back to english');

    resetOnerror();
  });

  test('the application should fallback to en when the pleroma instance sends no languages', async function(assert) {
    setupOnerror(function(err) {
      assert.ok(err);
    });

    let intlService = this.owner.lookup('service:intl');

    this.server.get('/api/v1/instance', () => ({}), 200);

    await dangerouslyVisit('/');

    assert.equal(intlService.primaryLocale, 'en', 'The applications language will fall back to english');

    resetOnerror();
  });
});

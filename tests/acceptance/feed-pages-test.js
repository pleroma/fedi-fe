import { module, test, skip } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentURL,
  find,
  findAll,
  settled,
  waitUntil,
  triggerKeyEvent,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import promiseEachSeries from 'p-each-series';
import { setupIntl, t } from 'ember-intl/test-support';
import config from 'pleroma-pwa/config/environment';
import { set } from '@ember/object';

module('Acceptance | feed pages', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');

    this.testInfiniteScroll = async(assert, max, groupsOf = config.APP.FEED_PAGE_SIZE, selector = 'status') => {
      let initialSet = findAll(generateTestSelector(selector)).length;
      let loops = new Array(Math.ceil((max - initialSet) / groupsOf));

      await settled();

      await promiseEachSeries(loops, async(element, index) => {
        if (index === 0) {
          assert.dom(generateTestSelector(selector)).exists({ count: initialSet });
        } else {
          assert.dom(generateTestSelector(selector)).exists({ count: Math.min((index * groupsOf) + initialSet, max) });
        }

        await settled();

        if (find(generateTestSelector('feed-infinity-loader'))) {
          assert.dom(generateTestSelector('feed-infinity-loader')).exists();

          find(generateTestSelector('feed-infinity-loader')).scrollIntoView();
        } else {
          assert.dom(generateTestSelector('feed-fully-loaded'));
        }

        await settled();

        await waitUntil(() => {
          return findAll(generateTestSelector(selector)).length === Math.min((index + 1) * groupsOf + initialSet, max);
        });
      });
    };
  });

  test('feed pages header has all of the proper bits', async function(assert) {
    await dangerouslyVisit('/feeds/all');

    assert.dom(generateTestSelector('feed-menu-trigger')).exists();
    assert.dom(generateTestSelector('feed-menu-trigger')).hasAttribute('id', 'feed__header');
    assert.dom(generateTestSelector('feed-menu-trigger')).hasAria('label', t('selectThisFeed', { header: t('wholeKnownNetwork') }));

    assert.dom(generateTestSelector('status-form-trigger')).doesNotExist();
    assert.dom(generateTestSelector('feed-heading')).hasText(t('wholeKnownNetwork'), 'templates the feed name correct');
  });

  test('it renders the status-form component when the session is authenticated', async function(assert) {

    await dangerouslyVisit('/feeds/public');

    assert.dom(generateTestSelector('status-form-trigger')).doesNotExist();

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/feeds/public');

    assert.dom(generateTestSelector('status-form-trigger')).exists();
  });

  test('statuses within the feed are passed and template the correct position within the feed', async function(assert) {
    this.server.createList('status', 5);

    let store = this.owner.lookup('service:store');
    let feeds = this.owner.lookup('service:feeds');
    let publicFeed = feeds.public;

    await dangerouslyVisit('/feeds/public');

    assert.dom(generateTestSelector('feed')).exists();

    assert.dom(generateTestSelector('status')).exists({ count: 5 });

    assert.dom(findAll(generateTestSelector('status'))[0]).hasAria('posinset', '1');
    assert.dom(findAll(generateTestSelector('status'))[1]).hasAria('posinset', '2');
    assert.dom(findAll(generateTestSelector('status'))[2]).hasAria('posinset', '3');
    assert.dom(findAll(generateTestSelector('status'))[3]).hasAria('posinset', '4');
    assert.dom(findAll(generateTestSelector('status'))[4]).hasAria('posinset', '5');

    let newStatus = this.server.create('status');
    let newStatusFromStore = await store.loadRecord('status', newStatus.id);
    let otherNewStatus = this.server.create('status');
    let otherNewStatusFromStore = await store.loadRecord('status', otherNewStatus.id);

    publicFeed.content.pushObjects([newStatusFromStore, otherNewStatusFromStore]);

    await settled();

    assert.dom(generateTestSelector('status')).exists({ count: 7 });

    assert.dom(findAll(generateTestSelector('status'))[0]).hasAria('posinset', '1');
    assert.dom(findAll(generateTestSelector('status'))[1]).hasAria('posinset', '2');
    assert.dom(findAll(generateTestSelector('status'))[2]).hasAria('posinset', '3');
    assert.dom(findAll(generateTestSelector('status'))[3]).hasAria('posinset', '4');
    assert.dom(findAll(generateTestSelector('status'))[4]).hasAria('posinset', '5');
    assert.dom(findAll(generateTestSelector('status'))[5]).hasAria('posinset', '6');
    assert.dom(findAll(generateTestSelector('status'))[6]).hasAria('posinset', '7');
  });

  test('statuses within the feed are passed and template the correct length of the feed', async function(assert) {
    this.server.createList('status', 5);

    let store = this.owner.lookup('service:store');
    let feeds = this.owner.lookup('service:feeds');
    let publicFeed = feeds.public;

    await dangerouslyVisit('/feeds/public');

    assert.dom(generateTestSelector('feed')).exists();

    assert.equal(findAll(generateTestSelector('status')).length, 5);

    assert.dom(findAll(generateTestSelector('status'))[0]).hasAria('setsize', '5');
    assert.dom(findAll(generateTestSelector('status'))[1]).hasAria('setsize', '5');
    assert.dom(findAll(generateTestSelector('status'))[2]).hasAria('setsize', '5');
    assert.dom(findAll(generateTestSelector('status'))[3]).hasAria('setsize', '5');
    assert.dom(findAll(generateTestSelector('status'))[4]).hasAria('setsize', '5');

    let newStatus = this.server.create('status');
    let newStatusFromStore = await store.loadRecord('status', newStatus.id);
    let otherNewStatus = this.server.create('status');
    let otherNewStatusFromStore = await store.loadRecord('status', otherNewStatus.id);
    publicFeed.content.pushObjects([newStatusFromStore, otherNewStatusFromStore]);

    await settled();

    assert.equal(findAll(generateTestSelector('status')).length, 7);

    assert.dom(findAll(generateTestSelector('status'))[0]).hasAria('setsize', '7');
    assert.dom(findAll(generateTestSelector('status'))[1]).hasAria('setsize', '7');
    assert.dom(findAll(generateTestSelector('status'))[2]).hasAria('setsize', '7');
    assert.dom(findAll(generateTestSelector('status'))[3]).hasAria('setsize', '7');
    assert.dom(findAll(generateTestSelector('status'))[4]).hasAria('setsize', '7');
    assert.dom(findAll(generateTestSelector('status'))[4]).hasAria('setsize', '7');
    assert.dom(findAll(generateTestSelector('status'))[4]).hasAria('setsize', '7');
  });

  test('it has the proper a11y attributes', async function(assert) {
    let feeds = this.owner.lookup('service:feeds');
    let publicFeed = feeds.public;

    await dangerouslyVisit('/feeds/public');

    assert.dom(generateTestSelector('feed')).exists();

    assert.equal(find(generateTestSelector('feed')).tagName, 'MAIN', 'Should have the tagname main');
    assert.dom(generateTestSelector('feed')).hasAttribute('role', 'main', 'Should have the role feed');
    assert.dom(generateTestSelector('feed')).hasAria('busy', 'false', 'Aria busy should be the string false when not loading feed items');

    set(publicFeed, 'isLoading', true);
    await settled();

    assert.dom(generateTestSelector('feed')).hasAria('busy', 'true', 'Aria busy should be the string true when loading feed items');
    assert.dom(generateTestSelector('feed')).hasAria('labelledby', 'feed__header', 'the feed is "labelledby" its header');
  });

  test('will return focus to the chevron when closing the menu', async function(assert) {
    await dangerouslyVisit('/feeds/public');

    await click(generateTestSelector('feed-menu-trigger'));
    await settled();

    await triggerKeyEvent(document.body, 'keydown', 27);

    assert.dom(generateTestSelector('feed-menu-trigger')).isFocused();
  });

  module('feeds/all', function() {
    test('displays a list of statuses', async function(assert) {
      let user = this.server.create('user');

      this.server.createList('status', 10, {
        account: user,
      });

      await dangerouslyVisit('/feeds/all');
      assert.equal(currentURL(), '/feeds/all');

      assert.equal(findAll(generateTestSelector('status')).length, 10);
    });

    test('fetches more statuses when the final status is scrolled into viewport', async function(assert) {
      assert.timeout(60000);

      let user = this.server.create('user');

      this.server.createList('status', 75, {
        account: user,
      });

      await dangerouslyVisit('/feeds/all');

      await this.testInfiniteScroll(assert, 75);
    });

    skip('fetches new statuses and adds them to the top of the list', async function() {});
  });

  module('/feeds/public', function() {
    test('displays a list of statuses', async function(assert) {
      let user = this.server.create('user');

      this.server.createList('status', 15, {
        account: user,
      });

      await dangerouslyVisit('/feeds/public');
      assert.equal(currentURL(), '/feeds/public');

      assert.equal(findAll(generateTestSelector('status')).length, 15);
    });

    test('fetches more statuses when the final status is scrolled into viewport', async function(assert) {
      assert.timeout(60000);

      let user = this.server.create('user');

      this.server.createList('status', 95, {
        account: user,
      });

      await dangerouslyVisit('/feeds/public');

      await this.testInfiniteScroll(assert, 95);
    });

    skip('fetches new statuses and adds them to the top of the list', async function() {});
  });

  module('feeds/home', function() {
    test('displays a list of statuses', async function(assert) {
      let user = this.server.create('user', 'withToken', 'withSession');

      this.server.createList('status', 15, {
        account: user,
      });

      await dangerouslyVisit('/feeds/home');
      assert.equal(currentURL(), '/feeds/home');

      assert.equal(findAll(generateTestSelector('status')).length, 15);
    });

    test('fetches more statuses when the final status is scrolled into viewport', async function(assert) {
      assert.timeout(60000);

      let user = this.server.create('user', 'withToken', 'withSession');

      this.server.createList('status', 57, {
        account: user,
      });

      await dangerouslyVisit('/feeds/home');

      await this.testInfiniteScroll(assert, 57);
    });

    skip('fetches new statuses and adds them to the top of the list', async function() {});
  });

  test('when selecting a new feed from the feed switcher menu, the menu will close', async function(assert) {
    await dangerouslyVisit('/feeds/all');

    await click(generateTestSelector('feed-menu-trigger'));

    assert.dom(generateTestSelector('feed-switcher-dropdown')).exists();

    await click(generateTestSelector('feed-switcher-public-link'));

    assert.dom(generateTestSelector('feed-switcher-dropdown')).doesNotExist();
    assert.equal(currentURL(), '/feeds/public');
  });
});

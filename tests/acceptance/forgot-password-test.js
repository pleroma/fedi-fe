import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  fillIn,
  resetOnerror,
  settled,
  setupOnerror,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl } from 'ember-intl/test-support';
import faker from 'faker';

module('Acceptance | forgot password', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('shows a message when the instance has no mailer setup', async function(assert) {
    let nodeInfo = this.server.create('node-info');

    nodeInfo.update({
      metadata: {
        ...nodeInfo.metaData,
        mailerEnabled: false,
      },
    });

    this.server.create('instance', {
      nodeInfo,
    });

    await dangerouslyVisit('/');

    assert.dom(generateTestSelector('account-cta-sign-in-button')).exists();

    await click(generateTestSelector('account-cta-sign-in-button'));
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-form')).exists();

    await click(generateTestSelector('forgot-password-link'));
    await settled();

    assert.dom(generateTestSelector('forgot-password-not-enabled-callout')).exists();
  });

  test('shows the forgot password form when the instance has a mailed setup', async function(assert) {
    this.server.create('instance');

    await dangerouslyVisit('/');

    assert.dom(generateTestSelector('account-cta-sign-in-button')).exists();

    await click(generateTestSelector('account-cta-sign-in-button'));
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-form')).exists();

    await click(generateTestSelector('forgot-password-link'));
    await settled();

    assert.dom(generateTestSelector('forgot-password-form')).exists();
  });

  test('does not show errors when the server returns errors', async function(assert) {
    let email = faker.internet.email();

    this.server.create('instance');

    setupOnerror(function(err) {
      assert.ok(err);
    });

    await dangerouslyVisit('/');

    assert.dom(generateTestSelector('account-cta-sign-in-button')).exists();

    await click(generateTestSelector('account-cta-sign-in-button'));
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-form')).exists();

    await click(generateTestSelector('forgot-password-link'));
    await settled();

    assert.dom(generateTestSelector('forgot-password-form')).exists();

    // server should return error since this user does not exist
    await fillIn(generateTestSelector('forgot-email-input'), email);
    await settled();

    await click(generateTestSelector('forgot-password-form-submit-button'));
    await settled();

    assert.dom(generateTestSelector('forgot-password-success')).exists();

    resetOnerror();
  });

  test('shows the forgotpassword success screen when the server gives success', async function(assert) {
    let user = this.server.create('user');

    this.server.create('instance');

    await dangerouslyVisit('/');

    assert.dom(generateTestSelector('account-cta-sign-in-button')).exists();

    await click(generateTestSelector('account-cta-sign-in-button'));
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-form')).exists();

    await click(generateTestSelector('forgot-password-link'));
    await settled();

    assert.dom(generateTestSelector('forgot-password-form')).exists();

    await fillIn(generateTestSelector('forgot-email-input'), user.email);
    await settled();

    await click(generateTestSelector('forgot-password-form-submit-button'));
    await settled();

    assert.dom(generateTestSelector('forgot-password-success')).exists();
  });
});

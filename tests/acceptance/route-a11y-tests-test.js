import { module, skip } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { setupApplicationTest } from 'ember-qunit';
import a11yAudit from 'ember-a11y-testing/test-support/audit';

module('Acceptance | route a11y tests', function(hooks) {
  setupApplicationTest(hooks);

  module('feed routes', function() {
    skip('visiting /feeds/all', async function(assert) {
      await dangerouslyVisit('/feeds/all');
      await a11yAudit();
      assert.ok(true, 'no a11y errors found!');
    });

    skip('visiting /feeds/direct', async function(assert) {
      await dangerouslyVisit('/feeds/direct');
      await a11yAudit();
      assert.ok(true, 'no a11y errors found!');
    });

    skip('visiting /feeds/home', async function(assert) {
      await dangerouslyVisit('/feeds/home');
      await a11yAudit();
      assert.ok(true, 'no a11y errors found!');
    });

    skip('visiting /feeds/notification', async function(assert) {
      await dangerouslyVisit('/feeds/notification');
      await a11yAudit();
      assert.ok(true, 'no a11y errors found!');
    });

    skip('visiting /feeds/public', async function(assert) {
      await dangerouslyVisit('/feeds/public');
      await a11yAudit();
      assert.ok(true, 'no a11y errors found!');
    });
  });

  module('sign-in routes', function() {
    skip('visiting /sign-in', async function(assert) {
      await dangerouslyVisit('/sign-in');
      await a11yAudit();
      assert.ok(true, 'no a11y errors found!');
    });
  });

  module('account routes', function() {
    skip('visiting /account/details', async function(assert) {
      let user = this.server.create('user');
      await dangerouslyVisit(`/account/${user.id}`);
      await a11yAudit();
      assert.ok(true, 'no a11y errors found!');
    });
  });
});

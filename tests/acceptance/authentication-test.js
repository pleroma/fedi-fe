import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import signIn from 'pleroma-pwa/tests/helpers/sign-in';
import { setupMirage } from 'ember-cli-mirage/test-support';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  currentURL,
  resetOnerror,
  settled,
  setupOnerror,
  visit,
} from '@ember/test-helpers';
import { setupIntl, t } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Acceptance | Authentication tests', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  hooks.afterEach(function() {
    let documentService = this.owner.lookup('service:-document');
    documentService.documentElement.classList.remove('is-signed-in');
  });

  module('success', function() {
    test('from the sign-in route, Sign In with username + password', async function(assert) {
      await visit('/sign-in');

      await signIn(this.server.create('user'));

      assert.equal(currentURL(), '/feeds/home');
    });

    test('from the sign-in modal, Sign In with username + password', async function(assert) {
      await dangerouslyVisit('/feeds/all');

      await signIn(this.server.create('user'));

      assert.equal(currentURL(), '/feeds/all');
    });

    test('the html element should get an is-signed-in class', async function(assert) {
      await dangerouslyVisit('/feeds/all');

      await click(generateTestSelector('account-cta-sign-in-button'));

      await signIn(this.server.create('user'));
      await settled();

      assert.dom(document.documentElement).hasClass('is-signed-in');
    });
  });

  module('failure', function() {
    test('Will display errors when the form is incomplete', async function(assert) {
      await signIn(this.server.create('user', {
        password: null,
      }));

      assert.equal(currentRouteName(), 'sign-in');

      assert.dom(generateTestSelector('sign-in-error')).exists();

      assert.dom(generateTestSelector('sign-in-error')).hasText(t('youMustEnterPassword'));
    });

    test('Will display useful errors if the username/password is incorrect', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      let user = this.server.create('user');

      await signIn({
        username: user.username,
        password: 'incorrectPassword',
      });

      assert.equal(currentRouteName(), 'sign-in');
      assert.dom(generateTestSelector('sign-in-error')).exists();
      assert.dom(generateTestSelector('sign-in-error')).hasText('Invalid credentials');

      resetOnerror();
    });

    test('the html element should not have the is-signed-in class', async function(assert) {
      let documentService = this.owner.lookup('service:-document');

      setupOnerror(function(err) {
        assert.ok(err);
      });

      let user = this.server.create('user');

      await signIn({
        username: user.username,
        password: 'incorrectPassword',
      });
      await settled();
      await settled();

      assert.dom(documentService.documentElement).doesNotHaveClass('is-signed-in');

      resetOnerror();
    });
  });
});

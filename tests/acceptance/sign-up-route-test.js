import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { currentRouteName } from '@ember/test-helpers';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | sign up route', function(hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks, 'en');
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.create('instance');

    enableFeature('registration');
  });

  test('has the right parts', async function(assert) {
    await dangerouslyVisit('/sign-up');

    assert.equal(currentRouteName(), 'sign-up');

    assert.dom(generateTestSelector('registration-form')).exists();
  });
});

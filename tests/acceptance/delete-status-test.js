import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  settled,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl, t } from 'ember-intl/test-support';
import sinon from 'sinon';

module('Acceptance | delete status', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function() {
    this.server.create('instance');
  });

  test('you can delete a status from the status menu', async function(assert) {
    let newConfirm = sinon.fake.returns(true);
    let oldConfirm = window.confirm;

    window.confirm = newConfirm;

    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'delete-status'));
    await settled();

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.peekRecord('status', status.id);

    assert.ok(statusFromStore.isDeleted);

    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`)
      .doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu'))
      .doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'delete-status'))
      .doesNotExist();

    window.confirm = oldConfirm;
  });

  test('denying deletion from the confirm modal will result no change', async function(assert) {
    let newConfirm = sinon.fake.returns(false);
    let oldConfirm = window.confirm;

    window.confirm = newConfirm;

    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'delete-status'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'success')).doesNotExist();

    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.peekRecord('status', status.id);

    assert.notOk(statusFromStore.isDeleted);

    window.confirm = oldConfirm;
  });

  test('closes the feed-item-menu on success', async function(assert) {
    let newConfirm = sinon.fake.returns(true);
    let oldConfirm = window.confirm;

    window.confirm = newConfirm;

    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'delete-status'));
    await settled();

    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`)
      .doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu'))
      .doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'delete-status'))
      .doesNotExist();

    window.confirm = oldConfirm;
  });

  test('closes the feed-item-menu on cancel delete', async function(assert) {
    let newConfirm = sinon.fake.returns(false);
    let oldConfirm = window.confirm;

    window.confirm = newConfirm;

    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'delete-status'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'success')).doesNotExist();

    assert.dom(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`)
      .exists();
    assert.dom(generateTestSelector('feed-item-menu'))
      .doesNotExist();
    assert.dom(generateTestSelector('feed-item-menu-item', 'delete-status'))
      .doesNotExist();

    window.confirm = oldConfirm;
  });

  test('deleting a status will fire a toast', async function(assert) {
    let newConfirm = sinon.fake.returns(true);
    let oldConfirm = window.confirm;

    window.confirm = newConfirm;

    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'delete-status'));
    await settled();

    assert.dom(generateTestSelector('toast-type', 'success')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('statusDeleted'));

    window.confirm = oldConfirm;
  });

  test('deleting a status will remove it from all feeds', async function(assert) {
    let newConfirm = sinon.fake.returns(true);
    let oldConfirm = window.confirm;

    window.confirm = newConfirm;

    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await dangerouslyVisit('/feeds/all');

    await click(`${generateTestSelector('status-id', status.id)} ${generateTestSelector('feed-item-menu-trigger')}`);
    await settled();

    await click(generateTestSelector('feed-item-menu-item', 'delete-status'));
    await settled();

    let feeds = this.owner.lookup('service:feeds');

    feeds.feedList.forEach((feed) => {
      assert.notOk(feed.content.findBy('id', status.id));
    });

    window.confirm = oldConfirm;
  });

  test('when not signed in, attempting to delete a status will show you the auth modal with a good reason', async function(assert) {
    let user = this.server.create('user');
    let status = this.server.create('status', {
      account: user,
    });
    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.peekRecord('status', status.id);

    await dangerouslyVisit('/feeds/all');

    let statusActions = this.owner.lookup('service:status-actions');

    await statusActions.deleteStatus.perform(statusFromStore);
    await settled();

    assert.dom(generateTestSelector('auth-modal')).exists();
    assert.dom(generateTestSelector('sign-in-reason')).hasText(t('youMustSignInToDeleteStatus'));
  });

  test('attempting to delete a status you do not own, will trigger an error toast with a good message', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let otherUser = this.server.create('user');
    let status = this.server.create('status', {
      account: otherUser,
    });
    let store = this.owner.lookup('service:store');
    let statusFromStore = await store.loadRecord('status', status.id);

    await dangerouslyVisit('/feeds/all');

    let statusActions = this.owner.lookup('service:status-actions');

    await statusActions.deleteStatus.perform(statusFromStore);

    assert.dom(generateTestSelector('toast-type', 'error')).exists();
    assert.dom(generateTestSelector('toast-message')).hasText(t('youCanOnlyDeleteYourOwnStatuses'));
  });
});

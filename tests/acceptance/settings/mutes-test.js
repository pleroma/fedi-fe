import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { click, find, currentRouteName } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | settings | mutes', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('userSettings'), false);

    // Enable it for tests.
    enableFeature('userSettings');
  });

  test('the content should be there', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings');
    await click(generateTestSelector('settings-link', 'mutes'));
    assert.equal(currentRouteName(), 'settings.mutes');

    assert.dom(generateTestSelector('mutes-select-all')).exists();
  });

  test('succeeds as expected', async function(assert) {
    let numUsers = 3;

    // create some muted users.
    this.server.createList('user', numUsers, 'muted');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/mutes');

    assert.dom(generateTestSelector('mute-select')).exists({ count: numUsers });

    // unmute first user in list by clicking bulk unmute button.
    await click(find(generateTestSelector('mute-select')));
    await click(generateTestSelector('bulk-unmute'));

    assert.dom(generateTestSelector('toast-type', 'user-unmuted')).includesText('has been successfully unmuted');
    assert.dom(generateTestSelector('mute-select')).exists({ count: numUsers - 1 });

    // unmute first user in list by clicking individual unmute button.
    await click(find(generateTestSelector('individual-unmute')));

    assert.dom(generateTestSelector('toast-type', 'user-unmuted')).includesText('has been successfully unmuted');
    assert.dom(generateTestSelector('mute-select')).exists({ count: numUsers - 2 });

    // unmute first user using individual unmute.
    await click(find(generateTestSelector('individual-unmute')));

    assert.dom(generateTestSelector('toast-type', 'user-unmuted')).includesText('has been successfully unmuted');
    assert.dom(generateTestSelector('mute-select')).exists({ count: numUsers - 3 });

    assert.equal(currentRouteName(), 'settings.mutes', 'not redirected away');
  });

});

import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { click, currentRouteName } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | settings | advanced', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('userSettings'), false);

    // Enable it for tests.
    enableFeature('userSettings');
  });

  test('the content should be there', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings');
    await click(generateTestSelector('settings-link', 'advanced'));
    assert.equal(currentRouteName(), 'settings.advanced');

    assert.dom(generateTestSelector('advanced-settings')).exists();
    assert.dom(generateTestSelector('settings-import')).exists({ count: 2 });
    assert.dom(generateTestSelector('settings-export')).exists({ count: 2 });
  });

  test('saving advanced settings should work as expected', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/advanced');

    await click(generateTestSelector('strip-rich-text-from-all-posts'));
    await click(generateTestSelector('allow-discovery-of-this-account'));
    await click(generateTestSelector('save-advanced-settings'));

    assert
      .dom(generateTestSelector('toast-type', 'success'))
      .includesText(t('accountHasBeenUpdated'));
  });

  test('exporting blocks should work as expected', async function(assert) {
    let numUsers = 3;

    // create some blocked users.
    this.server.createList('user', numUsers, 'blocked');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/advanced');

    await click(
      `${generateTestSelector('export', 'blocking')} ${generateTestSelector(
        'run',
        'export',
      )}`,
    );

    assert.dom(generateTestSelector('export-count', numUsers)).exists();

    assert
      .dom(generateTestSelector('toast-type', 'success'))
      .hasText(t('yourBlocksHaveBeenExported'));
  });

  test('revoking oauth tokens should work as expected', async function(assert) {
    let user = this.server.create('user', 'withToken', 'withSession');

    let numTokens = 5;

    // Once fake user logs in, another is created.
    let expectedNumTokens = numTokens + 1;

    let tokens = this.server.createList('token', numTokens, { user });

    await dangerouslyVisit('/settings/advanced');

    assert.dom('[data-test-oauth-token]').exists({ count: expectedNumTokens });

    await click(`[data-test-oauth-token="${tokens[0].id}"] [data-test-run="revoke"]`);

    assert.dom('[data-test-oauth-token]').exists({ count: expectedNumTokens - 1 });

    await click(`[data-test-oauth-token="${tokens[1].id}"] [data-test-run="revoke"]`);
    await click(`[data-test-oauth-token="${tokens[2].id}"] [data-test-run="revoke"]`);

    assert.dom('[data-test-oauth-token]').exists({ count: expectedNumTokens - 3 });
  });
});

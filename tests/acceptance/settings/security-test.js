import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import {
  click,
  currentRouteName,
  fillIn,
  settled,
} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import Service from '@ember/service';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl, t } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | settings | security', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('userSettings'), false);

    // Enable it for tests.
    enableFeature('userSettings');
  });

  test('the content should be there', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings');
    await click(generateTestSelector('settings-link', 'security'));
    assert.equal(currentRouteName(), 'settings.security');

    assert.dom(generateTestSelector('security-settings-password-form')).exists();
    assert.dom(generateTestSelector('security-settings-delete-account-form')).exists();
  });

  module('changing password', function() {
    test('fails without any text entry', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/settings/security');
      assert.dom(generateTestSelector('change-password')).isDisabled();
    });

    test('fails without matching passwords', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      let formSelector = generateTestSelector('security-settings-password-form');
      let errorsSelector = `${formSelector} ${generateTestSelector('error')}`;

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-current-password'), 'a');
      await fillIn(generateTestSelector('security-new-password'), 's');
      await fillIn(generateTestSelector('security-new-password-confirm'), 'd');
      await click(generateTestSelector('change-password'));

      assert.dom(errorsSelector).exists();

      assert.dom(errorsSelector).hasText(t('securitySettingsPasswordError'));
    });

    test('fails without valid existing password', async function(assert) {
      this.server.post('/api/pleroma/change_password', function() {
        // Pleroma returns 200 with error JSON. *twitch*
        return { error: 'Invalid password.' };
      });

      this.server.create('user', 'withToken', 'withSession');

      let formSelector = generateTestSelector('security-settings-password-form');
      let errorsSelector = `${formSelector} ${generateTestSelector('error')}`;

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-current-password'), 'a');
      await fillIn(generateTestSelector('security-new-password'), 'd');
      await fillIn(generateTestSelector('security-new-password-confirm'), 'd');
      await click(generateTestSelector('change-password'));

      assert.dom(errorsSelector).exists();

      assert.dom(errorsSelector).hasText('Invalid password.');
    });

    test('succeeds with valid existing password', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-current-password'), 'a');
      await fillIn(generateTestSelector('security-new-password'), 'd');
      await fillIn(generateTestSelector('security-new-password-confirm'), 'd');

      await click(generateTestSelector('change-password'));

      assert.dom(generateTestSelector('toast-type', 'success')).exists();
    });

    test('succeeds and logs user out', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-current-password'), 'a');
      await fillIn(generateTestSelector('security-new-password'), 'd');
      await fillIn(generateTestSelector('security-new-password-confirm'), 'd');

      await click(generateTestSelector('change-password'));
      await settled();

      assert.notEqual(currentRouteName(), 'settings.security', 'redirected away');
      assert.dom(generateTestSelector('sidebar-nav')).doesNotIncludeText(t('myFeed'));
    });
  });

  module('deleting account', function() {
    test('fails without any text entry', async function(assert) {
      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/settings/security');
      assert.dom(generateTestSelector('delete-account')).isDisabled();
    });

    test('fails if user cancels confirmation popup', async function(assert) {
      // mock canceling confirmation popup
      this.owner.register('service:window', Service.extend({
        confirm: () => false,
      }));

      this.server.create('user', 'withToken', 'withSession');

      let formSelector = generateTestSelector('security-settings-delete-account-form');
      let errorsSelector = `${formSelector} ${generateTestSelector('error')}`;

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-delete-account-current-password'), 'a');
      await click(generateTestSelector('delete-account'));

      assert.dom(errorsSelector).doesNotExist();
    });

    test('fails without valid existing password', async function(assert) {
      this.owner.register('service:window', Service.extend({
        confirm: () => true,
      }));

      this.server.post('/api/pleroma/delete_account', function() {
        // Pleroma returns 200 with error JSON. *twitch*
        return { error: 'Invalid password.' };
      });

      this.server.create('user', 'withToken', 'withSession');

      let formSelector = generateTestSelector('security-settings-delete-account-form');
      let errorsSelector = `${formSelector} ${generateTestSelector('error')}`;

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-delete-account-current-password'), 'a');
      await click(generateTestSelector('delete-account'));

      assert.dom(errorsSelector).exists();
      assert.dom(errorsSelector).includesText('Invalid password.');
    });

    test('succeeds with valid existing password', async function(assert) {
      this.owner.register('service:window', Service.extend({
        confirm: () => true,
      }));

      this.server.create('user', 'withToken', 'withSession');

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-delete-account-current-password'), 'a');

      await click(generateTestSelector('delete-account'));

      assert.dom(generateTestSelector('toast-type', 'success')).exists();
    });

    test('succeeds and logs user out', async function(assert) {
      this.owner.register('service:window', Service.extend({
        confirm: () => true,
      }));

      let user = this.server.create('user', 'withToken', 'withSession')

      await dangerouslyVisit('/settings/security');
      await fillIn(generateTestSelector('security-delete-account-current-password'), 'a');

      await click(generateTestSelector('delete-account'));

      assert.notEqual(currentRouteName(), 'settings.security', 'redirected away');
      assert.dom(generateTestSelector('sidebar'))
        .doesNotIncludeText(user.username, 'no sign user is logged in');
      assert.dom(generateTestSelector('sidebar-nav')).doesNotIncludeText(t('myFeed'));
    });
  });
});

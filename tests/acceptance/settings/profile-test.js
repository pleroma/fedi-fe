import { module, skip } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { click, currentRouteName, triggerEvent, settled } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';
import { dataURLToBlob } from 'blob-util';
import { resolve } from 'rsvp';
import AvatarComponent from 'pleroma-pwa/components/settings/profile/avatar';

module('Acceptance | settings | profile', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('userSettings'), false);

    // Enable it for tests.
    enableFeature('userSettings');
  });

  // skipping because its flaky: `Uncaught Error: The first argument is required and must be an <img> or <canvas> element.`
  skip('the content should be there', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings');
    await click(generateTestSelector('settings-link', 'profile'));
    assert.equal(currentRouteName(), 'settings.profile');

    assert.dom(generateTestSelector('settings-profile-avatar')).exists();
    assert.dom(generateTestSelector('settings-profile-details')).exists();
    assert.dom(generateTestSelector('settings-profile-options')).exists();
  });

  // skipping because its flaky: `Uncaught Error: The first argument is required and must be an <img> or <canvas> element.`
  skip('saving new avatar should work as expected', async function(assert) {
    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let avatar = dataURLToBlob(smiley);

    let AvatarComponentStub = class extends AvatarComponent {
      getCroppedImage = () => resolve(avatar);
    }

    this.owner.register('component:settings/profile/avatar', AvatarComponentStub);

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/profile');

    await triggerEvent(
      generateTestSelector('settings-new-avatar'),
      'change',
      { files: [dataURLToBlob(smiley)] },
    );
    await settled();

    await click(generateTestSelector('avatar', 'save'));

    await settled();

    let avatarImage = `${generateTestSelector('settings-profile-avatar')} ${generateTestSelector('user-avatar')}`;

    assert
      .dom(avatarImage)
      .hasAttribute('src', smiley, 'the smiley is saved as user avatar as data url');
  });
});

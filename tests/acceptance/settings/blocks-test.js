import { module, test } from 'qunit';
import dangerouslyVisit from 'pleroma-pwa/tests/helpers/dangerously-visit';
import { click, find, currentRouteName } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import { setupIntl } from 'ember-intl/test-support';
import { enableFeature } from 'ember-feature-flags/test-support';

module('Acceptance | settings | blocks', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  hooks.beforeEach(function(assert) {
    this.server.create('instance');

    // Ensure feature is NOT enabled by default
    let featuresService = this.owner.lookup('service:features');
    assert.equal(featuresService.get('userSettings'), false);

    // Enable it for tests.
    enableFeature('userSettings');
  });

  test('the content should be there', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings');
    await click(generateTestSelector('settings-link', 'blocks'));
    assert.equal(currentRouteName(), 'settings.blocks');

    assert.dom(generateTestSelector('blocks-select-all')).exists();
  });

  test('succeeds as expected', async function(assert) {
    let numUsers = 3;

    // create some blocked users.
    this.server.createList('user', numUsers, 'blocked');

    this.server.create('user', 'withToken', 'withSession');

    await dangerouslyVisit('/settings/blocks');

    assert.dom(generateTestSelector('block-select')).exists({ count: numUsers });

    // unblock first user in list by clicking bulk unblock button.
    await click(find(generateTestSelector('block-select')));
    await click(generateTestSelector('bulk-unblock'));

    assert.dom(generateTestSelector('toast-type', 'user-unblocked')).includesText('has been successfully unblocked');
    assert.dom(generateTestSelector('block-select')).exists({ count: numUsers - 1 });

    // unblock first user in list by clicking individual unblock button.
    await click(find(generateTestSelector('individual-unblock')));

    assert.dom(generateTestSelector('toast-type', 'user-unblocked')).includesText('has been successfully unblocked');
    assert.dom(generateTestSelector('block-select')).exists({ count: numUsers - 2 });

    // unblock first user using individual unblock.
    await click(find(generateTestSelector('individual-unblock')));

    assert.dom(generateTestSelector('toast-type', 'user-unblocked')).includesText('has been successfully unblocked');
    assert.dom(generateTestSelector('block-select')).exists({ count: numUsers - 3 });

    assert.equal(currentRouteName(), 'settings.blocks', 'not redirected away');
  });

});

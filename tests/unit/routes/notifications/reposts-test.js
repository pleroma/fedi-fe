import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | notifications/reposts', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:notifications/reposts');
    assert.ok(route);
  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | account/favorites', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:account/favorites');
    assert.ok(route);
  });
});

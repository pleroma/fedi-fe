import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { set } from '@ember/object';

module('Unit | Model | status', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('status', {});

    assert.deepEqual(model.toJSON(), {
      account: null,
      card: null,
      content: null,
      createdAt: null,
      favourited: false,
      favouritesCount: 0,
      inReplyToAccountId: null,
      inReplyToConversationId: null,
      inReplyToId: null,
      mediaIds: [],
      pleroma: null,
      poll: null,
      reblog: null,
      reblogged: false,
      reblogsCount: 0,
      repliesCount: 0,
      sensitive: false,
      spoilerText: null,
      to: [],
      uri: null,
      visibility: 'public',
    });
  });

  module('computed properties', function() {
    test('isReply becomes true when both inReplyToId and inReplyToAccountId are set', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {});

      assert.notOk(model.isReply);

      model.set('inReplyToId', '1223');

      assert.notOk(model.isReply);

      model.set('inReplyToAccountId', '1223');

      assert.ok(model.isReply);
    });

    test('favoritesCount reads the favouritesCount', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {});

      assert.equal(model.favoritesCount, 0);

      model.set('favouritesCount', 1);

      assert.equal(model.favoritesCount, 1);
    });

    test('repostsCount reads the reblogsCount', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {});

      assert.equal(model.repostsCount, 0);

      model.set('reblogsCount', 1);

      assert.equal(model.repostsCount, 1);
    });

    test('favorited reads the favourited prop', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {});

      assert.notOk(model.favorited);

      model.set('favourited', true);

      assert.ok(model.favorited);
    });

    test('reposted reads the reblogged prop', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {});

      assert.notOk(model.reposted);

      model.set('reblogged', true);

      assert.ok(model.reposted);
    });

    test('isPublic reads the visibility prop', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {});

      assert.ok(model.isPublic);
      assert.notOk(model.isDirectMessage);
      assert.notOk(model.isPrivate);
      assert.notOk(model.isUnlisted);
    });

    test('isDirectMessage reads the visibility prop', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {
        visibility: 'direct',
      });

      assert.ok(model.isDirectMessage);
      assert.notOk(model.isPublic);
      assert.notOk(model.isPrivate);
      assert.notOk(model.isUnlisted);
    });

    test('isPrivate reads the visibility prop', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {
        visibility: 'private',
      });

      assert.ok(model.isPrivate);
      assert.notOk(model.isDirectMessage);
      assert.notOk(model.isPublic);
      assert.notOk(model.isUnlisted);
    });

    test('isUnlisted reads the visibility prop', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status', {
        visibility: 'unlisted',
      });

      assert.ok(model.isUnlisted);
      assert.notOk(model.isDirectMessage);
      assert.notOk(model.isPublic);
      assert.notOk(model.isPrivate);
    });

    test('repostOf reads the reblog prop', async function(assert) {
      let store = this.owner.lookup('service:store');
      let reblog = store.createRecord('status');

      let model = store.createRecord('status', {
        reblog,
      });

      assert.equal(model.repostOf, reblog);
    });

    test('visuallyMuted is a conditional dependent on on diff props', async function(assert) {
      let store = this.owner.lookup('service:store');
      let statusExtra = store.createRecord('status-extra', {
        threadMuted: false,
      });
      let relationship = store.createRecord('relationship', {
        muting: false,
      });
      let userExtra = store.createRecord('user-extra', {
        relationship,
      });
      let user = store.createRecord('user', {
        pleroma: userExtra,
      });
      let model = store.createRecord('status', {
        muted: false,
        pleroma: statusExtra,
        account: user,
      });

      assert.equal(model.visuallyMuted, false);

      set(model, 'muted', true);

      assert.equal(model.visuallyMuted, true);

      set(model, 'muted', false);

      assert.equal(model.visuallyMuted, false);

      set(statusExtra, 'threadMuted', true);

      assert.equal(model.visuallyMuted, true);

      set(statusExtra, 'threadMuted', false);

      assert.equal(model.visuallyMuted, false);

      set(userExtra.relationship, 'muting', true);

      assert.equal(model.visuallyMuted, true);

      set(userExtra.relationship, 'muting', false);

      assert.equal(model.visuallyMuted, false);
    });
  });

  module('memberActions', function() {
    test('you can call favorite on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status');

      assert.ok(typeof model.favorite === 'function');
    });

    test('you can call unfavorite on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status');

      assert.ok(typeof model.unfavorite === 'function');
    });

    test('you can call repost on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status');

      assert.ok(typeof model.repost === 'function');
    });

    test('you can call unrepost on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status');

      assert.ok(typeof model.unrepost === 'function');
    });

    test('you can call mute on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status');

      assert.ok(typeof model.mute === 'function');
    });

    test('you can call unmute on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('status');

      assert.ok(typeof model.unmute === 'function');
    });
  });
});

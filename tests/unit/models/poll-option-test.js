import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | poll option', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('poll-option', {});

    assert.deepEqual(model.toJSON(), {
      title: null,
      votesCount: 0,
    });
  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | conversation', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('conversation', {});

    assert.deepEqual(model.toJSON(), {
      lastStatus: null,
      unread: false,
    });
  });

  module('member actions', function() {
    test('knows how to mark itself as read', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('conversation', {});

      assert.ok(model.markAsRead);
    });
  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | poll', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('poll', {});

    assert.deepEqual(model.toJSON(), {
      emojis: [],
      expired: false,
      expiresAt: null,
      expiresIn: null,
      multiple: false,
      ownVotes: [],
      voted: false,
      votersCount: 0,
      votesCount: 0,
    });
  });

  module('computed properties', function() {
    test('winningVoteCount is the max voteCount of all poll options', async function(assert) {
      let store = this.owner.lookup('service:store');
      let options = [
        store.createRecord('poll-option', { votesCount: 3 }),
        store.createRecord('poll-option', { votesCount: 5 }),
        store.createRecord('poll-option', { votesCount: 4 }),
      ];
      let poll = store.createRecord('poll', {
        options,
      });

      assert.equal(poll.winningVoteCount, 5);
    });
  });
});

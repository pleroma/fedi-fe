import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import faker from 'faker';

module('Unit | Model | hashtag', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('hashtag', {});

    assert.deepEqual(model.toJSON(), {
      name: null,
      url: null,
    });
  });

  test('has a special tag property that prepends the a # to the name', function(assert) {
    let name = faker.lorem.word();
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('hashtag', {
      name,
    });

    assert.equal(model.tag, `#${name}`);
  });
});

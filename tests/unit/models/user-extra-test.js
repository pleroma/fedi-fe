import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';

module('Unit | Model | user-extra', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks, 'en');

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('user-extra', {});

    assert.deepEqual(model.toJSON(), {
      backgroundImage: null,

      hideFavorites: false,

      hideFollowers: false,
      hideFollowersCount: false,
      hideFollows: false,
      hideFollowsCount: false,

      notificationSettings: null,

      relationship: null,
      user: null,
    });
  });
});

import { module, test } from 'qunit';
import { settled } from '@ember/test-helpers';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { dataURLToBlob } from 'blob-util';

module('Unit | Model | attachment', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('attachment', {});

    assert.deepEqual(model.toJSON(), {
      blurhash: null,
      description: null,
      file: undefined,
      meta: {},
      pleroma: null,
      previewUrl: null,
      remoteUrl: null,
      textUrl: null,
      type: null,
      url: null,
    });
  });

  test('creating and saving new attachment works as expected', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    let store = this.owner.lookup('service:store');
    let attachment = await store
      .createRecord('attachment', { file })
      .save();
    await settled();

    assert.ok(attachment.id);
    assert.equal(attachment.type, 'image');
    assert.equal(attachment.description, 'smiley.png');
    assert.equal(attachment.url, smiley);
    assert.equal(attachment.previewUrl, smiley);
    assert.equal(attachment.remoteUrl, smiley);
    assert.equal(attachment.textUrl, smiley);
  });

  test('using new attachment to post status works as expected', async function(assert) {
    this.server.create('user', 'withToken', 'withSession');

    let smiley = 'data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7';
    let blob = dataURLToBlob(smiley);
    let file = new File([blob], 'smiley.png', { type: blob.type });

    let store = this.owner.lookup('service:store');
    let attachment = await store
      .createRecord('attachment', { file })
      .save();
    await settled();

    assert.ok(attachment.id);

    let status = await store
      .createRecord('status', { mediaIds: [attachment.id] })
      .save();
    await settled();

    assert.equal(status.mediaAttachments.firstObject.id, attachment.id, 'media got attached');
  });
});

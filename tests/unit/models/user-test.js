import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import faker from 'faker';

module('Unit | Model | user', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('user', {});

    assert.deepEqual(model.toJSON(), {
      acct: null,
      avatar: null,
      displayName: null,
      email: null,
      followersCount: null,
      followingCount: null,
      locale: null,
      locked: false,
      note: null,
      pleroma: null,
      source: null,
      statusesCount: null,
      url: null,
      username: null,
    });
  });

  module('computed properties', function() {
    test('the bio field reads from the note field', async function(assert) {
      let store = this.owner.lookup('service:store');
      let bio = faker.lorem.words();
      let model = store.createRecord('user', { note: bio });
      assert.equal(model.bio, bio);
    });

    test('remoteFollowUrl is correct', async function(assert) {
      let url = faker.internet.url();
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user', {
        url,
      });

      let parsedUrl = new URL(url);
      let remoteFollowUrl = `${parsedUrl.protocol}//${parsedUrl.host}/main/ostatus`

      assert.equal(model.remoteFollowUrl, remoteFollowUrl);
    });

    test('isLocal looks for an @ symbol in the acct', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user', {
        acct: `${faker.lorem.word()}@${faker.internet.domainName()}`,
      });

      assert.equal(model.isLocal, false);

      model.set('acct', faker.lorem.word());

      assert.ok(model.isLocal);
    });
  });

  module('memberActions', function() {
    test('you can call block on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user', {});

      assert.ok(typeof model.block === 'function');
    });

    test('you can call unblock on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user', {});

      assert.ok(typeof model.unblock === 'function');
    });

    test('you can call mute on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user', {});

      assert.ok(typeof model.mute === 'function');
    });

    test('you can call unmute on it', async function(assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user', {});

      assert.ok(typeof model.unmute === 'function');
    });
  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import config from 'pleroma-pwa/config/environment';

module('Unit | Model | pleroma instance', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks, 'en');

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('instance', {});

    assert.deepEqual(model.toJSON(), {
      credentials: null,
      description: null,
      email: null,
      languages: ['en'],
      maxTootChars: config.APP.MAX_STATUS_CHARS,
      nodeInfo: null,
      pollLimits: { minExpiration: 0, maxOptions: 20, maxOptionChars: 200, maxExpiration: 31536000 },
      registrations: false,
      stats: {},
      thumbnail: null,
      title: null,
      token: null,
      uri: null,
      urls: {},
      userBioLength: 5000,
      userNameLength: 100,
      version: null,
      avatarUploadLimit: 2000000,
      bannerUploadLimit: 4000000,
      uploadLimit: 16000000,
      backgroundUploadLimit: 4000000,
    });
  });

  module('computed properties', function() {
    test('chosenLanguage is chosen from the lang files we define, union, with the languages pleroma tells us to use', async function(assert) {
      let store = this.owner.lookup('service:store');
      let intl = {
        locales: ['en'],
      };
      let model = store.createRecord('instance', { intl });

      assert.equal(model.chosenLanguage, 'en', 'defaults to en');

      model.set('languages', ['cn']);

      assert.equal(model.chosenLanguage, 'en', 'will choose a language that ember-intl does not also support');

      model.set('languages', ['cn']);
      intl.locales.push('cn');

      assert.equal(model.chosenLanguage, 'cn', 'when both ember intl and pleroma are in alignment, that language is chosen');
    });
  });
});

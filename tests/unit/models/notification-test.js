import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | notification', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('notification', {});

    assert.deepEqual(model.toJSON(), {
      account: null,
      chatMessage: null,
      createdAt: null,
      pleroma: null,
      status: null,
      type: null,
    });
  });
});

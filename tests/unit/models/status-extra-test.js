import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';

module('Unit | Model | status-extra', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks, 'en');

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('status-extra', {});

    assert.deepEqual(model.toJSON(), {
      content: {},
      conversationId: null,
      directConversationId: null,
      inReplyToAccountAcct: null,
      local: false,
      threadMuted: false,
    });
  });
});

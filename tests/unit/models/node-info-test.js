import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | node info', function(hooks) {
  setupTest(hooks);

  test('no properties were accidentally added or removed, also secretly check default values.', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('node-info', {});

    assert.deepEqual(model.toJSON(), {
      version: null,
      metadata: {},
      openRegistrations: {},
      protocols: [],
      services: {},
      software: {},
      useage: {},
    });
  });
});

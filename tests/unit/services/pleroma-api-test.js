import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import config from 'pleroma-pwa/config/environment';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Unit | Service | pleroma-api', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);

  test('endpoints are not accidentally deleted or added', function(assert) {
    let service = this.owner.lookup('service:pleroma-api');
    let { apiBaseUrl } = service;

    let methods =
      Object.getOwnPropertyNames(service.endpoints.__proto__)
      .filter(m => !['constructor'].includes(m));

    assert.deepEqual(methods.sort(), [
      'accountBlocks',
      'accountFavorites',
      'accountFollowers',
      'accountFollowing',
      'accountMedia',
      'accountMutes',
      'accountSearch',
      'accountStatuses',
      'assertChat',
      'blocksImport',
      'captcha',
      'changePassword',
      'chatMarkAsRead',
      'chatMessage',
      'chatMessageMarkAsRead',
      'chatMessages',
      'chats',
      'clientCredentials',
      'conversation',
      'conversations',
      'currentUser',
      'currentUsersFavorites',
      'customEmojis',
      'deleteAccount',
      'directMessageFeed',
      'followImport',
      'hashtagFeed',
      'instance',
      'media',
      'nodeInfo',
      'notificationSettings',
      'notifications',
      'notificationsMarkAsRead',
      'oauthToken',
      'oauthTokens',
      'pushSubscription',
      'registration',
      'relationships',
      'requestForgotPasswordEmail',
      'revokeToken',
      'search',
      'setAvatar',
      'setBackground',
      'statusEmojiReact',
      'statusEmojiReactions',
      'timelineAll',
      'timelineDirect',
      'timelineHome',
      'token',
      'updateUser',
      'updateUserAvatar',
      'vote',
    ]);

    assert.equal(service.endpoints.accountFavorites(2), `${apiBaseUrl}/${config.APP.pleromaApiNamespace}/accounts/2/favourites`);
    assert.equal(service.endpoints.accountSearch, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/accounts/search`);
    assert.equal(service.endpoints.clientCredentials, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/apps`);
    assert.equal(service.endpoints.conversations, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/conversations`);
    assert.equal(service.endpoints.currentUser, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/accounts/verify_credentials`);
    assert.equal(service.endpoints.currentUsersFavorites, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/favourites`);
    assert.equal(service.endpoints.instance, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/instance`);
    assert.equal(service.endpoints.media, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/media`);
    assert.equal(service.endpoints.nodeInfo, `${apiBaseUrl}/nodeinfo/2.0.json`);
    assert.equal(service.endpoints.notifications, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/notifications`);
    assert.equal(service.endpoints.notificationsMarkAsRead, `${apiBaseUrl}/${config.APP.pleromaApiNamespace}/notifications/read`);
    assert.equal(service.endpoints.pushSubscription, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/push/subscription`);
    assert.equal(service.endpoints.registration, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/accounts`);
    assert.equal(service.endpoints.requestForgotPasswordEmail, `${apiBaseUrl}/auth/password`);
    assert.equal(service.endpoints.revokeToken, `${apiBaseUrl}/oauth/revoke`);
    assert.equal(service.endpoints.timelineAll, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/timelines/public`);
    assert.equal(service.endpoints.timelineHome, `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/timelines/home`);
    assert.equal(service.endpoints.token, `${apiBaseUrl}/oauth/token`);
    assert.equal(service.endpoints.updateUserAvatar, `${apiBaseUrl}/${config.APP.pleromaApiNamespace}/accounts/update_avatar`);
    assert.equal(service.endpoints.vote(1), `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/polls/1/votes`);
    assert.equal(service.endpoints.hashtagFeed('pleroma'), `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/timelines/tag/pleroma`);

    assert.equal(service.endpoints.accountStatuses(4), `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/accounts/4/statuses`);
    assert.equal(service.endpoints.accountFollowing(5), `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/accounts/5/following`);
    assert.equal(service.endpoints.accountFollowers(6), `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/accounts/6/followers`);
    assert.equal(service.endpoints.accountMedia(7), `${apiBaseUrl}/${config.APP.mastodonApiNamespace}/accounts/7/statuses?only_media=1`);
    assert.equal(service.endpoints.conversation(8), `${apiBaseUrl}/${config.APP.pleromaApiNamespace}/conversations/8`);
  });
});

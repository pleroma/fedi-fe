import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | features', function(hooks) {
  setupTest(hooks);

  hooks.beforeEach(function() {
    this.subject = (options = {}) => {
      return this.owner.factoryFor('service:features').create(options);
    };
  });

  test('deployTarget computed property returns empty string when deployTarget is not present', function(assert) {
    let config = {};
    assert.equal(
      this.subject({ config }).deployTarget,
      '',
      'return empty string when deployTarget is not present',
    );
  });

  module('configuredFlags', function() {
    test('converts flags loaded from configuration to camelCase for consistency', function(assert) {
      let config = {
        featureFlags: {
          'new-flag': true,
        },
      };
      let configuredFlags = this.subject({ config }).configuredFlags();
      assert.equal(
        configuredFlags.newFlag,
        true,
        'config value gets loaded properly',
      );
      assert.notOk(
        configuredFlags['new-flag'],
        'no dasherized flag value available',
      );
    });

    test('converts flags loaded from cookies to camelCase for consistency', function(assert) {
      let config = {
        environment: 'development',
      };
      let cookies = {
        read: () => ({ 'ff-new-flag': '1' }),
      };
      let configuredFlags = this.subject({ config, cookies }).configuredFlags();
      assert.equal(
        configuredFlags.newFlag,
        true,
        'cookie flag value gets loaded properly',
      );
      assert.notOk(
        configuredFlags['new-flag'],
        'no dasherized flag value available',
      );
    });

    test('cookie value overrides config value', function(assert) {
      let config = {
        environment: 'development',
        featureFlags: {
          'new-flag': true,
        },
      };
      let cookies = {
        read: () => ({ 'ff-new-flag': '0' }),
      };
      let configuredFlags = this.subject({ config, cookies }).configuredFlags();
      assert.equal(configuredFlags.newFlag, false);
    });
  });

  module('isEnabled', function() {
    test('allows any case as input', function(assert) {
      let configuredFlags = () => ({ newFlag: true });
      let features = this.subject({ configuredFlags });
      assert.equal(features.isEnabled('newFlag'), true);
      assert.equal(features.isEnabled('new-flag'), true);
    });
  });

  module('get', function() {
    test('allows any case as input', function(assert) {
      let configuredFlags = () => ({ anyFeature: true });
      let features = this.subject({ configuredFlags });
      assert.equal(features.get('anyFeature'), true);
      assert.equal(features.get('any-feature'), true);
    });
  });
});

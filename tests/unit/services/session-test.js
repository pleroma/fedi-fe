import { module, test } from 'qunit';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import { setupTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import sinon from 'sinon';
import config from 'pleroma-pwa/config/environment';

module('Unit | Service | session', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);

  module('username password auth', function() {
    test('can authenticate basic', function(assert) {
      let service = this.owner.lookup('service:session');
      assert.ok(service.authenticateBasic);
    });

    test('authenticate basic will attempt to authenticate via oauth2, email, password', async function(assert) {
      let service = this.owner.lookup('service:session');

      let stub = sinon.stub(service, 'authenticate');

      await service.authenticateBasic('name@dockyard.com', '1q2w3e4r');

      assert.ok(stub.calledOnce, 'Authenticate should have been called once');
      assert.ok(stub.calledWithExactly('authenticator:password-grant', 'name@dockyard.com', '1q2w3e4r', config.APP.OAUTH_SCOPES), 'Authenticate was called with the correct params');
    });

  });

  test('authentication will attempt to fetch the sessions current user', async function(assert) {
    let service = this.owner.lookup('service:session');

    let stub = sinon.stub(service, 'loadCurrentUser');

    await authenticateSession({
      'token_type': 'Bearer',
      'access_token': 123456789,
    });

    assert.ok(stub.calledOnce, 'Load Current User was called once');
  });

  test('invalidation will clear the current user', async function(assert) {
    let service = this.owner.lookup('service:session');
    let user = this.server.create('user');
    let tokenRecord = this.server.create('token', {
      user,
    });

    await authenticateSession({
      'token_type': 'Bearer',
      'access_token': tokenRecord.token,
    });

    assert.ok(service.isAuthenticated, 'The service became authenticated');

    await invalidateSession({
      'token_type': 'Bearer',
      'access_token': tokenRecord.token,
    });

    assert.equal(service.currentUser, null);
  });

  test('the sessions current user is an instance of the ember-data USER', async function(assert) {
    let service = this.owner.lookup('service:session');
    let user = this.server.create('user', 'withToken', 'withSession');
    await service.loadCurrentUser();

    assert.ok(service.isAuthenticated, 'The service became authenticated');

    assert.equal(service.currentUser.username, user.username);
  });

  test('the load current user method will push a user into the store', async function(assert) {
    let storeService = this.owner.lookup('service:store');
    let sessionService = this.owner.lookup('service:session');
    let user = this.server.create('user', 'withToken', 'withSession');
    await sessionService.loadCurrentUser();

    assert.equal(storeService.peekRecord('user', user.id).email, sessionService.currentUser.email);
  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  settled,
} from '@ember/test-helpers';
import { setupMirage } from 'ember-cli-mirage/test-support';
import sinon from 'sinon';

module('Unit | Service | feeds', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);

  test('knows about the notifications feed', async function(assert) {
    let service = this.owner.lookup('service:feeds');

    assert.ok(service.notifications);
    assert.equal(service.notifications.modelName, 'notification');
    assert.ok(service.notifications.content);
    assert.notOk(service.notifications.isLoadingMore);
    assert.ok(service.notifications.authenticated);
    assert.notOk(service.notifications.subscribedTo);
  });

  test('knows about the direct feed', async function(assert) {
    let service = this.owner.lookup('service:feeds');

    assert.ok(service.direct);
    assert.equal(service.direct.modelName, 'status');
    assert.ok(service.direct.content);
    assert.notOk(service.direct.isLoadingMore);
    assert.ok(service.direct.authenticated);
    assert.notOk(service.direct.subscribedTo);
  });

  test('knows about the public feed', async function(assert) {
    let service = this.owner.lookup('service:feeds');

    assert.ok(service.public);
    assert.equal(service.public.modelName, 'status');
    assert.ok(service.public.content);
    assert.notOk(service.public.isLoadingMore);
    assert.notOk(service.public.authenticated);
    assert.notOk(service.public.subscribedTo);
    assert.deepEqual(service.public.params, { withMuted: true, local: true });
  });

  test('knows about the all feed', async function(assert) {
    let service = this.owner.lookup('service:feeds');

    assert.ok(service.all);
    assert.equal(service.all.modelName, 'status');
    assert.ok(service.all.content);
    assert.notOk(service.all.isLoadingMore);
    assert.notOk(service.all.authenticated);
    assert.notOk(service.all.subscribedTo);
    assert.deepEqual(service.all.params, { withMuted: true });
  });

  test('knows about the home feed', async function(assert) {
    let service = this.owner.lookup('service:feeds');

    assert.ok(service.home);
    assert.equal(service.home.modelName, 'status');
    assert.ok(service.home.content);
    assert.notOk(service.home.isLoadingMore);
    assert.ok(service.home.authenticated);
    assert.notOk(service.home.subscribedTo);
    assert.deepEqual(service.home.params, { withMuted: false, excludeVisibilities: ['direct'] });
  });

  test('it can push items into a feed', async function(assert) {
    let feedsService = this.owner.lookup('service:feeds');
    let user = this.server.create('user', 'withToken', 'withSession');
    let status = this.server.create('status', {
      account: user,
    });

    await feedsService.subscribe('home');

    await feedsService.addItem('home', status);

    assert.equal(feedsService.home.content.lastObject.id, status.id);
  });

  test('it can remove items from a feed', async function(assert) {
    let feedsService = this.owner.lookup('service:feeds');
    let user = this.server.create('user', 'withToken', 'withSession');
    await this.owner.lookup('service:session').loadCurrentUser();
    let status = this.server.create('status', {
      account: user,
    });

    await feedsService.subscribe('home');

    assert.equal(feedsService.home.content.lastObject.id, status.id);

    await feedsService.removeItem('home', feedsService.home.content.lastObject);

    assert.ok(feedsService.home.content.length === 0);
  });

  test('it can hide a users statuses from all feeds', async function(assert) {
    let feedsService = this.owner.lookup('service:feeds');
    let storeService = this.owner.lookup('service:store');
    let user = this.server.create('user', 'withToken', 'withSession');

    let userFromStore = await storeService.loadRecord('user', user.id);

    this.server.create('status', {
      account: user,
    });

    await feedsService.subscribe('home');
    await feedsService.subscribe('all');
    await feedsService.subscribe('public');

    assert.ok(feedsService.home.content.lastObject);
    assert.ok(feedsService.all.content.lastObject);
    assert.ok(feedsService.public.content.lastObject);

    await feedsService.hideStatusesFromUser(userFromStore);
    await settled();

    assert.notOk(feedsService.home.content.lastObject);
    assert.notOk(feedsService.all.content.lastObject);
    assert.notOk(feedsService.public.content.lastObject);
  });

  test('it can refresh all of the known feeds', async function(assert) {
    let feedsService = this.owner.lookup('service:feeds');
    let user = this.server.create('user', 'withToken', 'withSession');

    this.server.create('status', {
      account: user,
    });

    let refreshFeedStub = sinon.fake();
    sinon.replace(feedsService, 'refreshFeed', refreshFeedStub);

    assert.ok(refreshFeedStub.notCalled);

    await feedsService.refreshAll();
    await settled();

    assert.equal(refreshFeedStub.callCount, feedsService.feedList.length);
  });

  test('it can remove items from all feeds', async function(assert) {
    let feedsService = this.owner.lookup('service:feeds');
    let user = this.server.create('user', 'withToken', 'withSession');

    this.server.create('status', {
      account: user,
    });

    let removeItemStub = sinon.fake();
    sinon.replace(feedsService, 'removeItem', removeItemStub);

    assert.ok(removeItemStub.notCalled);

    await feedsService.removeItemFromAllFeeds();
    await settled();

    assert.equal(removeItemStub.callCount, feedsService.feedList.length);
  });
});

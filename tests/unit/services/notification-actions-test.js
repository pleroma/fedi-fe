import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import sinon from 'sinon';

module('Unit | Service | notification-actions', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);

  module('markAsRead', function() {
    test('marks the notification as read', async function(assert) {
      let instances = this.owner.lookup('service:instances');
      let notificationActionsService = this.owner.lookup('service:notification-actions');
      let user = this.server.create('user', 'withToken', 'withSession');
      await this.owner.lookup('service:session').loadCurrentUser();

      let notificationExtra = this.server.create('notification-extra', {
        isSeen: false,
      });

      let notification = this.server.create('notification', {
        pleroma: notificationExtra,
        account: user,
      });

      await notificationActionsService.markAsRead.perform(notification);
      await instances.refreshTask.perform();

      assert.ok(notification.pleroma.isSeen);
    });

    test("deletes the notification when it's not found on the server", async function(assert) {
      let instances = this.owner.lookup('service:instances');
      let store = this.owner.lookup('service:store');
      let notificationActionsService = this.owner.lookup('service:notification-actions');
      let user = this.server.create('user', 'withToken', 'withSession');
      await this.owner.lookup('service:session').loadCurrentUser();

      let notificationExtra = this.server.create('notification-extra', {
        isSeen: false,
      });

      let notification = this.server.create('notification', {
        pleroma: notificationExtra,
        account: user,
      });

      let url = this.owner.lookup('service:pleromaApi').endpoints.notificationsMarkAsRead;
      this.server.post(url, () => ({
        message: ['Not found'],
      }), 404);

      let notificationFromStore = await store.loadRecord('notification', notification.id);
      let deleteNotificationStub = sinon.fake();
      sinon.replace(notificationFromStore, 'deleteNotification', deleteNotificationStub);

      await notificationActionsService.markAsRead.perform(notificationFromStore);
      await instances.refreshTask.perform();

      assert.equal(deleteNotificationStub.callCount, 1);
    });
  });
});

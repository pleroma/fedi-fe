import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import AdapterError from '@ember-data/adapter/error';

module('Unit | Service | error', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks, 'en');

  let genericErrorMessage = 'Something went wrong';

  module('formErrors', function() {
    test('accepts plain text error object', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ error: genericErrorMessage }), [{
        source: 'generic',
        detail: genericErrorMessage,
      }]);
    });

    test('accepts error object with embedded json object', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ error: '{ "captcha": ["Invalid CAPTCHA"] }' }), [{
        source: 'captcha',
        detail: 'Invalid CAPTCHA',
      }]);
    });

    test('accepts unprocessed errors object with array value', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ errors: { password: ['is not long enough'] } }), [{
        source: 'password',
        detail: 'Password is not long enough',
      }]);
    });

    test('accepts unprocessed errors object with string value', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ errors: { password: 'is not long enough' } }), [{
        source: 'password',
        detail: 'Password is not long enough',
      }]);
    });

    test('accepts unprocessed errors object with string value with uppercase first letter', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ errors: { detail: 'Internal server error' } }), [{
        source: 'detail',
        detail: 'Internal server error',
      }]);
    });

    test('accepts processed object with errors', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ errors: [{source: 'password', detail: 'is not long enough'}] }), [{
        source: 'password',
        detail: 'Password is not long enough',
      }]);
    });

    test('does not prefix source if detail starts with uppercase letter', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ errors: [{source: 'captcha', detail: 'Invalid CAPTCHA'}] }), [{
        source: 'captcha',
        detail: 'Invalid CAPTCHA',
      }]);
    });

    test('accepts Error object', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors(new Error(genericErrorMessage)), [{
        source: 'generic',
        detail: genericErrorMessage,
      }]);
    });

    test('accepts multiple Error objects', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ errors: [new Error('Invalid CAPTCHA'), new Error('username is taken')] }), [
      {
        source: 'generic',
        detail: 'Invalid CAPTCHA',
      }, {
        source: 'generic',
        detail: 'username is taken',
      }]);
    });

    test('accepts AdapterError object with no source', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors(new AdapterError([{detail: genericErrorMessage}])), [{
        source: 'generic',
        detail: genericErrorMessage,
      }]);
    });

    test('accepts AdapterError object with source', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors(new AdapterError([{source: 'username', detail: 'is taken'}])), [{
        source: 'username',
        detail: 'Username is taken',
      }]);
    });

    test('accepts AdapterError object with json detail', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors(new AdapterError([{detail: `{"error": "${genericErrorMessage}"}`}])), [{
        source: 'generic',
        detail: genericErrorMessage,
      }]);
    });

    test('accepts object with message key', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors({ message: genericErrorMessage }), [{
        source: 'generic',
        detail: genericErrorMessage,
      }]);
    });

    test('accepts string', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formErrors(genericErrorMessage), [{
        source: 'generic',
        detail: genericErrorMessage,
      }]);
    });

    test('with no input falls back to generic error message', async function(assert) {
      let error = this.owner.lookup('service:error');
      let intl = this.owner.lookup('service:intl');
      assert.deepEqual(error.formErrors(), [{
        source: 'generic',
        detail: intl.t('somethingWentWrongTryAgain'),
      }]);
    });

    test('with junk input falls back to generic error message', async function(assert) {
      let error = this.owner.lookup('service:error');
      let intl = this.owner.lookup('service:intl');
      assert.deepEqual(error.formErrors(23, undefined), [{
        source: 'generic',
        detail: intl.t('somethingWentWrongTryAgain'),
      }]);
    });
  });

  module('formatPayloadErrors', function() {
    test('accepts plain text error object', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formatPayloadErrors({ error: genericErrorMessage }), {
        errors: [{
          source: 'generic',
          detail: genericErrorMessage,
        }],
      });
    });

    test('accepts unprocessed object with errors', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.deepEqual(error.formatPayloadErrors({ errors: { password: ['is not long enough'] } }), {
        errors: [{
          source: 'password',
          detail: 'is not long enough',
        }],
      });
    });

    test('accepts processed object with errors', async function(assert) {
      let error = this.owner.lookup('service:error');
      let obj = { errors: [{source: 'password', detail: 'is not long enough'}] };
      assert.deepEqual(error.formatPayloadErrors(obj), obj, 'output should be same as input');
    });
  });

  module('detectErrantSuccessCode', function() {
    test('does not change code if it is proper error code', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.equal(error.detectErrantSuccessCode(401, {}), 401);
      assert.equal(error.detectErrantSuccessCode(401, { error: genericErrorMessage }), 401);
      assert.equal(error.detectErrantSuccessCode(401, { errors: {} }), 401);
      assert.equal(error.detectErrantSuccessCode(401, { errors: [] }), 401);
    });

    test('does not change code if it is proper success code', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.equal(error.detectErrantSuccessCode(200, {}), 200);
      assert.equal(error.detectErrantSuccessCode(101, { data: {} }), 101);
      assert.equal(error.detectErrantSuccessCode(302, null), 302);
    });

    test('changes code to 400 if it is not properly set as an error code', async function(assert) {
      let error = this.owner.lookup('service:error');
      assert.equal(error.detectErrantSuccessCode(201, { error: genericErrorMessage }), 400);
      assert.equal(error.detectErrantSuccessCode(101, { errors: {} }), 400);
      assert.equal(error.detectErrantSuccessCode(202, { errors: [] }), 400);
    });
  });
});

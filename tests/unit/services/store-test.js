import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Unit | Service | store', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  module('normaizeAndStore method', function() {
    test('it normalizes a json payload into the store', function(assert) {
      let store = this.owner.lookup('service:store');

      let user = this.server.create('user');
      let json = user.toJSON();

      let [result] = store.normalizeAndStore('user', [json]);

      assert.equal(result.constructor.modelName, 'user', 'Returns a serialized ember-data obj of the correct type');

      let record = store.peekRecord('user', user.id);

      assert.ok(record, 'Its pushes into the store');
    });
  });
});

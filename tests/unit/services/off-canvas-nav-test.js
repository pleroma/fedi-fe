import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import sinon from 'sinon';

module('Unit | Service | off-canvas-nav', function(hooks) {
  setupTest(hooks);

  test('turns on and off correct', function(assert) {
    let service = this.owner.lookup('service:off-canvas-nav');

    assert.ok(service.isClosed);
    assert.notOk(service.isOpen);

    service.open();

    assert.ok(service.isOpen);
    assert.notOk(service.isClosed);

    service.open();

    assert.ok(service.isOpen);
    assert.notOk(service.isClosed);

    service.close();

    assert.ok(service.isClosed);
    assert.notOk(service.isOpen);
  });

  test('fires opened and closed events', function(assert) {
    let service = this.owner.lookup('service:off-canvas-nav');
    let openedFake = sinon.fake();
    let closedFake = sinon.fake();

    service.on('opened', openedFake);
    service.on('closed', closedFake);

    service.open();

    assert.ok(openedFake.calledOnce);
    assert.equal(closedFake.callCount, 0);

    service.open();

    assert.equal(openedFake.callCount, 2);
    assert.equal(closedFake.callCount, 0);

    service.close();

    assert.equal(openedFake.callCount, 2);
    assert.equal(closedFake.callCount, 1);
  });
});

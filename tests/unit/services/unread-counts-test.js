import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';

module('Unit | Service | unread-counts', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('gets the notifications unread count correctly', async function(assert) {
    let service = this.owner.lookup('service:unread-counts');
    let number = 19;
    this.server.create('user', 'withToken', 'withSession');
    await this.owner.lookup('service:session').loadCurrentUser();

    this.server.createList('notification', number);

    assert.equal(service.unreadNotificationsCount, null);

    let feeds = this.owner.lookup('service:feeds');
    await feeds.subscribe('notifications');

    assert.equal(service.unreadNotificationsCount, number);
  });

  test('gets the unread direct messages count correctly', async function(assert) {
    let service = this.owner.lookup('service:unread-counts');
    let number = 19;
    this.server.create('user', 'withToken', 'withSession');
    this.server.createList('chat', number, {
      unread: 1,
    });
    await this.owner.lookup('service:session').loadCurrentUser();

    assert.equal(service.unreadChatCount, null);

    let feeds = this.owner.lookup('service:feeds');
    await feeds.subscribe('chats');

    assert.equal(service.unreadChatCount, number);
  });

  test('can decrement the notifications count, does not become negative, when zero is null', async function(assert) {
    let service = this.owner.lookup('service:unread-counts');

    service.setUnreadNotificationsCount(2);

    assert.equal(service.unreadNotificationsCount, 2);

    service.decrementNotificationsCount();

    assert.equal(service.unreadNotificationsCount, 1);

    service.decrementNotificationsCount();

    assert.equal(service.unreadNotificationsCount, null);

    service.decrementNotificationsCount();

    assert.equal(service.unreadNotificationsCount, null);
  });

  test('can decrement the chat count, does not become negative, when zero is null', async function(assert) {
    let service = this.owner.lookup('service:unread-counts');

    service.setUnreadChatCount(2);

    assert.equal(service.unreadChatCount, 2);

    service.decrementUnreadChatCount();

    assert.equal(service.unreadChatCount, 1);

    service.decrementUnreadChatCount();

    assert.equal(service.unreadChatCount, null);

    service.decrementUnreadChatCount();

    assert.equal(service.unreadChatCount, null);
  });

  test('can decrement the conversation count, does not become negative, when zero is null', async function(assert) {
    let service = this.owner.lookup('service:unread-counts');

    service.setUnreadConversationCount(2);

    assert.equal(service.unreadConversationCount, 2);

    service.decrementUnreadConversationCount();

    assert.equal(service.unreadConversationCount, 1);

    service.decrementUnreadConversationCount();

    assert.equal(service.unreadConversationCount, null);

    service.decrementUnreadConversationCount();

    assert.equal(service.unreadConversationCount, null);
  });
});

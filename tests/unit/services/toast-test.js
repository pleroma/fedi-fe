import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | toast', function(hooks) {
  setupTest(hooks);

  test('it accepts toast objects', async function(assert) {
    let service = this.owner.lookup('service:toast');

    service.notify({
      type: 'test-type',
    });

    assert.equal(service.activeToasts.length, 1);

    service.notify({
      type: 'test-type',
    });

    assert.equal(service.activeToasts.length, 2);
  });

  test('toast default type is success', async function(assert) {
    let service = this.owner.lookup('service:toast');

    service.notify({});

    assert.equal(service.activeToasts[0].type, 'success');
  });

  test('it can dismiss toast', async function(assert) {
    let service = this.owner.lookup('service:toast');

    let toast = service.notify({});

    assert.equal(service.activeToasts[0].id, toast.id);

    service.dismiss(toast.id);

    assert.equal(service.activeToasts.length, 0);
  });

  test('it keeps a list of toasts it will non success type toasts', async function(assert) {
    let service = this.owner.lookup('service:toast');
    assert.deepEqual(service.nonSuccessToasts, ['error', 'dismissibleMessage']);
  });
});

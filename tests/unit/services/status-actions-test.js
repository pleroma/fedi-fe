import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | status-actions', function(hooks) {
  setupTest(hooks);

  test('can knows how to toggle reposting', async function(assert) {
    let service = this.owner.lookup('service:status-actions');

    assert.ok(service.toggleRepost);
  });

  test('can favorite a status', async function(assert) {
    let service = this.owner.lookup('service:status-actions');

    assert.ok(service.toggleFavorite);
  });

  test('can share a status to your clipboard', async function(assert) {
    let service = this.owner.lookup('service:status-actions');

    assert.ok(service.shareStatusToClipboard);
  });

  test('can mute a conversation', async function(assert) {
    let service = this.owner.lookup('service:status-actions');

    assert.ok(service.muteConversation);
  });

  test('can unmute a conversation', async function(assert) {
    let service = this.owner.lookup('service:status-actions');

    assert.ok(service.unmuteConversation);
  });

  test('can delete a status', async function(assert) {
    let service = this.owner.lookup('service:status-actions');

    assert.ok(service.deleteStatus);
  });
});

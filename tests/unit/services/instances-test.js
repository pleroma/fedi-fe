import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { settled } from '@ember/test-helpers';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';

module('Unit | Service | instances', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('it exists', function(assert) {
    let service = this.owner.lookup('service:instances');
    assert.ok(service);
  });

  test('has a refresh task which refreshes its properties', async function(assert) {
    let instance = this.server.create('instance');
    let service = this.owner.lookup('service:instances');

    assert.notOk(service.current);

    assert.ok(service.refreshTask.perform);

    await service.refreshTask.perform();

    await settled();

    assert.equal(service.current.title, instance.title);
    assert.equal(service.current.description, instance.description);
    assert.equal(service.current.email, instance.email);
    assert.equal(service.current.registation, instance.registation);
    assert.equal(service.current.thumbnail, instance.thumbnail);
    assert.equal(service.current.uri, instance.uri);
    assert.equal(service.current.version, instance.version);
    assert.deepEqual(service.current.urls, instance.urls);

    assert.equal(service.current.credentials.vapidKey, '369d61f9-f16c-4c7b-bb63-33c8aca33057', 'has correct vapid key');
  });
});

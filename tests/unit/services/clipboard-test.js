import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import faker from 'faker';

module('Unit | Service | clipboard', function(hooks) {
  setupTest(hooks);

  test('during tests, set a window.testClipboardText that has the clipboard text on it.', async function(assert) {
    let service = this.owner.lookup('service:clipboard');
    let sentence = faker.lorem.sentence();

    await service.copy(sentence);

    assert.equal(window.testClipboardText, sentence);
  });
});

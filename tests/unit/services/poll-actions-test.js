import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Unit | Service | poll-actions', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);

  test('keeps a cache of unique timeouts to ensure only 1 reload timeout is scheduled per poll', async function(assert) {
    let service = this.owner.lookup('service:poll-actions');

    assert.deepEqual(service.reloadCache, {});

    service.reloadPollIn('1', 500);

    assert.deepEqual(Object.keys(service.reloadCache), ['1']);

    service.reloadPollIn('1', 700);

    assert.deepEqual(Object.keys(service.reloadCache), ['1'], 'only schedules unique timeouts');

    service.reloadPollIn('2', 700);

    assert.deepEqual(Object.keys(service.reloadCache), ['1', '2']);
  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { settled } from '@ember/test-helpers';

module('Unit | Service | conversation-actions', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks, 'en');
  setupMirage(hooks);

  hooks.beforeEach(async function() {
    this.server.create('user', 'withToken', 'withSession');
    await this.owner.lookup('service:session').loadCurrentUser();
  });

  module('mark as read', function() {
    test('it knows how to mark a conversation as read', async function(assert) {
      let service = this.owner.lookup('service:conversation-actions');
      let store = this.owner.lookup('service:store');

      let conversation = this.server.create('conversation', {
        unread: true,
      });

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      assert.ok(conversationFromStore.unread);

      await service.markAsRead.perform(conversationFromStore);

      conversationFromStore = await store.loadRecords('conversation', conversation.id);

      assert.notOk(conversationFromStore.unread);
    });

    test('decrements the unread conversation count', async function(assert) {
      let service = this.owner.lookup('service:conversation-actions');
      let store = this.owner.lookup('service:store');
      let unreadCounts = this.owner.lookup('service:unreadCounts');
      let userExtras = this.server.create('user-extra', {
        unreadConversationCount: 3,
      });
      let user = this.server.schema.users.first();
      user.update({ pleroma: userExtras });

      let conversation = this.server.create('conversation', {
        unread: true,
      });

      await unreadCounts.collectUnreadCounts.perform();

      let conversationFromStore = await store.loadRecord('conversation', conversation.id);

      assert.equal(unreadCounts.unreadConversationCount, 3);

      userExtras.update({
        unreadConversationCount: 2,
      });

      await service.markAsRead.perform(conversationFromStore);

      await settled();

      assert.equal(unreadCounts.unreadConversationCount, 2);
    });
  });
});

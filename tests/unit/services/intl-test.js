import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { A } from '@ember/array';

module('Unit | Service | intl', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks, 'en');
  setupMirage(hooks);

  // This service extends ember-intl service, so I will only tests the bits I added.
  test('the isRTL computed property should return true when the language is a rtl language', function(assert) {
    let service = this.owner.lookup('service:intl');

    assert.ok(service.rightToLeftLocales, 'Contains on it a list of rtl locales');

    service.rightToLeftLocales = A([
      'ar-ae',
    ]);

    service.setLocale('ar-ae');

    assert.ok(service.isRTL, 'The service is correctly set as right-to-left');

    service.setLocale('en');

    assert.notOk(service.isRTL, 'The service correctly set as NOT right to left');
  });
});

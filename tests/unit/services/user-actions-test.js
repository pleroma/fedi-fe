import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  setupOnerror,
  resetOnerror,
} from '@ember/test-helpers';
import { setupIntl, t } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Unit | Service | user-actions', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  module('updateAvatar', function() {
    test('throws error if is not a valid file', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      let service = this.owner.lookup('service:user-actions');

      assert.throws(() => {
        service.updateAvatar('should-error');
      }, t('errorUpdatingAvatarTryAgain'));

      resetOnerror();
    });

    test('throws error if file is too large', async function(assert) {
      setupOnerror(function(err) {
        assert.ok(err);
      });

      let instances = this.owner.lookup('service:instances');
      let service = this.owner.lookup('service:user-actions');
      await instances.refreshTask.perform();

      assert.throws(() => {
        service.updateAvatar(new File(['a'.repeat(instances.current.avatarUploadLimit + 1)], 'test-image.jpg', { type: 'jpg' }));
      }, t('errorUpdatingAvatarTryAgain'));

      resetOnerror();
    });
  });

  test('knows how to block a user', async function(assert) {
    let service = this.owner.lookup('service:user-actions');

    assert.ok(service.blockUser);
  });

  test('knows how to unblock a user', async function(assert) {
    let service = this.owner.lookup('service:user-actions');

    assert.ok(service.unblockUser);
  });

  test('knows how to mute a user', async function(assert) {
    let service = this.owner.lookup('service:user-actions');

    assert.ok(service.muteUser);
  });

  test('knows how to unmute a user', async function(assert) {
    let service = this.owner.lookup('service:user-actions');

    assert.ok(service.unmuteUser);
  });

  test('knows how to follow a user', async function(assert) {
    let service = this.owner.lookup('service:user-actions');

    assert.ok(service.followUser);
  });

  test('knows how to unfollow a user', async function(assert) {
    let service = this.owner.lookup('service:user-actions');

    assert.ok(service.unfollowUser);
  });

  test('knows how to register a user', async function(assert) {
    let service = this.owner.lookup('service:user-actions');

    assert.ok(service.registerUser);
  });
});

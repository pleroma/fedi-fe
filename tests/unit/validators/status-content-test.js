import { module, test } from 'qunit';
import validateStatusContent from 'pleroma-pwa/validators/status-content';

module('Unit | Validator | status-content');

test('it exists', function(assert) {
  assert.ok(validateStatusContent());
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupIntl } from 'ember-intl/test-support';
import FavoritesFeed from 'pleroma-pwa/feeds/favorites';
import { enqueueTask } from 'ember-concurrency-decorators';
import sinon from 'sinon';

module('Unit | Feed | favorites', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);
  setupIntl(hooks, 'en');

  test('does not require auth', async function(assert) {
    let factory = this.owner.factoryFor('feed:favorites');
    let feed = factory.create();
    assert.equal(feed.authenticated, false);
  });

  test('contains statuses', async function(assert) {
    let factory = this.owner.factoryFor('feed:favorites');
    let feed = factory.create();
    assert.equal(feed.modelName, 'status');
  });

  module('with no arguments', function() {
    test('accessing id will throw error', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      let feed = factory.create();

      assert.throws(
        () => feed.id,
        'You must pass a userId to this feeds constructor',
      );
    });

    test('accessing url will throw error', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      let feed = factory.create();

      assert.throws(
        () => feed.url,
        'You must pass a userId to this feeds constructor',
      );
    });
  });

  module('with userId argument', function() {
    test('accessing id will return valid id', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      let userId = 'abc';
      let feed = factory.create({ userId });
      assert.equal(feed.id, 'favorites-abc');
    });

    test('accessing url will return valid url', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      let userId = 'abc';
      let feed = factory.create({ userId });
      let { apiBaseUrl } = this.owner.lookup('service:pleroma-api');
      assert.equal(
        feed.url,
        `${apiBaseUrl}/api/v1/pleroma/accounts/abc/favourites`,
      );
    });
  });

  module('with valid user', function() {
    test('if loadMore fails, feed subscribe fails', async function(assert) {
      let userId = 'abc';
      let longPollerStart = sinon.spy();

      let FavoritesFeedStub = class FavoritesFeedStub extends FavoritesFeed {
        @enqueueTask*
        loadMore() {
          yield;
          throw new Error();
        }

        longPoller = { start: longPollerStart };
      }

      this.owner.register('feed:favorites', FavoritesFeedStub);
      let factory = this.owner.factoryFor('feed:favorites');

      let feed = factory.create({ userId });

      await feed.subscribe.perform();

      assert.ok(longPollerStart.notCalled, 'longPoller not called');
      assert.equal(feed.subscribedTo, false, 'feed not subscribedTo');
      assert.equal(feed.canLoadMore, false, 'feed cannot load more');
    });

    test('if loadMore succeeds, feed subscribe succeeds', async function(assert) {
      let userId = 'abc';
      let longPollerStart = sinon.spy();

      let FavoritesFeedStub = class FavoritesFeedStub extends FavoritesFeed {
        @enqueueTask*
        loadMore() {
          yield;
        }

        longPoller = { start: longPollerStart };
      }

      this.owner.register('feed:favorites', FavoritesFeedStub);
      let factory = this.owner.factoryFor('feed:favorites');

      let feed = factory.create({ userId });

      await feed.subscribe.perform();

      assert.ok(longPollerStart.calledOnce, 'longPoller called');
      assert.equal(feed.subscribedTo, true, 'feed is subscribedTo');
      assert.equal(feed.canLoadMore, true, 'feed can load more');
    });

    test('with feed user that does not match current user, should use user-specific URL', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      this.server.create('user', 'withToken', 'withSession');
      await this.owner.lookup('service:session').loadCurrentUser();

      let otherUser = this.server.create('user', 'withToken', 'withSession');
      let { id: userId } = otherUser;
      let feed = factory.create({ userId });

      assert.equal(
        feed.url,
        this.owner.lookup('service:pleromaApi').endpoints.accountFavorites(userId),
        'matches accountFavorites url',
      );
    });

    test('with feed user that does not match current user, should get favorites', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      let user = this.server.create('user', 'withToken', 'withSession');
      await this.owner.setupRouter();
      await this.owner.lookup('service:session').loadCurrentUser();

      let otherUser = this.server.create('user', 'withToken', 'withSession');
      let { id: userId } = otherUser;

      this.server.createList('status', 20, {
        account: user,
      });

      let feed = factory.create({ userId });

      assert.equal(feed.content.length, 0);

      await feed.subscribe.perform();

      assert.equal(feed.content.length, 20);

      await feed.reset();
      assert.equal(feed.content.length, 0);
    });

    test('with feed user that matches current user, should use other URL', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      let user = this.server.create('user', 'withToken', 'withSession');
      await this.owner.lookup('service:session').loadCurrentUser();
      let { id: userId } = user;
      let feed = factory.create({ userId });

      assert.equal(
        feed.url,
        this.owner.lookup('service:pleromaApi').endpoints.currentUsersFavorites,
        'matches currentUsersFavorites url',
      );
    });

    test('with feed user that matches current user, should get favorites', async function(assert) {
      let factory = this.owner.factoryFor('feed:favorites');
      let user = this.server.create('user', 'withToken', 'withSession');
      await this.owner.setupRouter();
      await this.owner.lookup('service:session').loadCurrentUser();

      let { id: userId } = user;

      this.server.createList('status', 20, {
        account: user,
      });

      let feed = factory.create({ userId });

      assert.equal(feed.content.length, 0);

      await feed.subscribe.perform();

      assert.equal(feed.content.length, 20);

      await feed.reset();
      assert.equal(feed.content.length, 0);
    });

  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Serializer | attachment', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('attachment');

    assert.ok(serializer);
  });

  test('it provides an ID to attachment-extra pleroma data', function(assert) {
    let store = this.owner.lookup('service:store');
    let attachment = store.modelFor('attachment');
    let serializer = store.serializerFor('attachment');

    let id = 'id';
    let url = 'url';

    let hash = {
      id,
      url,
      pleroma: {
        'mime_type': 'mime_type',
      },
    };

    let normalized = serializer.normalize(attachment, hash, 'attachments');

    assert.equal(normalized.data.type, 'attachment');
    assert.equal(normalized.data.id, id);
    assert.ok(normalized.data.attributes);
    assert.equal(normalized.data.attributes.url, url);

    let pleroma = normalized.included.find((obj) => obj.type === 'attachment-extra');

    assert.ok(pleroma, 'extra data should exist');

    assert.equal(pleroma.type, 'attachment-extra');
    assert.equal(pleroma.id, id);
    assert.ok(pleroma.attributes);
    assert.equal(pleroma.attributes.mimeType, 'mime_type');
  });
});

import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Serializer | user-extra', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('user-extra');

    assert.ok(serializer);
  });

  test('it provides an ID to notificationSettings data', function(assert) {
    let store = this.owner.lookup('service:store');
    let pleroma = store.modelFor('user-extra');
    let serializer = store.serializerFor('user-extra');

    /* eslint-disable camelcase */
    let id = 'id';
    let hide_favorites = true;

    let hash = {
      id,
      hide_favorites,
      notification_settings: {
        'block_from_strangers': false,
        'hide_notification_contents': false,
      },
    };

    let normalized = serializer.normalize(pleroma, hash, 'userExtras');

    assert.equal(normalized.data.type, 'user-extra');
    assert.equal(normalized.data.id, id);
    assert.ok(normalized.data.attributes);
    assert.equal(normalized.data.attributes.hideFavorites, hide_favorites);

    let notificationSettings = normalized.included.find((obj) => obj.type === 'notification-setting');

    assert.ok(notificationSettings, 'extra data should exist');

    assert.equal(notificationSettings.type, 'notification-setting');
    assert.equal(notificationSettings.id, id);
    assert.ok(notificationSettings.attributes);
    assert.equal(notificationSettings.attributes.blockFromStrangers, false);
    assert.equal(notificationSettings.attributes.hideNotificationContents, false);
    /* eslint-enable camelcase */
  });
});

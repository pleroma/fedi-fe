import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Serializer | user', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('user');

    assert.ok(serializer);
  });

  test('it provides an ID to user-source pleroma data', function(assert) {
    let store = this.owner.lookup('service:store');
    let user = store.modelFor('user');
    let serializer = store.serializerFor('user');

    let id = 'id';
    let username = 'username';

    let hash = {
      id,
      username,
      source: {
        privacy: 'private',
      },
    };

    let normalized = serializer.normalize(user, hash, 'users');

    assert.equal(normalized.data.type, 'user');
    assert.equal(normalized.data.id, id);
    assert.ok(normalized.data.attributes);
    assert.equal(normalized.data.attributes.username, username);

    let pleroma = normalized.included.find((obj) => obj.type === 'user-source');

    assert.ok(pleroma, 'extra data should exist');

    assert.equal(pleroma.type, 'user-source');
    assert.equal(pleroma.id, id);
    assert.ok(pleroma.attributes);
    assert.equal(pleroma.attributes.privacy, 'private');
  });

  test('it provides an ID to user-extra pleroma data', function(assert) {
    let store = this.owner.lookup('service:store');
    let user = store.modelFor('user');
    let serializer = store.serializerFor('user');

    let id = 'id';
    let username = 'username';

    let hash = {
      id,
      username,
      pleroma: {
        'hide_favorites': true,
      },
    };

    let normalized = serializer.normalize(user, hash, 'users');

    assert.equal(normalized.data.type, 'user');
    assert.equal(normalized.data.id, id);
    assert.ok(normalized.data.attributes);
    assert.equal(normalized.data.attributes.username, username);

    let pleroma = normalized.included.find((obj) => obj.type === 'user-extra');

    assert.ok(pleroma, 'extra data should exist');

    assert.equal(pleroma.type, 'user-extra');
    assert.equal(pleroma.id, id);
    assert.ok(pleroma.attributes);
    assert.equal(pleroma.attributes.hideFavorites, true);
  });
});

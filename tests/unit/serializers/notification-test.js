import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Serializer | notification', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('notification');

    assert.ok(serializer);
  });

  test('it provides an ID to notification-extra pleroma data', function(assert) {
    let store = this.owner.lookup('service:store');
    let notification =store.modelFor('notification');
    let serializer = store.serializerFor('notification');

    let id = 'id';
    let type = 'type';

    let hash = {
      id,
      type,
      pleroma: {
        'is_seen': false,
      },
    };

    let normalized = serializer.normalize(notification, hash, 'notifications');

    assert.equal(normalized.data.type, 'notification');
    assert.equal(normalized.data.id, id);
    assert.ok(normalized.data.attributes);
    assert.equal(normalized.data.attributes.type, type);

    let pleroma = normalized.included.find(obj => obj.type === 'notification-extra');

    assert.ok(pleroma, 'extra data should exist');

    assert.equal(pleroma.type, 'notification-extra');
    assert.equal(pleroma.id, id);
    assert.ok(pleroma.attributes);
    assert.equal(pleroma.attributes.isSeen, false);
  });
});

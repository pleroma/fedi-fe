import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Serializer | status', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('status');

    assert.ok(serializer);
  });

  test('it provides an ID to status-extra pleroma data', function(assert) {
    let store = this.owner.lookup('service:store');
    let status =store.modelFor('status');
    let serializer = store.serializerFor('status');

    let id = 'id';
    let content = 'content';

    let hash = {
      id,
      content,
      pleroma: {
        'conversation_id': 23,
      },
    };

    let normalized = serializer.normalize(status, hash, 'statuses');

    assert.equal(normalized.data.type, 'status');
    assert.equal(normalized.data.id, id);
    assert.ok(normalized.data.attributes);
    assert.equal(normalized.data.attributes.content, content);

    let pleroma = normalized.included.find(obj => obj.type === 'status-extra');

    assert.ok(pleroma, 'extra data should exist');

    assert.equal(pleroma.type, 'status-extra');
    assert.equal(pleroma.id, id);
    assert.ok(pleroma.attributes);
    assert.equal(pleroma.attributes.conversationId, 23);
  });
});

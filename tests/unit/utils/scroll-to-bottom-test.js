import scrollToBottom from 'pleroma-pwa/utils/scroll-to-bottom';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  find,
  render,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import hbs from 'htmlbars-inline-precompile';

module('Unit | Utility | scroll-to-bottom', function(hooks) {
  setupRenderingTest(hooks);

  test('it scrolls the passed element to the bottom', async function(assert) {
    await render(hbs('<div style="height: 400px; overflow: hidden;" data-test-selector="outer"><div style="height: 800px;"></div></div>'));

    let element = find(generateTestSelector('outer'));

    element.scrollTo({ top: 0 }); // ensure at the top

    scrollToBottom(element);

    assert.equal(element.scrollTop, element.scrollHeight - element.clientHeight);
  });
});

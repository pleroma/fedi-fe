import uniqueArrayBy from 'pleroma-pwa/utils/unique-array-by';
import { module, test } from 'qunit';

module('Unit | Utility | unique-array-by', function() {
  test('returns the items from the second array that are not in the first', function(assert) {
    let firstArray = [{ id: 'a' }, { id: 'b' }, { id: 'c' }];
    let secondArray = [{ id: 'a' }, { id: 'b' }, { id: 'd' }];
    let result = null;

    result = uniqueArrayBy(firstArray, secondArray, 'id');

    assert.deepEqual(result, [{ id: 'd' }]);

    result = uniqueArrayBy(secondArray, firstArray, 'id');

    assert.deepEqual(result, [{ id: 'c' }]);
  });
});

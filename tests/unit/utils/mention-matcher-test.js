import { matchesMention } from 'pleroma-pwa/utils/mention-matcher';
import { module, test } from 'qunit';
import faker from 'faker';

module('Unit | Utility | mention-matcher', function() {
  test('matches a mention with the pleroma url', async function(assert) {
    let mention = {
      acct: 'username@pleroma.com',
    };

    assert.ok(matchesMention(mention, 'https://pleroma.com/users/username'));
    assert.notOk(matchesMention(mention, 'https://pleroma.com/users/username2'));
    assert.notOk(matchesMention(mention, `https://example.com/users/username`));
  });

  test('matches a mention with the mastodon url', async function(assert) {
    let mention = {
      acct: 'username@mastodon.com',
    };

    assert.ok(matchesMention(mention, 'https://mastodon.com/@username'));
    assert.notOk(matchesMention(mention, 'https://mastodon.com/@username2'));
    assert.notOk(matchesMention(mention, `https://example.com/@username`));
  });

  test('matches a mention with the exact url', async function(assert) {
    let mention = {
      url: `${faker.internet.url()}/users/username`,
      acct: `username@instance.com`,
    }

    assert.ok(matchesMention(mention, mention.url));
    assert.notOk(matchesMention(mention, `${faker.internet.url()}/users/username`));
    assert.notOk(matchesMention(mention, `${faker.internet.url()}/@username`));
  });
});

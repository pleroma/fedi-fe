import elementIsVisible from 'pleroma-pwa/utils/element-is-visible';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  find,
  render,
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';

module('Unit | Utility | element-is-visible', function(hooks) {
  setupRenderingTest(hooks);

  test('tells if something is display none or visibility hidden', async function(assert) {
    await render(hbs`
      <div data-test-selector="should-return-true-for"></div>
      <div data-test-selector="display-none" style="display: none;"></div>
      <div data-test-selector="visibility-hidden" style="visibility: hidden;"></div>
    `);

    assert.ok(elementIsVisible(find(generateTestSelector('should-return-true-for'))));
    assert.notOk(elementIsVisible(find(generateTestSelector('display-none'))));
    assert.notOk(elementIsVisible(find(generateTestSelector('visibility-hidden'))));
  });
});

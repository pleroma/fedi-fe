import { durationFromSeconds } from 'pleroma-pwa/utils/duration';
import { module, test } from 'qunit';

module('Unit | Utility | duration', function() {
  test('durationFromSeconds', function(assert) {
    let minute = 60;
    let hour = 60 * minute;
    let day = 24 * hour;

    let days = Math.floor(Math.random() * Math.floor(8));
    let hours = Math.floor(Math.random() * Math.floor(23));
    let minutes = Math.floor(Math.random() * Math.floor(59));

    let seconds = days * day + hours * hour + minutes * minute;

    let result = durationFromSeconds(seconds);

    assert.equal(result.days, days);
    assert.equal(result.hours, hours);
    assert.equal(result.minutes, minutes);
  });
});

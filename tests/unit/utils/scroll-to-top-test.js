import scrollToTop from 'pleroma-pwa/utils/scroll-to-top';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  find,
  render,
} from '@ember/test-helpers';
import generateTestSelector from 'pleroma-pwa/tests/helpers/generate-test-selector';
import hbs from 'htmlbars-inline-precompile';

module('Unit | Utility | scroll-to-top', function(hooks) {
  setupRenderingTest(hooks);

  test('it scrolls the passed element to its top', async function(assert) {
    await render(hbs('<div style="height: 400px; overflow: hidden;" data-test-selector="outer"><div style="height: 800px;"></div></div>'));

    let element = find(generateTestSelector('outer'));

    element.scrollTo({ top: 300 }); // ensure scrolled

    scrollToTop(element);

    assert.equal(element.scrollTop, 0);
  });
});

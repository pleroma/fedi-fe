import humanizeFileSize from 'pleroma-pwa/utils/humanize-file-size';
import { module, test } from 'qunit';

module('Unit | Utility | humanize-file-size', function() {
  test('correctly formates a string for file sizes', function(assert) {
    assert.equal(humanizeFileSize(0), '0 B');
    assert.equal(humanizeFileSize(10), '10 B');
    assert.equal(humanizeFileSize(100), '100 B');
    assert.equal(humanizeFileSize(1000), '1.0 kB');
    assert.equal(humanizeFileSize(10000), '10.0 kB');
    assert.equal(humanizeFileSize(100000), '100.0 kB');
    assert.equal(humanizeFileSize(1000000), '1.0 MB');
    assert.equal(humanizeFileSize(10000000), '10.0 MB');
    assert.equal(humanizeFileSize(100000000), '100.0 MB');
    assert.equal(humanizeFileSize(1000000000), '1.0 GB');
    assert.equal(humanizeFileSize(10000000000), '10.0 GB');
    assert.equal(humanizeFileSize(100000000000), '100.0 GB');
    assert.equal(humanizeFileSize(1000000000000), '1.0 TB');
    assert.equal(humanizeFileSize(10000000000000), '10.0 TB');
    assert.equal(humanizeFileSize(100000000000000), '100.0 TB');
    assert.equal(humanizeFileSize(1000000000000000), '1.0 PB');
    assert.equal(humanizeFileSize(10000000000000000), '10.0 PB');
    assert.equal(humanizeFileSize(100000000000000000), '100.0 PB');
  });
});

'use strict';

module.exports = {
  arrowParens: 'always',
  bracketSpacing: true,
  singleQuote: true,
  quoteProps: 'preserve',
  semi: true,
  trailingComma: 'all',
  printWidth: 90,
};

# Fedi-FE

PWA client for [Pleroma](https://pleroma.social/) instances.  Built with [Ember](https://emberjs.com/).

![Home Timeline](/uploads/6bd338c5a23544289dcc79f3398d926e/Pleroma_-_Home.png)

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/)
* [Yarn](https://yarnpkg.com/)(new enough that it supports workspaces)
* [Ember CLI](https://ember-cli.com/)

## Setup

* `git clone https://git.pleroma.social/pleroma/fedi-fe.git`
* `cd fedi-fe`
* `yarn install`

## Deploying a FediFE build to Pleroma

* Requires Pleroma 2.1 or later
* Update your Pleroma server configuration e.g., `config :pleroma, :frontends, primary: %{"name" => "fedife", "ref" => "dist"}`
* run
  * yarn build
  * copy the `dist` directory to your `static_dir`, e.g., **instance/static/frontends/fedife/dist**
  * Start your Pleroma instance

## Running / Development

* `npm start`
* Visit your app at [http://localhost:4200](https://localhost:4200).

Note: Because I like working with real data and an API for this already exists, I have our Mirage server turned off in development.

## Developing against local pleroma-api

* Ensure you have postgres installed locally,
* If you dont, you can easily install via homebrew + run `/usr/local/opt/postgres/bin/createuser -s postgres` to create the default postgres user
* Clone the api: https://git.pleroma.social/pleroma/pleroma
* run
  * `mix deps.get`
  * `mix deps.compile`
  * `mix ecto.create`
  * `mix ecto.migrate`
  * `mix phx.server`

### Consuming existing Phoenix instance

* `API_BASE_URL=http://localhost:4000 npm start`

### Performance and Error monitoring.

Right now, during our beta phases, we have some perf monitoring and error tracking enabled. It's only active if `SENTRY_DSN` is present during build time. See `DEBUGGING.md` for more information.


If you want to rip it all out, you will want to remove the sentry service, the sentry deps from package.json, and the `ember-user-performance-monitoring` lib from package.json.

### SVG's

* We are using [SVG Jar](https://github.com/ivanvotti/ember-svg-jar).
* Just add an SVG to the public directory, restart the development server.
* Invoke SVG's like so: `{{svg-jar "dockyard-logo" width="40px" height="40px"}}`

### Styleguides

#### KSS

* We are using [KSS](https://warpspire.com/kss/styleguides/) styleguides.
* They are automatically created when running the development server.
* The server will spit out a file path to open the generated styleguide.
* Open that file in your browser. (paste that path into your browsers url bar)
* The styleguide will rebuild on changes within `app/styles`.
* To see the new changes, refresh the page.

#### Sassdoc

* We are using [SASSDOC](http://sassdoc.com/).
* They are automatically created when running the development server.
* The server will spit out a file path to open the generated docs.
* Open that file in your browser. (paste that path into your browser url bar);
* The docs will rebuild on changes within `app/styles`.
* TO see the new changes, refresh the page

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Tests

#### Running Tests

* `ember exam`
* `ember exam --server`
* `ember exam --filter='acceptance'`

See [Ember Exam](https://github.com/ember-cli/ember-exam)

#### Writing Tests

* Network/Api - [Mirage](https://www.ember-cli-mirage.com/)
* Stubs/Spys - [Sinon](https://github.com/elwayman02/ember-sinon-qunit)
* Selecting Dom Elements - [ember-test-selectors](https://github.com/simplabs/ember-test-selectors)

### Linting

* `yarn lint:hbs`
* `yarn lint:js`
* `yarn lint:js --fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Debugging bundle size

* After starting the ember server, visit `https://localhost:4200/_analyze` to view the bundle analyzer

### Debugging service worker

* Navigate to chrome://serviceworker-internals/

## Further Reading / Useful Links

* [Pleroma](https://pleroma.social/)
* [Activity Pub](https://www.w3.org/TR/activitypub/)
* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* [PWA's](https://developers.google.com/web/progressive-web-apps/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

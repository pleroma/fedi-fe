const chalk = require('chalk');
const fs = require('fs');
const Isemail = require('isemail');
const path = require('path');
const SpellChecker = require('spellchecker');
const YAML = require('yaml');

const yaml = fs.readFileSync(path.join(__dirname, 'translations', 'en.yaml'));
const parsed = YAML.parse(yaml.toString());
const phrases = Object.values(parsed);
const errors = [];
const properNames = [
  'DockYard',
  'Pleroma',
];

phrases.forEach((phrase) => searchPhrase(phrase, errors));

if (errors.length) {
  process.exitCode = 1;
} else {
  process.exitCode = 0;
}

function searchPhrase(phrase, errors) {
  if (typeof phrase === 'object') {
    Object.values(phrase).forEach((innerPhrase) => searchPhrase(innerPhrase, errors));
  } else {
    phrase.split(' ')
      .filter((word) => !Isemail.validate(word))
      .map((word) => word.replace(/[^a-zA-Z0-9']/g, ' '))
      .forEach((word) => {
        if (word.split(' ').length > 1) {
          searchPhrase(word, errors);
        } else {
          if (SpellChecker.isMisspelled(word)) {
            console.log(chalk.red(`Found misspelled translation for: ${word}, In the phrase: ${phrase}`));
            errors.push(word);
          }

          properNames.forEach((properName) => {
            if (word.toLowerCase() === properName.toLowerCase() && word !== properName) {
              console.log(chalk.red(`You need to fix the proper name: ${word} it should have been ${properName}, In the phrase: ${phrase}`));
              errors.push(properName);
            }
          });
        }
      });
  }
}

'use strict';

module.exports = {
  // extends: 'octane',
  extends: 'recommended',
  rules: {
    'no-bare-strings': ['error'],
    'quotes': 'double',
    'no-implicit-this': {
      allow: [
        'app-version',
        'position-overlay-top-right-of-trigger',
      ],
    },

    'no-negated-condition': false,
  },

  ignore: [
    'kss-theme/**',
  ],
};

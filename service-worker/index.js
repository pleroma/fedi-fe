/* eslint-disable no-console */
self.addEventListener('activate', function(event) {
  console.debug('PLEROMA SW, Activate');
  event.waitUntil(self.clients.claim()); // Become available to all pages
});

self.addEventListener('push', function(event) {
  console.debug('PLEROMA SW, PUSH');

  try {
    let payload = event.data && event.data.json();

    event.waitUntil(self.clients.matchAll()
      .then((clientList) => {
        let focused = clientList.some((client) => client.focused);

        // No need to notify, when user is looking at the app
        if (focused) { return; }

        if (payload) {
          return self.registration.showNotification(payload.title, {
            body: payload.body,
            icon: payload.icon,
            data: payload,
          });
        }
      }));
  } catch(e) {
    console.error('PLEROMA SW, PUSH ERROR ', e);
  }
});

self.addEventListener('notificationclick', function(event) {
  console.debug('SW, NOTIFICATION CLICKED');

  event.notification.close();

  event.waitUntil(
    // Retrieve a list of the clients of this service worker.
    self.clients.matchAll().then(function(clientList) {
      // If there is at least one client, focus it.
      if (clientList.length > 0) {
        return clientList[0].focus();
      }

      // Otherwise, open a new page.
      return self.clients.openWindow('/notifications');
    }),
  );
});
/* eslint-enable no-console */

'use strict';

module.exports = {
  root: true,

  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      legacyDecorators: true,
    },
  },

  plugins: [
    'ember',
    'ember-suave',
    'ember-data-sync-relationships',
    'prettier',
  ],

  extends: [
    'eslint:recommended',
    'plugin:ember-suave/recommended',
    'plugin:ember/recommended',
    'prettier',
  ],

  env: {
    browser: true,
  },

  rules: {
    'prettier/prettier': 'off',

    // TODO: Remove when is https://github.com/babel/babel-eslint/issues/530 fixed
    indent: 'off',

    // Allow single-line functions
    'brace-style': ['error', '1tbs', { allowSingleLine: true }],

    'comma-dangle': ['error', 'always-multiline'],

    // if..else with returns is often clearer
    'no-else-return': 'off',

    // Allow using hoisted functions before definition
    'no-use-before-define': ['error', 'nofunc'],

    // Increase max statements per line for nicer guards
    'max-statements-per-line': ['error', { max: 2 }],

    // Don't require radix for parsing base-10 strings
    radix: ['error', 'as-needed'],

    // Ember's computed properties don't play well with arrow functions:
    // https://github.com/babel/ember-cli-babel/issues/30
    'func-names': 'off',
    'prefer-arrow-callback': 'off',

    // Because we are not living in 1975
    'max-len': 'off',

    // Allow underscores to dangle for Ember's _super and private APIs
    'no-underscore-dangle': [
      'error',
      {
        allow: ['_super'],
      },
    ],

    // Allow function call when using the || and && operators. ex `doSomething() || doSomethingElse()`
    'no-unused-expressions': [
      'error',
      {
        allowShortCircuit: true,
      },
    ],

    'new-cap': ['error'],

    'dot-notation': ['error', { allowPattern: '^_' }],

    'prefer-destructuring': 'error',

    'ember-suave/no-const-outside-module-scope': 'error',
    'ember-suave/require-access-in-comments': 'error',
    'ember-suave/no-direct-property-access': 'error',

    'ember/alias-model-in-controller': 'error',
    'ember/avoid-leaking-state-in-ember-objects': 'error',
    'ember/classic-decorator-hooks': 'error',
    'ember/classic-decorator-no-classic-methods': 'error',
    'ember/closure-actions': 'error',
    'ember/new-module-imports': 'error',
    // 'ember/no-actions-hash': 'error',
    'ember/no-attrs-in-components': 'error',
    'ember/no-attrs-snapshot': 'error',
    'ember/no-capital-letters-in-routes': 'error',
    // 'ember/no-classic-components': 'error',
    // 'ember/no-computed-properties-in-native-classes': 'error',
    'ember/no-duplicate-dependent-keys': 'error',
    'ember/no-empty-attrs': 'error',
    'ember/no-global-jquery': 'error',
    'ember/no-jquery': 'error',
    'ember/no-observers': 'error',
    'ember/no-old-shims': 'error',
    'ember/order-in-components': 'error',
    'ember/order-in-controllers': 'error',
    'ember/order-in-models': 'error',
    'ember/order-in-routes': 'error',
    'ember/require-super-in-init': 'error',
    // 'ember/require-tagless-components': 'error',
    'ember/routes-segments-snake-case': 'error',
    'ember/use-brace-expansion': 'error',
    'ember/use-ember-get-and-set': 'error',

    'ember-data-sync-relationships/no-async-relationships': 'error',
  },

  overrides: [
    // don't check mirage models
    {
      files: 'mirage/**/*.js',
      rules: {
        'ember-data-sync-relationships/no-async-relationships': 'off',
      },
    },

    // node files
    {
      files: [
        '.eslintrc.js',
        '.eucrc.js',
        '.prettierrc.js',
        '.sassdocrc.js',
        '.stylelintrc.js',
        '.template-lintrc.js',
        'blueprints/*/index.js',
        'config/**/*.js',
        'ember-cli-build.js',
        'infrastructure.js',
        'lib/**/*.js',
        'server/**/*.js',
        'spellcheck-translations.js',
        'testem.js',
      ],

      parserOptions: {
        sourceType: 'script',
      },

      env: {
        browser: false,
        node: true,
      },

      plugins: ['node'],

      rules: Object.assign({}, require('eslint-plugin-node').configs.recommended.rules, {
        // add your custom rules and overrides for node files here

        // this can be removed once the following is fixed
        // https://github.com/mysticatea/eslint-plugin-node/issues/77
        'node/no-unpublished-require': 'off',
      }),
    },

    // enable console.log files
    {
      files: ['spellcheck-translations.js'],

      rules: {
        'no-console': 'off',
      },
    },

    // overrides for mirage
    {
      files: ['mirage/**/*.js'],

      rules: {
        'prettier/prettier': 'off',
      },
    },

    // overrides for tests
    {
      files: ['tests/**/*.js'],

      rules: {
        'prettier/prettier': 'off',
        'ember/use-ember-get-and-set': 'off',
      },
    },
  ],
};

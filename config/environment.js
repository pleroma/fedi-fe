'use strict';

let flag = require('node-env-flag');
let Inflector = require('inflected');
let { apiBaseUrl, testApiBaseUrl } = require('./api-base-url');
let contentSecurityPolicy = require('./content-security-policy');
const { keys } = Object;

const mastodonApiNamespace = 'api/v1';
const pleromaApiNamespace = 'api/v1/pleroma';

const a11yTurnAuditOff = true;

module.exports = function(environment) {
  let deployTarget = process.env.DEPLOY_TARGET;
  let enableExceptionErrorLogging = !!process.env.SENTRY_DSN && environment !== 'test';

  let ENV = {
    modulePrefix: 'pleroma-pwa',
    deployTarget,
    environment,
    rootURL: '/',
    locationType: 'auto',
    historySupportMiddleware: true,
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },

      EXTEND_PROTOTYPES: {
        Date: false,
        Function: false,
        String: false,
      },
    },

    APP: {
      contentSecurityPolicy: deployTarget !== 'production' ? contentSecurityPolicy(environment) : {},

      // Here you can pass flags/options to your application instance
      // when it is created
      emberSimpleAuthConfig: {
        authenticationRoute: 'sign-in',
        routeAfterAuthentication: '/feeds/home',
        routeIfAlreadyAuthenticated: '/feeds/home',
      },

      apiBaseUrl,
      CONVERSATION_MAX_DEPTH: 6,
      DEFAULT_AVATAR: environment === 'test' ? '/assets/images/avatars/default.jpg' : `${apiBaseUrl}/images/avi.png`,
      DEFAULT_POLL_EXPIRATION: 60 * 10, // seconds
      FEED_PAGE_SIZE: 25,
      MARK_AS_READ_TIMEOUT: 4000,
      mastodonApiNamespace,
      MAX_AVATAR_SIZE: 2000000,
      MAX_BACKGROUND_SIZE: 4000000,
      MAX_BANNER_SIZE: 4000000,
      MAX_BIO_LENGTH: 5000,
      MAX_STATUS_CHARS: 5000,
      MAX_UPLOAD_SIZE: 16000000,
      OAUTH_SCOPES: ['read', 'write', 'follow', 'push'],
      pleromaApiNamespace,
      POLLING_TIMEOUT: 6000,
      SENTRY_DSN: process.env.SENTRY_DSN,
      testApiBaseUrl,
      TOAST_TIMEOUT: 5000,
      USER_NAME_LENGTH: 100,
      USERNAME_REGEX: '^[a-zA-Z0-9_-]*$',
    },

    'ember-a11y-testing': {
      componentOptions: {
        turnAuditOff: a11yTurnAuditOff,
        axeOptions: {
          checks: {
            'color-contrast': {
              enabled: false,
            },
          },
        },
      },
    },

    'ember-user-performance-monitoring': {
      enablePerformanceMeasures: enableExceptionErrorLogging,
      includeAssetTimings: enableExceptionErrorLogging,
      includeConnection: enableExceptionErrorLogging,
      observeComponentRenders: enableExceptionErrorLogging,
      observeLoad: enableExceptionErrorLogging,
      observeTTI: enableExceptionErrorLogging,
      watchTransitions: enableExceptionErrorLogging,
      assetTimingOptions: {
        watchedAssets: {
          appJS: { matches: 'assets/app.*js$' },
          appCSS: { matches: 'assets/app.*css$' },
          vendorJS: { matches: 'assets/vendor.*js$' },
          vendorCSS: { matches: 'assets/vendor.*css$' },
        },
      },
    },

    exportApplicationGlobal: deployTarget !== 'production',

    // Using camelCase syntax here to avoid ESLint.
    featureFlags: {
      accountLocking: false,
      backgroundImage: false,
      directMessages: false,
      emojiReactions: false,
      hideUnseenFeedItems: false,
      polls: false,
      pushNotifications: false,
      registration: false,
      reportUsers: false,
      search: false,
      showDirectMessagesClassic: false,
      showFullScreenName: false,
      simulateFirstUploadFail: false,
      statusDetails: false,
      statusDetailsFindRecord: false,
      statusMedia: false,
      usePreload: false,
      userProfiles: false,
      userSettings: false,
      useUpdatedTextareaAutoGrow: true,
    },
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV['ember-cli-mirage'] = {
      enabled: false,
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
    ENV.APP.POLLING_TIMEOUT = 100;
    ENV.APP.TOAST_TIMEOUT = 100;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }


  /**
   * -----------------------------
   * Shepherd features to maturity
   * -----------------------------
   */

  // baby features 👶
  if (environment === 'development') {
    // here you can enable features that are starting out
    ENV.featureFlags.backgroundImage = true;
  }

  // adolescent features 🧒
  if (deployTarget !== 'production' && environment !== 'test') {
    // here you can enable features that have an identity and are finding their way
    ENV.featureFlags.emojiReactions = true;
    ENV.featureFlags.pushNotifications = true;
  }

  // fully-grown adult features 🧑
  if (environment !== 'test') {
    // here you can enable features ready to show off what they can do
    ENV.featureFlags.directMessages = true;
    ENV.featureFlags.hideUnseenFeedItems = true;
    ENV.featureFlags.polls = true;
    ENV.featureFlags.pushNotifications = true;
    ENV.featureFlags.registration = true;
    ENV.featureFlags.showDirectMessagesClassic = true;
    ENV.featureFlags.showFullScreenName = true;
    ENV.featureFlags.search = true;
    ENV.featureFlags.statusDetails = true;
    ENV.featureFlags.statusDetailsFindRecord = true;
    ENV.featureFlags.statusMedia = true;
    ENV.featureFlags.usePreload = true;
    ENV.featureFlags.userProfiles = true;
    ENV.featureFlags.userSettings = true;
  }


  /**
   * ------------------------------------------
   * You can probably safely ignore this stuff.
   * ------------------------------------------
   */

  // Convert flags into camelCase.
  ENV.featureFlags = keys(ENV.featureFlags).reduce((obj, key) => {
    let value = flag(ENV.featureFlags[key], false);
    let convertedKey = Inflector.camelize(key.replace('-', '_'), false);
    obj[convertedKey] = value;
    return obj;
  }, {});

  // Import flags from environment variables.
  for (let key in process.env) {
    if (key.toLowerCase().startsWith('ff') && key.length > 3) {
      let convertedKey = Inflector.camelize(key.slice(3).toLowerCase(), false);
      ENV.featureFlags[convertedKey] = flag(process.env[key], false);
    }
  }

  return ENV;
};

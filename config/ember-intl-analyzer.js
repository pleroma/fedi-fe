export default {
  whitelist: [
    /^youMustSignInToBlockUser/,
    /^youMustSignInToCreateStatus/,
    /^youMustSignInToFavoriteStatus/,
    /^youMustSignInToMuteUser/,
    /^youMustSignInToRepostStatus/,
    /^youMustSignInToUnblockUser/,
    /^youMustSignInToUnmuteUser/,
  ],
};

'use strict';

module.exports = function(/* environment, appConfig */) {
  return {
    name: 'Pleroma PWA by DockYard',
    'short_name': 'Pleroma PWA by DockYard',
    description: '',
    'start_url': '/',
    display: 'standalone',
    'background_color': '#ebeff5',
    'theme_color': '#2e405c',
    lang: 'en',
    icons: [
      ...[32, 192, 280, 512]
        .map((size) => ({
          src: `/assets/icons/appicon-${size}.png`,
          sizes: `${size}x${size}`,
          type: 'image/png',
        })),
    ],

    ms: {
      tileColor: '#ebeff5',
    },
  };
};

let apiBaseUrl = '';

if (typeof process.env.API_BASE_URL !== 'undefined') {
  apiBaseUrl = process.env.API_BASE_URL;
}

const testApiBaseUrl = 'https://pleroma.example.com';

module.exports = {
  apiBaseUrl,
  testApiBaseUrl,
};

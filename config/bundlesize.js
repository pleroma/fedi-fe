'use strict';

module.exports = {
  app: {
    javascript: {
      compression: 'gzip',
      limit: '500KB',
      pattern: 'assets/*.js',
    },

    css: {
      compression: 'gzip',
      limit: '50KB',
      pattern: 'assets/*.css',
    },
  },
};

/* global self */
self.deprecationWorkflow = self.deprecationWorkflow || {};
self.deprecationWorkflow.config = {
  workflow: [
    { handler: "silence", matchId: "ember-data:Model.data" },
    { handler: "silence", matchId: "ember-data:model.toJSON" },
    { handler: "silence", matchId: "ember-runtime.deprecate-copy-copyable" },
    { handler: "silence", matchId: "ember-simple-auth.authenticator.no-reject-with-response" },
    { handler: "silence", matchId: "implicit-modifier-manager-capabilities" },
    { handler: "silence", matchId: "computed-property.override" },
    { handler: "silence", matchId: "ember-console.deprecate-logger" },
  ],
};

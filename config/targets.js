'use strict';

// Service Worker support
const browsers = [
  'last 1 Chrome versions',
  'last 1 ChromeAndroid versions',
  'last 1 Edge versions',
  'last 1 Firefox versions',
  'last 1 FirefoxAndroid versions',
  'last 1 ios_saf versions',
  'last 1 Safari versions',
];

module.exports = {
  browsers,
};

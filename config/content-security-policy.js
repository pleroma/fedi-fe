let { apiBaseUrl, testApiBaseUrl } = require('./api-base-url');

module.exports = function(environment) {
  return {
    delivery: ['meta'],
    enabled: true,
    failTests: true,
    reportOnly: false,
    policy: {
      // Deny everything by default
      'default-src': ["'none'"],

      'script-src': [
        "'self'",
        "'sha256-NfxbADAdpgr826AXWkHohZnFAXD2WGmzSoLLGG5RQpg='", // Ember service worker registration

        // Note: `ember-auto-import` uses eval in development. This makes
        // source map generation faster. Ignore its usage so we can still have
        // source maps generated more quickly.
        // @see https://github.com/ef4/ember-auto-import#i-use-content-security-policy-csp-and-it-breaks-ember-auto-import
        ...(['development', 'test'].includes(environment) ? ["'unsafe-eval'"] : []),

        // Note: Update this as hash of injected service worker script changes (rare).
        ...(environment === 'production' ? ["'sha256-RytLgjMPWyWm4BULKhyUN31LoNCPvJW6uUlVOm6MbDk='"] : []),

        // When sentry wants to render a user-feedback form, it downloads and executed this
        'https://sentry.io/api/embed/error-page/',
        'https://*.ingest.sentry.io/api/embed/',
      ],

      'font-src': ["'self'"],

      'connect-src': [
        "'self'",
        apiBaseUrl,
        ...(environment === 'test' ? [testApiBaseUrl] : []),

        'https://*.ingest.sentry.io', // Sentry error reporting
        'https://sentry.io/api/embed/error-page/', // Sentry user-feedback endpoint
      ],

      'img-src': [
        "'self'",
        "data:",
        apiBaseUrl,
        ...(environment === 'test' ? [testApiBaseUrl] : []),
        'https://*',
      ],

      'manifest-src': ["'self'"],

      'style-src': [
        "'self'",
        "'unsafe-inline'", // Ember basic dropdown inline styles: https://github.com/cibernox/ember-basic-dropdown/issues/424
      ],

      'media-src': [
        "'self'",
        "data:",
        apiBaseUrl,
        'https://*',
      ],
    },
  };
}
